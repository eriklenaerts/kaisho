VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Begin VB.Form frmLidDetail 
   Caption         =   "Detail"
   ClientHeight    =   7830
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6975
   Icon            =   "frmMemberDetail.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7830
   ScaleWidth      =   6975
   Begin VB.Frame fraPersoonlijkeGegevens 
      Caption         =   "Persoonlijk"
      Height          =   3615
      Left            =   120
      TabIndex        =   28
      Top             =   840
      Width           =   6735
      Begin VB.TextBox txtEmail 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   285
         Left            =   1560
         TabIndex        =   43
         Top             =   3240
         Width           =   4815
      End
      Begin VB.TextBox txtVoornaam 
         DataField       =   "Voornaam"
         DataMember      =   "Lid"
         DataSource      =   "DataEnvironment1"
         Height          =   285
         Left            =   1560
         TabIndex        =   1
         ToolTipText     =   "De voornaam van het lid"
         Top             =   720
         Width           =   3735
      End
      Begin VB.CommandButton cmdFoto 
         Caption         =   "&Foto"
         Height          =   350
         Left            =   5520
         TabIndex        =   4
         Top             =   1375
         Width           =   855
      End
      Begin VB.TextBox txtLicNr 
         DataField       =   "LicentieNummer"
         DataMember      =   "Lid"
         DataSource      =   "DataEnvironment1"
         Height          =   285
         Left            =   1560
         TabIndex        =   0
         ToolTipText     =   "De voornaam van het lid"
         Top             =   360
         Width           =   3735
      End
      Begin VB.TextBox txtLidID 
         DataField       =   "LidID"
         DataMember      =   "Lid"
         DataSource      =   "DataEnvironment1"
         Height          =   285
         Left            =   6132
         TabIndex        =   40
         Top             =   465
         Visible         =   0   'False
         Width           =   660
      End
      Begin VB.TextBox txtAchternaam 
         DataField       =   "Achternaam"
         DataMember      =   "Lid"
         DataSource      =   "DataEnvironment1"
         Height          =   285
         Left            =   1560
         TabIndex        =   2
         ToolTipText     =   "De achternaam van het lid"
         Top             =   1080
         Width           =   3735
      End
      Begin VB.TextBox txtStraat 
         DataField       =   "Straat"
         DataMember      =   "Lid"
         DataSource      =   "DataEnvironment1"
         Height          =   285
         Left            =   1560
         TabIndex        =   3
         ToolTipText     =   "De straatnaam waar het lid woont"
         Top             =   1440
         Width           =   3735
      End
      Begin VB.TextBox txtPostCode 
         DataField       =   "PostCode"
         DataMember      =   "Lid"
         DataSource      =   "DataEnvironment1"
         Height          =   285
         Left            =   1560
         TabIndex        =   5
         ToolTipText     =   "De postcode van het lid"
         Top             =   1800
         Width           =   852
      End
      Begin VB.TextBox txtWoonplaats 
         DataField       =   "Woonplaats"
         DataMember      =   "Lid"
         DataSource      =   "DataEnvironment1"
         Height          =   285
         Left            =   3840
         TabIndex        =   6
         ToolTipText     =   "De stad, gemeente of dorp van het lid"
         Top             =   1800
         Width           =   2532
      End
      Begin VB.TextBox txtProvincie 
         DataField       =   "Provincie"
         DataMember      =   "Lid"
         DataSource      =   "DataEnvironment1"
         Height          =   285
         Left            =   1560
         TabIndex        =   7
         ToolTipText     =   "De provincie van het lid"
         Top             =   2160
         Width           =   1572
      End
      Begin VB.TextBox txtLand 
         DataField       =   "Land"
         DataMember      =   "Lid"
         DataSource      =   "DataEnvironment1"
         Height          =   285
         Left            =   3840
         TabIndex        =   8
         ToolTipText     =   "Het land waarin het lid woont"
         Top             =   2160
         Width           =   2532
      End
      Begin VB.TextBox txtTelefoon 
         DataField       =   "Telefoon"
         DataMember      =   "Lid"
         DataSource      =   "DataEnvironment1"
         Height          =   285
         Left            =   1560
         TabIndex        =   9
         ToolTipText     =   "De telefoonnummer van het lid"
         Top             =   2520
         Width           =   4812
      End
      Begin VB.TextBox txtGeslacht 
         DataField       =   "Geslacht"
         DataMember      =   "Lid"
         DataSource      =   "DataEnvironment1"
         Height          =   285
         Left            =   1560
         TabIndex        =   10
         ToolTipText     =   "Het geslacht van het lid, dit kan enkel M(an) of V(rouw) zijn"
         Top             =   2880
         Width           =   852
      End
      Begin VB.TextBox txtGeboortedatum 
         DataField       =   "Geboortedatum"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd MMMM yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2067
            SubFormatType   =   3
         EndProperty
         DataMember      =   "Lid"
         DataSource      =   "DataEnvironment1"
         Height          =   285
         Left            =   3840
         TabIndex        =   11
         ToolTipText     =   "De geboortedatum van het lid"
         Top             =   2880
         Width           =   2520
      End
      Begin VB.Label Label1 
         Caption         =   "E-mail"
         Height          =   255
         Left            =   240
         TabIndex        =   42
         Top             =   3240
         Width           =   735
      End
      Begin VB.Image imgFoto 
         Height          =   975
         Left            =   5520
         Top             =   360
         Width           =   855
      End
      Begin VB.Label lblLicNr 
         AutoSize        =   -1  'True
         Caption         =   "Licentie nr:"
         Height          =   195
         Left            =   240
         TabIndex        =   41
         Top             =   360
         Width           =   780
      End
      Begin VB.Label lblLidID 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "LidID:"
         Height          =   255
         Left            =   4290
         TabIndex        =   39
         Top             =   510
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.Label lblAchternaam 
         AutoSize        =   -1  'True
         Caption         =   "Achternaam:"
         Height          =   255
         Left            =   240
         TabIndex        =   38
         Top             =   1080
         Width           =   1215
      End
      Begin VB.Label lblVoornaam 
         AutoSize        =   -1  'True
         Caption         =   "Voornaam:"
         Height          =   255
         Left            =   240
         TabIndex        =   37
         Top             =   720
         Width           =   1215
      End
      Begin VB.Label lblStraat 
         AutoSize        =   -1  'True
         Caption         =   "Straat:"
         Height          =   255
         Left            =   240
         TabIndex        =   36
         Top             =   1440
         Width           =   1215
      End
      Begin VB.Label lblPostCode 
         AutoSize        =   -1  'True
         Caption         =   "PostCode:"
         Height          =   255
         Left            =   240
         TabIndex        =   35
         Top             =   1800
         Width           =   1215
      End
      Begin VB.Label lblWoonplaats 
         AutoSize        =   -1  'True
         Caption         =   "Woonplaats:"
         Height          =   255
         Left            =   2760
         TabIndex        =   34
         Top             =   1800
         Width           =   1215
      End
      Begin VB.Label lblProvincie 
         AutoSize        =   -1  'True
         Caption         =   "Provincie:"
         Height          =   255
         Left            =   240
         TabIndex        =   33
         Top             =   2160
         Width           =   1215
      End
      Begin VB.Label lblLand 
         AutoSize        =   -1  'True
         Caption         =   " Land:"
         Height          =   195
         Left            =   3240
         TabIndex        =   32
         Top             =   2160
         Width           =   435
      End
      Begin VB.Label lblTelefoon 
         AutoSize        =   -1  'True
         Caption         =   "Telefoon:"
         Height          =   255
         Left            =   240
         TabIndex        =   31
         Top             =   2520
         Width           =   1215
      End
      Begin VB.Label lblGeslacht 
         AutoSize        =   -1  'True
         Caption         =   "Geslacht:"
         Height          =   255
         Left            =   240
         TabIndex        =   30
         Top             =   2880
         Width           =   1215
      End
      Begin VB.Label lblGeboortedatum 
         AutoSize        =   -1  'True
         Caption         =   "Geboortedatum:"
         Height          =   255
         Left            =   2520
         TabIndex        =   29
         Top             =   2880
         Width           =   1215
      End
   End
   Begin VB.Frame fraKarateGegevens 
      Caption         =   "Karate"
      Height          =   1215
      Left            =   120
      TabIndex        =   25
      Top             =   4560
      Width           =   6735
      Begin VB.CheckBox chkNieuw 
         DataField       =   "Nieuw"
         DataMember      =   "Lid"
         DataSource      =   "DataEnvironment1"
         Height          =   285
         Left            =   1560
         TabIndex        =   12
         ToolTipText     =   $"frmMemberDetail.frx":030A
         Top             =   360
         Width           =   330
      End
      Begin VB.TextBox txtGraad 
         BackColor       =   &H80000004&
         DataField       =   "Graad"
         DataMember      =   "Lid"
         DataSource      =   "DataEnvironment1"
         Height          =   285
         Left            =   1560
         Locked          =   -1  'True
         TabIndex        =   20
         ToolTipText     =   "De huidige graad van het lid"
         Top             =   720
         Width           =   2136
      End
      Begin VB.CommandButton cmdGradenHistoriek 
         Caption         =   "&Graad Historiek  ..."
         Height          =   375
         Left            =   3960
         TabIndex        =   13
         ToolTipText     =   "Dit geeft een overzicht van de graden van het waar en wanneer hij of zij deze behaalt heeft"
         Top             =   720
         Width           =   2532
      End
      Begin VB.Label lblNieuwLid 
         AutoSize        =   -1  'True
         Caption         =   "Nieuw Lid:"
         Height          =   195
         Left            =   240
         TabIndex        =   27
         Top             =   360
         Width           =   750
      End
      Begin VB.Label lblGraad 
         AutoSize        =   -1  'True
         Caption         =   "Graad:"
         Height          =   255
         Left            =   240
         TabIndex        =   26
         Top             =   705
         Width           =   1215
      End
   End
   Begin VB.Frame fraGroepGegevens 
      Caption         =   "Extra"
      Height          =   1215
      Left            =   120
      TabIndex        =   22
      Top             =   5880
      Width           =   6735
      Begin VB.CheckBox chkInactief 
         Alignment       =   1  'Right Justify
         Caption         =   "InActief"
         DataField       =   "InActief"
         DataMember      =   "Lid"
         DataSource      =   "DataEnvironment1"
         Height          =   255
         Left            =   3960
         TabIndex        =   44
         Top             =   720
         Width           =   2175
      End
      Begin VB.CheckBox chkTrainer 
         Alignment       =   1  'Right Justify
         Caption         =   "Trainer"
         DataField       =   "Groep_Trainer"
         DataMember      =   "Lid"
         DataSource      =   "DataEnvironment1"
         Height          =   255
         Left            =   3960
         TabIndex        =   15
         Top             =   360
         Width           =   2175
      End
      Begin VB.CheckBox chkGM 
         Alignment       =   1  'Right Justify
         DataField       =   "Groep_GM"
         DataMember      =   "Lid"
         DataSource      =   "DataEnvironment1"
         Height          =   252
         Left            =   1515
         TabIndex        =   14
         ToolTipText     =   "Het lid is werknemer van General Motors"
         Top             =   360
         Width           =   240
      End
      Begin MSDataListLib.DataCombo dcFamilie 
         Bindings        =   "frmMemberDetail.frx":03A9
         DataField       =   "Groep_FamilieID"
         DataMember      =   "Lid"
         DataSource      =   "DataEnvironment1"
         Height          =   288
         Left            =   1560
         TabIndex        =   16
         Top             =   643
         Width           =   2172
         _ExtentX        =   3836
         _ExtentY        =   556
         _Version        =   393216
         ListField       =   "FamilieNaam"
         BoundColumn     =   "FamilieID"
         Text            =   "(Geen)"
         Object.DataMember      =   "Familie"
      End
      Begin VB.Label lblFamilie 
         Caption         =   "Familie :"
         Height          =   375
         Left            =   240
         TabIndex        =   24
         Top             =   720
         Width           =   855
      End
      Begin VB.Label lblGM 
         Caption         =   "General Motors :"
         Height          =   255
         Left            =   240
         TabIndex        =   23
         Top             =   360
         Width           =   1335
      End
   End
   Begin VB.CommandButton cmdToepassen 
      Caption         =   "&Toepassen"
      Enabled         =   0   'False
      Height          =   375
      Left            =   4080
      TabIndex        =   18
      Top             =   7320
      Width           =   1335
   End
   Begin VB.CommandButton cmdAnnuleren 
      Cancel          =   -1  'True
      Caption         =   "&Annuleren"
      Height          =   375
      Left            =   5520
      TabIndex        =   19
      Top             =   7320
      Width           =   1335
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Default         =   -1  'True
      Height          =   375
      Left            =   2640
      TabIndex        =   17
      Top             =   7320
      Width           =   1335
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   240
      Picture         =   "frmMemberDetail.frx":03FB
      Top             =   210
      Width           =   480
   End
   Begin VB.Label lblTitel 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "LidDetail"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   120
      TabIndex        =   21
      Top             =   120
      Width           =   6735
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   6840
      Y1              =   7200
      Y2              =   7200
   End
End
Attribute VB_Name = "frmLidDetail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mblnDataChanged As Boolean
Private mMode As kaDataMode

Public Property Let DataChanged(value As Boolean)
    mblnDataChanged = value
    cmdToepassen.Enabled = mblnDataChanged
End Property

Public Property Get DataChanged() As Boolean
    DataChanged = mblnDataChanged
End Property

Private Sub NieuwLid()
    DataEnvironment1.rsLid.AddNew
    Me.chkNieuw = vbChecked
    Me.chkGM = vbUnchecked
    Me.chkTrainer = vbUnchecked
    Me.txtGeslacht = "M"
    Me.dcFamilie = "(Geen)"
    lblAchternaam.FontBold = True
    lblVoornaam.FontBold = True
    lblGeslacht.FontBold = True
    lblNieuwLid.FontBold = True
    cmdGradenHistoriek.Enabled = False
    Me.Caption = "Nieuw Lid"
    lblTitel.Caption = vbCrLf + Me.Caption
    DataChanged = False
    'txtVoornaam.SetFocus
End Sub

Private Sub ShowPicture(LicNr As String)
On Error GoTo ErrorHandler
    'If no LicNr exist, it is a new member
    If Len(LicNr) > 0 And LicNr > "0" Then
        Me.imgFoto.Stretch = True
        Me.imgFoto.Picture = LoadPicture(App.Path + "\Foto\" + LicNr + ".jpg")
    End If
    Exit Sub
ErrorHandler:
    'no foto available
    If Err.Number = 53 Then
        cmdFoto.Enabled = True
    Else
        MsgBox "Error : " & Err.Description
    End If
End Sub

Public Sub OpenForm(lngLidID As Long)
    If (lngLidID = mclngNieuwLid) Then
        mMode = kaCreate
        StartUsingRecordset DataEnvironment1.rsLid
        NieuwLid
    Else
        mMode = kaUpdate
        StartUsingRecordset DataEnvironment1.rsLid
        With DataEnvironment1.rsLid
            .Find "LidID=" + CStr(lngLidID)
            Me.Caption = "Details van " + .Fields("Voornaam") + " " + .Fields("Achternaam")
            lblTitel.Caption = vbCrLf + Me.Caption
        End With
        If (Len(txtGraad) = 0) Then
            cmdGradenHistoriek.Enabled = False
        Else
            cmdGradenHistoriek.Enabled = True
        End If
        
        ShowPicture Me.txtLicNr
    End If
End Sub

Private Sub chkGM_Click()
    DataChanged = True
End Sub

Private Sub chkInactief_Click()
    DataChanged = True
End Sub

Private Sub chkNieuw_Click()
    DataChanged = True
'    If (mMode = kaCreate Or mMode = kaUpdate And Len(txtGeboortedatum) = 0) Then
'        lblGeboortedatum.FontBold = Not CBool(chkNieuw)
'    End If
'    If mMode = kaCreate And chkNieuw = vbUnchecked Then
'        If (MsgBox("Let op! Dit is een nieuw lid, als je dit vakje afkruist dan moet de verzekering van dit lid al inorde zijn." & vbCrLf & vbCrLf & "Ben je zeker dat dit lid al een verzkering heeft?", vbInformation + vbYesNo) = vbNo) Then chkNieuw.value = vbChecked
'    End If
'    If mMode = kaUpdate And chkNieuw = vbChecked Then
'        If (MsgBox("Let op! Er was reeds een verzekering aanwezig voor dit lid, door dit vakje leeg te maken zal het lid geen verzekering meer hebben voor Kaisho Admin. Bijgevolg zal het lid niet meer in de statistieken voorkomen en zal hij of zij in de toekomst onderaan de lijsten verschijnen." & vbCrLf & vbCrLf & "Ben je zeker dat dit lid geen verzkering meer heeft?", vbInformation + vbYesNo) = vbNo) Then chkNieuw.value = vbUnchecked
'    End If
End Sub

Private Sub chkTrainer_Click()
    DataChanged = True
End Sub

Private Sub cmdFoto_Click()
    Dim frmFoto As New frmFoto
    
    If DataEnvironment1.rsLid("LicentieNummer").value = 0 Or IsNull(DataEnvironment1.rsLid("LicentieNummer").value) Then
        MsgBox "Je kan slechts een foto toevoegen nadat het licentienummer is ingevuld.", vbExclamation
    Else
        frmFoto.LicNr = Me.txtLicNr
        frmFoto.Show vbModal
        
        ShowPicture Me.txtLicNr
        
        Set frmFoto = Nothing
    End If
End Sub

Private Sub cmdGradenHistoriek_Click()
    Set mfrmLidGraden = New frmLidGraden
    mfrmLidGraden.OpenForm CLng(txtLidID.text)
    mfrmLidGraden.Show vbModal
End Sub

Private Sub cmdFamilie_Click()
    Set mfrmLidFamilie = New frmLidFamilie
    mfrmLidFamilie.Show vbModal
    DataEnvironment1.rsFamilie.Resync adAffectAllChapters, adResyncUnderlyingValues
    dcFamilie.ReFill
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If (UnloadMode <> vbFormCode) Then
        DataEnvironment1.rsLid.CancelUpdate
    End If
End Sub

Private Sub Form_Resize()
    If mMode = kaCreate Then txtVoornaam.SetFocus
End Sub

Private Sub Form_Unload(Cancel As Integer)
    EndUsingRecordset DataEnvironment1.rsLid
    Set mfrmLidDetail = Nothing
End Sub

Private Sub cmdAnnuleren_Click()
    mMode = kaUpdateUnfinished
    DataEnvironment1.rsLid.CancelUpdate
    Unload Me
End Sub

Private Sub cmdOk_Click()
    cmdToepassen_Click
    If (mMode <> kaCreateUnfinished And mMode <> kaUpdateUnfinished) Then
        Unload Me
    Else
        'mMode = kaCreate
    End If
End Sub

Private Function ScreenValidate() As Boolean
    ScreenValidate = True
    If (Me.txtAchternaam = "") Then ScreenValidate = False
    If (Me.txtVoornaam = "") Then ScreenValidate = False
    If (chkNieuw = vbUnchecked) Then
        'A member that isn't new must have a birthdate
        If (Not IsDate(txtGeboortedatum)) Then ScreenValidate = False
    End If
End Function

Private Sub cmdToepassen_Click()
Dim adCommand As ADODB.Command
On Error GoTo ErrorHandler

    With DataEnvironment1
        If ((mMode = kaCreate Or mMode = kaCreateUnfinished) And (Not ScreenValidate)) Then
            Err.Raise cErrorCreateMember, "KaishoAdmin:frmLidDetail:cmdToepassen", "Om een nieuw lid aan te maken moet minstens voor - en achtenaam ingevuld zijn." & vbCrLf & "Indien het vakje 'Nieuw lid' afgekruist is ben je ook verplicht een geboortedatum op te geven." & vbCrLf & vbCrLf & "Verder gaan met het aanmaken van een nieuw lid?"
        End If
        If ((mMode = kaUpdate Or mMode = kaUpdateUnfinished) And (Not ScreenValidate)) Then
            Err.Raise cErrorUpdateMember, "KaishoAdmin:frmLidDetail:cmdToepassen", "Om een nieuw lid aan te maken moet minstens voor - en achtenaam ingevuld zijn." & vbCrLf & "Indien het vakje 'Nieuw lid' afgekruist is ben je ook verplicht een geboortedatum op te geven." & vbCrLf & vbCrLf & "Verder gaan met het aanpassen van de gegevens van het lid?"
        End If
        
        .rsLid.Move (0)
        .rsLid.Update
        SetMutationDate Now
        If (.rstblLidAanwezigheid.State = adStateOpen) Then .rstblLidAanwezigheid.Requery
        If (.rstblLidAanwezigheidNieuw.State = adStateOpen) Then .rstblLidAanwezigheidNieuw.Requery
        If (.rstblLidAanwezigheidBestaand.State = adStateOpen) Then .rstblLidAanwezigheidBestaand.Requery
        If (.rstblLidBetaling.State = adStateOpen) Then .rstblLidBetaling.Requery
        If (.rstblLidBetalingNieuw.State = adStateOpen) Then .rstblLidBetalingNieuw.Requery
        If (.rstblLidBetalingBestaand.State = adStateOpen) Then .rstblLidBetalingBestaand.Requery
    End With

    'Remove the familieID's set to none
    Set adCommand = New ADODB.Command
    Set adCommand.ActiveConnection = DataEnvironment1.Connection1
    adCommand.CommandText = "UPDATE tbllid SET groep_familieid = NULL WHERE groep_familieid = (SELECT familieid FROM tblfamilie WHERE familienaam like '(Geen)')"
    adCommand.Execute

    'Add a beginner's degree in the degree table
    If mMode = kaCreate Then
        Dim acUpdate As New ADODB.Command
        With DataEnvironment1
            Set acUpdate.ActiveConnection = .Connection1
            acUpdate.CommandText = "INSERT INTO tblGraad(LidID,Graad,ExamenDatum,ExamenPlaats,Actief,Afgedrukt) VALUES(" + _
                                   CStr(.rsLid!LidID) + ", " + _
                                   "'01-Beginner', " + _
                                   "'" + Format(Now, "dd/mm/yyyy") + "', " + _
                                   "'Wommelgem', " + _
                                   "True, False)"
            acUpdate.Execute , , adExecuteNoRecords
        End With
    End If
    
    DataChanged = False
    
    Exit Sub
    
ErrorHandler:
    Select Case Err.Number
        Case cErrorCreateMember
            mMode = kaCreateUnfinished
            If (MsgBox(Err.Description, vbInformation + vbYesNo) = vbYes) Then
                DataEnvironment1.rsLid.CancelUpdate
                NieuwLid
                txtVoornaam.SetFocus
            Else
                DataEnvironment1.rsLid.CancelUpdate
                mMode = kaCreate
            End If
        Case cErrorUpdateMember
            mMode = kaUpdateUnfinished
            If (MsgBox(Err.Description, vbInformation + vbYesNo) = vbYes) Then
                'DataEnvironment1.rsLid.CancelUpdate
                txtGeboortedatum.SetFocus
                lblGeboortedatum.FontBold = True
                'mMode = kaUpdate
            Else
                DataEnvironment1.rsLid.CancelUpdate
                mMode = kaUpdate
                Unload Me
            End If
        Case Else
            MsgBox "Er is een fout opgetreden, sluit het scherm af en probeer opnieuw. Indien de fout zich blijft herhalen, contacteer dan " & mcstrAdministrator & " en geef volgende informatie : " & vbCrLf & Err.Description & vbCrLf & Err.Source & vbCrLf & Err.Number, vbExclamation
    End Select
End Sub

Private Sub dcFamilie_Click(Area As Integer)
    DataChanged = True
End Sub

Private Sub txtEmail_DblClick()
    ShellExecute 0, "open", "mailto:" & txtEmail.text, 0, 0, 1
End Sub

Private Sub txtGeslacht_Validate(Cancel As Boolean)
    txtGeslacht = UCase(txtGeslacht)
    If (txtGeslacht <> "M" And txtGeslacht <> "V") Then
        Beep
        MsgBox "Het geslacht moet 'M' of 'V' zijn", vbExclamation
        txtGeslacht.SelStart = 0
        txtGeslacht.SelLength = Len(txtGeslacht)
        Cancel = True
    End If
End Sub

