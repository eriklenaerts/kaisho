VERSION 5.00
Object = "{065E6FD1-1BF9-11D2-BAE8-00104B9E0792}#3.0#0"; "SSA3D30.OCX"
Begin VB.Form frmMain 
   Caption         =   "Kaisho Administratie"
   ClientHeight    =   5280
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   3195
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5280
   ScaleWidth      =   3195
   StartUpPosition =   2  'CenterScreen
   Begin Threed.SSCommand cmdRapporten 
      Height          =   612
      Left            =   120
      TabIndex        =   6
      Top             =   3720
      Width           =   2892
      _ExtentX        =   5106
      _ExtentY        =   1085
      _Version        =   196609
      PictureFrames   =   1
      Picture         =   "frmMain.frx":0000
      Caption         =   "Rapporten"
      PictureAlignment=   1
   End
   Begin Threed.SSCommand cmdExamen 
      Height          =   612
      Left            =   120
      TabIndex        =   5
      Top             =   3000
      Width           =   2892
      _ExtentX        =   5106
      _ExtentY        =   1085
      _Version        =   196609
      Caption         =   "Examen"
   End
   Begin Threed.SSCommand cmdStatistieken 
      Height          =   615
      Left            =   120
      TabIndex        =   4
      Top             =   2280
      Width           =   2895
      _ExtentX        =   5106
      _ExtentY        =   1085
      _Version        =   196609
      PictureFrames   =   1
      Picture         =   "frmMain.frx":031A
      Caption         =   "Statistieken"
      PictureAlignment=   1
   End
   Begin Threed.SSCommand cmdSluiten 
      Cancel          =   -1  'True
      Height          =   612
      Left            =   120
      TabIndex        =   3
      Top             =   4560
      Width           =   2892
      _ExtentX        =   5106
      _ExtentY        =   1085
      _Version        =   196609
      PictureFrames   =   1
      Picture         =   "frmMain.frx":0634
      Caption         =   "&Sluiten"
      PictureAlignment=   1
   End
   Begin Threed.SSCommand cmdLidLijst 
      Height          =   615
      Left            =   120
      TabIndex        =   2
      Top             =   1560
      Width           =   2895
      _ExtentX        =   5106
      _ExtentY        =   1085
      _Version        =   196609
      PictureFrames   =   1
      Picture         =   "frmMain.frx":094E
      Caption         =   "&Ledenlijst"
      PictureAlignment=   1
   End
   Begin Threed.SSCommand cmdBetaling 
      Height          =   615
      Left            =   120
      TabIndex        =   1
      Top             =   840
      Width           =   2895
      _ExtentX        =   5106
      _ExtentY        =   1085
      _Version        =   196609
      PictureFrames   =   1
      Picture         =   "frmMain.frx":1244
      Caption         =   "&Betalinglijst"
      PictureAlignment=   1
   End
   Begin Threed.SSCommand cmdAanwezigheid 
      Height          =   615
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2895
      _ExtentX        =   5106
      _ExtentY        =   1085
      _Version        =   196609
      PictureFrames   =   1
      Picture         =   "frmMain.frx":155E
      Caption         =   "Aanwezigheidslijst"
      PictureAlignment=   1
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdAanwezigheid_Click()
    Set mfrmAanwezigheid = New frmAanwezigheid
    mfrmAanwezigheid.OpenForm 2000, 1
    mfrmAanwezigheid.Show
End Sub

Private Sub cmdBetaling_Click()
    Set mfrmBetalingen = New frmBetalingen
    mfrmBetalingen.OpenForm 2000
    mfrmBetalingen.Show
End Sub

Private Sub cmdExamen_Click()
    Set mfrmExamen = New frmExamen
    mfrmExamen.Show
End Sub

Private Sub cmdLidLijst_Click()
    Set mfrmLidLijst = New frmLidLijst
    mfrmLidLijst.Show
End Sub

Private Sub cmdSluiten_Click()
    End
End Sub

Private Sub cmdStatistieken_Click()
    Set mfrmStatistieken = New frmStatistieken
    mfrmStatistieken.Show
End Sub

Private Sub cmdAanwezigheid_MouseEnter(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    cmdAanwezigheid.PictureAnimationEnabled = True
End Sub

Private Sub cmdAanwezigheid_MouseExit(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    cmdAanwezigheid.PictureAnimationEnabled = False
    cmdAanwezigheid.PictureFrame = 1
End Sub

Private Sub cmdBetaling_MouseEnter(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    cmdBetaling.PictureAnimationEnabled = True
End Sub

Private Sub cmdBetaling_MouseExit(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    cmdBetaling.PictureAnimationEnabled = False
    cmdBetaling.PictureFrame = 1
End Sub

Private Sub cmdLidLijst_MouseEnter(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    cmdLidLijst.PictureAnimationEnabled = True
End Sub

Private Sub cmdLidLijst_MouseExit(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    cmdLidLijst.PictureAnimationEnabled = False
    cmdLidLijst.PictureFrame = 1
End Sub

Private Sub cmdStatistieken_MouseEnter(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    cmdStatistieken.PictureAnimationEnabled = True
End Sub

Private Sub cmdStatistieken_MouseExit(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    cmdStatistieken.PictureAnimationEnabled = False
    cmdStatistieken.PictureFrame = 1
End Sub

Private Sub cmdSluiten_MouseEnter(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    cmdSluiten.PictureAnimationEnabled = True
End Sub

Private Sub cmdSluiten_MouseExit(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    cmdSluiten.PictureAnimationEnabled = False
    cmdSluiten.PictureFrame = 1
End Sub

Private Sub Form_Load()
    cmdAanwezigheid.PictureAnimationEnabled = False
    cmdLidLijst.PictureAnimationEnabled = False
    cmdStatistieken.PictureAnimationEnabled = False
    cmdBetaling.PictureAnimationEnabled = False
    cmdSluiten.PictureAnimationEnabled = False
End Sub
