VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmFoto 
   Caption         =   "Form1"
   ClientHeight    =   2895
   ClientLeft      =   5880
   ClientTop       =   4350
   ClientWidth     =   4665
   LinkTopic       =   "Form1"
   ScaleHeight     =   2895
   ScaleWidth      =   4665
   Begin MSComDlg.CommonDialog cmdOpenPhoto 
      Left            =   3360
      Top             =   1560
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdChangePicture 
      Caption         =   "&Wijzigen"
      Height          =   375
      Left            =   3120
      TabIndex        =   3
      Top             =   840
      Width           =   1335
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Default         =   -1  'True
      Height          =   375
      Left            =   1800
      TabIndex        =   2
      Top             =   2400
      Width           =   1335
   End
   Begin VB.CommandButton cmdAnnuleren 
      Cancel          =   -1  'True
      Caption         =   "&Annuleren"
      Height          =   375
      Left            =   3240
      TabIndex        =   1
      Top             =   2400
      Width           =   1335
   End
   Begin VB.Label Label1 
      Caption         =   "Huidige foto :"
      Height          =   375
      Left            =   240
      TabIndex        =   4
      Top             =   840
      Width           =   1095
   End
   Begin VB.Image imgFoto 
      Height          =   1215
      Left            =   1800
      Top             =   840
      Width           =   1095
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   4560
      Y1              =   2280
      Y2              =   2280
   End
   Begin VB.Label lblTitel 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Foto van het lid"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4455
   End
End
Attribute VB_Name = "frmFoto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mstrLicNr As String
Private mblnDirty As Boolean

Public Property Let LicNr(NewNr As String)
    mstrLicNr = NewNr
    ShowPicture mstrLicNr
End Property

Private Sub cmdAnnuleren_Click()
    Me.Hide
End Sub

Private Sub ShowPicture(LicNr As String)
On Error GoTo ErrorHandler
    'If no LicNr exist, it is a new member
    If Len(LicNr) > 0 Then
        imgFoto.Stretch = True
        imgFoto.Picture = LoadPicture(App.Path + "\Foto\" + LicNr + ".jpg")
    End If
    Exit Sub
ErrorHandler:
    'no foto available
    If Err.Number = 53 Then
    Else
        MsgBox "Error : " & Err.Description
    End If
End Sub

Private Sub cmdChangePicture_Click()
On Error GoTo ErrorHandler
    
    cmdOpenPhoto.DialogTitle = "Selecteer een foto"
    cmdOpenPhoto.InitDir = "\"
    cmdOpenPhoto.FileName = ""
    cmdOpenPhoto.Filter = " (*.jpg)|*.jpg|(*.gif)|*.gif|(*.bmp)|*.bmp|All Files (*.*)|*.*"
    cmdOpenPhoto.FilterIndex = 4
    cmdOpenPhoto.Flags = cdlOFNFileMustExist
    cmdOpenPhoto.ShowOpen
    
    If (Len(cmdOpenPhoto.FileName) > 0) Then
        'set picture
        imgFoto.Stretch = True
        imgFoto.Picture = LoadPicture(cmdOpenPhoto.FileName)
        mblnDirty = True
    End If
    
    Exit Sub
ErrorHandler:
    If Err.Number = 481 Then
        MsgBox "Dit is geen geldig bestand om als foto te gebruiken", vbExclamation
    Else
        MsgBox "Error : " & Err.Description
    End If
End Sub

Private Sub cmdOk_Click()
On Error GoTo ErrorHandler

    'If no LicNr exist, it is a new member
    If mblnDirty Then
        'Save photo
        FileCopy cmdOpenPhoto.FileName, App.Path + "\Foto\" + mstrLicNr + Right(cmdOpenPhoto.FileName, 4)
    End If
    Me.Hide
    Exit Sub
ErrorHandler:
    MsgBox "Error : " & Err.Description
End Sub
