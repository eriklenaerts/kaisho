VERSION 5.00
Begin VB.Form frmLidGroepen 
   Caption         =   "Groep"
   ClientHeight    =   4428
   ClientLeft      =   60
   ClientTop       =   348
   ClientWidth     =   4488
   Icon            =   "frmLidGroepen.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4428
   ScaleWidth      =   4488
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Default         =   -1  'True
      Height          =   375
      Left            =   1560
      TabIndex        =   1
      Top             =   3960
      Width           =   1335
   End
   Begin VB.CommandButton cmdAnnuleren 
      Cancel          =   -1  'True
      Caption         =   "&Annuleren"
      Height          =   375
      Left            =   3000
      TabIndex        =   0
      Top             =   3960
      Width           =   1335
   End
   Begin VB.Label lblTitel 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Groepen"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   612
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   4212
   End
   Begin VB.Line Line2 
      Visible         =   0   'False
      X1              =   120
      X2              =   4320
      Y1              =   4440
      Y2              =   4440
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   4320
      Y1              =   3840
      Y2              =   3840
   End
End
Attribute VB_Name = "frmLidGroepen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdAnnuleren_Click()
    Unload Me
End Sub

Private Sub cmdOk_Click()
    Unload Me
End Sub
