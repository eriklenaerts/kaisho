VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmLidGraden 
   Caption         =   "Graden"
   ClientHeight    =   4530
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8175
   Icon            =   "frmLidGraden.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4530
   ScaleWidth      =   8175
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.ImageList imlGraden 
      Left            =   3120
      Top             =   3960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   14
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidGraden.frx":030A
            Key             =   "picBEGINNER"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidGraden.frx":0626
            Key             =   "pic8KYU"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidGraden.frx":0942
            Key             =   "pic7KYU"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidGraden.frx":0C5E
            Key             =   "pic6KYU"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidGraden.frx":0F7A
            Key             =   "pic5KYU"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidGraden.frx":1296
            Key             =   "pic4KYU"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidGraden.frx":15B2
            Key             =   "pic3KYU"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidGraden.frx":18CE
            Key             =   "pic2KYU"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidGraden.frx":1BEA
            Key             =   "pic1KYU"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidGraden.frx":1F06
            Key             =   "pic1DAN"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidGraden.frx":2222
            Key             =   "pic2DAN"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidGraden.frx":253E
            Key             =   "pic3DAN"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidGraden.frx":285A
            Key             =   "pic4DAN"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidGraden.frx":2B76
            Key             =   "pic5DAN"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lvwGraden 
      Height          =   3012
      Left            =   120
      TabIndex        =   0
      Top             =   840
      Width           =   7932
      _ExtentX        =   13996
      _ExtentY        =   5318
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   12632256
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Graad"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Examen Datum"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Examen Plaats"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Default         =   -1  'True
      Height          =   375
      Left            =   6720
      TabIndex        =   2
      Top             =   4080
      Width           =   1335
   End
   Begin VB.Label lblTitel 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Graden"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   612
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   7932
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   8040
      Y1              =   3960
      Y2              =   3960
   End
End
Attribute VB_Name = "frmLidGraden"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Sub OpenForm(lngLidID As Long)
    StartUsingRecordset DataEnvironment1.rsLidGraad
    DataEnvironment1.rsLidGraad.Filter = "LidID=" + CStr(lngLidID)
    Me.Caption = "Historiek voor " + DataEnvironment1.rsLid("Voornaam") + " " + DataEnvironment1.rsLid("Achternaam")
    lblTitel.Caption = vbCrLf + Me.Caption
    FillListView
End Sub

Private Sub cmdOk_Click()
    Unload Me
End Sub

Private Sub FillListView()
Dim itmX As ListItem
Dim lstrGraad As String
   
    lvwGraden.Visible = False
   
    
    Set lvwGraden.SmallIcons = imlGraden
    lvwGraden.View = lvwReport   ' Set view to Report.
    lvwGraden.BorderStyle = ccFixedSingle
    
    If (Not DataEnvironment1.rsLidGraad.EOF) Then DataEnvironment1.rsLidGraad.MoveFirst
    DataEnvironment1.rsLidGraad.Sort = "Graad"
    While Not DataEnvironment1.rsLidGraad.EOF
       lstrGraad = Mid$(DataEnvironment1.rsLidGraad("Graad").value, 4)
       Set itmX = lvwGraden.ListItems.Add(, , , , "pic" + UCase(Join(Split(lstrGraad, " "), "")))
       itmX.SubItems(1) = lstrGraad
       itmX.SubItems(2) = DataEnvironment1.rsLidGraad("ExamenDatum").value
       itmX.SubItems(3) = DataEnvironment1.rsLidGraad("ExamenPlaats").value
       DataEnvironment1.rsLidGraad.MoveNext
    Wend
    
    lvwGraden.Visible = True
    lvwGraden.Refresh

End Sub

Private Sub Form_Unload(Cancel As Integer)
    StartUsingRecordset DataEnvironment1.rsLidGraad
End Sub
