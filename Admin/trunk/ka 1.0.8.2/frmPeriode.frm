VERSION 5.00
Begin VB.Form frmPeriode 
   Caption         =   "Periode"
   ClientHeight    =   2400
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6810
   Icon            =   "frmPeriode.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   2400
   ScaleWidth      =   6810
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdOpenInExcel 
      Caption         =   "&Open in Excel"
      Height          =   372
      Left            =   2520
      TabIndex        =   8
      Top             =   1920
      Width           =   1332
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      Default         =   -1  'True
      Height          =   372
      Left            =   3960
      TabIndex        =   7
      Top             =   1920
      Width           =   1332
   End
   Begin VB.TextBox txtJaar 
      Height          =   288
      Left            =   4320
      TabIndex        =   2
      Top             =   1320
      Width           =   1692
   End
   Begin VB.CommandButton cmdAnnuleren 
      Cancel          =   -1  'True
      Caption         =   "&Annuleren"
      Height          =   372
      Left            =   5400
      TabIndex        =   3
      Top             =   1920
      Width           =   1332
   End
   Begin VB.ComboBox cboMaand 
      Height          =   288
      Left            =   1320
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   1320
      Width           =   1692
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   240
      Picture         =   "frmPeriode.frx":030A
      Top             =   210
      Width           =   480
   End
   Begin VB.Label lblJaar 
      Caption         =   "Jaar :"
      Height          =   252
      Left            =   3240
      TabIndex        =   6
      Top             =   1320
      Width           =   852
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   6720
      Y1              =   1800
      Y2              =   1800
   End
   Begin VB.Label lblMaand 
      Caption         =   "Maand :"
      Height          =   252
      Left            =   240
      TabIndex        =   5
      Top             =   1320
      Width           =   972
   End
   Begin VB.Label lblUitleg 
      Height          =   252
      Left            =   120
      TabIndex        =   4
      Top             =   840
      Width           =   6492
   End
   Begin VB.Label lblTitel 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Periode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   612
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   6612
   End
End
Attribute VB_Name = "frmPeriode"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mMode As kaPeriodeMode
Private WithEvents mfrmStatus As frmStatus
Attribute mfrmStatus.VB_VarHelpID = -1
Private mCancelled As Boolean

Private Sub cmdAnnuleren_Click()
    Set mfrmPeriode = Nothing
    Unload Me
End Sub

Private Sub cmdOpenInExcel_Click()
    LoadDataInExcel
End Sub

Private Sub cmdPrint_Click()
    LoadDataInExcel True
End Sub

Private Function ParseName(FamilieNaam As Variant, Naam2 As String) As String
Static ANaam As String
    If IsNull(FamilieNaam) Then
        ParseName = Naam2
    Else
        
        If (ANaam = CStr(FamilieNaam)) Then
            ParseName = Naam2
        Else
            ParseName = "-" + CStr(FamilieNaam) + " " + Trim$(Naam2)
        End If
        ANaam = FamilieNaam
    End If
End Function

Private Sub LoadDataInExcel(Optional blnPrint As Boolean = False)
Dim intJaar As Integer, btMaand As Byte
Dim Fields() As Variant
Dim colTrainingDays As Collection
Dim TrainingsDay As Variant
Dim lngColumnCounter As Long, lngRowCounter As Long, lngCounter As Long
Dim objxlWorkSheet As Excel.Worksheet
Dim objExcelExt As clsExcelExtension
Dim Range_Cell1 As Variant, Range_Cell2 As Variant
Dim strListTitle As String
Dim strSum As String
Dim lngSum As Long
Dim strRange As String
Dim lngStartRow As Long
Dim lngEndRow As Long
Dim blnEndOfList As Boolean
Dim blnEndSearch As Boolean
Dim blnMerge As Boolean
Dim intTel As Integer
Dim lngFamilyPosition As Long
Dim lngNrOfFamilies As Long
Dim lngProgress As Long

    
On Error GoTo ErrorHandler

    Set objExcelExt = New clsExcelExtension
    'Check For Excel and if open, ask for excel to close.
    If (objExcelExt.CloseExcelIfRunning(False)) Then
        MsgBox "Excel draait blijkbaar nog, er kan geen lijst worden aangemaakt als excel draait. Sluit Excel vooraleer je een lijst wil afdrukken of bekijken, indien excel volgens jou niet draait, contacteer dan Erik Lenaerts."
        End
    End If

    objLogger.WriteLog ("Get year")
    'Check & get Year
    If (Not IsNumeric(txtJaar)) Then
        MsgBox "Het opgegeven jaartal is niet numerisch.", vbExclamation
        Exit Sub
    Else
        intJaar = CLng(txtJaar)
    End If
    
    objLogger.WriteLog ("Get Month")
    'Get month
    If (cboMaand.Visible) Then
        btMaand = cboMaand.ListIndex + 1
    End If
    
    If (mMode = pmBetaling) Then
        With DataEnvironment1
            Dim rs As New ADODB.Recordset
            Dim sql As String
            sql = "SELECT tblLid.LidID, IIf(IsNull(tblFamilie.FamilieNaam),tblLid.Achternaam,tblFamilie.FamilieNaam) & ' ' & tblLid.Voornaam AS Naam, tblLid.Nieuw, tblFamilie.FamilieNaam, IIf(IsNull(tblFamilie.FamilieNaam),tblLid.Achternaam,'             ') & ' ' & tblLid.Voornaam AS Naam2 " + _
                  "FROM tblFamilie RIGHT JOIN tblLid ON tblFamilie.FamilieID = tblLid.Groep_FamilieID " + _
                  "WHERE tblLid.Inactief=False " + _
                  "ORDER BY 2;"
            
            rs.Open sql, DataEnvironment1.Connection1.ConnectionString, adOpenStatic, adLockReadOnly, adCmdText
                
            objLogger.WriteLog ("start using rs leden (Betaling)")
            'StartUsingRecordset .rsLeden, , False
            'StartUsingRecordset .rsLeden
            
            ReDim Fields(rs.RecordCount + 10, 12)
            
            'HeaderRow
            Fields(0, 0) = "Naam"
            Fields(0, 1) = "Bedrag"
            Fields(0, 2) = "Opmerkingen      "
            For lngColumnCounter = 1 To 10
                Fields(0, lngColumnCounter + 2) = MaandNaamVoorBetaling(CInt(lngColumnCounter))
            Next lngColumnCounter
            
            objLogger.WriteLog ("Add Members (Betaling)")
            'Add members
            rs.Filter = "Nieuw=0"
            lngRowCounter = lngRowCounter + 1
            While Not rs.EOF
                Fields(lngRowCounter, 0) = ParseName(rs("FamilieNaam").value, rs("Naam2").value)
                Fields(lngRowCounter, 1) = BerekenBedrag(rs("LidID").value)
                lngRowCounter = lngRowCounter + 1
                rs.MoveNext
            Wend
        
            Fields(lngRowCounter + 1, 0) = "NIEUWE LEDEN"
            lngRowCounter = lngRowCounter + 2
            
            objLogger.WriteLog ("Add New Members (Betaling)")
            'Add New Members
            rs.Filter = "Nieuw=1"
            'rs.Sort = "Achternaam"
            While Not rs.EOF
                Fields(lngRowCounter, 0) = ParseName(rs("FamilieNaam").value, rs("Naam2").value)
                Fields(lngRowCounter, 1) = "-"
                lngRowCounter = lngRowCounter + 1
                rs.MoveNext
            Wend
            
            'Every Family is prefixed with an "-" sign. In order to set a propper
            'layout, we also must find out how many members one family counts.
            'The number of members will be added with the family prefix.
            'So -4Jannsens points out, the start of a 4 member family.
            
            'Start from 1 row (row 0 is de header)
            lngFamilyPosition = -1
            For lngRowCounter = 1 To UBound(Fields, 1)
                'Set family pointer
                If (lngFamilyPosition = -1) Then
                    lngFamilyPosition = IIf(Left(Fields(lngRowCounter, 0), 1) = "-", lngRowCounter, -1)
                End If
                'Add nr of family members
                If (Left(Fields(lngRowCounter, 0), 1) <> " ") And (lngFamilyPosition <> -1) And (lngRowCounter <> lngFamilyPosition) Then
                    lngNrOfFamilies = lngNrOfFamilies + 1
                    Fields(lngFamilyPosition, 0) = "-" + CStr(lngRowCounter - lngFamilyPosition) + Mid$(Fields(lngFamilyPosition, 0), 2)
                    lngFamilyPosition = IIf(Left(Fields(lngRowCounter, 0), 1) = "-", lngRowCounter, -1)
                End If
            Next lngRowCounter
            
            strListTitle = "Betalingslijst " & intJaar
        End With
    ElseIf (mMode = pmAanwezigheid) Then
        With DataEnvironment1
            objLogger.WriteLog ("start using rs leden (Aanwezigheid)")
            'StartUsingRecordset .rsLeden, , False
            StartUsingRecordset .rsLeden
            .rsLeden.Sort = "Naam"
            Set colTrainingDays = GetTrainingDays(btMaand, intJaar)
            ReDim Fields(.rsLeden.RecordCount + 10, colTrainingDays.Count + 2)
            
            objLogger.WriteLog ("headerrow (Aanwezigheid)")
            'HeaderRow
            Fields(0, 0) = "Naam"
            Fields(0, 1) = "Betaald"
            Fields(0, 2) = "Opmerking"
            lngColumnCounter = 3
            For Each TrainingsDay In colTrainingDays
                Fields(0, lngColumnCounter) = TrainingsDay
                lngColumnCounter = lngColumnCounter + 1
            Next TrainingsDay
            
            objLogger.WriteLog ("add members (Aanwezigheid)")
            'Add members
            .rsLeden.Filter = "Nieuw=0 AND InActief=0"
            lngRowCounter = lngRowCounter + 1
            While Not .rsLeden.EOF
                Fields(lngRowCounter, 0) = .rsLeden("Naam").value
                lngRowCounter = lngRowCounter + 1
                .rsLeden.MoveNext
            Wend
        
            Fields(lngRowCounter + 1, 0) = "NIEUWE LEDEN"
            lngRowCounter = lngRowCounter + 2
            
            objLogger.WriteLog ("add new members (Aanwezigheid)")
            'Add New Members
            .rsLeden.Filter = "Nieuw=1 AND InActief=0"
            While Not .rsLeden.EOF
                Fields(lngRowCounter, 0) = .rsLeden("Naam").value
                lngRowCounter = lngRowCounter + 1
                .rsLeden.MoveNext
            Wend
            strListTitle = "Aanwezigheidslijst " & MaandNaam(CInt(btMaand)) & " " & intJaar
        End With
    End If
    
    Unload Me
    objLogger.WriteLog ("new extension object")
    
    objLogger.WriteLog ("Displaydata in sheet")
    Set objxlWorkSheet = objExcelExt.DisplayDataInSheet(Fields, Range_Cell1, Range_Cell2, strListTitle, "Laatste mutatie " & Format(GetMutationDate, "dd mmmm yyyy"), True)

    objLogger.WriteLog ("display message")
    'Display message
    Set mfrmStatus = New frmStatus
    mfrmStatus.lblText = "Bezig met het openen van Excel"
    mfrmStatus.OpenForm kaMetProgress

    WaitAWhile 5
    mfrmStatus.prgMain = 10
    
    
    If (objxlWorkSheet Is Nothing) Then Err.Raise cErrorUserrequestedCancel
   
    mfrmStatus.prgMain = 20
   
    objLogger.WriteLog ("Put Headingformat")
    'Put Headingformat
    objxlWorkSheet.Range("D4", Mid$(Range_Cell2, 1, 1) & "4").NumberFormat = "00"
    
    mfrmStatus.prgMain = 30
    
    objLogger.WriteLog ("Autofit columns")
    'Autofit columns
    For lngColumnCounter = 1 To UBound(Fields, 2)
        objxlWorkSheet.Columns(lngColumnCounter).AutoFit
    Next lngColumnCounter
        
    mfrmStatus.prgMain = 40
    
    objLogger.WriteLog ("Display Raster")
    'Display Raster
    With objxlWorkSheet.Range("A4", Range_Cell2)
        With .Borders(xlEdgeLeft)
            .LineStyle = xlContinuous
            .Weight = xlMedium
            .ColorIndex = 1
        End With
        With .Borders(xlEdgeTop)
            .LineStyle = xlContinuous
            .Weight = xlMedium
            .ColorIndex = 1
        End With
        With .Borders(xlEdgeBottom)
            .LineStyle = xlContinuous
            .Weight = xlMedium
            .ColorIndex = 1
        End With
        With .Borders(xlEdgeRight)
            .LineStyle = xlContinuous
            .Weight = xlMedium
            .ColorIndex = 1
        End With
        With .Borders(xlInsideVertical)
            .LineStyle = xlContinuous
            .Weight = xlThin
            .ColorIndex = xlAutomatic
        End With
        With .Borders(xlInsideHorizontal)
            .LineStyle = xlContinuous
            .Weight = xlThin
            .ColorIndex = xlAutomatic
        End With
    End With
   
    mfrmStatus.prgMain = 55
   
    objLogger.WriteLog ("PageSetup")
    'PageSetup
    With objxlWorkSheet.PageSetup
        .LeftMargin = 14.1732283464567 'Application.InchesToPoints(0.196850393700787)
        mfrmStatus.prgMain = 70
        .RightMargin = 14.1732283464567 'Application.InchesToPoints(0.196850393700787)
    End With
    
    'Extra formating for the payment list
    Dim lngStatusCounter As Long
    Dim blnNewMembers As Boolean
    lngStartRow = 4
    objLogger.WriteLog ("Nr of Families : " & lngNrOfFamilies)
    If (mMode = pmBetaling) Then
    'If (2 = 3) Then
        With objxlWorkSheet
            blnEndOfList = False
            
            Do While Not blnEndOfList
                lngStatusCounter = lngStatusCounter + 1
                lngProgress = 55 + lngStatusCounter \ (lngNrOfFamilies / 40)
                mfrmStatus.prgMain = IIf(lngProgress > 100, 100, lngProgress)
                'find start row (search for family signs ("-")
                blnEndSearch = False
                blnMerge = True
                Do While Not blnEndSearch
                    lngStartRow = lngStartRow + 1
                    strRange = "A" + CStr(lngStartRow) + ":A" + CStr(lngStartRow)
                    If (Left(.Range(strRange).value, 1) = "-") Then
                        blnEndSearch = True
                        'trim special familiesign ("-")
                        '.Range(strRange).value = Mid$(.Range(strRange).value, 2)   'with Count
                        .Range(strRange).value = Mid$(.Range(strRange).value, 3)    'without count
                    ElseIf (.Range(strRange).value = "NIEUWE LEDEN") Then
                        blnNewMembers = True
                        Exit Do
                    ElseIf (.Range(strRange).value = "") Then
                        If blnNewMembers Then blnEndOfList = True
                        Exit Do
                    End If
                Loop
                
                'No family editing todo enymore
                If blnEndOfList And blnNewMembers Then Exit Do
                
                lngEndRow = lngStartRow
                blnEndSearch = False
                'find end row (search for nonspace starting rows)
                Do While Not blnEndSearch
                    lngEndRow = lngEndRow + 1
                    strRange = "A" + CStr(lngEndRow) + ":A" + CStr(lngEndRow)
                    If (Left(.Range(strRange).value, 2) <> "  ") Then
                        blnEndSearch = True
                        lngEndRow = lngEndRow - 1
                    ElseIf (.Range(strRange).value = "NIEUWE LEDEN") Then
                        lngEndRow = lngEndRow - 2
                        blnEndOfList = True
                        Exit Do
                    ElseIf (.Range(strRange).value = "Naam") Then
                        blnMerge = False
                    End If
                Loop
                    
                strRange = "A" + CStr(lngStartRow) + ":A" + CStr(lngEndRow)
                .Range(strRange).Select
'                .Application.Selection.Borders(xlDiagonalDown).LineStyle = xlNone
'                .Application.Selection.Borders(xlDiagonalUp).LineStyle = xlNone
'                .Application.Selection.Borders(xlEdgeLeft).LineStyle = xlNone
'                .Application.Selection.Borders(xlEdgeTop).LineStyle = xlNone
'                .Application.Selection.Borders(xlEdgeBottom).LineStyle = xlNone
'                .Application.Selection.Borders(xlEdgeRight).LineStyle = xlNone
'                .Application.Selection.Borders(xlInsideVertical).LineStyle = xlNone
'                .Application.Selection.Borders(xlInsideHorizontal).LineStyle = xlNone
'                .Application.Selection.Borders(xlDiagonalDown).LineStyle = xlNone
'                .Application.Selection.Borders(xlDiagonalUp).LineStyle = xlNone
'                With .Application.Selection.Borders(xlEdgeLeft)
'                    .LineStyle = xlContinuous
'                    .Weight = xlThin
'                    .ColorIndex = xlAutomatic
'                End With
'                With .Application.Selection.Borders(xlEdgeTop)
'                    .LineStyle = xlContinuous
'                    .Weight = xlThin
'                    .ColorIndex = xlAutomatic
'                End With
'                With .Application.Selection.Borders(xlEdgeBottom)
'                    .LineStyle = xlContinuous
'                    .Weight = xlThin
'                    .ColorIndex = xlAutomatic
'                End With
'                With .Application.Selection.Borders(xlEdgeRight)
'                    .LineStyle = xlContinuous
'                    .Weight = xlThin
'                    .ColorIndex = xlAutomatic
'                End With
                .Application.Selection.Borders(xlInsideHorizontal).LineStyle = xlNone
                strRange = "B" + CStr(lngStartRow) + ":B" + CStr(lngEndRow)
                .Range(strRange).Select
                'For intTel = lngStartRow To lngEndRow
                '    lngSum = lngSum + CLng(.Range("B" + CStr(intTel)).value)
                'Next intTel
                If (.Range("B" + CStr(lngStartRow)).value) <> "-" Then
                    lngSum = lngSum + CLng(.Range("B" + CStr(lngStartRow)).value)
                    strSum = CStr(lngSum)
                    If strSum = "0" Then strSum = ""
                    .Range(strRange).Select
                    With .Application.Selection
                        If blnMerge Then
                            .ClearContents
    '                        .HorizontalAlignment = xlGeneral
                            .VerticalAlignment = xlCenter
    '                        .WrapText = False
    '                        .Orientation = 0
    '                        .AddIndent = False
    '                        .ShrinkToFit = False
                            .MergeCells = True
                            .Application.ActiveCell.value = strSum
                        Else
                            .ClearContents
                            strRange = "B" + CStr(lngStartRow) + ":B" + CStr(lngStartRow)
                            .Range(strRange).value = strSum
                        End If
                    End With
                    .Range("A3").ClearContents
                    lngStartRow = lngEndRow
                    lngSum = 0
                End If
            Loop
        End With
    End If
    
    mfrmStatus.prgMain = 95

    Unload mfrmStatus
    Set mfrmStatus = Nothing
   
    objLogger.WriteLog ("print")
    If (blnPrint) Then
        'Print
        If (Not objxlWorkSheet Is Nothing) Then
            objxlWorkSheet.PrintOut
        End If
        
        If objExcelExt.ExcelWasNotRunning Then objxlWorkSheet.Application.Quit
        
        Set objxlWorkSheet = Nothing
    Else
        objLogger.WriteLog ("show excel")
        'Show excel
        objxlWorkSheet.Application.Visible = True
        
        'If objExcelExt.ExcelWasNotRunning Then objxlWorkSheet.Application.Quit
        
        Set objxlWorkSheet = Nothing
    End If
    
    Exit Sub
    
ErrorHandler:
    Select Case Err.Number
        Case cErrorUserrequestedCancel
            MsgBox "Actie geannuleerd", vbInformation
            mCancelled = False
        Case Else
            MsgBox "Unspecified Error : " & Err.Description & "(" & Err.Number & " at " & Err.Source & ")"
    End Select

    If (Not mfrmStatus Is Nothing) Then
        Unload mfrmStatus
        Set mfrmStatus = Nothing
    End If
End Sub

Private Sub Form_Load()
Dim intMaandTeller As Integer

    For intMaandTeller = 1 To 12
        cboMaand.AddItem MaandNaam(intMaandTeller)
    Next intMaandTeller
    cboMaand.ListIndex = 0
    txtJaar = Year(Now())
End Sub

Public Sub OpenForm(Mode As kaPeriodeMode)
    mMode = Mode
       
    If (Mode = pmAanwezigheid) Then
        lblTitel = vbCrLf + "Periode voor Aanwezigheidslijst"
        lblUitleg.Caption = "Kies een maand en een jaar : "
        txtJaar = Year(Now())
        lblJaar.Left = 3240
        txtJaar.Left = 4320
    Else
        lblTitel = vbCrLf + "Periode voor Betalingslijst"
        lblUitleg.Caption = "Kies een jaar :"
        lblJaar.Left = 240
        txtJaar.Left = 1320
        cboMaand.Visible = False
        lblMaand.Visible = False
    End If
    Me.Show vbModal
End Sub

Private Sub mfrmStatus_Cancelled()
    mCancelled = True
End Sub
