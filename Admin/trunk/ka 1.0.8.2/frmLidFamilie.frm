VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmLidFamilie 
   Caption         =   "Familie beheer"
   ClientHeight    =   4425
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4485
   Icon            =   "frmLidFamilie.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4425
   ScaleWidth      =   4485
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdFamilieWijzigen 
      Caption         =   "&Wijzigen"
      Height          =   375
      Left            =   1560
      TabIndex        =   6
      Top             =   3240
      Width           =   1335
   End
   Begin VB.CommandButton cmdFamilieToevoegen 
      Caption         =   " &Toevoegen"
      Height          =   372
      Left            =   120
      TabIndex        =   1
      Top             =   3240
      Width           =   1335
   End
   Begin VB.CommandButton cmdFamilieVerwijderen 
      Caption         =   "&Verwijderen"
      Height          =   372
      Left            =   3000
      TabIndex        =   2
      Top             =   3240
      Width           =   1335
   End
   Begin MSComctlLib.ListView lvwFamilie 
      Height          =   2292
      Left            =   120
      TabIndex        =   0
      Top             =   840
      Width           =   4212
      _ExtentX        =   7435
      _ExtentY        =   4048
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   12632256
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   1
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Naam"
         Object.Width           =   7056
      EndProperty
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Default         =   -1  'True
      Height          =   375
      Left            =   1560
      TabIndex        =   3
      Top             =   3960
      Width           =   1335
   End
   Begin VB.CommandButton cmdAnnuleren 
      Cancel          =   -1  'True
      Caption         =   "&Annuleren"
      Height          =   375
      Left            =   3000
      TabIndex        =   5
      Top             =   3960
      Width           =   1335
   End
   Begin MSComctlLib.ImageList imlColumnHeadings 
      Left            =   1080
      Top             =   3960
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      UseMaskColor    =   0   'False
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidFamilie.frx":030A
            Key             =   "picDown"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidFamilie.frx":0626
            Key             =   "picUp"
         EndProperty
      EndProperty
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   240
      Picture         =   "frmLidFamilie.frx":0942
      Top             =   210
      Width           =   480
   End
   Begin VB.Label lblTitel 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Familie beheer"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   612
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   4212
   End
   Begin VB.Line Line2 
      Visible         =   0   'False
      X1              =   120
      X2              =   4320
      Y1              =   4440
      Y2              =   4440
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   4320
      Y1              =   3840
      Y2              =   3840
   End
End
Attribute VB_Name = "frmLidFamilie"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mblnFamilyAdded As Boolean

Private Sub cmdAnnuleren_Click()
    Unload Me
End Sub

Private Function GetDependantMembers(lngFamilieID As Long) As String
Dim lstrMembers As String

    StartUsingRecordset DataEnvironment1.rsLid
    With DataEnvironment1.rsLid
        .Filter = "Groep_FamilieID=" + CStr(lngFamilieID)
        Do While Not .EOF
            lstrMembers = lstrMembers + vbTab + DataEnvironment1.rsLid("VoorNaam").value + " " + DataEnvironment1.rsLid("AchterNaam").value + vbCrLf
            .MoveNext
        Loop
        .Filter = ""
    End With
    
    If (Len(lstrMembers) > 0) Then
        lstrMembers = "Er zijn leden gevonden die naar deze familienaam verwijzen : " + vbCrLf + vbCrLf + lstrMembers + vbCrLf + vbCrLf
    End If
    
    GetDependantMembers = lstrMembers
    
End Function

Private Sub cmdFamilieVerwijderen_Click()
    If (lvwFamilie.SelectedItem Is Nothing) Then
        MsgBox "Om een familienaam te verwijderen moet je er eerst ��n aan duiden in de lijst.", vbInformation
    ElseIf (lvwFamilie.SelectedItem.text = "(Geen)") Then
        MsgBox "Je kan deze familienaam niet verwijderen.", vbExclamation
    Else
        With DataEnvironment1.rsFamilie
            If (Not (.EOF And .BOF)) Then
                .MoveFirst
                .Find "FamilieID=" + Mid$(lvwFamilie.SelectedItem.Key, 2)
                
                'Get a list of all dependant Members
                If (MsgBox(GetDependantMembers(CLng(Mid$(lvwFamilie.SelectedItem.Key, 2))) + "Ben je zeker dat je de familienaam " + lvwFamilie.SelectedItem.text + " wilt verwijderen?", vbYesNo) = vbYes) Then
                    If (Not .EOF) Then
                        .Delete
                        .Requery
                    End If
                End If
                .MoveFirst
            End If
        End With
        FillListView
    End If
End Sub

Private Sub cmdFamilieWijzigen_Click()
Dim lsNieuweFamilie As String
    
    If (lvwFamilie.SelectedItem Is Nothing) Then
        MsgBox "Om een familienaam te wijzigen moet je er eerst ��n aan duiden in de lijst.", vbInformation
    Else
        With DataEnvironment1.rsFamilie
            If (Not (.EOF And .BOF)) Then
                .MoveFirst
                .Find "FamilieID=" + Mid$(lvwFamilie.SelectedItem.Key, 2)
                If (Not .EOF) Then
                    lsNieuweFamilie = InputBox("Geef een nieuwe naam voor deze familie:  ", "Familie wijzigen", .Fields("FamilieNaam").value)
                    If Len(lsNieuweFamilie) = 0 Then
                        MsgBox "De familienaam kan niet leeg zijn"
                    Else
                        .Fields("FamilieNaam").value = lsNieuweFamilie
                        .Update
                        .Requery
                    End If
                End If
                .MoveFirst
            End If
        End With
        FillListView
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    EndUsingRecordset DataEnvironment1.rsFamilie
End Sub

Private Sub lvwFamilie_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    SortListViewColumn Me.lvwFamilie, ColumnHeader
End Sub

Private Sub cmdFamilieToevoegen_Click()
Dim lsNieuweFamilie As String

    lsNieuweFamilie = InputBox("Geef de naam van de nieuwe familie :  ", "Nieuwe familie")
    
    If (Len(lsNieuweFamilie) = 0) Then
        MsgBox "Geen familie toegevoegd.", vbExclamation, "Nieuwe familie"
    Else
        With DataEnvironment1.rsFamilie
            .AddNew
            .Fields("FamilieNaam").value = lsNieuweFamilie
            .Update
            .Requery
            mblnFamilyAdded = True
        End With
        FillListView
    End If
End Sub

Private Sub cmdOk_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    FillListView
End Sub

Private Sub FillListView()
Dim itmX As ListItem
   
    StartUsingRecordset DataEnvironment1.rsFamilie
    Set lvwFamilie.ColumnHeaderIcons = imlColumnHeadings
   
    With lvwFamilie
        .ListItems.Clear
        .Visible = False
        .View = lvwReport   ' Set view to Report.
        .BorderStyle = ccFixedSingle
    End With
    
    With DataEnvironment1.rsFamilie
        If (.EOF) Then .MoveFirst
        While Not .EOF
           Set itmX = lvwFamilie.ListItems.Add(, "K" + CStr(.Fields("FamilieID").value), .Fields("FamilieNaam").value)
           .MoveNext
        Wend
    End With
    
    With lvwFamilie
        .Visible = True
        .Refresh
    End With
End Sub


