VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "SSDW3BO.OCX"
Begin VB.Form frmBetalingen 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Betalingen"
   ClientHeight    =   8640
   ClientLeft      =   48
   ClientTop       =   336
   ClientWidth     =   13572
   Icon            =   "frmBetalingen.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8640
   ScaleWidth      =   13572
   Begin VB.CommandButton cmdOpenInExcel 
      Caption         =   "Open In &Excel"
      Height          =   372
      Left            =   3960
      TabIndex        =   9
      Top             =   8160
      Width           =   1332
   End
   Begin VB.CommandButton cmdAfdrukken 
      Caption         =   "Af&drukken"
      Height          =   375
      Left            =   5400
      TabIndex        =   2
      Top             =   8160
      Width           =   1335
   End
   Begin VB.CommandButton cmdVernieuwen 
      Caption         =   "&Vernieuwen"
      Height          =   375
      Left            =   6840
      TabIndex        =   3
      Top             =   8160
      Width           =   1335
   End
   Begin VB.CommandButton cmdToepassen 
      Caption         =   "&Toepassen"
      Enabled         =   0   'False
      Height          =   375
      Left            =   10680
      TabIndex        =   5
      Top             =   8160
      Width           =   1335
   End
   Begin VB.CommandButton cmdAnnuleren 
      Cancel          =   -1  'True
      Caption         =   "&Annuleren"
      Height          =   375
      Left            =   12120
      TabIndex        =   6
      Top             =   8160
      Width           =   1335
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Default         =   -1  'True
      Height          =   375
      Left            =   9240
      TabIndex        =   4
      Top             =   8160
      Width           =   1335
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid SSOleDBGrid1 
      Bindings        =   "frmBetalingen.frx":030A
      Height          =   5055
      Left            =   120
      TabIndex        =   0
      Top             =   840
      Width           =   13335
      _Version        =   196617
      DataMode        =   1
      CheckBox3D      =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      SelectTypeRow   =   1
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   402
      ExtraHeight     =   169
      Columns.Count   =   14
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "BetalingID"
      Columns(0).Name =   "BetalingID"
      Columns(0).Alignment=   1
      Columns(0).CaptionAlignment=   1
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   12632256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "LidID"
      Columns(1).Name =   "LidID"
      Columns(1).Alignment=   1
      Columns(1).CaptionAlignment=   1
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   3
      Columns(1).FieldLen=   256
      Columns(2).Width=   4445
      Columns(2).Caption=   "Naam"
      Columns(2).Name =   "Naam"
      Columns(2).CaptionAlignment=   0
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   130
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(2).Style=   1
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   12632256
      Columns(3).Width=   1905
      Columns(3).Caption=   "Bedrag"
      Columns(3).Name =   "Bedrag"
      Columns(3).CaptionAlignment=   0
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   3
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   12632256
      Columns(4).Width=   1588
      Columns(4).Caption=   "Januari"
      Columns(4).Name =   "Januari"
      Columns(4).Alignment=   1
      Columns(4).CaptionAlignment=   1
      Columns(4).AllowSizing=   0   'False
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   11
      Columns(4).FieldLen=   256
      Columns(4).Style=   2
      Columns(5).Width=   1588
      Columns(5).Caption=   "Februari"
      Columns(5).Name =   "Februari"
      Columns(5).Alignment=   1
      Columns(5).CaptionAlignment=   1
      Columns(5).AllowSizing=   0   'False
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   11
      Columns(5).FieldLen=   256
      Columns(5).Style=   2
      Columns(6).Width=   1588
      Columns(6).Caption=   "Maart"
      Columns(6).Name =   "Maart"
      Columns(6).Alignment=   1
      Columns(6).CaptionAlignment=   1
      Columns(6).AllowSizing=   0   'False
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   11
      Columns(6).FieldLen=   256
      Columns(6).Style=   2
      Columns(7).Width=   1588
      Columns(7).Caption=   "April"
      Columns(7).Name =   "April"
      Columns(7).Alignment=   1
      Columns(7).CaptionAlignment=   1
      Columns(7).AllowSizing=   0   'False
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   11
      Columns(7).FieldLen=   256
      Columns(7).Style=   2
      Columns(8).Width=   1588
      Columns(8).Caption=   "Mei"
      Columns(8).Name =   "Mei"
      Columns(8).Alignment=   1
      Columns(8).CaptionAlignment=   1
      Columns(8).AllowSizing=   0   'False
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   11
      Columns(8).FieldLen=   256
      Columns(8).Style=   2
      Columns(9).Width=   1588
      Columns(9).Caption=   "Juni"
      Columns(9).Name =   "Juni"
      Columns(9).Alignment=   1
      Columns(9).CaptionAlignment=   1
      Columns(9).AllowSizing=   0   'False
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   11
      Columns(9).FieldLen=   256
      Columns(9).Style=   2
      Columns(10).Width=   1588
      Columns(10).Caption=   "September"
      Columns(10).Name=   "September"
      Columns(10).Alignment=   1
      Columns(10).CaptionAlignment=   1
      Columns(10).AllowSizing=   0   'False
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   11
      Columns(10).FieldLen=   256
      Columns(10).Style=   2
      Columns(11).Width=   1588
      Columns(11).Caption=   "Oktober"
      Columns(11).Name=   "Oktober"
      Columns(11).Alignment=   1
      Columns(11).CaptionAlignment=   1
      Columns(11).AllowSizing=   0   'False
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   11
      Columns(11).FieldLen=   256
      Columns(11).Style=   2
      Columns(12).Width=   1588
      Columns(12).Caption=   "November"
      Columns(12).Name=   "November"
      Columns(12).Alignment=   1
      Columns(12).CaptionAlignment=   1
      Columns(12).AllowSizing=   0   'False
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   11
      Columns(12).FieldLen=   256
      Columns(12).Style=   2
      Columns(13).Width=   1588
      Columns(13).Caption=   "December"
      Columns(13).Name=   "December"
      Columns(13).Alignment=   1
      Columns(13).CaptionAlignment=   1
      Columns(13).AllowSizing=   0   'False
      Columns(13).DataField=   "Column 13"
      Columns(13).DataType=   11
      Columns(13).FieldLen=   256
      Columns(13).Style=   2
      _ExtentX        =   23521
      _ExtentY        =   8916
      _StockProps     =   79
      Caption         =   "Leden"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.4
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataMember      =   "tblLidBetalingBestaand"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid SSOleDBGrid2 
      Bindings        =   "frmBetalingen.frx":0329
      Height          =   1815
      Left            =   120
      TabIndex        =   1
      Top             =   5880
      Width           =   13335
      _Version        =   196617
      DataMode        =   1
      GroupHeaders    =   0   'False
      ColumnHeaders   =   0   'False
      CheckBox3D      =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   402
      ExtraHeight     =   191
      Columns.Count   =   14
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "BetalingID"
      Columns(0).Name =   "BetalingID"
      Columns(0).Alignment=   1
      Columns(0).CaptionAlignment=   1
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   12632256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "LidID"
      Columns(1).Name =   "LidID"
      Columns(1).Alignment=   1
      Columns(1).CaptionAlignment=   1
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   3
      Columns(1).FieldLen=   256
      Columns(2).Width=   4445
      Columns(2).Caption=   "Naam"
      Columns(2).Name =   "Naam"
      Columns(2).CaptionAlignment=   0
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   130
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(2).Style=   1
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   12632256
      Columns(3).Width=   1905
      Columns(3).Caption=   "Bedrag"
      Columns(3).Name =   "Bedrag"
      Columns(3).CaptionAlignment=   0
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   3
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   12632256
      Columns(4).Width=   1588
      Columns(4).Caption=   "Januari"
      Columns(4).Name =   "Januari"
      Columns(4).Alignment=   1
      Columns(4).CaptionAlignment=   1
      Columns(4).AllowSizing=   0   'False
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   11
      Columns(4).FieldLen=   256
      Columns(4).Style=   2
      Columns(5).Width=   1588
      Columns(5).Caption=   "Februari"
      Columns(5).Name =   "Februari"
      Columns(5).Alignment=   1
      Columns(5).CaptionAlignment=   1
      Columns(5).AllowSizing=   0   'False
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   11
      Columns(5).FieldLen=   256
      Columns(5).Style=   2
      Columns(6).Width=   1588
      Columns(6).Caption=   "Maart"
      Columns(6).Name =   "Maart"
      Columns(6).Alignment=   1
      Columns(6).CaptionAlignment=   1
      Columns(6).AllowSizing=   0   'False
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   11
      Columns(6).FieldLen=   256
      Columns(6).Style=   2
      Columns(7).Width=   1588
      Columns(7).Caption=   "April"
      Columns(7).Name =   "April"
      Columns(7).Alignment=   1
      Columns(7).CaptionAlignment=   1
      Columns(7).AllowSizing=   0   'False
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   11
      Columns(7).FieldLen=   256
      Columns(7).Style=   2
      Columns(8).Width=   1588
      Columns(8).Caption=   "Mei"
      Columns(8).Name =   "Mei"
      Columns(8).Alignment=   1
      Columns(8).CaptionAlignment=   1
      Columns(8).AllowSizing=   0   'False
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   11
      Columns(8).FieldLen=   256
      Columns(8).Style=   2
      Columns(9).Width=   1588
      Columns(9).Caption=   "Juni"
      Columns(9).Name =   "Juni"
      Columns(9).Alignment=   1
      Columns(9).CaptionAlignment=   1
      Columns(9).AllowSizing=   0   'False
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   11
      Columns(9).FieldLen=   256
      Columns(9).Style=   2
      Columns(10).Width=   1588
      Columns(10).Caption=   "September"
      Columns(10).Name=   "September"
      Columns(10).Alignment=   1
      Columns(10).CaptionAlignment=   1
      Columns(10).AllowSizing=   0   'False
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   11
      Columns(10).FieldLen=   256
      Columns(10).Style=   2
      Columns(11).Width=   1588
      Columns(11).Caption=   "Oktober"
      Columns(11).Name=   "Oktober"
      Columns(11).Alignment=   1
      Columns(11).CaptionAlignment=   1
      Columns(11).AllowSizing=   0   'False
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   11
      Columns(11).FieldLen=   256
      Columns(11).Style=   2
      Columns(12).Width=   1588
      Columns(12).Caption=   "November"
      Columns(12).Name=   "November"
      Columns(12).Alignment=   1
      Columns(12).CaptionAlignment=   1
      Columns(12).AllowSizing=   0   'False
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   11
      Columns(12).FieldLen=   256
      Columns(12).Style=   2
      Columns(13).Width=   1588
      Columns(13).Caption=   "December"
      Columns(13).Name=   "December"
      Columns(13).Alignment=   1
      Columns(13).CaptionAlignment=   1
      Columns(13).AllowSizing=   0   'False
      Columns(13).DataField=   "Column 13"
      Columns(13).DataType=   11
      Columns(13).FieldLen=   256
      Columns(13).Style=   2
      _ExtentX        =   23521
      _ExtentY        =   3201
      _StockProps     =   79
      Caption         =   "Nieuwe Leden"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.4
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataMember      =   "tblLidBetalingNieuw"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Image Image1 
      Height          =   384
      Left            =   240
      Picture         =   "frmBetalingen.frx":0348
      Top             =   204
      Width           =   384
   End
   Begin VB.Label lblMutatie 
      Caption         =   "Laatste mutatie : xx/xx/xxxx"
      Height          =   240
      Left            =   120
      TabIndex        =   8
      Top             =   7800
      Width           =   3735
   End
   Begin VB.Label lblTitel 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Betalingslijst"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   13335
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   13440
      Y1              =   8040
      Y2              =   8040
   End
End
Attribute VB_Name = "frmBetalingen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mblnDataChanged As Boolean
Private mCancelled As Boolean
Private mMode As kaFormMode
Private mobjExcelExt As clsExcelExtension
Private WithEvents mfrmStatus As frmStatus
Attribute mfrmStatus.VB_VarHelpID = -1

Public Property Let Mode(NewMode As kaFormMode)
    mMode = NewMode
    SetButtons mMode
    SetMenu mMode
End Property

Public Property Get Mode() As kaFormMode
    Mode = mMode
End Property

Private Sub cmdOpenInExcel_Click()
Dim lxlWorkSheet As Excel.Worksheet

    Set lxlWorkSheet = LaadDataInExcel
    
    'Show excel
    lxlWorkSheet.Application.Visible = True
End Sub

Private Sub mfrmStatus_Cancelled()
    mCancelled = True
End Sub

Public Property Let DataChanged(value As Boolean)
    mblnDataChanged = value
    cmdToepassen.Enabled = mblnDataChanged
End Property

Public Property Get DataChanged() As Boolean
    DataChanged = mblnDataChanged
End Property

Private Sub cmdAfdrukken_Click()
    Afdrukken
End Sub

Public Sub Afdrukken()
Dim lxlWorkSheet As Excel.Worksheet

On Error GoTo ErrorHandler
    
    Set lxlWorkSheet = LaadDataInExcel
    
    'Print
    If (Not lxlWorkSheet Is Nothing) Then
        lxlWorkSheet.PrintOut
    End If
    
    If mobjExcelExt.ExcelWasNotRunning Then lxlWorkSheet.Application.Quit
    
    Set lxlWorkSheet = Nothing
    
    Exit Sub
    
ErrorHandler:
    Select Case Err.Number
        Case cErrorUserrequestedCancel
            MsgBox "Afdrukken van de betalingslijst op aanvraag van de gebruiker gešnuleerd", vbInformation
            mCancelled = False
        Case Else
            MsgBox "Unspecified Error : " & Err.Description & "(" & Err.Number & " at " & Err.Source & ")"
    End Select

    If (Not mfrmStatus Is Nothing) Then
        Unload mfrmStatus
        Set mfrmStatus = Nothing
    End If
End Sub

Public Function LaadDataInExcel() As Excel.Worksheet
Dim lxlWorkSheet As Excel.Worksheet
Dim Fields() As Variant
Dim lvntLedenBookmark As Variant, lvntNieuweLedenBookmark As Variant
Dim llngRowCounter As Long, llngColumnCounter As Long, llngVisibleColumnsCounter As Long, llngVisibleColumns As Long, llngColumns As Long, llngRows As Long, llngColumnCount As Long
Dim Range_Cell1 As Variant, Range_Cell2 As Variant
Dim PreviousDay As Integer, lintWeekDay As Integer

On Error GoTo ErrorHandler

    Mode = kaFormVerwerking
    Me.Hide
    WaitAWhile 2

    'Display message
    Set mfrmStatus = New frmStatus
    mfrmStatus.lblText = "Bezig met het verzamelen van de gegevens"
    mfrmStatus.OpenForm kaMetProgress
    
    lvntLedenBookmark = SSOleDBGrid1.Bookmark
    lvntNieuweLedenBookmark = SSOleDBGrid2.Bookmark
    
    'Clear Clipboard
    Clipboard.Clear
    
    'Get the number of visible columns
    llngColumns = SSOleDBGrid1.Columns.Count - 1
    For llngColumnCounter = 0 To llngColumns
        If (SSOleDBGrid1.Columns(llngColumnCounter).Visible) Then
            llngVisibleColumns = llngVisibleColumns + 1
        End If
        DoEvents
        If (mCancelled) Then Err.Raise cErrorUserrequestedCancel
        mfrmStatus.prgMain = (llngColumnCounter / llngColumns) * 10
    Next llngColumnCounter
    
    SSOleDBGrid1.MoveLast
    SSOleDBGrid2.MoveLast
    llngRows = (SSOleDBGrid1.Rows - 1) + (SSOleDBGrid2.Rows - 1) + 10
    ReDim Fields(1 To llngRows + 2, 1 To llngVisibleColumns)
    
    'Copy Header Row
    For llngColumnCounter = 0 To llngColumns
        If (SSOleDBGrid1.Columns(llngColumnCounter).Visible) Then
            llngVisibleColumnsCounter = llngVisibleColumnsCounter + 1
            Fields(1, llngVisibleColumnsCounter) = SSOleDBGrid1.Columns(llngColumnCounter).Caption
        End If
        DoEvents
        If (mCancelled) Then Err.Raise cErrorUserrequestedCancel
        mfrmStatus.prgMain = (llngColumnCounter / llngColumns) * 10
    Next llngColumnCounter
    
    'Copy rows (Existing Members)
    SSOleDBGrid1.MoveFirst
    llngRowCounter = 0
    For llngRowCounter = 0 To SSOleDBGrid1.Rows - 1
        llngVisibleColumnsCounter = 0
        For llngColumnCounter = 0 To llngColumns
            If (SSOleDBGrid1.Columns(llngColumnCounter).Visible) Then
                If (Len(SSOleDBGrid1.Columns(llngColumnCounter).Text) = 1) Then
                    Fields(llngRowCounter + 2, llngVisibleColumnsCounter + 1) = ""
                ElseIf (Len(SSOleDBGrid1.Columns(llngColumnCounter).Text) = 2) Then
                    Fields(llngRowCounter + 2, llngVisibleColumnsCounter + 1) = "X"
                Else
                    Fields(llngRowCounter + 2, llngVisibleColumnsCounter + 1) = SSOleDBGrid1.Columns(llngColumnCounter).Text
                End If
                llngVisibleColumnsCounter = llngVisibleColumnsCounter + 1
            End If
        Next llngColumnCounter
        DoEvents
        If (mCancelled) Then Err.Raise cErrorUserrequestedCancel
        mfrmStatus.prgMain = ((llngRowCounter / llngRows) * 75) + 25
        SSOleDBGrid1.MoveNext
    Next llngRowCounter

    Fields(llngRowCounter + 4, 1) = "NIEUWE LEDEN"
    
    'Copy rows (New Members)
    SSOleDBGrid2.MoveFirst
    For llngRowCounter = 0 To SSOleDBGrid2.Rows - 1
        llngVisibleColumnsCounter = 0
        For llngColumnCounter = 0 To llngColumns
            If (SSOleDBGrid2.Columns(llngColumnCounter).Visible) Then
                If (Len(SSOleDBGrid2.Columns(llngColumnCounter).Text) = 1) Then
                    Fields(llngRowCounter + SSOleDBGrid1.Rows + 6, llngVisibleColumnsCounter + 1) = ""
                ElseIf (Len(SSOleDBGrid2.Columns(llngColumnCounter).Text) = 2) Then
                    Fields(llngRowCounter + SSOleDBGrid1.Rows + 6, llngVisibleColumnsCounter + 1) = "X"
                Else
                    Fields(llngRowCounter + SSOleDBGrid1.Rows + 6, llngVisibleColumnsCounter + 1) = SSOleDBGrid2.Columns(llngColumnCounter).Text
                End If
                llngVisibleColumnsCounter = llngVisibleColumnsCounter + 1
            End If
        Next llngColumnCounter
        DoEvents
        SSOleDBGrid2.MoveNext
    Next llngRowCounter

    Set lxlWorkSheet = mobjExcelExt.DisplayDataInSheet(Fields, Range_Cell1, Range_Cell2, Me.Caption, Me.lblMutatie, True)
        
    mfrmStatus.lblText = "Bezig met het openen van Excel"
    WaitAWhile 5
    mfrmStatus.ZOrder
    mfrmStatus.prgMain = 10

    If (lxlWorkSheet Is Nothing) Then Err.Raise cErrorUserrequestedCancel

    mfrmStatus.prgMain = 20

    'Put Headingformat
    lxlWorkSheet.Range("C3", Mid$(Range_Cell2, 1, 1) & "3").NumberFormat = "00"

    mfrmStatus.prgMain = 30

    'Autofit columns
    For llngColumnCount = 1 To llngColumns
        lxlWorkSheet.Columns(llngColumnCount).AutoFit
        mfrmStatus.prgMain = CLng((llngColumnCount / llngColumns) * 30)
    Next llngColumnCount

    mfrmStatus.prgMain = 60

    'Display Raster
    With lxlWorkSheet.Range("A4", Range_Cell2)
        With .Borders(xlEdgeLeft)
            .LineStyle = xlContinuous
            .Weight = xlMedium
            .ColorIndex = 1
        End With
        With .Borders(xlEdgeTop)
            .LineStyle = xlContinuous
            .Weight = xlMedium
            .ColorIndex = 1
        End With
        With .Borders(xlEdgeBottom)
            .LineStyle = xlContinuous
            .Weight = xlMedium
            .ColorIndex = 1
        End With
        With .Borders(xlEdgeRight)
            .LineStyle = xlContinuous
            .Weight = xlMedium
            .ColorIndex = 1
        End With
        With .Borders(xlInsideVertical)
            .LineStyle = xlContinuous
            .Weight = xlThin
            .ColorIndex = xlAutomatic
        End With
        With .Borders(xlInsideHorizontal)
            .LineStyle = xlContinuous
            .Weight = xlThin
            .ColorIndex = xlAutomatic
        End With
    End With

    mfrmStatus.prgMain = 70

    'PageSetup
    With lxlWorkSheet.PageSetup
        .LeftMargin = Application.InchesToPoints(0.196850393700787)
        mfrmStatus.prgMain = 75
        .RightMargin = Application.InchesToPoints(0.196850393700787)
'        .TopMargin = Application.InchesToPoints(0.984251968503937)
'        .BottomMargin = Application.InchesToPoints(0.984251968503937)
'        .HeaderMargin = Application.InchesToPoints(0.511811023622047)
'        .FooterMargin = Application.InchesToPoints(0.511811023622047)
    End With

    mfrmStatus.prgMain = 80

    'Center Data
'    With lxlWorkSheet.Range("C4", Range_Cell2)
'        .HorizontalAlignment = xlCenter
'        .VerticalAlignment = xlBottom
'    End With

    mfrmStatus.prgMain = 95

    Unload mfrmStatus
    Set mfrmStatus = Nothing
   
    Me.Show
    
    SSOleDBGrid1.Bookmark = lvntLedenBookmark
    SSOleDBGrid2.Bookmark = lvntNieuweLedenBookmark
    
    Mode = kaFormOpen
    
    Set LaadDataInExcel = lxlWorkSheet
    
    Exit Function
    
ErrorHandler:
    Set LaadDataInExcel = Nothing
    Unload Me
    
    If (Not mfrmStatus Is Nothing) Then
        Unload mfrmStatus
        Set mfrmStatus = Nothing
    End If
    
    Select Case Err.Number
        Case cErrorUserrequestedCancel
            Err.Raise cErrorUserrequestedCancel
        Case Else
            MsgBox "Unspecified Error : " & Err.Description & "(" & Err.Number & " at " & Err.Source & ")"
    End Select
End Function

Private Sub cmdAnnuleren_Click()
    Unload Me
End Sub

Private Sub cmdOK_Click()
    cmdToepassen_Click
    Unload Me
End Sub

Public Sub Vernieuwen()
    Me.MousePointer = vbHourglass
    Mode = kaFormVerversen
    SSOleDBGrid1.Rebind
    SSOleDBGrid2.Rebind
    Mode = kaFormOpen
    Me.MousePointer = vbNormal
End Sub

Private Sub cmdVernieuwen_Click()
    Vernieuwen
End Sub

Private Sub cmdToepassen_Click()
On Error GoTo ErrorHandler

    If (DataChanged) Then
        
        Mode = kaFormVerwerking
        
        'Commit changes
        SSOleDBGrid1.Update
        
        'Reset dirty flag
        DataChanged = False
        
        'Update mutation date
        StartUsingRecordset DataEnvironment1.rsMutatie
        DataEnvironment1.rsMutatie("MutatieDatumBetaling").value = Format(Now(), mcSysDateFormat)
        DataEnvironment1.rsMutatie.Update
        SetMutationDate Now()
    
        Mode = kaFormOpen
    End If
    
    Exit Sub

ErrorHandler:
    MsgBox Err.Description, vbExclamation
End Sub

Private Sub SetMutationDate(Optional vntMutatieDatum As Variant)
Dim lvMutatieDatum As Variant

    'Fill mutation date
    If (IsMissing(vntMutatieDatum)) Then
        StartUsingRecordset DataEnvironment1.rsMutatie
        lvMutatieDatum = DataEnvironment1.rsMutatie("MutatieDatumBetaling").value
    Else
        lvMutatieDatum = vntMutatieDatum
    End If
    
    If (IsDate(lvMutatieDatum)) Then
        lblMutatie.Caption = "Laatste wijziging : " + Format(lvMutatieDatum, mcSysDateFormatDisplay)
    Else
        lblMutatie.Caption = "Nog geen wijzigingen gedaan."
    End If
End Sub

Public Sub OpenForm(lngJaar As Integer)
'    Mode = kaFormLaden
    DataEnvironment1.rstblLidBetaling.Filter = "Jaar=" + CStr(lngJaar)
    DataEnvironment1.rstblLidBetalingNieuw.Filter = "Jaar=" + CStr(lngJaar)
    DataEnvironment1.rstblLidBetalingBestaand.Filter = "Jaar=" + CStr(lngJaar)
    
    Me.Caption = "Betalingslijst : " + CStr(lngJaar)
    lblTitel.Caption = vbCrLf + Me.Caption
    SetMutationDate
    Me.Left = 100
    Me.Top = 100
    Me.Show
    Me.ZOrder
    Mode = kaFormOpen
End Sub

Private Sub Form_Load()
    Mode = kaFormLaden
    Set mobjExcelExt = New clsExcelExtension

    If (mAppMode = kaSimpel Or mAppMode = kaNormaal) Then
        cmdAnnuleren.Caption = "&Sluiten"
        cmdOK.Visible = False
        cmdToepassen.Visible = False
        cmdOpenInExcel.Visible = False
        DisableGridEdit
    End If

    InitGridLayout SSOleDBGrid1
    Mode = kaFormOpen
End Sub

Private Sub DisableGridEdit()
Dim ssColumn As SSDataWidgets_B_OLEDB.Column
    For Each ssColumn In SSOleDBGrid1.Columns
        ssColumn.Locked = True
        ssColumn.BackColor = RGB(192, 192, 192)
    Next ssColumn
    For Each ssColumn In SSOleDBGrid2.Columns
        ssColumn.Locked = True
        ssColumn.BackColor = RGB(192, 192, 192)
    Next ssColumn
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Mode = kaFormOntLaden
    With DataEnvironment1
        .rstblLidBetaling.Filter = ""
        .rstblLidBetalingNieuw.Filter = ""
        .rstblLidBetalingBestaand.Filter = ""
    End With
    Set mfrmBetalingen = Nothing
    Set mobjExcelExt = Nothing
End Sub

Private Sub SSOleDBGrid1_BtnClick()
    Mode = kaFormVerwerking
    Set mfrmLidDetail = New frmLidDetail
    mfrmLidDetail.OpenForm SSOleDBGrid1.Columns("LidID").value
    mfrmLidDetail.Show vbModal
    cmdVernieuwen_Click
    Mode = kaFormOpen
End Sub

Private Sub SSOleDBGrid2_BtnClick()
    Mode = kaFormVerwerking
    Set mfrmLidDetail = New frmLidDetail
    mfrmLidDetail.OpenForm SSOleDBGrid2.Columns("LidID").value
    mfrmLidDetail.Show vbModal
    cmdVernieuwen_Click
    Mode = kaFormOpen
End Sub

Private Sub SSOleDBGrid1_Change()
    DataChanged = True
End Sub

Private Sub SSOleDBGrid2_Change()
    DataChanged = True
End Sub

Private Sub UnboundPositionData(rs As ADODB.Recordset, StartLocation As Variant, ByVal NumberOfRowsToMove As Long, NewLocation As Variant)
    With rs
        If (IsNull(StartLocation)) Then
            If (NumberOfRowsToMove = 0) Then
                Exit Sub
            ElseIf (NumberOfRowsToMove < 0) Then
                .MoveLast
            Else
                .MoveFirst
            End If
        Else
            .Bookmark = StartLocation
        End If
        
        .Move NumberOfRowsToMove
        
        NewLocation = .Bookmark
    End With
End Sub

Private Sub UnboundReadData(dbGrid As SSDataWidgets_B_OLEDB.SSOleDBGrid, rs As ADODB.Recordset, ByVal RowBuf As SSDataWidgets_B_OLEDB.ssRowBuffer, StartLocation As Variant, ByVal ReadPriorRows As Boolean)
Dim lintRBRow As Integer
Dim lintGridRows As Integer

    lintGridRows = 0
    
    With rs
        StartUsingRecordset rs, False, False
        
        If (.BOF And .EOF) Then
            dbGrid.AllowAddNew = False
            Exit Sub
        End If
        
        If (IsNull(StartLocation)) Then
            If (ReadPriorRows) Then
                .MoveLast
            Else
                .MoveFirst
            End If
        Else
            .Bookmark = StartLocation
            If (ReadPriorRows) Then
                .MovePrevious
            Else
                .MoveNext
            End If
        End If
    
        For lintRBRow = 0 To RowBuf.RowCount - 1
            If (.BOF Or .EOF) Then
                dbGrid.AllowAddNew = False
                Exit For
            End If
            Select Case RowBuf.ReadType
                Case ssReadTypeAllData
                    RowBuf.Bookmark(lintRBRow) = .Bookmark
                    
                    RowBuf.value(lintRBRow, kaBetalingsKolom.kaBetalingID) = .Fields("BetalingID").value
                    RowBuf.value(lintRBRow, kaBetalingsKolom.kaLidID) = .Fields("LidID").value
                    RowBuf.value(lintRBRow, kaBetalingsKolom.kaNaam) = .Fields("Naam").value
                    RowBuf.value(lintRBRow, kaBetalingsKolom.kaBedrag) = .Fields("Bedrag").value
                    RowBuf.value(lintRBRow, kaBetalingsKolom.kaJanuari) = .Fields("Januari").value
                    RowBuf.value(lintRBRow, kaBetalingsKolom.kaFebruari) = .Fields("Februari").value
                    RowBuf.value(lintRBRow, kaBetalingsKolom.kaMaart) = .Fields("Maart").value
                    RowBuf.value(lintRBRow, kaBetalingsKolom.kaApril) = .Fields("April").value
                    RowBuf.value(lintRBRow, kaBetalingsKolom.kaMei) = .Fields("Mei").value
                    RowBuf.value(lintRBRow, kaBetalingsKolom.kaJuni) = .Fields("Juni").value
                    RowBuf.value(lintRBRow, kaBetalingsKolom.kaSeptember) = .Fields("September").value
                    RowBuf.value(lintRBRow, kaBetalingsKolom.kaOktober) = .Fields("Oktober").value
'                    RowBuf.value(lintRBRow, kaBetalingsKolom.kaNovember) = .Fields("November").value
'                    RowBuf.value(lintRBRow, kaBetalingsKolom.kaDecember) = .Fields("December").value
                Case ssReadTypeBookmarkOnly
                    RowBuf.Bookmark(lintRBRow) = .Bookmark
            End Select
            
            If (ReadPriorRows) Then
                .MovePrevious
            Else
                .MoveNext
            End If
            
            lintGridRows = lintGridRows + 1
        Next lintRBRow
        
        RowBuf.RowCount = lintGridRows
    End With
End Sub

Private Sub UnboundWriteData(dbGrid As SSDataWidgets_B_OLEDB.SSOleDBGrid, rs As ADODB.Recordset, ByVal RowBuf As SSDataWidgets_B_OLEDB.ssRowBuffer, WriteLocation As Variant)
Dim lintWeekCount As Integer

    With rs
        .Bookmark = WriteLocation
        .Fields("Januari").value = dbGrid.Columns("Januari").value
        .Fields("Februari").value = dbGrid.Columns("Februari").value
        .Fields("Maart").value = dbGrid.Columns("Maart").value
        .Fields("April").value = dbGrid.Columns("April").value
        .Fields("Mei").value = dbGrid.Columns("Mei").value
        .Fields("Juni").value = dbGrid.Columns("Juni").value
        .Fields("September").value = dbGrid.Columns("September").value
        .Fields("Oktober").value = dbGrid.Columns("Oktober").value
        .Fields("November").value = dbGrid.Columns("November").value
        .Fields("December").value = dbGrid.Columns("December").value
        .Update
    End With
End Sub

Private Sub SSOleDBGrid1_UnboundPositionData(StartLocation As Variant, ByVal NumberOfRowsToMove As Long, NewLocation As Variant)
    UnboundPositionData DataEnvironment1.rstblLidBetalingBestaand, StartLocation, NumberOfRowsToMove, NewLocation
End Sub

Private Sub SSOleDBGrid1_UnboundReadData(ByVal RowBuf As SSDataWidgets_B_OLEDB.ssRowBuffer, StartLocation As Variant, ByVal ReadPriorRows As Boolean)
    UnboundReadData SSOleDBGrid1, DataEnvironment1.rstblLidBetalingBestaand, RowBuf, StartLocation, ReadPriorRows
End Sub

Private Sub SSOleDBGrid1_UnboundWriteData(ByVal RowBuf As SSDataWidgets_B_OLEDB.ssRowBuffer, WriteLocation As Variant)
    UnboundWriteData SSOleDBGrid1, DataEnvironment1.rstblLidBetalingBestaand, RowBuf, WriteLocation
End Sub

Private Sub SSOleDBGrid2_UnboundPositionData(StartLocation As Variant, ByVal NumberOfRowsToMove As Long, NewLocation As Variant)
    UnboundPositionData DataEnvironment1.rstblLidBetalingNieuw, StartLocation, NumberOfRowsToMove, NewLocation
End Sub

Private Sub SSOleDBGrid2_UnboundReadData(ByVal RowBuf As SSDataWidgets_B_OLEDB.ssRowBuffer, StartLocation As Variant, ByVal ReadPriorRows As Boolean)
    UnboundReadData SSOleDBGrid2, DataEnvironment1.rstblLidBetalingNieuw, RowBuf, StartLocation, ReadPriorRows
End Sub

Private Sub SSOleDBGrid2_UnboundWriteData(ByVal RowBuf As SSDataWidgets_B_OLEDB.ssRowBuffer, WriteLocation As Variant)
    UnboundWriteData SSOleDBGrid2, DataEnvironment1.rstblLidBetalingNieuw, RowBuf, WriteLocation
End Sub

Public Sub SetButtons(Mode As kaFormMode)
    Select Case Mode
        Case kaFormMode.kaFormLaden, kaFormMode.kaFormOntLaden
            Me.cmdAfdrukken.Enabled = False
            Me.cmdAnnuleren.Enabled = False
            Me.cmdOK.Enabled = False
            Me.cmdToepassen.Enabled = False
            Me.cmdVernieuwen.Enabled = False
        Case kaFormMode.kaFormOpen
            Me.cmdAfdrukken.Enabled = True
            Me.cmdAnnuleren.Enabled = True
            Me.cmdOK.Enabled = True
            Me.cmdVernieuwen.Enabled = True
        Case kaFormMode.kaFormVerversen, kaFormMode.kaFormVerwerking
            Me.cmdAfdrukken.Enabled = False
            Me.cmdAnnuleren.Enabled = True
            Me.cmdOK.Enabled = False
            Me.cmdVernieuwen.Enabled = False
    End Select
    DoEvents
End Sub

Public Sub SetMenu(Mode As kaFormMode)
    Select Case Mode
        Case kaFormMode.kaFormLaden
            mfrmMDIMain.mnuBetLijst.Enabled = False
            mfrmMDIMain.mnuBetVernieuwen.Enabled = False
            mfrmMDIMain.mnuBetAfdrukken.Enabled = False
            mfrmMDIMain.mnuBetalingen.Enabled = False
        Case kaFormMode.kaFormOpen
            mfrmMDIMain.mnuBetLijst.Enabled = False
            mfrmMDIMain.mnuBetVernieuwen.Enabled = True
            mfrmMDIMain.mnuBetAfdrukken.Enabled = True
            mfrmMDIMain.mnuBetalingen.Enabled = True
        Case kaFormMode.kaFormVerversen, kaFormMode.kaFormVerwerking
            mfrmMDIMain.mnuBetLijst.Enabled = False
            mfrmMDIMain.mnuBetVernieuwen.Enabled = False
            mfrmMDIMain.mnuBetAfdrukken.Enabled = False
            mfrmMDIMain.mnuBetalingen.Enabled = True
        Case kaFormMode.kaFormOntLaden
            mfrmMDIMain.mnuBetLijst.Enabled = True
            mfrmMDIMain.mnuBetVernieuwen.Enabled = False
            mfrmMDIMain.mnuBetAfdrukken.Enabled = False
            mfrmMDIMain.mnuBetalingen.Enabled = True
    End Select
    DoEvents
End Sub

