VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmOptions 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Instellingen"
   ClientHeight    =   4920
   ClientLeft      =   2565
   ClientTop       =   1500
   ClientWidth     =   6150
   Icon            =   "frmOptions.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4920
   ScaleWidth      =   6150
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab SSTab1 
      Height          =   3375
      Left            =   120
      TabIndex        =   9
      Top             =   840
      Width           =   5895
      _ExtentX        =   10398
      _ExtentY        =   5953
      _Version        =   393216
      Tab             =   1
      TabHeight       =   520
      TabCaption(0)   =   "Algemeen"
      TabPicture(0)   =   "frmOptions.frx":030A
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "fraApplicatie"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Lijsten"
      TabPicture(1)   =   "frmOptions.frx":0326
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Frame1"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Examen"
      TabPicture(2)   =   "frmOptions.frx":0342
      Tab(2).ControlEnabled=   0   'False
      Tab(2).ControlCount=   0
      Begin VB.Frame Frame1 
         Caption         =   "Excel Settings"
         Height          =   2895
         Left            =   120
         TabIndex        =   14
         Top             =   360
         Width           =   5655
         Begin VB.TextBox txtRowHeight 
            Height          =   285
            Left            =   2040
            TabIndex        =   18
            Top             =   840
            Width           =   1335
         End
         Begin VB.TextBox txtNrRowsPerSheet 
            Height          =   285
            Left            =   2040
            TabIndex        =   17
            Top             =   480
            Width           =   1335
         End
         Begin VB.Label Label2 
            Caption         =   "Rijhoogte:"
            Height          =   255
            Left            =   480
            TabIndex        =   16
            Top             =   840
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "Aantal rijen/blad:"
            Height          =   255
            Left            =   480
            TabIndex        =   15
            Top             =   480
            Width           =   1575
         End
      End
      Begin VB.Frame fraApplicatie 
         Caption         =   "Applicatie"
         Height          =   2895
         Left            =   -74880
         TabIndex        =   11
         Top             =   360
         Width           =   5655
         Begin VB.ComboBox cboAppMode 
            Height          =   315
            ItemData        =   "frmOptions.frx":035E
            Left            =   960
            List            =   "frmOptions.frx":036B
            Style           =   2  'Dropdown List
            TabIndex        =   12
            Top             =   360
            Width           =   2535
         End
         Begin VB.Label lblMode 
            Caption         =   "Mode:"
            Height          =   255
            Left            =   240
            TabIndex        =   13
            Top             =   360
            Width           =   735
         End
      End
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      Height          =   3780
      Index           =   3
      Left            =   -20000
      ScaleHeight     =   3780
      ScaleWidth      =   5685
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraSample4 
         Caption         =   "Sample 4"
         Height          =   1785
         Left            =   2100
         TabIndex        =   8
         Top             =   840
         Width           =   2055
      End
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      Height          =   3780
      Index           =   2
      Left            =   -20000
      ScaleHeight     =   3780
      ScaleWidth      =   5685
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraSample3 
         Caption         =   "Sample 3"
         Height          =   1785
         Left            =   1545
         TabIndex        =   7
         Top             =   675
         Width           =   2055
      End
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      Height          =   3780
      Index           =   1
      Left            =   -20000
      ScaleHeight     =   3780
      ScaleWidth      =   5685
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraSample2 
         Caption         =   "Sample 2"
         Height          =   1785
         Left            =   645
         TabIndex        =   6
         Top             =   300
         Width           =   2055
      End
   End
   Begin VB.CommandButton cmdToepassen 
      Caption         =   "&Toepassen"
      Enabled         =   0   'False
      Height          =   375
      Left            =   3240
      TabIndex        =   2
      Top             =   4440
      Width           =   1335
   End
   Begin VB.CommandButton cmdAnnuleren 
      Cancel          =   -1  'True
      Caption         =   "&Annuleren"
      Height          =   375
      Left            =   4680
      TabIndex        =   1
      Top             =   4440
      Width           =   1335
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "&OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   1800
      TabIndex        =   0
      Top             =   4440
      Width           =   1335
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   6000
      Y1              =   4320
      Y2              =   4320
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   240
      Picture         =   "frmOptions.frx":038C
      Top             =   195
      Width           =   480
   End
   Begin VB.Label lblTitel 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Instellingen"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   120
      TabIndex        =   10
      Top             =   120
      Width           =   5775
   End
End
Attribute VB_Name = "frmOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private mblnDataChanged As Boolean

Public Property Let DataChanged(value As Boolean)
    mblnDataChanged = value
    cmdToepassen.Enabled = mblnDataChanged
End Property

Public Property Get DataChanged() As Boolean
    DataChanged = mblnDataChanged
End Property

Private Sub cboAppMode_Click()
    DataChanged = True
    mAppMode = cboAppMode.ListIndex
End Sub

Private Sub cmdAnnuleren_Click()
    Unload Me
End Sub

Private Sub cmdOK_Click()
    cmdToepassen_Click
    Unload Me
End Sub

Private Sub cmdToepassen_Click()
    'toepassen
    If (DataChanged) Then
        SetAppOptions
        cmdToepassen.Enabled = False
        mfrmMDIMain.FillListItems
        mfrmMDIMain.FillMenu
        DataChanged = False
    End If
End Sub

Private Sub Form_Load()
    cboAppMode.ListIndex = mAppMode
    txtNrRowsPerSheet = mLinesPerPage
    txtRowHeight = mRowHeight
    cmdToepassen.Enabled = False
End Sub

Private Sub txtNrRowsPerSheet_Change()
    DataChanged = True
    If (IsNumeric(txtNrRowsPerSheet)) Then mLinesPerPage = CInt(txtNrRowsPerSheet)
End Sub

Private Sub txtRowHeight_Change()
    DataChanged = True
    If (IsNumeric(txtRowHeight)) Then mRowHeight = CDbl(txtRowHeight)
End Sub
