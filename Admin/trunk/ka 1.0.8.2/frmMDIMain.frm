VERSION 5.00
Object = "{15138B51-7EB6-11D0-9BB7-0000C0F04C96}#1.0#0"; "SSLstBar.ocx"
Begin VB.MDIForm frmMDIMain 
   BackColor       =   &H8000000C&
   Caption         =   "Kaisho Administratie"
   ClientHeight    =   8265
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   12600
   Icon            =   "frmMDIMain.frx":0000
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin Listbar.SSListBar SSListBar1 
      Align           =   3  'Align Left
      Height          =   8265
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   1125
      _ExtentX        =   1984
      _ExtentY        =   14579
      _Version        =   65537
      BackColor       =   8421376
      OLEDragMode     =   1
      OLEDropMode     =   2
      IconsLargeCount =   8
      IconsSmallCount =   8
      Image(1).Index  =   1
      Image(1).Picture=   "frmMDIMain.frx":030A
      Image(1).Key    =   "picLargeAanwezigheidslijst"
      Image(2).Index  =   2
      Image(2).Picture=   "frmMDIMain.frx":0626
      Image(2).Key    =   "picLargeBetalingslijst"
      Image(3).Index  =   3
      Image(3).Picture=   "frmMDIMain.frx":0942
      Image(3).Key    =   "picLargeLedenlijst"
      Image(4).Index  =   4
      Image(4).Picture=   "frmMDIMain.frx":0C5E
      Image(4).Key    =   "picLargeStatistieken"
      Image(5).Index  =   5
      Image(5).Picture=   "frmMDIMain.frx":0F7A
      Image(5).Key    =   "picLargeExamen"
      Image(6).Index  =   6
      Image(6).Picture=   "frmMDIMain.frx":1296
      Image(6).Key    =   "picLargeRapporten"
      Image(7).Index  =   7
      Image(7).Picture=   "frmMDIMain.frx":15B2
      Image(7).Key    =   "picLargeSluiten"
      Image(8).Index  =   8
      Image(8).Picture=   "frmMDIMain.frx":18CC
      Image(8).Key    =   "picLargeFamilie"
      SmallImage(1).Index=   1
      SmallImage(1).Picture=   "frmMDIMain.frx":1BE6
      SmallImage(1).Key=   "picSmallAanwezigheidslijst"
      SmallImage(2).Index=   2
      SmallImage(2).Picture=   "frmMDIMain.frx":1F02
      SmallImage(2).Key=   "picSmallBetalingslijst"
      SmallImage(3).Index=   3
      SmallImage(3).Picture=   "frmMDIMain.frx":221E
      SmallImage(3).Key=   "picSmallLedenlijst"
      SmallImage(4).Index=   4
      SmallImage(4).Picture=   "frmMDIMain.frx":253A
      SmallImage(4).Key=   "picSmallStatistieken"
      SmallImage(5).Index=   5
      SmallImage(5).Picture=   "frmMDIMain.frx":2856
      SmallImage(5).Key=   "picSmallExamen"
      SmallImage(6).Index=   6
      SmallImage(6).Picture=   "frmMDIMain.frx":2B72
      SmallImage(6).Key=   "picSmallRapporten"
      SmallImage(7).Index=   7
      SmallImage(7).Picture=   "frmMDIMain.frx":2E8E
      SmallImage(7).Key=   "picSmallSluiten"
      SmallImage(8).Index=   8
      SmallImage(8).Picture=   "frmMDIMain.frx":31A8
      SmallImage(8).Key=   "picSmallFamilie"
      Groups(1).BackColor=   8421376
      Groups(1).CurrentGroup=   -1  'True
      Groups(1).Caption=   "Snelkoppelingen"
   End
   Begin VB.Menu mnuSchermen 
      Caption         =   "&Schermen"
      Begin VB.Menu mnuAanwezigheid 
         Caption         =   "&Aanwezigheden"
         Begin VB.Menu mnuAanLijst 
            Caption         =   "&Lijst"
         End
         Begin VB.Menu mnuAanSep 
            Caption         =   "-"
            Index           =   0
         End
         Begin VB.Menu mnuAanVernieuwen 
            Caption         =   "&Vernieuwen"
            Enabled         =   0   'False
         End
         Begin VB.Menu mnuAanAfdrukken 
            Caption         =   "Af&drukken"
            Enabled         =   0   'False
         End
      End
      Begin VB.Menu mnuBetalingen 
         Caption         =   "&Betalingen"
         Begin VB.Menu mnuBetLijst 
            Caption         =   "&Lijst"
         End
         Begin VB.Menu mnuBetSep 
            Caption         =   "-"
            Index           =   0
         End
         Begin VB.Menu mnuBetVernieuwen 
            Caption         =   "&Vernieuwen"
            Enabled         =   0   'False
         End
         Begin VB.Menu mnuBetAfdrukken 
            Caption         =   "&Afdrukken"
            Enabled         =   0   'False
         End
      End
      Begin VB.Menu mnuLeden 
         Caption         =   "&Leden"
         Begin VB.Menu mnuFamilie 
            Caption         =   "&Familie"
         End
         Begin VB.Menu mnuLidLijst 
            Caption         =   "&Lijst"
         End
         Begin VB.Menu mnuLidSep0 
            Caption         =   "-"
         End
         Begin VB.Menu mnuLidNieuw 
            Caption         =   "&Nieuw Lid"
         End
         Begin VB.Menu mnuLidSep1 
            Caption         =   "-"
            Visible         =   0   'False
         End
         Begin VB.Menu mnuLidToevoegenAanLijsten 
            Caption         =   "Lid &toevoegen aan de lijsten ..."
            Visible         =   0   'False
         End
      End
      Begin VB.Menu mnuExamen 
         Caption         =   "&Examen"
         Begin VB.Menu mnuExaVoor 
            Caption         =   "&Voor het Examen..."
         End
         Begin VB.Menu mnuExaNa 
            Caption         =   "&Na het Examen ..."
         End
      End
      Begin VB.Menu mnuRapStat 
         Caption         =   "&Statistieken"
         Begin VB.Menu mnuRSManVrouw 
            Caption         =   "&Man/Vrouw verhoudingen"
         End
         Begin VB.Menu mnuRSGraden 
            Caption         =   "&Verhouding Graden"
         End
         Begin VB.Menu mnuRSLedenVerhouding 
            Caption         =   "Verhoudingen van de &leden"
         End
      End
      Begin VB.Menu mnulidsep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSluiten 
         Caption         =   "Sl&uiten"
      End
   End
   Begin VB.Menu mnuOpties 
      Caption         =   "&Opties"
      Begin VB.Menu mnuInstellingen 
         Caption         =   "&Instellingen"
      End
   End
   Begin VB.Menu mnuVenster 
      Caption         =   "&Venster"
      Visible         =   0   'False
      WindowList      =   -1  'True
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHlpInhoudsopgave 
         Caption         =   "&Inhoudsopgave"
      End
      Begin VB.Menu mnuHlpInfo 
         Caption         =   "&Info"
      End
   End
End
Attribute VB_Name = "frmMDIMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub MDIForm_Load()
    FillListItems
    FillMenu
End Sub

Public Sub FillMenu()
    'Calculate menu's
    mnuExamen.Visible = False
    mnuRapStat.Visible = False
    If (mAppMode = kaUitgebreid) Then
        mnuRapStat.Visible = True
        mnuExamen.Visible = True
    End If
    If (mAppMode = kaNormaal) Then
        'disable repports
        mnuRapStat.Visible = True
    End If
End Sub

Public Sub FillListItems()
Dim ssListItem As ssListItem

    'Fill ListItems
    SSListBar1.ListItems.Clear
    Set ssListItem = SSListBar1.ListItems.Add(, "Aanwezigheidslijst", "Aanwezigheidslijst")
    ssListItem.IconLarge = "picLargeAanwezigheidslijst": ssListItem.IconSmall = "picSmallAanwezigheidslijst"
    Set ssListItem = SSListBar1.ListItems.Add(, "Betalingslijst", "Betalingslijst")
    ssListItem.IconLarge = "picLargeBetalingslijst": ssListItem.IconSmall = "picSmallBetalingslijst"
    Set ssListItem = SSListBar1.ListItems.Add(, "Ledenlijst", "Ledenlijst")
    ssListItem.IconLarge = "picLargeLedenlijst": ssListItem.IconSmall = "picSmallLedenlijst"
    Set ssListItem = SSListBar1.ListItems.Add(, "FamilieBeheer", "FamilieBeheer")
    ssListItem.IconLarge = "picLargeFamilie": ssListItem.IconSmall = "picSmallFamilie"
    If (mAppMode = kaUitgebreid) Then
        Set ssListItem = SSListBar1.ListItems.Add(, "Examen", "Examen")
        ssListItem.IconLarge = "picLargeExamen": ssListItem.IconSmall = "picSmallExamen"
    End If
    If (mAppMode = kaNormaal Or mAppMode = kaUitgebreid) Then
        Set ssListItem = SSListBar1.ListItems.Add(, "Statistieken", "Statistieken")
        ssListItem.IconLarge = "picLargeStatistieken": ssListItem.IconSmall = "picSmallStatistieken"
    End If
    Set ssListItem = SSListBar1.ListItems.Add(, "Sluiten", "Sluiten")
    ssListItem.IconLarge = "picLargeSluiten": ssListItem.IconSmall = "picSmallSluiten"
End Sub

Private Sub MDIForm_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If (MsgBox("Ben je zeker dat je Kaisho Administratie wilt afsluiten?", vbQuestion + vbYesNo) = vbNo) Then
        Cancel = True
    Else
        End
        Set objLogger = Nothing
    End If
End Sub

Private Sub mnuAanAfdrukken_Click()
    mfrmAanwezigheid.Afdrukken
End Sub

Private Sub mnuAanLijst_Click()
    Set mfrmPeriode = New frmPeriode
    mfrmPeriode.OpenForm pmAanwezigheid
End Sub

Private Sub mnuAanVernieuwen_Click()
    mfrmAanwezigheid.Vernieuwen
End Sub

Private Sub mnuBetAfdrukken_Click()
    mfrmBetalingen.Afdrukken
End Sub

Private Sub mnuBetLijst_Click()
    Set mfrmPeriode = New frmPeriode
    mfrmPeriode.OpenForm pmBetaling
End Sub

Private Sub mnuBetVernieuwen_Click()
    mfrmBetalingen.Vernieuwen
End Sub

Private Sub mnuExaNa_Click()
    If (mfrmExamen Is Nothing) Then
        Set mfrmExamen = New frmExamen
    End If
    mfrmExamen.Show
    mfrmExamen.ZOrder
End Sub

Private Sub mnuFamilie_Click()
Dim frmLidFamilie As New frmLidFamilie
    frmLidFamilie.Show vbModal
End Sub

Private Sub mnuHlpInfo_Click()
Dim frmAbout As New frmAbout
    frmAbout.Show vbModal
End Sub

Private Sub mnuHlpInhoudsopgave_Click()
Dim objWordDoc As Word.Document
    If (MsgBox("In deze versie van Kaisho Admin is nog geen online help beschikbaar, druk op ja om de handleiding in word op te starten.", vbInformation + vbYesNo) = vbYes) Then
        Set objWordDoc = GetObject(App.Path & "\Help\Handleiding.doc")
        objWordDoc.Application.Visible = True
    End If
End Sub

Private Sub mnuInstellingen_Click()
    If (mfrmInstellingen Is Nothing) Then
        Set mfrmInstellingen = New frmOptions
    End If
    mfrmInstellingen.Show vbModal
End Sub

Private Sub mnuLidLijst_Click()
    If (mfrmLidLijst Is Nothing) Then
        Set mfrmLidLijst = New frmLidLijst
    End If
    mfrmLidLijst.OpenForm kaAlleLeden
End Sub

Private Sub mnuLidNieuw_Click()
    Set mfrmLidDetail = New frmLidDetail
    mfrmLidDetail.OpenForm mclngNieuwLid
    
    mfrmLidDetail.Show vbModal
    Set mfrmLidDetail = Nothing
End Sub


'Private Sub mnuLidToevoegenAanLijsten_Click()
'Dim lfrmNieuweLeden As frmNieuweLeden
'    If (lfrmNieuweLeden Is Nothing) Then
'        Set lfrmNieuweLeden = New frmNieuweLeden
'    End If
'    lfrmNieuweLeden.OpenForm mnbMaand, mniJaar
'End Sub

Private Sub mnuRSGraden_Click()
    If (mfrmStatistieken Is Nothing) Then
        Set mfrmStatistieken = New frmStatistieken
    End If
    mfrmStatistieken.OpenForm kaGraden
End Sub

Private Sub mnuRSLedenVerhouding_Click()
    If (mfrmStatistieken Is Nothing) Then
        Set mfrmStatistieken = New frmStatistieken
    End If
    mfrmStatistieken.OpenForm kaLedenVerhoudingen
End Sub

Private Sub mnuRSManVrouw_Click()
    If (mfrmStatistieken Is Nothing) Then
        Set mfrmStatistieken = New frmStatistieken
    End If
    mfrmStatistieken.OpenForm kaManVrouw
End Sub

Private Sub mnuSluiten_Click()
    End
End Sub

Private Sub SSListBar1_ListItemClick(ByVal ItemClicked As Listbar.ssListItem)
    Select Case ItemClicked.Key
        Case "Aanwezigheidslijst"
            mnuAanLijst_Click
        Case "Betalingslijst"
            mnuBetLijst_Click
        Case "Ledenlijst"
            mnuLidLijst_Click
        Case "FamilieBeheer"
            mnuFamilie_Click
        Case "Statistieken"
            If (mfrmStatistieken Is Nothing) Then
                Set mfrmStatistieken = New frmStatistieken
            End If
            mfrmStatistieken.OpenForm
        Case "Examen"
            mnuExaNa_Click
        Case "Sluiten"
            mnuSluiten_Click
    End Select
End Sub
