VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsLog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private gLogLineNr As Integer

Private Sub Class_Initialize()
    Open "c:\ka.log" For Output As 1
    gLogLineNr = 0
End Sub

Public Sub WriteLog(Optional text As String = "")
Dim strLogLine As String

    gLogLineNr = gLogLineNr + 1
    strLogLine = gLogLineNr
    If Len(text) > 0 Then
        strLogLine = gLogLineNr & " : " & text
    End If
    Print #1, strLogLine
End Sub

Private Sub Class_Terminate()
    Close 1
End Sub
