VERSION 5.00
Begin VB.Form frmSplash 
   BackColor       =   &H80000009&
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   6156
   ClientLeft      =   252
   ClientTop       =   1416
   ClientWidth     =   9228
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmSplash.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6156
   ScaleWidth      =   9228
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      BackColor       =   &H80000009&
      Height          =   6072
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   8976
      Begin VB.Timer Timer1 
         Left            =   6000
         Top             =   3720
      End
      Begin VB.Image Image1 
         Height          =   2976
         Left            =   7680
         Picture         =   "frmSplash.frx":000C
         Stretch         =   -1  'True
         Top             =   2760
         Width           =   1092
      End
      Begin VB.Image imgLogo 
         Height          =   3468
         Left            =   240
         Picture         =   "frmSplash.frx":10BAE
         Stretch         =   -1  'True
         Top             =   480
         Width           =   3132
      End
      Begin VB.Label lblCopyright 
         BackColor       =   &H80000009&
         Caption         =   "Copyright"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   7.8
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   252
         Left            =   120
         TabIndex        =   4
         Top             =   4920
         Width           =   2412
      End
      Begin VB.Label lblCompany 
         BackColor       =   &H80000009&
         Caption         =   "Company"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   7.8
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   252
         Left            =   120
         TabIndex        =   3
         Top             =   5160
         Width           =   2412
      End
      Begin VB.Label lblWarning 
         BackColor       =   &H80000009&
         Caption         =   "Warning : None"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.4
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   192
         Left            =   156
         TabIndex        =   2
         Top             =   5760
         Width           =   6852
      End
      Begin VB.Label lblVersion 
         AutoSize        =   -1  'True
         BackColor       =   &H80000009&
         Caption         =   "Version"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   288
         Left            =   4080
         TabIndex        =   5
         Top             =   2280
         Width           =   888
      End
      Begin VB.Label lblProductName 
         AutoSize        =   -1  'True
         BackColor       =   &H80000009&
         Caption         =   "Product"
         BeginProperty Font 
            Name            =   "Lucida Blackletter"
            Size            =   36
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   852
         Left            =   4080
         TabIndex        =   6
         Top             =   1320
         Width           =   2652
      End
      Begin VB.Label lblLicenseTo 
         BackColor       =   &H80000009&
         Caption         =   "LicenseTo : Frank Van Sealen"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.4
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   252
         Left            =   120
         TabIndex        =   1
         Top             =   5400
         Width           =   6852
      End
   End
End
Attribute VB_Name = "frmSplash"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    lblVersion.Caption = "Version " & App.Major & "." & App.Minor & "." & App.Revision
    lblProductName.Caption = App.Title
    lblCompany = App.CompanyName
    lblCopyright = App.LegalCopyright
    Timer1.Enabled = True
    Timer1.Interval = 3000
End Sub

Private Sub Timer1_Timer()
    Unload Me
End Sub
