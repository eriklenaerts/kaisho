VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "SSDW3BO.OCX"
Begin VB.Form frmAanwezigheid 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Aanwezigheidslijst"
   ClientHeight    =   8640
   ClientLeft      =   48
   ClientTop       =   336
   ClientWidth     =   13572
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.4
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmAanwezigheid.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8640
   ScaleWidth      =   13572
   Begin VB.CommandButton cmdOpenInExcel 
      Caption         =   "Open In &Excel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   372
      Left            =   3960
      TabIndex        =   10
      Top             =   8160
      Width           =   1332
   End
   Begin VB.CommandButton cmdNieuweLedenToevoegen 
      Caption         =   "&Nieuw lid toevoegen"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2040
      TabIndex        =   9
      Top             =   8160
      Width           =   1812
   End
   Begin VB.CommandButton cmdAfdrukken 
      Caption         =   "Af&drukken"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5400
      TabIndex        =   2
      Top             =   8160
      Width           =   1335
   End
   Begin VB.CommandButton cmdVernieuwen 
      Caption         =   "&Vernieuwen"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6840
      TabIndex        =   3
      Top             =   8160
      Width           =   1335
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Default         =   -1  'True
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9240
      TabIndex        =   4
      Top             =   8160
      Width           =   1335
   End
   Begin VB.CommandButton cmdAnnuleren 
      Cancel          =   -1  'True
      Caption         =   "&Annuleren"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   12120
      TabIndex        =   6
      Top             =   8160
      Width           =   1335
   End
   Begin VB.CommandButton cmdToepassen 
      Caption         =   "&Toepassen"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10680
      TabIndex        =   5
      Top             =   8160
      Width           =   1335
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid SSOleDBGrid1 
      Bindings        =   "frmAanwezigheid.frx":030A
      Height          =   5055
      Left            =   120
      TabIndex        =   0
      Top             =   840
      Width           =   13335
      _Version        =   196617
      DataMode        =   1
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      UseGroups       =   -1  'True
      CheckBox3D      =   0   'False
      AllowAddNew     =   -1  'True
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      ForeColorOdd    =   8421504
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   318
      Groups.Count    =   6
      Groups(0).Width =   3043
      Groups(0).Caption=   "Lid"
      Groups(0).Columns.Count=   7
      Groups(0).Columns(0).Width=   291
      Groups(0).Columns(0).Caption=   "AanwezigheidID"
      Groups(0).Columns(0).Name=   "AanwezigheidID"
      Groups(0).Columns(0).CaptionAlignment=   1
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   3
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(0).Locked=   -1  'True
      Groups(0).Columns(0).HasBackColor=   -1  'True
      Groups(0).Columns(0).BackColor=   12632256
      Groups(0).Columns(1).Width=   767
      Groups(0).Columns(1).Caption=   "LidID"
      Groups(0).Columns(1).Name=   "LidID"
      Groups(0).Columns(1).Alignment=   1
      Groups(0).Columns(1).CaptionAlignment=   1
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   3
      Groups(0).Columns(1).FieldLen=   256
      Groups(0).Columns(1).Locked=   -1  'True
      Groups(0).Columns(1).HasBackColor=   -1  'True
      Groups(0).Columns(1).BackColor=   12632256
      Groups(0).Columns(2).Width=   344
      Groups(0).Columns(2).Caption=   "Naam"
      Groups(0).Columns(2).Name=   "Naam"
      Groups(0).Columns(2).Alignment=   1
      Groups(0).Columns(2).CaptionAlignment=   1
      Groups(0).Columns(2).DataField=   "Column 2"
      Groups(0).Columns(2).DataType=   130
      Groups(0).Columns(2).FieldLen=   256
      Groups(0).Columns(2).Locked=   -1  'True
      Groups(0).Columns(2).Style=   1
      Groups(0).Columns(2).HasBackColor=   -1  'True
      Groups(0).Columns(2).BackColor=   12632256
      Groups(0).Columns(3).Width=   344
      Groups(0).Columns(3).Caption=   "Jaar"
      Groups(0).Columns(3).Name=   "Jaar"
      Groups(0).Columns(3).Alignment=   1
      Groups(0).Columns(3).CaptionAlignment=   1
      Groups(0).Columns(3).DataField=   "Column 3"
      Groups(0).Columns(3).DataType=   3
      Groups(0).Columns(3).FieldLen=   256
      Groups(0).Columns(3).Locked=   -1  'True
      Groups(0).Columns(3).HasBackColor=   -1  'True
      Groups(0).Columns(3).BackColor=   12632256
      Groups(0).Columns(4).Width=   344
      Groups(0).Columns(4).Caption=   "Maand"
      Groups(0).Columns(4).Name=   "Maand"
      Groups(0).Columns(4).Alignment=   1
      Groups(0).Columns(4).CaptionAlignment=   1
      Groups(0).Columns(4).DataField=   "Column 4"
      Groups(0).Columns(4).DataType=   3
      Groups(0).Columns(4).FieldLen=   256
      Groups(0).Columns(4).Locked=   -1  'True
      Groups(0).Columns(4).HasBackColor=   -1  'True
      Groups(0).Columns(4).BackColor=   12632256
      Groups(0).Columns(5).Width=   344
      Groups(0).Columns(5).Caption=   "Betaald"
      Groups(0).Columns(5).Name=   "Betaald"
      Groups(0).Columns(5).DataField=   "Column 5"
      Groups(0).Columns(5).FieldLen=   256
      Groups(0).Columns(5).Locked=   -1  'True
      Groups(0).Columns(5).Style=   2
      Groups(0).Columns(5).HasBackColor=   -1  'True
      Groups(0).Columns(5).BackColor=   12632256
      Groups(0).Columns(6).Width=   609
      Groups(0).Columns(6).Caption=   "Opmerking"
      Groups(0).Columns(6).Name=   "Opmerking"
      Groups(0).Columns(6).Alignment=   1
      Groups(0).Columns(6).CaptionAlignment=   1
      Groups(0).Columns(6).DataField=   "Column 6"
      Groups(0).Columns(6).DataType=   8
      Groups(0).Columns(6).FieldLen=   256
      Groups(0).Columns(6).HasBackColor=   -1  'True
      Groups(0).Columns(6).BackColor=   16777215
      Groups(1).Width =   3545
      Groups(1).Caption=   "Week 1"
      Groups(1).Columns.Count=   4
      Groups(1).Columns(0).Width=   820
      Groups(1).Columns(0).Caption=   "Maandag1"
      Groups(1).Columns(0).Name=   "Maandag1"
      Groups(1).Columns(0).Alignment=   1
      Groups(1).Columns(0).CaptionAlignment=   1
      Groups(1).Columns(0).DataField=   "Column 7"
      Groups(1).Columns(0).DataType=   11
      Groups(1).Columns(0).FieldLen=   256
      Groups(1).Columns(0).Style=   2
      Groups(1).Columns(1).Width=   820
      Groups(1).Columns(1).Caption=   "Dinsdag1"
      Groups(1).Columns(1).Name=   "Dinsdag1"
      Groups(1).Columns(1).CaptionAlignment=   1
      Groups(1).Columns(1).DataField=   "Column 8"
      Groups(1).Columns(1).DataType=   11
      Groups(1).Columns(1).FieldLen=   256
      Groups(1).Columns(1).Style=   2
      Groups(1).Columns(2).Width=   820
      Groups(1).Columns(2).Caption=   "Vrijdag1"
      Groups(1).Columns(2).Name=   "Vrijdag1"
      Groups(1).Columns(2).CaptionAlignment=   1
      Groups(1).Columns(2).DataField=   "Column 9"
      Groups(1).Columns(2).DataType=   11
      Groups(1).Columns(2).FieldLen=   256
      Groups(1).Columns(2).Style=   2
      Groups(1).Columns(3).Width=   1085
      Groups(1).Columns(3).Caption=   "Zaterdag1"
      Groups(1).Columns(3).Name=   "Zaterdag1"
      Groups(1).Columns(3).CaptionAlignment=   1
      Groups(1).Columns(3).DataField=   "Column 10"
      Groups(1).Columns(3).DataType=   11
      Groups(1).Columns(3).FieldLen=   256
      Groups(1).Columns(3).Style=   2
      Groups(2).Width =   3545
      Groups(2).Caption=   "Week 2"
      Groups(2).Columns.Count=   4
      Groups(2).Columns(0).Width=   820
      Groups(2).Columns(0).Caption=   "Maandag2"
      Groups(2).Columns(0).Name=   "Maandag2"
      Groups(2).Columns(0).CaptionAlignment=   1
      Groups(2).Columns(0).DataField=   "Column 11"
      Groups(2).Columns(0).DataType=   11
      Groups(2).Columns(0).FieldLen=   256
      Groups(2).Columns(0).Style=   2
      Groups(2).Columns(1).Width=   820
      Groups(2).Columns(1).Caption=   "Dinsdag2"
      Groups(2).Columns(1).Name=   "Dinsdag2"
      Groups(2).Columns(1).CaptionAlignment=   1
      Groups(2).Columns(1).DataField=   "Column 12"
      Groups(2).Columns(1).DataType=   11
      Groups(2).Columns(1).FieldLen=   256
      Groups(2).Columns(1).Style=   2
      Groups(2).Columns(2).Width=   820
      Groups(2).Columns(2).Caption=   "Vrijdag2"
      Groups(2).Columns(2).Name=   "Vrijdag2"
      Groups(2).Columns(2).CaptionAlignment=   1
      Groups(2).Columns(2).DataField=   "Column 13"
      Groups(2).Columns(2).DataType=   11
      Groups(2).Columns(2).FieldLen=   256
      Groups(2).Columns(2).Style=   2
      Groups(2).Columns(3).Width=   1085
      Groups(2).Columns(3).Caption=   "Zaterdag2"
      Groups(2).Columns(3).Name=   "Zaterdag2"
      Groups(2).Columns(3).CaptionAlignment=   1
      Groups(2).Columns(3).DataField=   "Column 14"
      Groups(2).Columns(3).DataType=   11
      Groups(2).Columns(3).FieldLen=   256
      Groups(2).Columns(3).Style=   2
      Groups(3).Width =   3545
      Groups(3).Caption=   "Week 3"
      Groups(3).Columns.Count=   4
      Groups(3).Columns(0).Width=   820
      Groups(3).Columns(0).Caption=   "Maandag3"
      Groups(3).Columns(0).Name=   "Maandag3"
      Groups(3).Columns(0).CaptionAlignment=   1
      Groups(3).Columns(0).DataField=   "Column 15"
      Groups(3).Columns(0).DataType=   11
      Groups(3).Columns(0).FieldLen=   256
      Groups(3).Columns(0).Style=   2
      Groups(3).Columns(1).Width=   820
      Groups(3).Columns(1).Caption=   "Dinsdag3"
      Groups(3).Columns(1).Name=   "Dinsdag3"
      Groups(3).Columns(1).CaptionAlignment=   1
      Groups(3).Columns(1).DataField=   "Column 16"
      Groups(3).Columns(1).DataType=   11
      Groups(3).Columns(1).FieldLen=   256
      Groups(3).Columns(1).Style=   2
      Groups(3).Columns(2).Width=   820
      Groups(3).Columns(2).Caption=   "Vrijdag3"
      Groups(3).Columns(2).Name=   "Vrijdag3"
      Groups(3).Columns(2).CaptionAlignment=   1
      Groups(3).Columns(2).DataField=   "Column 17"
      Groups(3).Columns(2).DataType=   11
      Groups(3).Columns(2).FieldLen=   256
      Groups(3).Columns(2).Style=   2
      Groups(3).Columns(3).Width=   1085
      Groups(3).Columns(3).Caption=   "Zaterdag3"
      Groups(3).Columns(3).Name=   "Zaterdag3"
      Groups(3).Columns(3).CaptionAlignment=   1
      Groups(3).Columns(3).DataField=   "Column 18"
      Groups(3).Columns(3).DataType=   11
      Groups(3).Columns(3).FieldLen=   256
      Groups(3).Columns(3).Style=   2
      Groups(4).Width =   3545
      Groups(4).Caption=   "Week 4"
      Groups(4).Columns.Count=   4
      Groups(4).Columns(0).Width=   820
      Groups(4).Columns(0).Caption=   "Maandag4"
      Groups(4).Columns(0).Name=   "Maandag4"
      Groups(4).Columns(0).CaptionAlignment=   1
      Groups(4).Columns(0).DataField=   "Column 19"
      Groups(4).Columns(0).DataType=   11
      Groups(4).Columns(0).FieldLen=   256
      Groups(4).Columns(0).Style=   2
      Groups(4).Columns(1).Width=   794
      Groups(4).Columns(1).Caption=   "Dinsdag4"
      Groups(4).Columns(1).Name=   "Dinsdag4"
      Groups(4).Columns(1).CaptionAlignment=   1
      Groups(4).Columns(1).DataField=   "Column 20"
      Groups(4).Columns(1).DataType=   11
      Groups(4).Columns(1).FieldLen=   256
      Groups(4).Columns(1).Style=   2
      Groups(4).Columns(2).Width=   847
      Groups(4).Columns(2).Caption=   "Vrijdag4"
      Groups(4).Columns(2).Name=   "Vrijdag4"
      Groups(4).Columns(2).CaptionAlignment=   1
      Groups(4).Columns(2).DataField=   "Column 21"
      Groups(4).Columns(2).DataType=   11
      Groups(4).Columns(2).FieldLen=   256
      Groups(4).Columns(2).Style=   2
      Groups(4).Columns(3).Width=   1085
      Groups(4).Columns(3).Caption=   "Zaterdag4"
      Groups(4).Columns(3).Name=   "Zaterdag4"
      Groups(4).Columns(3).CaptionAlignment=   1
      Groups(4).Columns(3).DataField=   "Column 22"
      Groups(4).Columns(3).DataType=   11
      Groups(4).Columns(3).FieldLen=   256
      Groups(4).Columns(3).Style=   2
      Groups(5).Width =   3545
      Groups(5).Caption=   "Week 5"
      Groups(5).Columns.Count=   4
      Groups(5).Columns(0).Width=   1111
      Groups(5).Columns(0).Caption=   "Maandag5"
      Groups(5).Columns(0).Name=   "Maandag5"
      Groups(5).Columns(0).CaptionAlignment=   1
      Groups(5).Columns(0).DataField=   "Column 23"
      Groups(5).Columns(0).DataType=   11
      Groups(5).Columns(0).FieldLen=   256
      Groups(5).Columns(0).Style=   2
      Groups(5).Columns(1).Width=   635
      Groups(5).Columns(1).Caption=   "Dinsdag5"
      Groups(5).Columns(1).Name=   "Dinsdag5"
      Groups(5).Columns(1).CaptionAlignment=   1
      Groups(5).Columns(1).DataField=   "Column 24"
      Groups(5).Columns(1).DataType=   11
      Groups(5).Columns(1).FieldLen=   256
      Groups(5).Columns(1).Style=   2
      Groups(5).Columns(2).Width=   873
      Groups(5).Columns(2).Caption=   "Vrijdag5"
      Groups(5).Columns(2).Name=   "Vrijdag5"
      Groups(5).Columns(2).CaptionAlignment=   1
      Groups(5).Columns(2).DataField=   "Column 25"
      Groups(5).Columns(2).DataType=   11
      Groups(5).Columns(2).FieldLen=   256
      Groups(5).Columns(2).Style=   2
      Groups(5).Columns(3).Width=   926
      Groups(5).Columns(3).Caption=   "Zaterdag5"
      Groups(5).Columns(3).Name=   "Zaterdag5"
      Groups(5).Columns(3).Alignment=   1
      Groups(5).Columns(3).CaptionAlignment=   1
      Groups(5).Columns(3).DataField=   "Column 26"
      Groups(5).Columns(3).DataType=   11
      Groups(5).Columns(3).FieldLen=   256
      Groups(5).Columns(3).Locked=   -1  'True
      Groups(5).Columns(3).Style=   2
      _ExtentX        =   23521
      _ExtentY        =   8916
      _StockProps     =   79
      Caption         =   "Leden"
      DataMember      =   "LidAanwezigheid"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid SSOleDBGrid2 
      Bindings        =   "frmAanwezigheid.frx":0329
      Height          =   1815
      Left            =   120
      TabIndex        =   1
      Top             =   5880
      Width           =   13335
      _Version        =   196617
      DataMode        =   1
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      ColumnHeaders   =   0   'False
      UseGroups       =   -1  'True
      CheckBox3D      =   0   'False
      AllowAddNew     =   -1  'True
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      ForeColorOdd    =   8421504
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   318
      Groups.Count    =   6
      Groups(0).Width =   3043
      Groups(0).Caption=   "Lid"
      Groups(0).Columns.Count=   7
      Groups(0).Columns(0).Width=   291
      Groups(0).Columns(0).Caption=   "AanwezigheidID"
      Groups(0).Columns(0).Name=   "AanwezigheidID"
      Groups(0).Columns(0).CaptionAlignment=   1
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   3
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(0).Locked=   -1  'True
      Groups(0).Columns(0).HasBackColor=   -1  'True
      Groups(0).Columns(0).BackColor=   12632256
      Groups(0).Columns(1).Width=   767
      Groups(0).Columns(1).Caption=   "LidID"
      Groups(0).Columns(1).Name=   "LidID"
      Groups(0).Columns(1).Alignment=   1
      Groups(0).Columns(1).CaptionAlignment=   1
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   3
      Groups(0).Columns(1).FieldLen=   256
      Groups(0).Columns(1).Locked=   -1  'True
      Groups(0).Columns(1).HasBackColor=   -1  'True
      Groups(0).Columns(1).BackColor=   12632256
      Groups(0).Columns(2).Width=   344
      Groups(0).Columns(2).Caption=   "Naam"
      Groups(0).Columns(2).Name=   "Naam"
      Groups(0).Columns(2).Alignment=   1
      Groups(0).Columns(2).CaptionAlignment=   1
      Groups(0).Columns(2).DataField=   "Column 2"
      Groups(0).Columns(2).DataType=   130
      Groups(0).Columns(2).FieldLen=   256
      Groups(0).Columns(2).Locked=   -1  'True
      Groups(0).Columns(2).Style=   1
      Groups(0).Columns(2).HasBackColor=   -1  'True
      Groups(0).Columns(2).BackColor=   12632256
      Groups(0).Columns(3).Width=   344
      Groups(0).Columns(3).Caption=   "Jaar"
      Groups(0).Columns(3).Name=   "Jaar"
      Groups(0).Columns(3).Alignment=   1
      Groups(0).Columns(3).CaptionAlignment=   1
      Groups(0).Columns(3).DataField=   "Column 3"
      Groups(0).Columns(3).DataType=   3
      Groups(0).Columns(3).FieldLen=   256
      Groups(0).Columns(3).Locked=   -1  'True
      Groups(0).Columns(3).HasBackColor=   -1  'True
      Groups(0).Columns(3).BackColor=   12632256
      Groups(0).Columns(4).Width=   344
      Groups(0).Columns(4).Caption=   "Maand"
      Groups(0).Columns(4).Name=   "Maand"
      Groups(0).Columns(4).Alignment=   1
      Groups(0).Columns(4).CaptionAlignment=   1
      Groups(0).Columns(4).DataField=   "Column 4"
      Groups(0).Columns(4).DataType=   3
      Groups(0).Columns(4).FieldLen=   256
      Groups(0).Columns(4).Locked=   -1  'True
      Groups(0).Columns(4).HasBackColor=   -1  'True
      Groups(0).Columns(4).BackColor=   12632256
      Groups(0).Columns(5).Width=   344
      Groups(0).Columns(5).Caption=   "Betaald"
      Groups(0).Columns(5).Name=   "Betaald"
      Groups(0).Columns(5).DataField=   "Column 5"
      Groups(0).Columns(5).FieldLen=   256
      Groups(0).Columns(5).Locked=   -1  'True
      Groups(0).Columns(5).Style=   2
      Groups(0).Columns(5).HasBackColor=   -1  'True
      Groups(0).Columns(5).BackColor=   12632256
      Groups(0).Columns(6).Width=   609
      Groups(0).Columns(6).Caption=   "Opmerking"
      Groups(0).Columns(6).Name=   "Opmerking"
      Groups(0).Columns(6).Alignment=   1
      Groups(0).Columns(6).CaptionAlignment=   1
      Groups(0).Columns(6).DataField=   "Column 6"
      Groups(0).Columns(6).DataType=   8
      Groups(0).Columns(6).FieldLen=   256
      Groups(0).Columns(6).HasBackColor=   -1  'True
      Groups(0).Columns(6).BackColor=   16777215
      Groups(1).Width =   3545
      Groups(1).Caption=   "Week 1"
      Groups(1).Columns.Count=   4
      Groups(1).Columns(0).Width=   820
      Groups(1).Columns(0).Caption=   "Maandag1"
      Groups(1).Columns(0).Name=   "Maandag1"
      Groups(1).Columns(0).Alignment=   1
      Groups(1).Columns(0).CaptionAlignment=   1
      Groups(1).Columns(0).DataField=   "Column 7"
      Groups(1).Columns(0).DataType=   11
      Groups(1).Columns(0).FieldLen=   256
      Groups(1).Columns(0).Style=   2
      Groups(1).Columns(1).Width=   820
      Groups(1).Columns(1).Caption=   "Dinsdag1"
      Groups(1).Columns(1).Name=   "Dinsdag1"
      Groups(1).Columns(1).CaptionAlignment=   1
      Groups(1).Columns(1).DataField=   "Column 8"
      Groups(1).Columns(1).DataType=   11
      Groups(1).Columns(1).FieldLen=   256
      Groups(1).Columns(1).Style=   2
      Groups(1).Columns(2).Width=   820
      Groups(1).Columns(2).Caption=   "Vrijdag1"
      Groups(1).Columns(2).Name=   "Vrijdag1"
      Groups(1).Columns(2).CaptionAlignment=   1
      Groups(1).Columns(2).DataField=   "Column 9"
      Groups(1).Columns(2).DataType=   11
      Groups(1).Columns(2).FieldLen=   256
      Groups(1).Columns(2).Style=   2
      Groups(1).Columns(3).Width=   1085
      Groups(1).Columns(3).Caption=   "Zaterdag1"
      Groups(1).Columns(3).Name=   "Zaterdag1"
      Groups(1).Columns(3).CaptionAlignment=   1
      Groups(1).Columns(3).DataField=   "Column 10"
      Groups(1).Columns(3).DataType=   11
      Groups(1).Columns(3).FieldLen=   256
      Groups(1).Columns(3).Style=   2
      Groups(2).Width =   3545
      Groups(2).Caption=   "Week 2"
      Groups(2).Columns.Count=   4
      Groups(2).Columns(0).Width=   820
      Groups(2).Columns(0).Caption=   "Maandag2"
      Groups(2).Columns(0).Name=   "Maandag2"
      Groups(2).Columns(0).CaptionAlignment=   1
      Groups(2).Columns(0).DataField=   "Column 11"
      Groups(2).Columns(0).DataType=   11
      Groups(2).Columns(0).FieldLen=   256
      Groups(2).Columns(0).Style=   2
      Groups(2).Columns(1).Width=   820
      Groups(2).Columns(1).Caption=   "Dinsdag2"
      Groups(2).Columns(1).Name=   "Dinsdag2"
      Groups(2).Columns(1).CaptionAlignment=   1
      Groups(2).Columns(1).DataField=   "Column 12"
      Groups(2).Columns(1).DataType=   11
      Groups(2).Columns(1).FieldLen=   256
      Groups(2).Columns(1).Style=   2
      Groups(2).Columns(2).Width=   820
      Groups(2).Columns(2).Caption=   "Vrijdag2"
      Groups(2).Columns(2).Name=   "Vrijdag2"
      Groups(2).Columns(2).CaptionAlignment=   1
      Groups(2).Columns(2).DataField=   "Column 13"
      Groups(2).Columns(2).DataType=   11
      Groups(2).Columns(2).FieldLen=   256
      Groups(2).Columns(2).Style=   2
      Groups(2).Columns(3).Width=   1085
      Groups(2).Columns(3).Caption=   "Zaterdag2"
      Groups(2).Columns(3).Name=   "Zaterdag2"
      Groups(2).Columns(3).CaptionAlignment=   1
      Groups(2).Columns(3).DataField=   "Column 14"
      Groups(2).Columns(3).DataType=   11
      Groups(2).Columns(3).FieldLen=   256
      Groups(2).Columns(3).Style=   2
      Groups(3).Width =   3545
      Groups(3).Caption=   "Week 3"
      Groups(3).Columns.Count=   4
      Groups(3).Columns(0).Width=   820
      Groups(3).Columns(0).Caption=   "Maandag3"
      Groups(3).Columns(0).Name=   "Maandag3"
      Groups(3).Columns(0).CaptionAlignment=   1
      Groups(3).Columns(0).DataField=   "Column 15"
      Groups(3).Columns(0).DataType=   11
      Groups(3).Columns(0).FieldLen=   256
      Groups(3).Columns(0).Style=   2
      Groups(3).Columns(1).Width=   820
      Groups(3).Columns(1).Caption=   "Dinsdag3"
      Groups(3).Columns(1).Name=   "Dinsdag3"
      Groups(3).Columns(1).CaptionAlignment=   1
      Groups(3).Columns(1).DataField=   "Column 16"
      Groups(3).Columns(1).DataType=   11
      Groups(3).Columns(1).FieldLen=   256
      Groups(3).Columns(1).Style=   2
      Groups(3).Columns(2).Width=   820
      Groups(3).Columns(2).Caption=   "Vrijdag3"
      Groups(3).Columns(2).Name=   "Vrijdag3"
      Groups(3).Columns(2).CaptionAlignment=   1
      Groups(3).Columns(2).DataField=   "Column 17"
      Groups(3).Columns(2).DataType=   11
      Groups(3).Columns(2).FieldLen=   256
      Groups(3).Columns(2).Style=   2
      Groups(3).Columns(3).Width=   1085
      Groups(3).Columns(3).Caption=   "Zaterdag3"
      Groups(3).Columns(3).Name=   "Zaterdag3"
      Groups(3).Columns(3).CaptionAlignment=   1
      Groups(3).Columns(3).DataField=   "Column 18"
      Groups(3).Columns(3).DataType=   11
      Groups(3).Columns(3).FieldLen=   256
      Groups(3).Columns(3).Style=   2
      Groups(4).Width =   3545
      Groups(4).Caption=   "Week 4"
      Groups(4).Columns.Count=   4
      Groups(4).Columns(0).Width=   820
      Groups(4).Columns(0).Caption=   "Maandag4"
      Groups(4).Columns(0).Name=   "Maandag4"
      Groups(4).Columns(0).CaptionAlignment=   1
      Groups(4).Columns(0).DataField=   "Column 19"
      Groups(4).Columns(0).DataType=   11
      Groups(4).Columns(0).FieldLen=   256
      Groups(4).Columns(0).Style=   2
      Groups(4).Columns(1).Width=   794
      Groups(4).Columns(1).Caption=   "Dinsdag4"
      Groups(4).Columns(1).Name=   "Dinsdag4"
      Groups(4).Columns(1).CaptionAlignment=   1
      Groups(4).Columns(1).DataField=   "Column 20"
      Groups(4).Columns(1).DataType=   11
      Groups(4).Columns(1).FieldLen=   256
      Groups(4).Columns(1).Style=   2
      Groups(4).Columns(2).Width=   847
      Groups(4).Columns(2).Caption=   "Vrijdag4"
      Groups(4).Columns(2).Name=   "Vrijdag4"
      Groups(4).Columns(2).CaptionAlignment=   1
      Groups(4).Columns(2).DataField=   "Column 21"
      Groups(4).Columns(2).DataType=   11
      Groups(4).Columns(2).FieldLen=   256
      Groups(4).Columns(2).Style=   2
      Groups(4).Columns(3).Width=   1085
      Groups(4).Columns(3).Caption=   "Zaterdag4"
      Groups(4).Columns(3).Name=   "Zaterdag4"
      Groups(4).Columns(3).CaptionAlignment=   1
      Groups(4).Columns(3).DataField=   "Column 22"
      Groups(4).Columns(3).DataType=   11
      Groups(4).Columns(3).FieldLen=   256
      Groups(4).Columns(3).Style=   2
      Groups(5).Width =   3545
      Groups(5).Caption=   "Week 5"
      Groups(5).Columns.Count=   4
      Groups(5).Columns(0).Width=   688
      Groups(5).Columns(0).Caption=   "Maandag5"
      Groups(5).Columns(0).Name=   "Maandag5"
      Groups(5).Columns(0).CaptionAlignment=   1
      Groups(5).Columns(0).DataField=   "Column 23"
      Groups(5).Columns(0).DataType=   11
      Groups(5).Columns(0).FieldLen=   256
      Groups(5).Columns(0).Style=   2
      Groups(5).Columns(1).Width=   688
      Groups(5).Columns(1).Caption=   "Dinsdag5"
      Groups(5).Columns(1).Name=   "Dinsdag5"
      Groups(5).Columns(1).CaptionAlignment=   1
      Groups(5).Columns(1).DataField=   "Column 24"
      Groups(5).Columns(1).DataType=   11
      Groups(5).Columns(1).FieldLen=   256
      Groups(5).Columns(1).Style=   2
      Groups(5).Columns(2).Width=   688
      Groups(5).Columns(2).Caption=   "Vrijdag5"
      Groups(5).Columns(2).Name=   "Vrijdag5"
      Groups(5).Columns(2).CaptionAlignment=   1
      Groups(5).Columns(2).DataField=   "Column 25"
      Groups(5).Columns(2).DataType=   11
      Groups(5).Columns(2).FieldLen=   256
      Groups(5).Columns(2).Style=   2
      Groups(5).Columns(3).Width=   1482
      Groups(5).Columns(3).Caption=   "Zaterdag5"
      Groups(5).Columns(3).Name=   "Zaterdag5"
      Groups(5).Columns(3).Alignment=   1
      Groups(5).Columns(3).CaptionAlignment=   1
      Groups(5).Columns(3).DataField=   "Column 26"
      Groups(5).Columns(3).DataType=   11
      Groups(5).Columns(3).FieldLen=   256
      Groups(5).Columns(3).Locked=   -1  'True
      Groups(5).Columns(3).Style=   2
      _ExtentX        =   23521
      _ExtentY        =   3201
      _StockProps     =   79
      Caption         =   "Nieuwe Leden"
      DataMember      =   "LidAanwezigheid"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Image Image1 
      Height          =   384
      Left            =   240
      Picture         =   "frmAanwezigheid.frx":0348
      Top             =   204
      Width           =   384
   End
   Begin VB.Label lblMutatie 
      Caption         =   "Laatste mutatie : xx/xx/xxxx"
      Height          =   240
      Left            =   120
      TabIndex        =   8
      Top             =   7800
      Width           =   3735
   End
   Begin VB.Label lblTitel 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Aanwezigheidslijst"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   13335
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   13440
      Y1              =   8040
      Y2              =   8040
   End
End
Attribute VB_Name = "frmAanwezigheid"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mniJaar As Integer
Private mnbMaand As Byte
Private mblnDataChanged As Boolean
Private mobjExcelExt As clsExcelExtension
Private mCancelled As Boolean
Private mMode As kaFormMode
Private WithEvents mfrmStatus As frmStatus
Attribute mfrmStatus.VB_VarHelpID = -1

Public Property Let Mode(NewMode As kaFormMode)
    mMode = NewMode
    SetButtons mMode
    SetMenu mMode
End Property

Public Property Get Mode() As kaFormMode
    Mode = mMode
End Property

Public Property Let DataChanged(value As Boolean)
    mblnDataChanged = value
    cmdToepassen.Enabled = mblnDataChanged
End Property

Public Property Get DataChanged() As Boolean
    DataChanged = mblnDataChanged
End Property

Public Sub OpenForm(lngJaar As Integer, lngMaand As Integer)
'    Mode = kaFormLaden
    SSOleDBGrid1.Visible = False
    SSOleDBGrid2.Visible = False
    DataEnvironment1.rstblLidAanwezigheidBestaand.Filter = "Jaar=" + CStr(lngJaar) + " and Maand=" + CStr(lngMaand)
    DataEnvironment1.rstblLidAanwezigheidNieuw.Filter = "Jaar=" + CStr(lngJaar) + " and Maand=" + CStr(lngMaand)
    mnbMaand = lngMaand
    mniJaar = lngJaar
    SetColumnHeaders SSOleDBGrid1
    SetColumnHeaders SSOleDBGrid2
    SSOleDBGrid1.Visible = True
    SSOleDBGrid2.Visible = True
    
    Me.Caption = "Aanwezigheidslijst : " + MonthName(lngMaand) + " " + CStr(lngJaar)
    lblTitel.Caption = vbCrLf + Me.Caption
    SetMutationDate
    Me.Left = 100
    Me.Top = 100
    Me.Show
    Me.ZOrder
    Mode = kaFormOpen
End Sub

Private Sub cmdAfdrukken_Click()
    Afdrukken
End Sub

Private Sub cmdOpenInExcel_Click()
Dim lxlWorkSheet As Excel.Worksheet

    Set lxlWorkSheet = LaadDataInExcel
    
    'Show excel
    lxlWorkSheet.Application.Visible = True
End Sub

Public Sub Afdrukken()
Dim lxlWorkSheet As Excel.Worksheet

On Error GoTo ErrorHandler
    
    Set lxlWorkSheet = LaadDataInExcel
    
    'Print
    If (Not lxlWorkSheet Is Nothing) Then
        lxlWorkSheet.PrintOut
    End If
    
    If mobjExcelExt.ExcelWasNotRunning Then lxlWorkSheet.Application.Quit
    
    Set lxlWorkSheet = Nothing
    
    Exit Sub
    
ErrorHandler:
    Select Case Err.Number
        Case cErrorUserrequestedCancel
            MsgBox "Afdrukken van de aanwezigheidslijst op aanvraag van de gebruiker gešnuleerd", vbInformation
            mCancelled = False
        Case Else
            MsgBox "Unspecified Error : " & Err.Description & "(" & Err.Number & " at " & Err.Source & ")"
    End Select

    If (Not mfrmStatus Is Nothing) Then
        Unload mfrmStatus
        Set mfrmStatus = Nothing
    End If
End Sub

Public Function LaadDataInExcel() As Excel.Worksheet
Dim lxlWorkSheet As Excel.Worksheet
Dim Fields() As Variant
Dim lvntLedenBookmark As Variant, lvntNieuweLedenBookmark As Variant
Dim llngRowCounter As Long, llngColumnCounter As Long, llngVisibleColumnsCounter As Long, llngVisibleColumns As Long, llngColumns As Long, llngRows As Long, llngColumnCount As Long
Dim Range_Cell1 As Variant, Range_Cell2 As Variant
Dim PreviousDay As Integer, lintWeekDay As Integer

On Error GoTo ErrorHandler

    Mode = kaFormVerwerking
    Me.Hide
    WaitAWhile 2
    
    'Clear Clipboard
    Clipboard.Clear
    
    'Display message
    Set mfrmStatus = New frmStatus
    mfrmStatus.lblText = "Bezig met het verzamelen van de gegevens"
    mfrmStatus.OpenForm kaMetProgress
    
    lvntLedenBookmark = SSOleDBGrid1.Bookmark
    lvntNieuweLedenBookmark = SSOleDBGrid2.Bookmark
    
       
    'Get the number of visible columns
    llngColumns = SSOleDBGrid1.Columns.Count - 1
    For llngColumnCounter = 0 To llngColumns
        If (SSOleDBGrid1.Columns(llngColumnCounter).Visible) Then
            llngVisibleColumns = llngVisibleColumns + 1
        End If
        DoEvents
        If (mCancelled) Then Err.Raise cErrorUserrequestedCancel
        mfrmStatus.prgMain = (llngColumnCounter / llngColumns) * 10
    Next llngColumnCounter
    
    SSOleDBGrid1.MoveLast
    SSOleDBGrid2.MoveLast
    llngRows = (SSOleDBGrid1.Rows - 1) + (SSOleDBGrid2.Rows - 1) + 10
    ReDim Fields(1 To llngRows + 2, 1 To llngVisibleColumns)

    'Create Header row
    PreviousDay = 0
    Fields(1, 1) = "Naam"
    Fields(1, 2) = "Betaald"
    Fields(1, 3) = "Opmerking"
    For llngColumnCounter = 1 To llngVisibleColumns - 3
        Do
            PreviousDay = PreviousDay + 1
            lintWeekDay = Weekday(CDate(CStr(mniJaar) + "-" + CStr(mnbMaand) + "-" + CStr(PreviousDay)))
        Loop Until (lintWeekDay = vbMonday Or lintWeekDay = vbTuesday Or lintWeekDay = vbFriday Or lintWeekDay = vbSaturday)
        Fields(1, llngColumnCounter + 3) = PreviousDay
        DoEvents
        If (mCancelled) Then Err.Raise cErrorUserrequestedCancel
        mfrmStatus.prgMain = ((llngColumnCounter / llngColumns) * 15) + 10
    Next llngColumnCounter
    
    'Copy rows (Existing Members)
    SSOleDBGrid1.MoveFirst
    llngRowCounter = 0
    For llngRowCounter = 0 To SSOleDBGrid1.Rows - 1
        llngVisibleColumnsCounter = 0
        For llngColumnCounter = 0 To llngColumns
            If (SSOleDBGrid1.Columns(llngColumnCounter).Visible) Then
                If (Len(SSOleDBGrid1.Columns(llngColumnCounter).Text) = 1) Then
                    Fields(llngRowCounter + 2, llngVisibleColumnsCounter + 1) = ""
                ElseIf (Len(SSOleDBGrid1.Columns(llngColumnCounter).Text) = 2) Then
                    Fields(llngRowCounter + 2, llngVisibleColumnsCounter + 1) = "X"
                Else
                    Fields(llngRowCounter + 2, llngVisibleColumnsCounter + 1) = SSOleDBGrid1.Columns(llngColumnCounter).Text
                End If
                llngVisibleColumnsCounter = llngVisibleColumnsCounter + 1
            End If
        Next llngColumnCounter
        DoEvents
        If (mCancelled) Then Err.Raise cErrorUserrequestedCancel
        mfrmStatus.prgMain = ((llngRowCounter / llngRows) * 60) + 25
        SSOleDBGrid1.MoveNext
    Next llngRowCounter

    Fields(llngRowCounter + 4, 1) = "NIEUWE LEDEN"
    
    'Copy rows (New Members)
    SSOleDBGrid2.MoveFirst
    For llngRowCounter = 0 To SSOleDBGrid2.Rows - 1
        llngVisibleColumnsCounter = 0
        For llngColumnCounter = 0 To llngColumns
            If (SSOleDBGrid2.Columns(llngColumnCounter).Visible) Then
                If (Len(SSOleDBGrid2.Columns(llngColumnCounter).Text) = 1) Then
                    Fields(llngRowCounter + SSOleDBGrid1.Rows + 6, llngVisibleColumnsCounter + 1) = ""
                ElseIf (Len(SSOleDBGrid2.Columns(llngColumnCounter).Text) = 2) Then
                    Fields(llngRowCounter + SSOleDBGrid1.Rows + 6, llngVisibleColumnsCounter + 1) = "X"
                Else
                    Fields(llngRowCounter + SSOleDBGrid1.Rows + 6, llngVisibleColumnsCounter + 1) = SSOleDBGrid2.Columns(llngColumnCounter).Text
                End If
                llngVisibleColumnsCounter = llngVisibleColumnsCounter + 1
            End If
        Next llngColumnCounter
        DoEvents
        SSOleDBGrid2.MoveNext
    Next llngRowCounter

    Set lxlWorkSheet = mobjExcelExt.DisplayDataInSheet(Fields, Range_Cell1, Range_Cell2, Me.Caption, Me.lblMutatie, True)
    
    mfrmStatus.lblText = "Bezig met het openen van Excel"
    WaitAWhile 5
    mfrmStatus.prgMain = 10
    
    If (lxlWorkSheet Is Nothing) Then Err.Raise cErrorUserrequestedCancel
   
    mfrmStatus.prgMain = 20
   
    'Put Headingformat
    lxlWorkSheet.Range("D4", Mid$(Range_Cell2, 1, 1) & "4").NumberFormat = "00"
    
    mfrmStatus.prgMain = 30
    
    'Autofit columns
    For llngColumnCount = 1 To llngColumns
        lxlWorkSheet.Columns(llngColumnCount).AutoFit
    Next llngColumnCount
        
    mfrmStatus.prgMain = 40
    
    'Display Raster
    With lxlWorkSheet.Range("A4", Range_Cell2)
        With .Borders(xlEdgeLeft)
            .LineStyle = xlContinuous
            .Weight = xlMedium
            .ColorIndex = 1
        End With
        With .Borders(xlEdgeTop)
            .LineStyle = xlContinuous
            .Weight = xlMedium
            .ColorIndex = 1
        End With
        With .Borders(xlEdgeBottom)
            .LineStyle = xlContinuous
            .Weight = xlMedium
            .ColorIndex = 1
        End With
        With .Borders(xlEdgeRight)
            .LineStyle = xlContinuous
            .Weight = xlMedium
            .ColorIndex = 1
        End With
        With .Borders(xlInsideVertical)
            .LineStyle = xlContinuous
            .Weight = xlThin
            .ColorIndex = xlAutomatic
        End With
        With .Borders(xlInsideHorizontal)
            .LineStyle = xlContinuous
            .Weight = xlThin
            .ColorIndex = xlAutomatic
        End With
    End With
   
    mfrmStatus.prgMain = 55
   
    'PageSetup
    With lxlWorkSheet.PageSetup
        .LeftMargin = Application.InchesToPoints(0.196850393700787)
        .RightMargin = Application.InchesToPoints(0.196850393700787)
'        .TopMargin = Application.InchesToPoints(0.984251968503937)
'        .BottomMargin = Application.InchesToPoints(0.984251968503937)
'        .HeaderMargin = Application.InchesToPoints(0.511811023622047)
'        .FooterMargin = Application.InchesToPoints(0.511811023622047)
    End With
    
    mfrmStatus.prgMain = 70
    
    'Center Data
    With lxlWorkSheet.Range("C5", Range_Cell2)
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
    End With
   
    mfrmStatus.prgMain = 95

    Unload mfrmStatus
    Set mfrmStatus = Nothing
   
    Me.Show
        
    'Show excel
    lxlWorkSheet.Application.Visible = True
    
    'Print Preview
    'lxlWorkSheet.PrintPreview
    
    SSOleDBGrid1.Bookmark = lvntLedenBookmark
    SSOleDBGrid2.Bookmark = lvntNieuweLedenBookmark
    
    Mode = kaFormOpen
    
    Set LaadDataInExcel = lxlWorkSheet
    
    Exit Function
    
ErrorHandler:
    Select Case Err.Number
        Case cErrorUserrequestedCancel
            MsgBox "Actie gešnuleerd", vbInformation
            mCancelled = False
        Case Else
            MsgBox "Unspecified Error : " & Err.Description & "(" & Err.Number & " at " & Err.Source & ")"
    End Select

    If (Not mfrmStatus Is Nothing) Then
        Unload mfrmStatus
        Set mfrmStatus = Nothing
    End If
    
    Mode = kaFormOpen
    Me.Visible = True
End Function

Private Sub cmdAnnuleren_Click()
    Unload Me
End Sub

Private Sub cmdNieuweLedenToevoegen_Click()
Dim lfrmNieuweLeden As frmNieuweLeden
    If (lfrmNieuweLeden Is Nothing) Then
        Set lfrmNieuweLeden = New frmNieuweLeden
    End If
    lfrmNieuweLeden.OpenForm mnbMaand, mniJaar, vbModal
    Vernieuwen
End Sub

Private Sub cmdOK_Click()
    cmdToepassen_Click
    Unload Me
End Sub

Private Sub cmdToepassen_Click()
Dim lintColCount As Integer
Dim col As SSDataWidgets_B_OLEDB.Column
On Error GoTo ErrorHandler

    If (DataChanged) Then
        
        Mode = kaFormVerwerking
        
        'Commit changes
        SSOleDBGrid1.Update
        SSOleDBGrid2.Update
        
        'Reset dirty flag
        DataChanged = False
        
        'Update mutation date
        StartUsingRecordset DataEnvironment1.rsMutatie
        DataEnvironment1.rsMutatie("MutatieDatumAanwezigheid").value = Format(Now(), mcSysDateFormat)
        DataEnvironment1.rsMutatie.Update
        SetMutationDate Now()
        
        Mode = kaFormOpen
    End If
    
    Exit Sub

ErrorHandler:
    MsgBox Err.Description, vbExclamation
End Sub

Private Sub SetMutationDate(Optional vntMutatieDatum As Variant)
Dim lvMutatieDatum As Variant

    'Fill mutation date
    If (IsMissing(vntMutatieDatum)) Then
        StartUsingRecordset DataEnvironment1.rsMutatie
        lvMutatieDatum = DataEnvironment1.rsMutatie("MutatieDatumAanwezigheid").value
    Else
        lvMutatieDatum = vntMutatieDatum
    End If
    
    If (IsDate(lvMutatieDatum)) Then
        lblMutatie.Caption = "Laatste wijziging : " + Format(lvMutatieDatum, mcSysDateFormatDisplay)
    Else
        lblMutatie.Caption = "Nog geen wijzigingen gedaan."
    End If
End Sub

Public Sub Vernieuwen()
    Me.MousePointer = vbHourglass
    Mode = kaFormVerversen
    SSOleDBGrid1.Rebind
    SSOleDBGrid2.Rebind
    Mode = kaFormOpen
    Me.MousePointer = vbNormal
End Sub

Private Sub cmdVernieuwen_Click()
    Vernieuwen
End Sub

Private Sub Form_Load()
    Mode = kaFormLaden
    Set mobjExcelExt = New clsExcelExtension
    
    If (mAppMode = kaSimpel Or mAppMode = kaNormaal) Then
        cmdAnnuleren.Caption = "&Sluiten"
        cmdNieuweLedenToevoegen.Visible = False
        cmdOK.Visible = False
        cmdToepassen.Visible = False
        cmdOpenInExcel.Visible = False
        DisableGridEdit
    End If
    If (mAppMode = kaNormaal) Then
        cmdNieuweLedenToevoegen.Visible = False
    End If
    
    InitGridLayout SSOleDBGrid1
    InitGridLayout SSOleDBGrid2
    Mode = kaFormOpen
End Sub

Private Sub DisableGridEdit()
Dim ssColumn As SSDataWidgets_B_OLEDB.Column
    For Each ssColumn In SSOleDBGrid1.Columns
        ssColumn.Locked = True
        ssColumn.BackColor = RGB(192, 192, 192)
    Next ssColumn
    For Each ssColumn In SSOleDBGrid2.Columns
        ssColumn.Locked = True
        ssColumn.BackColor = RGB(192, 192, 192)
    Next ssColumn
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Mode = kaFormOntLaden
    With DataEnvironment1
        .rstblLidAanwezigheid.Filter = ""
        .rstblLidAanwezigheidBestaand.Filter = ""
        .rstblLidAanwezigheidNieuw.Filter = ""
'        EndUsingRecordset .rstblLidAanwezigheid
'        EndUsingRecordset .rstblLidAanwezigheidBestaand
'        EndUsingRecordset .rstblLidAanwezigheidNieuw
    End With
    Set mfrmAanwezigheid = Nothing
    Set mobjExcelExt = Nothing
End Sub

Private Sub mfrmStatus_Cancelled()
    mCancelled = True
End Sub

Private Sub SSOleDBGrid1_BtnClick()
    Mode = kaFormVerwerking
    Set mfrmLidDetail = New frmLidDetail
    mfrmLidDetail.OpenForm SSOleDBGrid1.Columns("LidID").value
    mfrmLidDetail.Show vbModal
    cmdVernieuwen_Click
    Mode = kaFormOpen
End Sub

Private Sub SSOleDBGrid2_BtnClick()
    Mode = kaFormVerwerking
    Set mfrmLidDetail = New frmLidDetail
    mfrmLidDetail.OpenForm SSOleDBGrid2.Columns("LidID").value
    mfrmLidDetail.Show vbModal
    cmdVernieuwen_Click
    Mode = kaFormOpen
End Sub

Private Sub SetColumnHeaders(dbGrid As SSDataWidgets_B_OLEDB.SSOleDBGrid)
Dim llngColumnCounter As Long, llngDagCounter As Long
Dim lintWeekDag As Integer, lintMaandagCount As Integer, lintDinsdagCount As Integer, lintVrijdagCount As Integer, lintZaterdagCount As Integer, lintPositionCounter As Integer
    
'    Mode = kaFormVerwerking
    
    'Hide month & year
    dbGrid.Columns("AanwezigheidID").Visible = False
    dbGrid.Columns("LidID").Visible = False
    dbGrid.Columns("Jaar").Visible = False
    dbGrid.Columns("Maand").Visible = False
    
    'Resize groups
    dbGrid.Groups(1).Width = mclngDateColumnWidth * 4
    dbGrid.Groups(2).Width = mclngDateColumnWidth * 4
    dbGrid.Groups(3).Width = mclngDateColumnWidth * 4
    dbGrid.Groups(4).Width = mclngDateColumnWidth * 4
    
    lintPositionCounter = dbGrid.Columns("Maandag1").Position
    
    'Get date column headers
    For llngDagCounter = 1 To NrOfDaysForThisMonth(mnbMaand, mniJaar)
        lintWeekDag = Weekday(CDate(CStr(mniJaar) + "-" + CStr(mnbMaand) + "-" + CStr(llngDagCounter)))
        Select Case lintWeekDag
            Case vbMonday
                lintMaandagCount = lintMaandagCount + 1
                dbGrid.Columns("Maandag" + CStr(lintMaandagCount)).Caption = "" + Format(llngDagCounter, "00")
                dbGrid.Columns("Maandag" + CStr(lintMaandagCount)).Position = lintPositionCounter
                lintPositionCounter = lintPositionCounter + 1
                dbGrid.Columns("Maandag" + CStr(lintMaandagCount)).Group = lintMaandagCount
                dbGrid.Columns("Maandag" + CStr(lintMaandagCount)).Width = mclngDateColumnWidth
            Case vbTuesday
                lintDinsdagCount = lintDinsdagCount + 1
                dbGrid.Columns("Dinsdag" + CStr(lintDinsdagCount)).Caption = "" + Format(llngDagCounter, "00")
                dbGrid.Columns("Dinsdag" + CStr(lintDinsdagCount)).Position = lintPositionCounter
                lintPositionCounter = lintPositionCounter + 1
                dbGrid.Columns("Dinsdag" + CStr(lintDinsdagCount)).Group = lintDinsdagCount
                dbGrid.Columns("Dinsdag" + CStr(lintDinsdagCount)).Width = mclngDateColumnWidth
            Case vbFriday
                lintVrijdagCount = lintVrijdagCount + 1
                dbGrid.Columns("Vrijdag" + CStr(lintVrijdagCount)).Caption = "" + Format(llngDagCounter, "00")
                dbGrid.Columns("Vrijdag" + CStr(lintVrijdagCount)).Position = lintPositionCounter
                lintPositionCounter = lintPositionCounter + 1
                dbGrid.Columns("Vrijdag" + CStr(lintVrijdagCount)).Group = lintVrijdagCount
                dbGrid.Columns("Vrijdag" + CStr(lintVrijdagCount)).Width = mclngDateColumnWidth
            Case vbSaturday
                lintZaterdagCount = lintZaterdagCount + 1
                dbGrid.Columns("Zaterdag" + CStr(lintZaterdagCount)).Caption = "" + Format(llngDagCounter, "00")
                dbGrid.Columns("Zaterdag" + CStr(lintZaterdagCount)).Position = lintPositionCounter
                lintPositionCounter = lintPositionCounter + 1
                dbGrid.Columns("Zaterdag" + CStr(lintZaterdagCount)).Group = lintZaterdagCount
                dbGrid.Columns("Zaterdag" + CStr(lintZaterdagCount)).Width = mclngDateColumnWidth
        End Select
    Next llngDagCounter
    
    'Hide non-used Date Columns
    If (dbGrid.Columns("Maandag5").Caption = "Maandag5") Then dbGrid.Columns("Maandag5").Visible = False
    If (dbGrid.Columns("Dinsdag5").Caption = "Dinsdag5") Then dbGrid.Columns("Dinsdag5").Visible = False
    If (dbGrid.Columns("Vrijdag5").Caption = "Vrijdag5") Then dbGrid.Columns("Vrijdag5").Visible = False
    If (dbGrid.Columns("Zaterdag5").Caption = "Zaterdag5") Then dbGrid.Columns("Zaterdag5").Visible = False
    
    'Resize last group
    dbGrid.Groups(5).Width = mclngDateColumnWidth * (20 - (lintMaandagCount + lintDinsdagCount + lintVrijdagCount + lintZaterdagCount))

    'Resize first group & columns
    dbGrid.Groups(0).Width = (dbGrid.Width - (dbGrid.Groups(1).Width + dbGrid.Groups(2).Width + dbGrid.Groups(3).Width + dbGrid.Groups(4).Width + dbGrid.Groups(5).Width)) - 600
    dbGrid.Columns("Naam").Width = (dbGrid.Groups(0).Width / 20) * 11
    dbGrid.Columns("Opmerking").Width = (dbGrid.Groups(0).Width / 20) * 4
    dbGrid.Columns("Betaald").Width = (dbGrid.Groups(0).Width / 20) * 4
End Sub

Private Sub SSOleDBGrid1_Change()
    DataChanged = True
End Sub

Private Sub SSOleDBGrid2_Change()
    DataChanged = True
End Sub

Private Function GetBetaald(lngLidID As Long, lngJaar As Long, lngMaand As Long) As Boolean
    StartUsingRecordset DataEnvironment1.rstblLidBetaling
    If (Not DataEnvironment1.rstblLidBetaling.EOF Or Not DataEnvironment1.rstblLidBetaling.BOF) Then
        DataEnvironment1.rstblLidBetaling.MoveFirst
    Else
        GetBetaald = False
        Exit Function
    End If
    DataEnvironment1.rstblLidBetaling.Filter = "Jaar=" & lngJaar
    DataEnvironment1.rstblLidBetaling.Find "LidID=" & lngLidID
    If (Not DataEnvironment1.rstblLidBetaling.EOF) Then
        GetBetaald = DataEnvironment1.rstblLidBetaling.Fields(MaandNaam(CInt(lngMaand))).value
    Else
        GetBetaald = GetBetaald
    End If
    DataEnvironment1.rstblLidBetaling.Filter = ""
    DataEnvironment1.rstblLidBetaling.MoveFirst
End Function

Private Sub UnboundPositionData(rs As ADODB.Recordset, StartLocation As Variant, ByVal NumberOfRowsToMove As Long, NewLocation As Variant)
    With rs
        If (IsNull(StartLocation)) Then
            If (NumberOfRowsToMove = 0) Then
                Exit Sub
            ElseIf (NumberOfRowsToMove < 0) Then
                .MoveLast
            Else
                .MoveFirst
            End If
        Else
            .Bookmark = StartLocation
        End If
        
        .Move NumberOfRowsToMove
        
        NewLocation = .Bookmark
    End With
End Sub

Private Sub UnboundReadData(dbGrid As SSDataWidgets_B_OLEDB.SSOleDBGrid, rs As ADODB.Recordset, ByVal RowBuf As SSDataWidgets_B_OLEDB.ssRowBuffer, StartLocation As Variant, ByVal ReadPriorRows As Boolean)
Dim lintRBRow As Integer
Dim lintGridRows As Integer

    lintGridRows = 0
    
    With rs
        StartUsingRecordset rs, False, False
        
        If (.BOF And .EOF) Then
            dbGrid.AllowAddNew = False
            Exit Sub
        End If
        
        If (IsNull(StartLocation)) Then
            If (ReadPriorRows) Then
                .MoveLast
            Else
                .MoveFirst
            End If
        Else
            .Bookmark = StartLocation
            If (ReadPriorRows) Then
                .MovePrevious
            Else
                .MoveNext
            End If
        End If
    
        For lintRBRow = 0 To RowBuf.RowCount - 1
            If (.BOF Or .EOF) Then
                dbGrid.AllowAddNew = False
                Exit For
            End If
            Select Case RowBuf.ReadType
                Case ssReadTypeAllData
                    RowBuf.Bookmark(lintRBRow) = .Bookmark
                    
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaAanwezigheidID) = .Fields("AanwezigheidID").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaLidID) = .Fields("LidID").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaNaam) = .Fields("Naam").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaJaar) = .Fields("Jaar").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaMaand) = .Fields("Maand").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaBetaald) = GetBetaald(.Fields("LidID").value, .Fields("Jaar").value, .Fields("Maand").value)
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaOpmerking) = .Fields("Opmerking").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaMaandag1) = .Fields("Maandag1").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaDinsdag1) = .Fields("Dinsdag1").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaVrijdag1) = .Fields("Vrijdag1").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaZaterdag1) = .Fields("Zaterdag1").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaMaandag2) = .Fields("Maandag2").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaDinsdag2) = .Fields("Dinsdag2").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaVrijdag2) = .Fields("Vrijdag2").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaZaterdag2) = .Fields("Zaterdag2").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaMaandag3) = .Fields("Maandag3").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaDinsdag3) = .Fields("Dinsdag3").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaVrijdag3) = .Fields("Vrijdag3").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaZaterdag3) = .Fields("Zaterdag3").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaMaandag4) = .Fields("Maandag4").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaDinsdag4) = .Fields("Dinsdag4").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaVrijdag4) = .Fields("Vrijdag4").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaZaterdag4) = .Fields("Zaterdag4").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaMaandag5) = .Fields("Maandag5").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaDinsdag5) = .Fields("Dinsdag5").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaVrijdag5) = .Fields("Vrijdag5").value
                    RowBuf.value(lintRBRow, kaAanwezigheidKolom.kaZaterdag5) = .Fields("Zaterdag5").value
                Case ssReadTypeBookmarkOnly
                    RowBuf.Bookmark(lintRBRow) = .Bookmark
            End Select
            
            If (ReadPriorRows) Then
                .MovePrevious
            Else
                .MoveNext
            End If
            
            lintGridRows = lintGridRows + 1
        Next lintRBRow
        
        RowBuf.RowCount = lintGridRows
    End With
End Sub

Private Sub UnboundWriteData(dbGrid As SSDataWidgets_B_OLEDB.SSOleDBGrid, rs As ADODB.Recordset, ByVal RowBuf As SSDataWidgets_B_OLEDB.ssRowBuffer, WriteLocation As Variant)
Dim lintWeekCount As Integer

    With rs
        .Bookmark = WriteLocation
        .Fields("Opmerking").value = IIf(Len(dbGrid.Columns("Opmerking").value) = 0, Null, dbGrid.Columns("Opmerking").value)
        For lintWeekCount = 1 To 5
            .Fields("Maandag" & lintWeekCount).value = dbGrid.Columns("Maandag" & lintWeekCount).value
            .Fields("Dinsdag" & lintWeekCount).value = dbGrid.Columns("Dinsdag" & lintWeekCount).value
            .Fields("Vrijdag" & lintWeekCount).value = dbGrid.Columns("Vrijdag" & lintWeekCount).value
            .Fields("Zaterdag" & lintWeekCount).value = dbGrid.Columns("Zaterdag" & lintWeekCount).value
        Next lintWeekCount
        .Update
    End With
End Sub

Private Sub SSOleDBGrid1_UnboundPositionData(StartLocation As Variant, ByVal NumberOfRowsToMove As Long, NewLocation As Variant)
    UnboundPositionData DataEnvironment1.rstblLidAanwezigheidBestaand, StartLocation, NumberOfRowsToMove, NewLocation
End Sub

Private Sub SSOleDBGrid1_UnboundReadData(ByVal RowBuf As SSDataWidgets_B_OLEDB.ssRowBuffer, StartLocation As Variant, ByVal ReadPriorRows As Boolean)
    UnboundReadData SSOleDBGrid1, DataEnvironment1.rstblLidAanwezigheidBestaand, RowBuf, StartLocation, ReadPriorRows
End Sub

Private Sub SSOleDBGrid1_UnboundWriteData(ByVal RowBuf As SSDataWidgets_B_OLEDB.ssRowBuffer, WriteLocation As Variant)
    UnboundWriteData SSOleDBGrid1, DataEnvironment1.rstblLidAanwezigheidBestaand, RowBuf, WriteLocation
End Sub

Private Sub SSOleDBGrid2_UnboundPositionData(StartLocation As Variant, ByVal NumberOfRowsToMove As Long, NewLocation As Variant)
    UnboundPositionData DataEnvironment1.rstblLidAanwezigheidNieuw, StartLocation, NumberOfRowsToMove, NewLocation
End Sub

Private Sub SSOleDBGrid2_UnboundReadData(ByVal RowBuf As SSDataWidgets_B_OLEDB.ssRowBuffer, StartLocation As Variant, ByVal ReadPriorRows As Boolean)
    UnboundReadData SSOleDBGrid2, DataEnvironment1.rstblLidAanwezigheidNieuw, RowBuf, StartLocation, ReadPriorRows
End Sub

Private Sub SSOleDBGrid2_UnboundWriteData(ByVal RowBuf As SSDataWidgets_B_OLEDB.ssRowBuffer, WriteLocation As Variant)
    UnboundWriteData SSOleDBGrid2, DataEnvironment1.rstblLidAanwezigheidNieuw, RowBuf, WriteLocation
End Sub

Public Sub SetButtons(Mode As kaFormMode)
    Select Case Mode
        Case kaFormMode.kaFormLaden, kaFormMode.kaFormOntLaden
            Me.cmdAfdrukken.Enabled = False
            Me.cmdAnnuleren.Enabled = False
            Me.cmdOK.Enabled = False
            Me.cmdToepassen.Enabled = False
            Me.cmdVernieuwen.Enabled = False
            Me.cmdNieuweLedenToevoegen.Enabled = False
        Case kaFormMode.kaFormOpen
            Me.cmdAfdrukken.Enabled = True
            Me.cmdAnnuleren.Enabled = True
            Me.cmdOK.Enabled = True
            Me.cmdVernieuwen.Enabled = True
            Me.cmdNieuweLedenToevoegen.Enabled = True
        Case kaFormMode.kaFormVerversen, kaFormMode.kaFormVerwerking
            Me.cmdAfdrukken.Enabled = False
            Me.cmdAnnuleren.Enabled = True
            Me.cmdOK.Enabled = False
            Me.cmdVernieuwen.Enabled = False
            Me.cmdNieuweLedenToevoegen.Enabled = False
    End Select
    DoEvents
End Sub

Public Sub SetMenu(Mode As kaFormMode)
    Select Case Mode
        Case kaFormMode.kaFormLaden
            mfrmMDIMain.mnuAanLijst.Enabled = False
            mfrmMDIMain.mnuAanVernieuwen.Enabled = False
            mfrmMDIMain.mnuAanAfdrukken.Enabled = False
            mfrmMDIMain.mnuAanwezigheid.Enabled = False
        Case kaFormMode.kaFormOpen
            mfrmMDIMain.mnuAanLijst.Enabled = False
            mfrmMDIMain.mnuAanVernieuwen.Enabled = True
            mfrmMDIMain.mnuAanAfdrukken.Enabled = True
            mfrmMDIMain.mnuAanwezigheid.Enabled = True
        Case kaFormMode.kaFormVerversen, kaFormMode.kaFormVerwerking
            mfrmMDIMain.mnuAanLijst.Enabled = False
            mfrmMDIMain.mnuAanVernieuwen.Enabled = False
            mfrmMDIMain.mnuAanAfdrukken.Enabled = False
            mfrmMDIMain.mnuAanwezigheid.Enabled = True
        Case kaFormMode.kaFormOntLaden
            mfrmMDIMain.mnuAanLijst.Enabled = True
            mfrmMDIMain.mnuAanVernieuwen.Enabled = False
            mfrmMDIMain.mnuAanAfdrukken.Enabled = False
            mfrmMDIMain.mnuAanwezigheid.Enabled = True
    End Select
    DoEvents
End Sub

'Private Sub SSOleDBGrid1_UnboundAddData(ByVal RowBuf As SSDataWidgets_B_OLEDB.ssRowBuffer, NewRowBookmark As Variant)
'    With DataEnvironment1.rstblLidAanwezigheidBestaand
'        .AddNew
'        .Fields("LidID").value = RowBuf.value(0, akLidID)
'        .Fields("Naam").value = RowBuf.value(0, akNaam)
'        .Fields("Jaar").value = RowBuf.value(0, akJaar)
'        .Fields("Maand").value = RowBuf.value(0, akMaand)
'        .Fields("Opmerking").value = RowBuf.value(0, akOpmerking)
'        .Fields("Maandag1").value = RowBuf.value(0, akMaandag1)
'        .Fields("Dinsdag1").value = RowBuf.value(0, akDinsdag1)
'        .Fields("Vrijdag1").value = RowBuf.value(0, akVrijdag1)
'        .Fields("Zaterdag1").value = RowBuf.value(0, akZaterdag1)
'        .Fields("Maandag2").value = RowBuf.value(0, akMaandag2)
'        .Fields("Dinsdag2").value = RowBuf.value(0, akDinsdag2)
'        .Fields("Vrijdag2").value = RowBuf.value(0, akVrijdag2)
'        .Fields("Zaterdag2").value = RowBuf.value(0, akZaterdag2)
'        .Fields("Maandag3").value = RowBuf.value(0, akMaandag3)
'        .Fields("Dinsdag3").value = RowBuf.value(0, akDinsdag3)
'        .Fields("Vrijdag3").value = RowBuf.value(0, akVrijdag3)
'        .Fields("Zaterdag3").value = RowBuf.value(0, akZaterdag3)
'        .Fields("Maandag4").value = RowBuf.value(0, akMaandag4)
'        .Fields("Dinsdag4").value = RowBuf.value(0, akDinsdag4)
'        .Fields("Vrijdag4").value = RowBuf.value(0, akVrijdag4)
'        .Fields("Zaterdag4").value = RowBuf.value(0, akZaterdag4)
'        .Fields("Maandag5").value = RowBuf.value(0, akMaandag5)
'        .Fields("Dinsdag5").value = RowBuf.value(0, akDinsdag5)
'        .Fields("Vrijdag5").value = RowBuf.value(0, akVrijdag5)
'        .Fields("Zaterdag5").value = RowBuf.value(0, akZaterdag5)
'        .Update
'        .MoveLast
'        NewRowBookmark = .Bookmark
'    End With
'End Sub

'Private Sub SSOleDBGrid1_UnboundDeleteRow(Bookmark As Variant)
'    With DataEnvironment1.rstblLidAanwezigheidBestaand
'        .Bookmark = Bookmark
'        .Delete adAffectCurrent
'    End With
'End Sub

