VERSION 5.00
Begin VB.Form frmRapporten 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Rapporten"
   ClientHeight    =   1995
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6435
   Icon            =   "frmRapporten.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1995
   ScaleWidth      =   6435
   Begin VB.CommandButton cmdManVrouw 
      Caption         =   "Kengetallen voor de "
      Height          =   495
      Left            =   1440
      TabIndex        =   2
      Top             =   840
      Width           =   3735
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Default         =   -1  'True
      Height          =   375
      Left            =   5040
      TabIndex        =   1
      Top             =   1560
      Width           =   1335
   End
   Begin VB.Line Line1 
      X1              =   6360
      X2              =   120
      Y1              =   1440
      Y2              =   1440
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   240
      Picture         =   "frmRapporten.frx":030A
      Top             =   200
      Width           =   480
   End
   Begin VB.Label lblTitel 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   6255
   End
End
Attribute VB_Name = "frmRapporten"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Option Base 1

Private Sub optRapportGroep_Click(Index As Integer)
    ControleerGroepen
End Sub

Private Sub cmdOk_Click()
    Unload Me
    Set mfrmStatistieken = Nothing
End Sub

Private Sub Form_Load()
Dim lvntGraad As Variant
    
    lblTitel.Caption = vbCrLf + Me.Caption
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set mfrmStatistieken = Nothing
End Sub
