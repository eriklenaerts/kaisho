VERSION 5.00
Begin VB.Form frmStatistieken 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Statistieken"
   ClientHeight    =   3240
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6435
   Icon            =   "frmStatistieken.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3240
   ScaleWidth      =   6435
   Begin VB.CommandButton cmdKengetallenBond 
      Caption         =   "&Kengetallen voor d'n bond"
      Height          =   495
      Left            =   1440
      TabIndex        =   4
      Top             =   2040
      Width           =   3735
   End
   Begin VB.CommandButton cmdGraden 
      Caption         =   "Toon verhouding van de Graden"
      Height          =   495
      Left            =   1440
      TabIndex        =   2
      Top             =   1440
      Width           =   3735
   End
   Begin VB.CommandButton cmdManVrouw 
      Caption         =   "Toon verhouding tussen mannen && vrouwen"
      Height          =   495
      Left            =   1440
      TabIndex        =   1
      Top             =   840
      Width           =   3735
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Default         =   -1  'True
      Height          =   375
      Left            =   5040
      TabIndex        =   3
      Top             =   2760
      Width           =   1335
   End
   Begin VB.Line Line1 
      X1              =   6360
      X2              =   120
      Y1              =   2640
      Y2              =   2640
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   240
      Picture         =   "frmStatistieken.frx":030A
      Top             =   210
      Width           =   480
   End
   Begin VB.Label lblTitel 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   612
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   6252
   End
End
Attribute VB_Name = "frmStatistieken"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Option Base 1

Private mobjExcelExt As clsExcelExtension
Private WithEvents mfrmStatus As frmStatus
Attribute mfrmStatus.VB_VarHelpID = -1
Private mcastrGraden(19) As kaGraad
Private mCancelled As Boolean

Private Sub cmdKengetallenBond_Click()
Dim lvntData As Variant
Dim lxlChart As Excel.Chart
Dim lxlPoint As Excel.Point
Dim liTeller As Integer

On Error GoTo ErrorHandler

    'Display message
    Set mfrmStatus = New frmStatus
    mfrmStatus.lblText = "Bezig met voorbereiden van Microsoft Excel, even geduld a.u.b."
    mfrmStatus.OpenForm kaZonderProgress
    WaitAWhile 10

    lvntData = GetKengetallenBondStats
    If (IsNull(lvntData)) Then Exit Sub
    If mCancelled Then Err.Raise cErrorUserrequestedCancel
    
    Set lxlChart = mobjExcelExt.DisplayDataInChart(lvntData, xl3DColumnStacked)
    If mCancelled Then Err.Raise cErrorUserrequestedCancel
    lxlChart.ApplyDataLabels xlDataLabelsShowValue
    lxlChart.ChartGroups(1).VaryByCategories = True
    lxlChart.Axes(xlCategory).TickLabelPosition = xlNone
    lxlChart.Application.Visible = True
    
    Unload mfrmStatus
    Set mfrmStatus = Nothing
    
    Exit Sub
    
ErrorHandler:
    Select Case Err.Number
        Case cErrorUserrequestedCancel
            MsgBox "Actie gešnuleerd", vbInformation
            mCancelled = False
        Case Else
            MsgBox "Unspecified Error : " & Err.Description & "(" & Err.Number & " at " & Err.Source & ")"
    End Select

    If (Not mfrmStatus Is Nothing) Then
        Unload mfrmStatus
        Set mfrmStatus = Nothing
    End If
End Sub

Private Sub mfrmStatus_Cancelled()
    mCancelled = True
End Sub

Private Sub cmdGraden_Click()
Dim lvntData As Variant
Dim lxlChart As Excel.Chart
Dim lxlPoint As Excel.Point
Dim liTeller As Integer

On Error GoTo ErrorHandler

    'Display message
    Set mfrmStatus = New frmStatus
    mfrmStatus.lblText = "Bezig met voorbereiden van Microsoft Excel, even geduld a.u.b."
    mfrmStatus.OpenForm kaZonderProgress
    WaitAWhile 10

    lvntData = GetGraadStats
    If mCancelled Then Err.Raise cErrorUserrequestedCancel
    
    Set lxlChart = mobjExcelExt.DisplayDataInChart(lvntData, xlColumnStacked)
    If mCancelled Then Err.Raise cErrorUserrequestedCancel
    lxlChart.ApplyDataLabels xlDataLabelsShowLabel
    For Each lxlPoint In lxlChart.SeriesCollection(1).Points
        For liTeller = LBound(mcastrGraden) To UBound(mcastrGraden)
            If (lxlPoint.DataLabel.text = mcastrGraden(liTeller).Naam) Then
                lxlPoint.Interior.Color = mcastrGraden(liTeller).Kleur
                lxlPoint.DataLabel.Interior.Color = 16777215
                If mCancelled Then Err.Raise cErrorUserrequestedCancel
                Exit For
            End If
        Next
    Next lxlPoint

    lxlChart.Application.Visible = True

    Unload mfrmStatus
    Set mfrmStatus = Nothing

    Exit Sub
    
ErrorHandler:
    Select Case Err.Number
        Case cErrorUserrequestedCancel
            MsgBox "Actie gešnuleerd", vbInformation
            mCancelled = False
        Case Else
            MsgBox "Unspecified Error : " & Err.Description & " (" & Err.Number & " at " & Err.Source & ")"
    End Select

    If (Not mfrmStatus Is Nothing) Then
        Unload mfrmStatus
        Set mfrmStatus = Nothing
    End If
End Sub

Private Sub cmdManVrouw_Click()
Dim lvntData As Variant
Dim lxlChart As Excel.Chart
Dim llngNrOfMen As Long, llngNrOfWomen As Long

On Error GoTo ErrorHandler

    'Display message
    Set mfrmStatus = New frmStatus
    mfrmStatus.lblText = "Bezig met voorbereiden van Microsoft Excel, even geduld a.u.b."
    mfrmStatus.OpenForm kaZonderProgress
    WaitAWhile 10

    'Get data
    GetPersonStats llngNrOfMen, llngNrOfWomen
    If mCancelled Then Err.Raise cErrorUserrequestedCancel
    
    If (llngNrOfMen = 0 And llngNrOfWomen = 0) Then
        MsgBox "Er zijn problemen tijdens het ophalen van de data.", vbExclamation
    Else
        'Build variant array
        ReDim lvntData(2, 2)
        lvntData(1, 1) = "Man"
        lvntData(1, 2) = "Vrouw"
        
        lvntData(2, 1) = llngNrOfMen
        lvntData(2, 2) = llngNrOfWomen
        
        Set lxlChart = mobjExcelExt.DisplayDataInChart(lvntData, xlPie)
        If mCancelled Then Err.Raise cErrorUserrequestedCancel
        lxlChart.ApplyDataLabels xlDataLabelsShowLabelAndPercent
        lxlChart.Application.Visible = True
    End If
    
    Unload mfrmStatus
    Set mfrmStatus = Nothing
    
    Exit Sub
    
ErrorHandler:
    Select Case Err.Number
        Case cErrorUserrequestedCancel
            MsgBox "Actie gešnuleerd", vbInformation
            mCancelled = False
        Case Else
            MsgBox "Unspecified Error : " & Err.Description & "(" & Err.Number & " at " & Err.Source & ")"
    End Select

    If (Not mfrmStatus Is Nothing) Then
        Unload mfrmStatus
        Set mfrmStatus = Nothing
    End If
End Sub

Private Sub cmdOk_Click()
    Unload Me
    Set mfrmStatistieken = Nothing
End Sub

Public Sub OpenForm(Optional Mode As kaStatistieken = kaGeenStat)
Dim lvntGraad As Variant
    GetGraadTitels
    
    Set mobjExcelExt = New clsExcelExtension
    
    lblTitel.Caption = vbCrLf + Me.Caption
    
    Select Case Mode
        Case kaStatistieken.kaManVrouw
            cmdManVrouw_Click
        Case kaStatistieken.kaGraden
            cmdGraden_Click
    End Select
    Me.ZOrder
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set mfrmStatistieken = Nothing
    Set mobjExcelExt = Nothing
End Sub

Private Sub GetPersonStats(ByRef lngNrOfMen As Long, ByRef lngNrOfWomen As Long)
Dim lvntGeslacht As Variant

    'Get men/women query from DB
    StartUsingRecordset DataEnvironment1.rsGeslachtStat
    lvntGeslacht = DataEnvironment1.rsGeslachtStat.GetRows
    lngNrOfMen = CLng(lvntGeslacht(1, 0))
    lngNrOfWomen = CLng(lvntGeslacht(1, 1))
End Sub

Private Function GetGraadStats() As Variant
Dim lvntGraad As Variant
Dim llngColumnCount As Long

    StartUsingRecordset DataEnvironment1.rsGraadStat
    lvntGraad = DataEnvironment1.rsGraadStat.GetRows
    
    'Layout Header Column
    For llngColumnCount = LBound(lvntGraad, 2) To UBound(lvntGraad, 2)
        lvntGraad(LBound(lvntGraad, 1), llngColumnCount) = mcastrGraden(GetGraad(lvntGraad(LBound(lvntGraad, 1), llngColumnCount))).Naam
    Next llngColumnCount
    
    GetGraadStats = lvntGraad
End Function

Private Function GetKengetallenBondStats() As Variant
Dim lvntKengetallen(2, 12) As Variant
Dim rs As New ADODB.Recordset
Dim lstrSQL As String
    
On Error GoTo ErrorHandler
    
    If (DataEnvironment1.Connection1.State = adStateClosed) Then DataEnvironment1.Connection1.Open
    
    lstrSQL = "SELECT * from tblLid WHERE Inactief=False AND Woonplaats LIKE '%Brasschaat%' and Year(Now())-Year([tblLid].[Geboortedatum]) < 18"
    rs.Open lstrSQL, DataEnvironment1.Connection1
    lvntKengetallen(1, 1) = "Jonger dan 18 jaar wonend in Brasschaat"
    lvntKengetallen(2, 1) = rs.RecordCount
    rs.Close
    
    lstrSQL = "SELECT * from tblLid WHERE Inactief=False AND Woonplaats NOT LIKE '%Brasschaat%' and Year(Now())-Year([tblLid].[Geboortedatum]) < 18"
    rs.Open lstrSQL, DataEnvironment1.Connection1
    lvntKengetallen(1, 2) = "Jonger dan 18 jaar wonend buiten Brasschaat"
    lvntKengetallen(2, 2) = rs.RecordCount
    rs.Close

    lstrSQL = "SELECT * from tblLid WHERE Inactief=False AND Woonplaats LIKE '%Brasschaat%' and Year(Now())-Year([tblLid].[Geboortedatum]) >= 18"
    rs.Open lstrSQL, DataEnvironment1.Connection1
    lvntKengetallen(1, 3) = "Ouder dan 18 jaar wonend in Brasschaat"
    lvntKengetallen(2, 3) = rs.RecordCount
    rs.Close

    lstrSQL = "SELECT * from tblLid WHERE Inactief=False AND Woonplaats NOT LIKE '%Brasschaat%' and Year(Now())-Year([tblLid].[Geboortedatum]) >= 18"
    rs.Open lstrSQL, DataEnvironment1.Connection1
    lvntKengetallen(1, 4) = "Ouder dan 18 jaar wonend buiten Brasschaat"
    lvntKengetallen(2, 4) = rs.RecordCount
    rs.Close
    
    lstrSQL = "SELECT * from tblLid WHERE Inactief=False AND Woonplaats LIKE '%Brasschaat%' and Geslacht = 'M'"
    rs.Open lstrSQL, DataEnvironment1.Connection1
    lvntKengetallen(1, 5) = "Mannen wonend in Brasschaat"
    lvntKengetallen(2, 5) = rs.RecordCount
    rs.Close

    lstrSQL = "SELECT * from tblLid WHERE Inactief=False AND Woonplaats NOT LIKE '%Brasschaat%' and Geslacht = 'M'"
    rs.Open lstrSQL, DataEnvironment1.Connection1
    lvntKengetallen(1, 6) = "Mannen wonend buiten Brasschaat"
    lvntKengetallen(2, 6) = rs.RecordCount
    rs.Close

    lstrSQL = "SELECT * from tblLid WHERE Inactief=False AND Woonplaats LIKE '%Brasschaat%' and Geslacht = 'V'"
    rs.Open lstrSQL, DataEnvironment1.Connection1
    lvntKengetallen(1, 7) = "Vrouwen wonend in Brasschaat"
    lvntKengetallen(2, 7) = rs.RecordCount
    rs.Close

    lstrSQL = "SELECT * from tblLid WHERE Inactief=False AND Woonplaats NOT LIKE '%Brasschaat%' and Geslacht = 'V'"
    rs.Open lstrSQL, DataEnvironment1.Connection1
    lvntKengetallen(1, 8) = "Vrouwen wonend buiten Brasschaat"
    lvntKengetallen(2, 8) = rs.RecordCount
    rs.Close

    lstrSQL = "SELECT * from tblLid WHERE Inactief=False AND Woonplaats LIKE '%Brasschaat%' and Year(Now())-Year([tblLid].[Geboortedatum]) < 18 and Geslacht ='M'"
    rs.Open lstrSQL, DataEnvironment1.Connection1
    lvntKengetallen(1, 9) = "Jongens jonger dan 18 jaar wonend in Brasschaat"
    lvntKengetallen(2, 9) = rs.RecordCount
    rs.Close

    lstrSQL = "SELECT * from tblLid WHERE Inactief=False AND Woonplaats NOT LIKE '%Brasschaat%' and Year(Now())-Year([tblLid].[Geboortedatum]) < 18 and Geslacht ='M'"
    rs.Open lstrSQL, DataEnvironment1.Connection1
    lvntKengetallen(1, 10) = "Jongens jonger dan 18 jaar wonend buiten Brasschaat"
    lvntKengetallen(2, 10) = rs.RecordCount
    rs.Close

    lstrSQL = "SELECT * from tblLid WHERE Inactief=False AND Woonplaats LIKE '%Brasschaat%' and Year(Now())-Year([tblLid].[Geboortedatum]) < 18 and Geslacht ='v'"
    rs.Open lstrSQL, DataEnvironment1.Connection1
    lvntKengetallen(1, 11) = "Meisjes jonger dan 18 jaar wonend in Brasschaat"
    lvntKengetallen(2, 11) = rs.RecordCount
    rs.Close

    lstrSQL = "SELECT * from tblLid WHERE Inactief=False AND Woonplaats NOT LIKE '%Brasschaat%' and Year(Now())-Year([tblLid].[Geboortedatum]) < 18 and Geslacht ='V'"
    rs.Open lstrSQL, DataEnvironment1.Connection1
    lvntKengetallen(1, 12) = "Meisjes jonger dan 18 jaar wonend buiten Brasschaat"
    lvntKengetallen(2, 12) = rs.RecordCount
    rs.Close
    
    GetKengetallenBondStats = lvntKengetallen
    Exit Function

ErrorHandler:
    MsgBox "Fout tijdens het ophalen van de kengetallen, contacteer Erik Lenaerts en vermeld volgende foutboodschap : " & Err.Description & " (" & Err.Number & ")"
    GetKengetallenBondStats = Null
End Function

Private Function GetGraad(ByVal strGraad As String) As Long
    GetGraad = CLng(Mid$(strGraad, 1, 2))
End Function

Private Sub GetGraadTitels()
Dim llngPos As Long

    For llngPos = LBound(mcastrGraden) To UBound(mcastrGraden)
        Select Case llngPos
            Case 1
                mcastrGraden(llngPos).Naam = "Beginner"
            Case 2 To 9
                mcastrGraden(llngPos).Naam = CStr(Abs(10 - llngPos)) + " Kyu"
            Case 10 To 19
                mcastrGraden(llngPos).Naam = CStr(llngPos - 9) + " Dan"
        End Select
        Select Case llngPos
            Case 1 To 4
                mcastrGraden(llngPos).Kleur = 16777215
            Case 5 To 6
                mcastrGraden(llngPos).Kleur = 16711680
            Case 7 To 9
                mcastrGraden(llngPos).Kleur = 2113706
            Case Else
                mcastrGraden(llngPos).Kleur = 0
                '0
        End Select
    Next llngPos
End Sub


'Private Sub ToonGraden(vntFields As Variant)
'Dim lvntFieldsField As Variant
'Dim llngColCounter As Long, llngGraad As Long
'
'    With chtGraden
'        ' Displays a 3d chart with 8 columns and 8 rows
'        ' data.
'        .Title = "Verhouding graden"
'        .ChartType = VtChChartType2dBar
'        .ColumnCount = UBound(vntFields, 2) + 1
'        .RowCount = 1
'        .RowLabel = "Gr"
'
'        For llngColCounter = LBound(vntFields, 2) To UBound(vntFields, 2)
'            llngGraad = GetGraad(vntFields(0, llngColCounter))
'            .Column = llngColCounter + 1
'            .Row = 1
'            .Data = CLng(vntFields(1, llngColCounter))
'            .ColumnLabel = mcastrGraden(llngGraad).Naam
'            With chtGraden.Plot.SeriesCollection(llngColCounter + 1).DataPoints(-1)
'                .Brush.Style = VtBrushStyleSolid
'                .Brush.FillColor.Red = mcastrGraden(llngGraad).Kleur.Rood
'                .Brush.FillColor.Green = mcastrGraden(llngGraad).Kleur.Groen
'                .Brush.FillColor.Blue = mcastrGraden(llngGraad).Kleur.Blauw
'            End With
'        Next llngColCounter
'
'        .ShowLegend = True
'    End With
'End Sub
'
'
'Private Sub ToonManVrouwGrafiek(lngMen As Long, lngWomen As Long)
'Dim llngTotalNrOfPersons As Long, llngMenPercentage As Long, llngWomenPercentage As Long
'
'    llngTotalNrOfPersons = lngMen + lngWomen
'
'    llngMenPercentage = (lngMen / llngTotalNrOfPersons) * 100
'    llngWomenPercentage = (lngWomen / llngTotalNrOfPersons) * 100
'
'    With chtManVrouw
'        ' Displays a 3d chart with 8 columns and 8 rows
'        ' data.
'        .Title = "Verhouding man - vrouw"
'        .ChartType = VtChChartType2dPie
'        .ColumnCount = 2
'        .RowCount = 1
'
'        .Column = 1
'        .Row = 1
'        .Data = llngMenPercentage
'        .ColumnLabel = CStr(lngMen) + " Man" + IIf(lngMen > 1, "nen", "") + " - " + CStr(llngMenPercentage) + "%"
'
'        .Column = 2
'        .Row = 1
'        .Data = llngWomenPercentage
'        .ColumnLabel = CStr(lngWomen) + " Vrouw" + IIf(lngWomen > 1, "en", "") + " - " + CStr(llngWomenPercentage) + "%"
'
'        .ShowLegend = True
'    End With
'End Sub
'
'
