VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmExamen 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Examen"
   ClientHeight    =   5280
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10665
   Icon            =   "frmExamen.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5280
   ScaleWidth      =   10665
   Begin VB.CommandButton cmdCertificatenLijst 
      Caption         =   "&Examen lijst ..."
      Height          =   372
      Left            =   1920
      TabIndex        =   16
      ToolTipText     =   "Deze functionaliteit is nog niet volledigf operationeel"
      Top             =   4800
      Width           =   2172
   End
   Begin VB.CommandButton cmdToepassen 
      Caption         =   "&Toepassen"
      Enabled         =   0   'False
      Height          =   375
      Left            =   7800
      TabIndex        =   19
      ToolTipText     =   "Voer alle wijzigingen door"
      Top             =   4800
      Width           =   1335
   End
   Begin VB.Frame fraDatum 
      Caption         =   "Algemeen"
      Height          =   1332
      Left            =   120
      TabIndex        =   24
      Top             =   840
      Width           =   4452
      Begin VB.TextBox txtExamenPlaats 
         Height          =   288
         Left            =   2760
         TabIndex        =   1
         Text            =   "Text1"
         Top             =   840
         Width           =   1572
      End
      Begin MSComCtl2.DTPicker dtpExamenDatum 
         Height          =   372
         Left            =   2760
         TabIndex        =   0
         Top             =   360
         Width           =   1572
         _ExtentX        =   2778
         _ExtentY        =   661
         _Version        =   393216
         Format          =   22740993
         CurrentDate     =   36553
      End
      Begin VB.Label Label3 
         Caption         =   "Geef de plaats van het examen :"
         Height          =   252
         Left            =   120
         TabIndex        =   26
         Top             =   840
         Width           =   2412
      End
      Begin VB.Label Label1 
         Caption         =   "Geef de datum van het examen op :"
         Height          =   372
         Left            =   120
         TabIndex        =   25
         Top             =   360
         Width           =   2652
      End
   End
   Begin VB.Frame fraGraden 
      Caption         =   " Graad "
      Height          =   2295
      Left            =   120
      TabIndex        =   22
      Top             =   2280
      Width           =   4452
      Begin VB.CheckBox chkAlleLedenTonen 
         Caption         =   "&Alle leden tonen"
         Height          =   255
         Left            =   240
         TabIndex        =   27
         Top             =   1920
         Width           =   2415
      End
      Begin VB.OptionButton optGraad 
         Caption         =   "5 Dan"
         Height          =   375
         Index           =   13
         Left            =   3360
         TabIndex        =   14
         Top             =   360
         Width           =   975
      End
      Begin VB.OptionButton optGraad 
         Caption         =   "4 Dan"
         Height          =   375
         Index           =   12
         Left            =   2280
         TabIndex        =   13
         Top             =   1440
         Width           =   975
      End
      Begin VB.OptionButton optGraad 
         Caption         =   "3 Dan"
         Height          =   375
         Index           =   11
         Left            =   2280
         TabIndex        =   12
         Top             =   1080
         Width           =   975
      End
      Begin VB.OptionButton optGraad 
         Caption         =   "2 Dan"
         Height          =   375
         Index           =   10
         Left            =   2280
         TabIndex        =   11
         Top             =   720
         Width           =   975
      End
      Begin VB.OptionButton optGraad 
         Caption         =   "1 Dan"
         Height          =   375
         Index           =   9
         Left            =   2280
         TabIndex        =   10
         Top             =   360
         Width           =   975
      End
      Begin VB.OptionButton optGraad 
         Caption         =   "1 Kyu"
         Height          =   375
         Index           =   8
         Left            =   1200
         TabIndex        =   9
         Top             =   1440
         Width           =   975
      End
      Begin VB.OptionButton optGraad 
         Caption         =   "2 Kyu"
         Height          =   375
         Index           =   7
         Left            =   1200
         TabIndex        =   8
         Top             =   1080
         Width           =   975
      End
      Begin VB.OptionButton optGraad 
         Caption         =   "3 Kyu"
         Height          =   375
         Index           =   6
         Left            =   1200
         TabIndex        =   7
         Top             =   720
         Width           =   975
      End
      Begin VB.OptionButton optGraad 
         Caption         =   "4 Kyu"
         Height          =   375
         Index           =   5
         Left            =   1200
         TabIndex        =   6
         Top             =   360
         Width           =   975
      End
      Begin VB.OptionButton optGraad 
         Caption         =   "5 Kyu"
         Height          =   375
         Index           =   4
         Left            =   240
         TabIndex        =   5
         Top             =   1440
         Width           =   975
      End
      Begin VB.OptionButton optGraad 
         Caption         =   "6 Kyu"
         Height          =   375
         Index           =   3
         Left            =   240
         TabIndex        =   4
         Top             =   1080
         Width           =   975
      End
      Begin VB.OptionButton optGraad 
         Caption         =   "7 Kyu"
         Height          =   375
         Index           =   2
         Left            =   240
         TabIndex        =   3
         Top             =   720
         Width           =   975
      End
      Begin VB.OptionButton optGraad 
         Caption         =   "8 Kyu"
         Height          =   375
         Index           =   1
         Left            =   240
         TabIndex        =   2
         Top             =   360
         Width           =   975
      End
      Begin VB.OptionButton optGraad 
         Caption         =   "Beginner"
         Height          =   375
         Index           =   0
         Left            =   240
         TabIndex        =   23
         Top             =   120
         Visible         =   0   'False
         Width           =   975
      End
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Default         =   -1  'True
      Height          =   375
      Left            =   6360
      TabIndex        =   18
      ToolTipText     =   "Sluit het examenscherm en voer alle wijzigingen door."
      Top             =   4800
      Width           =   1335
   End
   Begin VB.CommandButton cmdAnnuleren 
      Cancel          =   -1  'True
      Caption         =   "&Annuleren"
      Height          =   375
      Left            =   9240
      TabIndex        =   21
      ToolTipText     =   "Sluit dit scherm en maak de wijzigingen ongedaan"
      Top             =   4800
      Width           =   1335
   End
   Begin VB.CommandButton cmdDetail 
      Caption         =   "&Details ..."
      Height          =   372
      Left            =   4200
      TabIndex        =   17
      ToolTipText     =   "Toon de detail gegevens van het geselecteerd lid"
      Top             =   4800
      Width           =   1332
   End
   Begin MSComctlLib.ImageList imlColumnHeadings 
      Left            =   960
      Top             =   4680
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmExamen.frx":030A
            Key             =   "picDown"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmExamen.frx":0626
            Key             =   "picUp"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlGraden 
      Left            =   240
      Top             =   4680
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   16
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmExamen.frx":0942
            Key             =   "picUnknownKYU"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmExamen.frx":0C5C
            Key             =   "picBEGINNER"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmExamen.frx":0F76
            Key             =   "pic9KYU"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmExamen.frx":1290
            Key             =   "pic8KYU"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmExamen.frx":15AC
            Key             =   "pic7KYU"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmExamen.frx":18C8
            Key             =   "pic6KYU"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmExamen.frx":1BE4
            Key             =   "pic5KYU"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmExamen.frx":1F00
            Key             =   "pic4KYU"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmExamen.frx":221C
            Key             =   "pic3KYU"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmExamen.frx":2538
            Key             =   "pic2KYU"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmExamen.frx":2854
            Key             =   "pic1KYU"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmExamen.frx":2B70
            Key             =   "pic1DAN"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmExamen.frx":2E8C
            Key             =   "pic2DAN"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmExamen.frx":31A8
            Key             =   "pic3DAN"
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmExamen.frx":34C4
            Key             =   "pic4DAN"
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmExamen.frx":37E0
            Key             =   "pic5DAN"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lvwLeden 
      Height          =   3735
      Left            =   4680
      TabIndex        =   15
      Top             =   840
      Width           =   5895
      _ExtentX        =   10398
      _ExtentY        =   6588
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   12632256
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   240
      Picture         =   "frmExamen.frx":3AFC
      Top             =   210
      Width           =   480
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   10560
      Y1              =   4680
      Y2              =   4680
   End
   Begin VB.Label lblTitel 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "ExamenLijst"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   120
      TabIndex        =   20
      Top             =   120
      Width           =   10455
   End
End
Attribute VB_Name = "frmExamen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mblnDataChanged As Boolean
Private msGekozenGraad As String
Private msGekozenGraadDB As String
Private msCurrentFilter As String

Public Property Let DataChanged(value As Boolean)
    mblnDataChanged = value
    cmdToepassen.Enabled = mblnDataChanged
End Property

Public Property Get DataChanged() As Boolean
    DataChanged = mblnDataChanged
End Property

Private Sub chkAlleLedenTonen_Click()
    If (chkAlleLedenTonen = vbChecked) Then
        DataEnvironment1.rsLeden.Filter = ""
    Else
        DataEnvironment1.rsLeden.Filter = msCurrentFilter
    End If
    FillListView
    If (lvwLeden.Visible) Then lvwLeden.SetFocus
End Sub

Private Sub cmdAnnuleren_Click()
    Unload Me
End Sub

Private Sub cmdCertificatenLijst_Click()
'Dim rs As ADODB.Recordset
'Dim sql As String
'
'    sql = "SELECT tblGraad.GraadID, tblGraad.LidID, tblLid.Voornaam & ' ' & tblLid.Achternaam AS Naam, Mid$ (tblGraad.Graad, 4) AS Graad, tblGraad.ExamenDatum, tblGraad.ExamenPlaats, tblGraad.Actief, tblGraad.Afgedrukt FROM tblLid INNER JOIN tblGraad ON tblLid.LidID = tblGraad.LidID WHERE tblGraad.ExamenDatum >= #24/1/2001# AND tblGraad.ExamenDatum <= #24/5/2001# ORDER BY Graad;"
'
'    Set rs = New ADODB.Recordset
'    rs.Open sql, DataEnvironment1.Connection1, adOpenForwardOnly, adLockOptimistic
'    'show repport
'    Set dtrCertificatenLijst.DataSource = rs

Dim frmDates As New frmDates

    Load frmDates

    frmDates.dtpStart.value = dtpExamenDatum.value
    frmDates.dtpEnd.value = DateAdd("d", 2, dtpExamenDatum.value)

    frmDates.Show vbModal
'    Set rs = Nothing

End Sub

Private Sub cmdDetail_Click()
    If (lvwLeden.SelectedItem Is Nothing) Then
        MsgBox "Kan geen details tonen. Selecteer een Lid door in deze lijst te klikken en druk dan op de knop details.", vbInformation
    Else
        ShowDetail lvwLeden.SelectedItem.SubItems(1)
    End If
End Sub

Private Sub cmdOk_Click()
    cmdToepassen_Click
    Unload Me
    DataEnvironment1.rsLeden.Filter = ""
End Sub

Private Sub cmdToepassen_Click()
Dim lnlRowCount As Long
Dim acUpdate As New ADODB.Command

On Error GoTo ErrorHandler

    If (DataChanged) Then
        'Set active flag off
        For lnlRowCount = 1 To lvwLeden.ListItems.Count
            If (lvwLeden.ListItems(lnlRowCount).Checked) Then
                Set acUpdate.ActiveConnection = DataEnvironment1.Connection1
                acUpdate.CommandText = "UPDATE tblGraad SET Actief = False WHERE LidID = " + CStr(lvwLeden.ListItems(lnlRowCount).SubItems(1))
                acUpdate.Execute
            End If
        Next lnlRowCount
        
        'Save Degree records
        For lnlRowCount = 1 To lvwLeden.ListItems.Count
            If (lvwLeden.ListItems(lnlRowCount).Checked) Then
                StartUsingRecordset DataEnvironment1.rsLidGraad, False, False
                With DataEnvironment1.rsLidGraad
                    .AddNew
                    .Fields("LidID") = lvwLeden.ListItems(lnlRowCount).SubItems(1)
                    .Fields("Graad") = msGekozenGraadDB
                    .Fields("ExamenDatum") = Format(Me.dtpExamenDatum, mcSysDateFormat)
                    .Fields("ExamenPlaats") = Me.txtExamenPlaats.text
                    .Fields("Actief") = True
                End With
            End If
        Next lnlRowCount
        DataEnvironment1.rsLidGraad.UpdateBatch adAffectAll
        DataEnvironment1.rsLidGraad.Requery
        StartUsingRecordset DataEnvironment1.rsLeden, False
        'StartUsingRecordset DataEnvironment1.rsAfTeDrukkenGraden
        FillListView
        
        'Reset dirty flag
        DataChanged = False
    End If
    
    Exit Sub

ErrorHandler:
    MsgBox Err.Description, vbExclamation
End Sub

Private Sub Form_Activate()
    lvwLeden.SetFocus
End Sub

Private Sub Form_Load()
    Me.Caption = "Na het Examen..."
    lblTitel.Caption = vbCrLf + Me.Caption
    optGraad(1).value = True
    txtExamenPlaats = "Wommelgem"
    dtpExamenDatum.value = Now
    'lblHelp = "In dit scherm kan je invullen wie welk examen heeft behaald. Eerst en vooral stel je hieronder de examendatum in waarop het examen is afgenomen. Vervolgens kies je een graad, rechts zal je afhankelijk van de gekozen graad de mensen zien die in aanmerking komen voor deze graad. Kruis het vakje voor de naam aan en als je klaar bent druk je op de knop 'Toepassen'"
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set mfrmLidLijst = Nothing
    DataEnvironment1.rsLeden.Filter = ""
    With DataEnvironment1
'        EndUsingRecordset .rsLeden
'        EndUsingRecordset .rsLidGraad
'        EndUsingRecordset .rsAfTeDrukkenGraden
    End With
End Sub

Private Sub ShowDetail(lngLidID As Long)
Dim frmDetails As New frmLidDetail
    frmDetails.OpenForm lngLidID
    frmDetails.Show vbModal
End Sub

Private Sub lvwLeden_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    SortListViewColumn Me.lvwLeden, ColumnHeader
End Sub

Private Sub lvwLeden_DblClick()
    ShowDetail lvwLeden.SelectedItem.SubItems(1)
End Sub

Private Sub FillListView()
Dim itmX As ListItem
Dim lvntGraad As Variant
   
       
    With lvwLeden
        .Visible = False
        Set .SmallIcons = imlGraden
        .View = lvwReport   ' Set view to Report.
        .BorderStyle = ccNone
        .ColumnHeaderIcons = imlColumnHeadings
        .ColumnHeaders.Clear
        .ListItems.Clear
    End With
       
    StartUsingRecordset DataEnvironment1.rsLeden, False, False
    If (DataEnvironment1.rsLeden.EOF) Then
        If (DataEnvironment1.rsLeden.RecordCount = 0) Then
            lvwLeden.CheckBoxes = False
            lvwLeden.ColumnHeaders.Add , , "Melding", lvwLeden.Width - 100
            Set itmX = lvwLeden.ListItems.Add(, , "Geen kandidaten voor " + msGekozenGraad)
            lvwLeden.Visible = True
            Exit Sub
        Else
            DataEnvironment1.rsLeden.MoveFirst
        End If
    End If
    
    With lvwLeden.ColumnHeaders
        .Add 1, , "Graad", (lvwLeden.Width / 20) * 5
        .Add 2, , "LidID", 0
        .Add 3, , "Naam", (lvwLeden.Width / 20) * 7
        .Add 4, , "Telefoon", (lvwLeden.Width / 20) * 7
    End With

    lvwLeden.CheckBoxes = True
        
    DataEnvironment1.rsLeden.Sort = "Naam"
    While Not DataEnvironment1.rsLeden.EOF
        lvntGraad = DataEnvironment1.rsLeden("Graad").value
        If (Not IsNull(lvntGraad)) Then
            Set itmX = lvwLeden.ListItems.Add(, , CStr(lvntGraad), , "pic" + UCase(Join(Split(CStr(lvntGraad), " "), "")))
        Else
            Set itmX = lvwLeden.ListItems.Add(, , , , "picUnknownKYU")
        End If
        itmX.SubItems(1) = DataEnvironment1.rsLeden("LidID").value
        itmX.SubItems(2) = IIf(IsNull(DataEnvironment1.rsLeden("Naam").value), "", DataEnvironment1.rsLeden("Naam").value)
        itmX.SubItems(3) = IIf(IsNull(DataEnvironment1.rsLeden("Telefoon").value), "", DataEnvironment1.rsLeden("Telefoon").value)
        DataEnvironment1.rsLeden.MoveNext
    Wend
    
    If (lvwLeden.ListItems.Count > 0) Then
        lvwLeden.ListItems(1).Selected = True
    End If
    
    lvwLeden.Visible = True
    lvwLeden.Refresh
End Sub
         
Private Sub lvwLeden_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    DataChanged = True
End Sub

Private Sub optGraad_Click(Index As Integer)
    msCurrentFilter = "Graad='" + Me.optGraad(Index - 1).Caption + "'"
'    chkAlleLedenTonen_Click
    'DataEnvironment1.rsLeden.Filter = msCurrentFilter
    msGekozenGraad = Me.optGraad(Index).Caption
    msGekozenGraadDB = Format(Index + 1, "00") + "-" + Me.optGraad(Index).Caption
    chkAlleLedenTonen_Click
    'FillListView
    If (lvwLeden.Visible) Then lvwLeden.SetFocus
End Sub
