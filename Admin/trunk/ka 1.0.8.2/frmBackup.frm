VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmBackup 
   Caption         =   "Backup Kaisho Admin Database"
   ClientHeight    =   1575
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   Icon            =   "frmBackup.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   1575
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Annuleer"
      Height          =   375
      Left            =   3000
      TabIndex        =   3
      Top             =   1080
      Width           =   1575
   End
   Begin VB.CommandButton cmdBackup 
      Caption         =   "Neem &Backup"
      Default         =   -1  'True
      Height          =   375
      Left            =   1200
      TabIndex        =   2
      Top             =   1080
      Width           =   1695
   End
   Begin MSComDlg.CommonDialog cdlBackup 
      Left            =   3720
      Top             =   480
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label Label2 
      Caption         =   "U hebt deze maand nog geen backup genomen !"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   4
      Top             =   120
      Width           =   4335
   End
   Begin VB.Label lblLastBackup 
      Caption         =   "lstBackup"
      Height          =   255
      Left            =   1560
      TabIndex        =   1
      Top             =   600
      Width           =   2295
   End
   Begin VB.Label Label1 
      Caption         =   "Laatste backup :"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   600
      Width           =   1335
   End
End
Attribute VB_Name = "frmBackup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdBackup_Click()
On Error GoTo ErrorHandler

    
    cdlBackup.DialogTitle = "Kies een backup locatie"
    cdlBackup.CancelError = True
    cdlBackup.InitDir = "\"
    cdlBackup.FileName = "ka backup " & Format(Now, "dd_mm_yyyy")
    cdlBackup.Filter = " (*.kbc)|*.kbc"
    cdlBackup.FilterIndex = 0
    cdlBackup.DefaultExt = ".kbc"
    cdlBackup.Flags = cdlOFNPathMustExist Or cdlOFNExtensionDifferent
    cdlBackup.ShowSave
    
    
    'make backup
    FileCopy App.Path + "\DB\Kaisho Admin.mdb", cdlBackup.FileName
    
    'update registry
    SaveSetting App.Title, "Application", "LastBackup", Format(Now, "dd/mm/yyyy")
    
    Me.Hide
    Exit Sub
    
ErrorHandler:
    'no foto available
    If Err.Number = 32755 Then
        If (MsgBox("U hebt op de Annuleer knop gedrukt." + vbCrLf + vbCrLf + "Wilt U alsnog een backup maken?", vbQuestion + vbYesNo) = vbNo) Then
            Me.Hide
        Else
            cmdBackup_Click
        End If
    Else
        MsgBox "Error : " & Err.Description
    End If
End Sub

Private Sub cmdCancel_Click()
    Me.Hide
End Sub
