VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmNieuweLeden 
   Caption         =   "Nieuwe Leden"
   ClientHeight    =   5490
   ClientLeft      =   60
   ClientTop       =   360
   ClientWidth     =   11190
   LinkTopic       =   "Form1"
   ScaleHeight     =   5490
   ScaleWidth      =   11190
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdAnnuleren 
      Cancel          =   -1  'True
      Caption         =   "&Annuleren"
      Height          =   375
      Left            =   9720
      TabIndex        =   4
      Top             =   5040
      Width           =   1335
   End
   Begin VB.CommandButton cmdSelecteren 
      Caption         =   "&Selecteren"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   375
      Left            =   8280
      TabIndex        =   1
      Top             =   5040
      Width           =   1335
   End
   Begin VB.CommandButton cmdDetail 
      Caption         =   "&Details ..."
      Height          =   372
      Left            =   5640
      TabIndex        =   0
      Top             =   5040
      Width           =   1332
   End
   Begin MSComctlLib.ImageList imlColumnHeadings 
      Left            =   3840
      Top             =   4920
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      UseMaskColor    =   0   'False
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNieuweLeden.frx":0000
            Key             =   "picDown"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNieuweLeden.frx":031C
            Key             =   "picUp"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlGraden 
      Left            =   3000
      Top             =   4920
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   15
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNieuweLeden.frx":0638
            Key             =   "picUnknownKYU"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNieuweLeden.frx":0952
            Key             =   "pic9KYU"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNieuweLeden.frx":0C6C
            Key             =   "pic8KYU"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNieuweLeden.frx":0F88
            Key             =   "pic7KYU"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNieuweLeden.frx":12A4
            Key             =   "pic6KYU"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNieuweLeden.frx":15C0
            Key             =   "pic5KYU"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNieuweLeden.frx":18DC
            Key             =   "pic4KYU"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNieuweLeden.frx":1BF8
            Key             =   "pic3KYU"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNieuweLeden.frx":1F14
            Key             =   "pic2KYU"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNieuweLeden.frx":2230
            Key             =   "pic1KYU"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNieuweLeden.frx":254C
            Key             =   "pic1DAN"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNieuweLeden.frx":2868
            Key             =   "pic2DAN"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNieuweLeden.frx":2B84
            Key             =   "pic3DAN"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNieuweLeden.frx":2EA0
            Key             =   "pic4DAN"
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNieuweLeden.frx":31BC
            Key             =   "pic5DAN"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lvwLeden 
      Height          =   3975
      Left            =   120
      TabIndex        =   2
      Top             =   840
      Width           =   10935
      _ExtentX        =   19288
      _ExtentY        =   7011
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   12632256
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Naam"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "LidID"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Straat"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Adres"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Telefoon"
         Object.Width           =   3528
      EndProperty
   End
   Begin VB.Label lblText 
      Height          =   375
      Left            =   120
      TabIndex        =   5
      Top             =   5040
      Width           =   5415
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   240
      Picture         =   "frmNieuweLeden.frx":34D8
      Top             =   195
      Width           =   480
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   11040
      Y1              =   4920
      Y2              =   4920
   End
   Begin VB.Label lblTitel 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Nieuwe Leden"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   10935
   End
End
Attribute VB_Name = "frmNieuweLeden"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mblnDataChanged As Boolean
Private mniJaar As Integer
Private mnbMaand As Byte

Public Property Let DataChanged(value As Boolean)
    mblnDataChanged = value
    cmdSelecteren.Enabled = mblnDataChanged
End Property

Public Property Get DataChanged() As Boolean
    DataChanged = mblnDataChanged
End Property

Private Sub cmdAnnuleren_Click()
    Unload Me
End Sub

Private Sub cmdDetail_Click()
    If (lvwLeden.SelectedItem Is Nothing) Then
        MsgBox "Kan geen details tonen. Selecteer een Lid door in deze lijst te klikken en druk dan op de knop details.", vbInformation
    Else
        ShowDetail lvwLeden.SelectedItem.SubItems(1)
    End If
End Sub

Private Function LidNogNietInBetalingsLijst(lngLidID As Long, intJaar As Integer) As Boolean
Dim rsLeden As ADODB.Recordset
Dim lstrSQL As String

    Set rsLeden = New ADODB.Recordset
    lstrSQL = "SELECT * FROM tblLidBetaling WHERE Jaar = " + CStr(intJaar) + " AND LidID = " + CStr(lngLidID)
    rsLeden.Open lstrSQL, DataEnvironment1.Connection1, adOpenDynamic, adLockReadOnly
    If (rsLeden.RecordCount > 0) Then
        LidNogNietInBetalingsLijst = False
    Else
        LidNogNietInBetalingsLijst = True
    End If
End Function

Private Sub cmdSelecteren_Click()
Dim llngLedenCounter As Long

    StartUsingRecordset DataEnvironment1.rstblLidAanwezigheid
    
    'Add the members to the presencelsit for selected year and month
    With DataEnvironment1.rstblLidAanwezigheid
        For llngLedenCounter = 1 To lvwLeden.ListItems.Count
            If (lvwLeden.ListItems(llngLedenCounter).Checked) Then
                lblText = "Bezig met toevoegen van " + lvwLeden.ListItems(llngLedenCounter).Text + " voor de aanwezigheidslijst van " + MaandNaam(CInt(mnbMaand)) + " " + CStr(mniJaar) + "."
                .AddNew
                .Fields("LidID") = lvwLeden.ListItems(llngLedenCounter).SubItems(1)
                .Fields("Jaar") = mniJaar
                .Fields("Maand") = mnbMaand
                WaitAWhile 50000
            End If
        Next llngLedenCounter
        .UpdateBatch adAffectAll
        .Requery
        DataEnvironment1.rstblLidAanwezigheidNieuw.Requery
        DataEnvironment1.rstblLidAanwezigheidBestaand.Requery
    End With

    'Check if this member is already in the PaymentList for this year.
    'if not tell the user and add them.
    With DataEnvironment1.rstblLidBetaling
        For llngLedenCounter = 1 To lvwLeden.ListItems.Count
            If (lvwLeden.ListItems(llngLedenCounter).Checked) Then
                If (LidNogNietInBetalingsLijst(lvwLeden.ListItems(llngLedenCounter).SubItems(1), mniJaar)) Then
                    lblText = "Bezig met toevoegen van " + lvwLeden.ListItems(llngLedenCounter).Text + " voor de betalingslijst van " + CStr(mniJaar) + "."
                    .AddNew
                    .Fields("LidID") = lvwLeden.ListItems(llngLedenCounter).SubItems(1)
                    .Fields("Jaar") = mniJaar
                Else
                    lblText = lvwLeden.ListItems(llngLedenCounter).Text + " is al aanwezig in de betalingslijst van " + CStr(mniJaar) + "."
                End If
                WaitAWhile 50000
            End If
        Next llngLedenCounter
        .UpdateBatch adAffectAll
        .Requery
        If (DataEnvironment1.rstblLidBetalingNieuw.State = adStateOpen) Then DataEnvironment1.rstblLidBetalingNieuw.Requery
        If (DataEnvironment1.rstblLidBetalingBestaand.State = adStateOpen) Then DataEnvironment1.rstblLidBetalingBestaand.Requery
    End With

    Unload Me
End Sub

Private Sub Form_Activate()
    lvwLeden.SetFocus
End Sub

Public Sub OpenForm(Maand As Byte, Jaar As Integer, Optional ShowMode As VBRUN.FormShowConstants = vbModeless)
    mnbMaand = Maand
    mniJaar = Jaar
    Me.Caption = Me.Caption + " voor " + MaandNaam(CInt(mnbMaand)) + " " + CStr(mniJaar)
    lblTitel.Caption = vbCrLf + Me.Caption
    FillListView
    Me.Show ShowMode
    Me.ZOrder
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set mfrmLidLijst = Nothing
End Sub

Private Sub ShowDetail(lngLidID As Long)
    Set mfrmLidDetail = New frmLidDetail
    mfrmLidDetail.OpenForm lngLidID
    mfrmLidDetail.Show vbModal
    Set mfrmLidDetail = Nothing
End Sub

Private Sub lvwLeden_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    SortListViewColumn Me.lvwLeden, ColumnHeader
End Sub

Private Sub lvwLeden_DblClick()
    ShowDetail lvwLeden.SelectedItem.SubItems(1)
End Sub

Private Sub FillListView()
Dim itmX As ListItem
Dim rsNieuweLeden As ADODB.Recordset
Dim lstrSQL As String
   
    lvwLeden.Visible = False
   
    Set lvwLeden.SmallIcons = imlGraden
    lvwLeden.View = lvwReport   ' Set view to Report.
    lvwLeden.BorderStyle = ccNone
    lvwLeden.ColumnHeaderIcons = imlColumnHeadings
    lvwLeden.ListItems.Clear
    lvwLeden.CheckBoxes = True
   
    lvwLeden.ColumnHeaders(1).Width = (lvwLeden.Width / 20) * 6
    lvwLeden.ColumnHeaders(2).Width = 0
    lvwLeden.ColumnHeaders(3).Width = (lvwLeden.Width / 20) * 5
    lvwLeden.ColumnHeaders(4).Width = (lvwLeden.Width / 20) * 4
    lvwLeden.ColumnHeaders(5).Width = (lvwLeden.Width / 20) * 4
   
    'Get Data
    Set rsNieuweLeden = New ADODB.Recordset
    lstrSQL = "SELECT tblLid.LidID, [tblLid].[Achternaam] & ' ' & [tblLid].[Voornaam] AS Naam, tblLid.Straat, [tblLid].[PostCode] & ' ' & [tblLid].[Woonplaats] & ' ' & [tblLid].[Provincie] AS Adres, tblLid.Telefoon, tblLid.Nieuw " + _
              "FROM tblLid " + _
              "WHERE ((((tblLid.LidID) Not In (SELECT qryLidAanwezigheidNieuw.LidID FROM qryLidAanwezigheidNieuw WHERE Jaar = " + CStr(mniJaar) + " AND Maand = " + CStr(mnbMaand) + ")) AND (tblLid.Nieuw)=True));"

    rsNieuweLeden.Open lstrSQL, DataEnvironment1.Connection1, adOpenDynamic, adLockReadOnly

    With rsNieuweLeden
        While Not .EOF
            Set itmX = lvwLeden.ListItems.Add(, , IIf(IsNull(.Fields("Naam").value), "", .Fields("Naam").value))
            itmX.SubItems(1) = .Fields("LidID").value
            itmX.SubItems(2) = IIf(IsNull(.Fields("Straat").value), "", .Fields("Straat").value)
            itmX.SubItems(3) = IIf(Trim$(.Fields("Adres").value) = "()", "", .Fields("Adres").value)
            itmX.SubItems(4) = IIf(IsNull(.Fields("Telefoon").value), "", .Fields("Telefoon").value)
            .MoveNext
        Wend
    End With
    
    If (lvwLeden.ListItems.Count > 0) Then
        lvwLeden.ListItems(1).Selected = True
    End If
    
    lvwLeden.Visible = True
    lvwLeden.Refresh
End Sub
         
Private Sub lvwLeden_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    DataChanged = True
End Sub
