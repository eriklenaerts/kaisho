Attribute VB_Name = "modMain"
Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As Long) As Long
Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long


'Constants
Public Const mclngDateColumnWidth As Long = 500
Public Const mcSysDateFormat As String = "yyyy-mm-dd"
Public Const mcSysDateFormatDisplay As String = "dddd dd mmmm yyyy"
Public Const mclngBackColorDisabled As Long = &HC0C0C0
Public Const mcnlNoFamilie As Long = -1
Public Const cErrorExcelWasNotRunning As Long = 429
Public Const cErrorUserrequestedCancel As Long = vbObjectError + 100
Public Const cErrorCreateMember As Long = vbObjectError + 105
Public Const cErrorUpdateMember As Long = vbObjectError + 107
Public Const cErrorNoPaymentsPresent As Long = vbObjectError + 110
Public Const WM_USER As Long = 1024
Public Const WM_CLOSE = &H10
Public Const WM_DESTROY = &H2
Public Const WM_NCDESTROY = &H82
Public Const mcstrAdministrator As String = "Erik Lenaerts (03/321.93.36)"
Public Const mclngNieuwLid As Long = 0

'Types
Public Type kaGraad
    Naam As String
    Kleur As Long
End Type

Public Type kaGridColumns
    Caption As String
    Position As Long
    Index As Long
End Type

'Enums
Public Enum kaFormMode
    kaFormLaden
    kaFormOpen
    kaFormVerversen
    kaFormVerwerking
    kaFormOntLaden
End Enum

Public Enum kaLedendLijstMode
    kaAlleLeden = 0
    kaActieveLeden = 1
    kaInactieveLeden = 2
    kaBestaandeLeden = 3
    kaNieuweLeden = 4
    ka9Kyu = 5
    ka8Kyu = 6
    ka7Kyu = 7
    ka6Kyu = 8
    ka5Kyu = 9
    ka4Kyu = 10
    ka3Kyu = 11
    ka2Kyu = 12
    ka1Kyu = 13
    ka1Dan = 14
    ka2Dan = 15
    ka3Dan = 16
    ka4Dan = 17
    ka5Dan = 18
End Enum

Public Enum kaStatistieken
    kaManVrouw = 1
    kaGraden = 2
    kaLedenVerhoudingen = 3
    kaGeenStat = 4
End Enum

Public Enum kaDataMode
    kaCreate
    kaRead
    kaUpdate
    kaDelete
    kaCreateUnfinished
    kaUpdateUnfinished
End Enum

Public Enum kaPeriodeMode
    pmAanwezigheid
    pmBetaling
End Enum

Public Enum kaBetalingsKolom
    kaBetalingID = 0
    kaLidID = 1
    kaNaam = 2
    kaBedrag = 3
    kaJanuari = 4
    kaFebruari = 5
    kaMaart = 6
    kaApril = 7
    kaMei = 8
    kaJuni = 9
    kaJuli = 10
    kaAugustus = 11
    kaSeptember = 12
    kaOktober = 13
    kaNovember = 14
    kaDecember = 15
End Enum

Public Enum kaAanwezigheidKolom
    kaAanwezigheidID = 0
    kaLidID = 1
    kaNaam = 2
    kaJaar = 3
    kaMaand = 4
    kaBetaald = 5
    kaOpmerking = 6
    kaMaandag1 = 7
    kaDinsdag1 = 8
    kaVrijdag1 = 9
    kaZaterdag1 = 10
    kaMaandag2 = 11
    kaDinsdag2 = 12
    kaVrijdag2 = 13
    kaZaterdag2 = 14
    kaMaandag3 = 15
    kaDinsdag3 = 16
    kaVrijdag3 = 17
    kaZaterdag3 = 18
    kaMaandag4 = 19
    kaDinsdag4 = 20
    kaVrijdag4 = 21
    kaZaterdag4 = 22
    kaMaandag5 = 23
    kaDinsdag5 = 24
    kaVrijdag5 = 25
    kaZaterdag5 = 26
End Enum

Public Enum kaCellPosition
    kaStart
    kaEnd
End Enum

Public Enum kaStatusMode
    kaMetProgress
    kaZonderProgress
End Enum

Public Enum kaAppMode
    kaSimpel
    kaNormaal
    kaUitgebreid
End Enum

Public Enum kaIniChapter
    kaApplicatieMode
End Enum

'Var's
'Public mfrmAanwezigheid As frmAanwezigheid
'Public mfrmBetalingen As frmBetalingen
Public mfrmLidDetail As frmLidDetail
Public mfrmLidLijst As frmLidLijst
Public mfrmStatistieken As frmStatistieken
Public mfrmLidFamilie As frmLidFamilie
Public mfrmLidGraden As frmLidGraden
Public mfrmExamen As frmExamen
Public mfrmPeriode As frmPeriode
Public mfrmInstellingen As frmOptions
Public mfrmMDIMain As frmMDIMain
Public mAppMode As kaAppMode
Public mLinesPerPage As Integer
Public mRowHeight As Double
Public mLastBackup As String
Public objLogger As clsLog

Private Sub CheckForBackup()
    If DateDiff("m", mLastBackup, Now) >= 1 Then
        
        Dim frmBackup As New frmBackup
        
        frmBackup.lblLastBackup = Format(mLastBackup, "dd mmm yyyy")
        frmBackup.Show vbModal
        
        Debug.Assert 1
        
        Set frmBackup = Nothing
    End If
End Sub

Public Sub Main()
Dim lfrmSplash As New frmSplash
Dim objExcelExt As New clsExcelExtension

    Set objLogger = New clsLog
    lfrmSplash.Show vbModal
    DataEnvironment1.Connection1.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & "\DB\Kaisho Admin.mdb;Mode=Read|Write|Share Deny None;Persist Security Info=False;"
    
    GetAppOptions
    
    'Check For Excel and if open, ask for excel to close.
    If Not objExcelExt.CloseExcelIfRunning Then
        
        'perform backups if posible
        CheckForBackup
        
        Set mfrmMDIMain = New frmMDIMain
        mfrmMDIMain.Show
    Else
        End
    End If
End Sub

'Public Sub InitGridLayout(ssGrid As SSOleDBGrid)
'    ssGrid.BackColorEven = &HFFFFFF
'    ssGrid.BackColorOdd = &HFFFFFF
'    ssGrid.ForeColorOdd = &H0&
'    ssGrid.ForeColorEven = &H0&
'    ssGrid.Font = "Tahoma"
'End Sub

Public Sub StartUsingRecordset(rs As ADODB.Recordset, Optional ResetFilter As Boolean = True, Optional RequeryIfOpen As Boolean = True)
    With rs
        If (.State = adStateClosed) Then
            .Open
        Else
            If (ResetFilter) Then .Filter = ""
            If (.EOF And .BOF) Then
            Else
                .MoveFirst
            End If
            If (RequeryIfOpen) Then .Requery
        End If
    End With
End Sub

Public Sub EndUsingRecordset(rs As ADODB.Recordset)
    If (rs.State = adStateOpen) Then
        rs.Close
    End If
End Sub

Public Function MaandNaam(intPositie As Integer) As String
    Select Case intPositie
        Case 1
            MaandNaam = "Januari"
        Case 2
            MaandNaam = "Februari"
        Case 3
            MaandNaam = "Maart"
        Case 4
            MaandNaam = "April"
        Case 5
            MaandNaam = "Mei"
        Case 6
            MaandNaam = "Juni"
        Case 7
            MaandNaam = "Juli"
        Case 8
            MaandNaam = "Augustus"
        Case 9
            MaandNaam = "September"
        Case 10
            MaandNaam = "Oktober"
        Case 11
            MaandNaam = "November"
        Case 12
            MaandNaam = "December"
    End Select
End Function

Public Function MaandNaamVoorBetaling(intPositie As Integer) As String
    Select Case intPositie
        Case 1
            MaandNaamVoorBetaling = "Sep"
        Case 2
            MaandNaamVoorBetaling = "Okt"
        Case 3
            MaandNaamVoorBetaling = "Nov"
        Case 4
            MaandNaamVoorBetaling = "Dec"
        Case 5
            MaandNaamVoorBetaling = "Jan"
        Case 6
            MaandNaamVoorBetaling = "Feb"
        Case 7
            MaandNaamVoorBetaling = "Mrt"
        Case 8
            MaandNaamVoorBetaling = "Apr"
        Case 9
            MaandNaamVoorBetaling = "Mei"
        Case 10
            MaandNaamVoorBetaling = "Jun"
        Case 11
            MaandNaamVoorBetaling = "Jul"
        Case 12
            MaandNaamVoorBetaling = "Aug"
    End Select
End Function

Public Function NrOfDaysForThisMonth(bMaand As Byte, iJaar As Integer) As Byte
    Select Case bMaand
        Case 1, 3, 5, 7, 8, 10, 12
            NrOfDaysForThisMonth = 31
        Case 4, 6, 9, 11
            NrOfDaysForThisMonth = 30
        Case 2
            NrOfDaysForThisMonth = IIf(IsLeapYear(iJaar), 29, 28)
    End Select
End Function

Public Function IsLeapYear(ByVal inYear As Integer) As Boolean
    IsLeapYear = ((inYear Mod 4 = 0) And (inYear Mod 100 <> 0) Or (inYear Mod 400 = 0))
End Function

Public Sub SortListViewColumn(ByVal lvw As MSComctlLib.ListView, ByVal ColumnHeader As MSComctlLib.ColumnHeader)
Dim c As MSComctlLib.ColumnHeader

    lvw.SortKey = ColumnHeader.Index - 1
    lvw.Sorted = True
    If (lvw.SortOrder = lvwAscending) Then
        lvw.SortOrder = lvwDescending
        For Each c In lvw.ColumnHeaders
            If (c Is ColumnHeader) Then
                c.Icon = "picDown"
            Else
                c.Icon = 0
            End If
        Next c
    Else
        lvw.SortOrder = lvwAscending
        For Each c In lvw.ColumnHeaders
            If (c Is ColumnHeader) Then
                c.Icon = "picUp"
            Else
                c.Icon = 0
            End If
        Next c
    End If
End Sub

'Swap rows with columns
Public Function TranposeMtx(vntMatrix As Variant) As Variant
Dim llngRowCount As Long, llngColumnCount As Long
Dim lvntMatrix As Variant

    ReDim lvntMatrix(UBound(vntMatrix, 2) - LBound(vntMatrix, 2), UBound(vntMatrix, 1) - LBound(vntMatrix, 1))
    
    For llngRowCount = LBound(vntMatrix, 1) To UBound(vntMatrix, 1)
        For llngColumnCount = LBound(vntMatrix, 2) To UBound(vntMatrix, 2)
            lvntMatrix(llngColumnCount, llngRowCount) = vntMatrix(llngRowCount, llngColumnCount)
        Next llngColumnCount
    Next llngRowCount
    TranposeMtx = lvntMatrix
End Function

Public Sub WaitAWhile(Times As Long)
Dim llngCounter As Long

    For llngCounter = 0 To Times
        DoEvents
    Next llngCounter
End Sub

Public Function BerekenBedrag(nlLidID As Long) As Long
Dim lvBookMark As Variant
Dim lnlFamilieID As Long, lnlAantalFamilieLeden As Long
Dim lbGM As Boolean
Dim lbTrainer As Boolean

    StartUsingRecordset DataEnvironment1.rsLid, , False
    With DataEnvironment1.rsLid
        'Get group figures
        .Filter = "LidID=" & nlLidID
        lnlFamilieID = IIf(IsNull(.Fields("Groep_FamilieID")), mcnlNoFamilie, .Fields("Groep_FamilieID"))
        lbGM = .Fields("Groep_GM")
        lbTrainer = .Fields("Groep_Trainer")
        
        'The member is in a familie
        If (lnlFamilieID <> mcnlNoFamilie) Then
            .Filter = "Groep_FamilieID=" & lnlFamilieID
            lnlAantalFamilieLeden = .RecordCount
        End If
        
        If (lnlAantalFamilieLeden = 0) Then lnlAantalFamilieLeden = 1
    End With
    
    If lbTrainer Then
        BerekenBedrag = 0
'    Else
        'BerekenBedrag = 300
'        BerekenBedrag = 13
    Else
'        Select Case lnlAantalFamilieLeden
'            Case 1
'                BerekenBedrag = 600
'            Case 2
'                BerekenBedrag = 500
'            Case 3 To 12
'                BerekenBedrag = 1200 \ lnlAantalFamilieLeden
'        End Select
        Select Case lnlAantalFamilieLeden
            Case 1
                If (lbGM) Then
                    BerekenBedrag = 13
                Else
                    'BerekenBedrag = 600
                    BerekenBedrag = 20
                End If
            Case 2
                If (lbGM) Then
                    BerekenBedrag = 25
                Else
                    'BerekenBedrag = 1000
                    BerekenBedrag = 30
                End If
            Case 3 To 12
                If (lbGM) Then
                    BerekenBedrag = 30
                Else
                    'BerekenBedrag = 1200
                    BerekenBedrag = 35
                End If
        End Select
    End If
End Function

Public Sub SetAppOptions()
On Error GoTo ErrorHandler
    SaveSetting App.Title, "Application", "Mode", CStr(mAppMode)
    SaveSetting App.Title, "Application", "LinesPerPage", CStr(mLinesPerPage)
    SaveSetting App.Title, "Application", "RowHeight", CStr(mRowHeight)
    
    Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            MsgBox "Er is een fout opgetreden tijdens het ophalen van de instellingen, gelieve Erik Lenaerts te contacteren...", vbCritical
    End Select
End Sub

Public Sub GetAppOptions()
On Error GoTo ErrorHandler
Dim strAppMode As String

    strAppMode = GetSetting(App.Title, "Application", "Mode")
    mLinesPerPage = GetSetting(App.Title, "Application", "LinesPerPage", 49)
    mRowHeight = GetSetting(App.Title, "Application", "RowHeight", 13)
    mLastBackup = GetSetting(App.Title, "Application", "LastBackup", "1/1/1999")
    
    If (Len(strAppMode) = 0) Then
        SetAppOptions
    Else
        mAppMode = CLng(strAppMode)
    End If
    
    Exit Sub
ErrorHandler:
    Select Case Err.Number
        Case Else
            MsgBox "Er is een fout opgetreden tijdens het ophalen van de instellingen, gelieve Erik Lenaerts te contacteren...", vbCritical
    End Select
End Sub

Public Function GetTrainingDays(Maand As Byte, Jaar As Integer) As Collection
Dim intWeekDag As Integer
        
    Set GetTrainingDays = New Collection
    
    For llngDagCounter = 1 To NrOfDaysForThisMonth(Maand, Jaar)
        intWeekDag = Weekday(CDate(CStr(Jaar) + "-" + CStr(Maand) + "-" + CStr(llngDagCounter)))
        If (intWeekDag = vbMonday Or intWeekDag = vbTuesday Or intWeekDag = vbFriday Or intWeekDag = vbSaturday) Then
            GetTrainingDays.Add Format(llngDagCounter, "00")
        End If
    Next llngDagCounter
End Function

Public Function GetMutationDate() As Date
    StartUsingRecordset DataEnvironment1.rsMutatie
    GetMutationDate = Format(DataEnvironment1.rsMutatie("MutatieDatumLeden").value, "dd mmmm yyyy")
End Function

Public Sub SetMutationDate(MutationDate As Date)
    If (Not IsDate(MutationDate)) Then
        MsgBox "Er is een ongeldige waarde opgegeven bij het schrijven van de mutatiedatum", vbExclamation
        Exit Sub
    End If
    StartUsingRecordset DataEnvironment1.rsMutatie
    DataEnvironment1.rsMutatie("MutatieDatumLeden").value = Format(MutationDate, "dd mmmm yyyy")
    DataEnvironment1.rsMutatie.Update
End Sub

