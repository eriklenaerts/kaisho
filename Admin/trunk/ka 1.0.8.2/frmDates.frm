VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmDates 
   Caption         =   "Form1"
   ClientHeight    =   2610
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6870
   LinkTopic       =   "Form1"
   ScaleHeight     =   2610
   ScaleWidth      =   6870
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdAnnuleren 
      Cancel          =   -1  'True
      Caption         =   "&Annuleren"
      Height          =   375
      Left            =   5400
      TabIndex        =   7
      ToolTipText     =   "Sluit dit scherm en maak de wijzigingen ongedaan"
      Top             =   2160
      Width           =   1335
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Default         =   -1  'True
      Height          =   375
      Left            =   3960
      TabIndex        =   6
      ToolTipText     =   "Sluit het examenscherm en voer alle wijzigingen door."
      Top             =   2160
      Width           =   1335
   End
   Begin MSComCtl2.DTPicker dtpEnd 
      Height          =   375
      Left            =   3840
      TabIndex        =   5
      Top             =   1440
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   661
      _Version        =   393216
      Format          =   22675457
      CurrentDate     =   37179
   End
   Begin MSComCtl2.DTPicker dtpStart 
      Height          =   375
      Left            =   1200
      TabIndex        =   1
      Top             =   1440
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   661
      _Version        =   393216
      Format          =   22675457
      CurrentDate     =   37179
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   240
      Picture         =   "frmDates.frx":0000
      Top             =   240
      Width           =   480
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   6720
      Y1              =   2040
      Y2              =   2040
   End
   Begin VB.Label Label3 
      Caption         =   "Eind datum :"
      Height          =   255
      Left            =   2880
      TabIndex        =   4
      Top             =   1440
      Width           =   975
   End
   Begin VB.Label Label2 
      Caption         =   "Begin datum :"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   1440
      Width           =   1335
   End
   Begin VB.Label Label1 
      Caption         =   "Ik wens alle leden af te drukken die exames hebben afgelegd tussen:"
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   840
      Width           =   6015
   End
   Begin VB.Label lblTitel 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Periode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   6615
   End
End
Attribute VB_Name = "frmDates"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdAnnuleren_Click()
    Unload Me
End Sub

Private Sub cmdOk_Click()
Dim strStart As String
Dim strEnd As String

    strStart = Format(dtpStart.value, "dd/mm/yyyy")
    strEnd = Format(dtpEnd.value, "dd/mm/yyyy")
    DataEnvironment1.AfTeDrukkenGraden strStart, strEnd
    dtrCertificatenLijst.Show vbModal
    DataEnvironment1.rsAfTeDrukkenGraden.Close
End Sub

