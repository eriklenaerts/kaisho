VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsExcelExtension"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Option Base 1

Private mobjApp As Excel.Application
Private WithEvents mfrmStatus As frmStatus
Attribute mfrmStatus.VB_VarHelpID = -1
Private mCancelled As Boolean
Private mxlCurrentWindow As Excel.Window
Private mblnExcelWasNotRunning As Boolean

Public Property Get ExcelWasNotRunning() As Variant
    ExcelWasNotRunning = mblnExcelWasNotRunning
End Property

Private Function GetCellName(vntData As Variant, CellPosition As kaCellPosition, Optional RowOffset As Long = 0, Optional ColumnOffset As Long = 0) As String
    If (CellPosition = kaStart) Then
        GetCellName = "A1"
    Else
        GetCellName = Chr(64 + (UBound(vntData, 2) + CStr(ColumnOffset))) & (UBound(vntData, 1) + CStr(RowOffset))
    End If
End Function

Public Function CloseExcelIfRunning(Optional blnWarning As Boolean = True) As Boolean
Dim blnExcelWasNotRunning As Boolean
Dim objApp As Excel.Application
Dim lhWnd As Long


    Set mobjApp = Nothing
    Set objApp = GetExcel(blnExcelWasNotRunning, False)
    
    If blnExcelWasNotRunning Then
        Exit Function
    Else
        If blnWarning Then
            If (MsgBox("Om fouten te voorkomen in Kaisho Admin is het aangeraden om Excel af te sluiten en al uw werk te bewaren. Eenmaal Excel is afgesloten kan je Kaisho Admin terug starten." & vbCrLf & vbCrLf & "In sommige situatie kan het voorkomen dat Excel draait terwijl je het niet ziet druk dan op 'Ja' om Excel alsnog af te sluiten.", vbInformation + vbYesNo) = vbYes) Then
                If (Not objApp.ActiveWorkbook Is Nothing) Then objApp.ActiveWorkbook.Close False
                objApp.Quit
                Set objApp = Nothing
            Else
                CloseExcelIfRunning = True
                Exit Function
            End If
        Else
            'GetWorkbook.Close False
            If (Not objApp.ActiveWorkbook Is Nothing) Then objApp.ActiveWorkbook.Close False
            objApp.Quit
            Set objApp = Nothing
        End If
    End If
        
    'If quit command failed we do it the hard way (like you would if yo end a task from the windows task list)
    Set objApp = GetExcel(blnExcelWasNotRunning, False)
    
    Do While Not blnExcelWasNotRunning
        lhWnd = FindWindow("XLMAIN", 0)
        If lhWnd = 0 Then ' 0 means Excel not running.
        Else
            SendMessage lhWnd, WM_CLOSE, 0, 0
            SendMessage lhWnd, WM_DESTROY, 0, 0
            SendMessage lhWnd, WM_NCDESTROY, 0, 0
        End If
        Set objApp = GetExcel(blnExcelWasNotRunning, False)
    Loop
End Function

Private Function GetExcel(ByRef blnExcelWasNotRunning As Boolean, Optional blnStartNewSession As Boolean = True) As Excel.Application
Dim lobjApp As Excel.Application
Dim lhWnd As Long

On Error GoTo ErrorHandler

    'Get Running Excel
    Set lobjApp = GetObject(, "Excel.Application")
    
    'Add Excel in ROT (Running Object Table)
    lhWnd = FindWindow("XLMAIN", 0)
    If lhWnd = 0 Then ' 0 means Excel not running.
    Else
        SendMessage lhWnd, WM_USER + 18, 0, 0
    End If
    
    'Start Excel if not present
    If (blnExcelWasNotRunning And blnStartNewSession) Then
        Set lobjApp = CreateObject("Excel.Application")
    End If

    Set GetExcel = lobjApp

    Exit Function

ErrorHandler:
    Select Case Err.Number
        Case cErrorExcelWasNotRunning
            blnExcelWasNotRunning = True
            Resume Next
        Case Else
            MsgBox "Unspecified Error : " & Err.Description & " (" & Err.Number & ")"
    End Select
    
End Function

Private Function ConvertTo1Base(vntData As Variant) As Variant
Dim llngRowCount As Long, llngColumnCount As Long
Dim lbRowOffset As Byte, lbColumnOffset As Byte
Dim lvntData As Variant
    
    'expand
    If (LBound(vntData, 1) = 0) Then lbRowOffset = 1
    If (LBound(vntData, 2) = 0) Then lbColumnOffset = 1
    
    ReDim lvntData(1 To UBound(vntData, 1) + lbRowOffset, 1 To UBound(vntData, 2) + lbColumnOffset)
        
    For llngRowCount = LBound(vntData, 1) To UBound(vntData, 1)
        For llngColumnCount = LBound(vntData, 2) To UBound(vntData, 2)
            lvntData(llngRowCount + lbRowOffset, llngColumnCount + lbColumnOffset) = vntData(llngRowCount, llngColumnCount)
        Next llngColumnCount
    Next llngRowCount
    
    ConvertTo1Base = lvntData
End Function

Private Function FillExcelSheet(Data As Variant, xlWorkSheet As Excel.Worksheet, Optional ByVal RowOffset As Long = 0, Optional ByVal ColumnOffset As Long = 0, Optional Status As Boolean = False) As Long
Dim llngRowCount As Long, llngColumnCount As Long, llngBreak As Long
Dim llngStatusCounter As Long, llngNrOfHeadingsInserted As Long
Dim vntFamilyCache() As Variant
Dim lngFamilyCount As Long
Dim i As Long
Dim y As Long
Dim blnFamilySplitted As Boolean
Dim llngdelayed As Long

    If (LBound(Data, 1) = 0 Or LBound(Data, 2) = 0) Then Data = ConvertTo1Base(Data)
    
    objLogger.WriteLog ("fillexcelsheet : create status form")
    If (Status) Then
        Set mfrmStatus = New frmStatus
        mfrmStatus.lblText = "Bezig met het overzetten van de data naar Microsoft Excel, even geduld a.u.b."
        mfrmStatus.OpenForm kaMetProgress
        WaitAWhile 10
    End If
    
    objLogger.WriteLog ("fillexcelsheet : fill sheet")
    For llngRowCount = LBound(Data, 1) To UBound(Data, 1)
        'Check for a family, if it is a family that gets interupted by an
        ' header row, blank rows must be inserted. (it is not allowed to split
        ' a family on two pages
        ' By caching the rows in a array, we insert them on the new page.
        
        '*******************************************************************
        'TODO : try to insert -4Adriassens where 4 stands for the number of
        'Members in the family
        'In that case we can calculate if the family will be interupted. And
        'if they are, how much blank lines to insert.
        '*******************************************************************
        
        lngFamilyCount = IIf(Left(CStr(Data(llngRowCount, 1)), 1) = "-", Mid$(CStr(Data(llngRowCount, 1)), 2, 1), -1)

        'Insert an Header row...
        llngBreak = mLinesPerPage + RowOffset
        If ((llngRowCount - llngBreak) Mod (llngBreak + 2) = 0) Then
            For llngColumnCount = LBound(Data, 2) To UBound(Data, 2)
                xlWorkSheet.Cells(llngRowCount + RowOffset + llngNrOfHeadingsInserted, llngColumnCount + ColumnOffset) = CStr(Data(1, llngColumnCount))
            Next llngColumnCount
            llngNrOfHeadingsInserted = llngNrOfHeadingsInserted + 1
            lngFamilyCount = -1
        End If
        
'        'If it is a family that will be splitted by an header row, blank lines will be inserted
'        If (lngFamilyCount <> -1) Then
'            blnFamilySplitted = False
'            For i = llngRowCount To llngRowCount + lngFamilyCount
'                If ((i - llngBreak) Mod (llngBreak + 2) = 0) Then
'                    blnFamilySplitted = True
'                    'the header row will split the family
'                    'insert i - llngRowCount blanco rows
'                    ReDim vntFamilyCache(1 To i - llngRowCount + 1, LBound(Data, 2) To UBound(Data, 2))
'                    For y = 1 To i - llngRowCount + 1
'                        'Cache the row and add an empty row
'                        For llngColumnCount = LBound(Data, 2) To UBound(Data, 2)
'                            'insert the empty row
'                            vntFamilyCache(y, llngColumnCount) = CStr(Data(llngRowCount, llngColumnCount))
'                            xlWorkSheet.Cells(llngRowCount + RowOffset + llngNrOfHeadingsInserted + llngdelayed, llngColumnCount + ColumnOffset) = ""
'                        Next llngColumnCount
'                        llngRowCount = llngRowCount + 1
'                    Next y
'                End If
'            Next i
'            If Not blnFamilySplitted Then
'                For llngColumnCount = LBound(Data, 2) To UBound(Data, 2)
'                    'insert the row
'                    xlWorkSheet.Cells(llngRowCount + RowOffset + llngNrOfHeadingsInserted + llngdelayed, llngColumnCount + ColumnOffset) = CStr(Data(llngRowCount, llngColumnCount))
'                    If (Status) Then mfrmStatus.prgMain.value = (llngStatusCounter / (UBound(Data, 1) * UBound(Data, 2))) * 100
'                    llngStatusCounter = llngStatusCounter + 1
'                Next llngColumnCount
'            End If
'        Else
'            'If we have something in the cache, write it out first.
'            If blnFamilySplitted Then
'                For i = LBound(vntFamilyCache) To UBound(vntFamilyCache)
'                    For llngColumnCount = LBound(vntFamilyCache, 2) To UBound(vntFamilyCache, 2)
'                        'insert the row
'                        xlWorkSheet.Cells(llngRowCount + RowOffset + llngNrOfHeadingsInserted + llngdelayed, llngColumnCount + ColumnOffset) = CStr(vntFamilyCache(i, llngColumnCount))
'                        If (Status) Then mfrmStatus.prgMain.value = (llngStatusCounter / (UBound(Data, 1) * UBound(Data, 2))) * 100
'                        llngStatusCounter = llngStatusCounter + 1
'                    Next llngColumnCount
'                    llngdelayed = llngdelayed + 1
'                    blnFamilySplitted = False
'                Next i
'            End If
            'Write the normal row
            For llngColumnCount = LBound(Data, 2) To UBound(Data, 2)
                'insert the row
                xlWorkSheet.Cells(llngRowCount + RowOffset + llngNrOfHeadingsInserted + llngdelayed, llngColumnCount + ColumnOffset) = CStr(Data(llngRowCount, llngColumnCount))
                If (Status) Then
                    mfrmStatus.prgMain.value = (llngStatusCounter / (UBound(Data, 1) * UBound(Data, 2))) * 100
                End If
                llngStatusCounter = llngStatusCounter + 1
            Next llngColumnCount
'        End If
        
        
    
        'Allow a user interupt.
        DoEvents
        If (mCancelled) Then
            Unload mfrmStatus
            Set mfrmStatus = Nothing
            Exit Function
        End If
    Next llngRowCount
    
    If (Status) Then
        Unload mfrmStatus
        Set mfrmStatus = Nothing
    End If

    FillExcelSheet = llngNrOfHeadingsInserted
End Function

Public Function GetWorkbook(Optional blnShow As Boolean = False) As Excel.Workbook
Dim lxlWorkBook As Excel.Workbook
'Dim lxlWorkSheet As Excel.Worksheet
'Dim lxlChart As Excel.Chart
Dim lxlWindow As Excel.Window

On Error GoTo ErrorHandler
    
    objLogger.WriteLog ("GetWorkBook : Getexcel")
    'Get new or running excel
    If (mobjApp Is Nothing) Then
        Set mobjApp = GetExcel(mblnExcelWasNotRunning)
    End If
    
    If (mblnExcelWasNotRunning = False) Then
        objLogger.WriteLog ("GetWorkBook : excel was running, get current window")
        'Get current user window
        For Each lxlWindow In mobjApp.Windows
            If (lxlWindow.Visible) Then
                Set mxlCurrentWindow = lxlWindow
            End If
        Next lxlWindow
    End If
    
    objLogger.WriteLog ("GetWorkBook : Create a new Workbook")
    'Create a new Workbook
    Set lxlWorkBook = mobjApp.Workbooks.Add
    Set GetWorkbook = mobjApp.ActiveWorkbook
    
    'Show Excel, the user must handle excel from here.
    If blnShow Then mobjApp.Visible = blnShow
    
    Exit Function

ErrorHandler:
    Select Case Hex(Err.Number)
        Case 80070485
            Resume Next
        Case Else
            MsgBox "Unspecified Error : " & Err.Description & " (" & Err.Number & ")"
    End Select
    
    'Stop using Excel
    If (Not GetWorkbook Is Nothing) Then GetWorkbook.Close False
    If (mblnExcelWasNotRunning) Then
        mobjApp.Application.Quit
    Else
 '       Set lxlWorkBook = Nothing
        If (Not mxlCurrentWindow Is Nothing) Then mxlCurrentWindow.Visible = True
    End If
    
'    Set lxlChart = Nothing
'    Set lxlWorkSheet = Nothing
'    Set lxlWorkBook = Nothing
    Set lxlWindow = Nothing
End Function

Public Function DisplayDataInSheet(vntData As Variant, Range_Cell1 As Variant, Range_Cell2 As Variant, Optional Titel As String = "", Optional SubTitel As String = "", Optional Status As Boolean = False, Optional ShowExcel As Boolean = False) As Excel.Worksheet
Dim lxlWorkBook As Excel.Workbook
Dim lxlWorkSheet As Excel.Worksheet
Dim llngExtraInsertedHeaders As Long
    
    objLogger.WriteLog ("Extension : GetWorkbook")
    Set lxlWorkBook = GetWorkbook(False)
    Set lxlWorkSheet = lxlWorkBook.Sheets.Item(1)
    
    'Set rowheight
    lxlWorkSheet.Cells.Select
    lxlWorkSheet.Application.Selection.RowHeight = mRowHeight
    lxlWorkSheet.Rows("1:1").Select
    lxlWorkSheet.Application.Selection.RowHeight = 25
    lxlWorkSheet.Rows("2:2").Select
    lxlWorkSheet.Application.Selection.RowHeight = 11

    
    'lxlWorkSheet.Paste
    If (Len(Titel) > 0) Then
        objLogger.WriteLog ("Extension : add titel")
        With lxlWorkSheet.Range("A1")
            .value = Titel
            .HorizontalAlignment = xlCenter
            .Font.Bold = True
            .Font.Underline = True
            .Font.Name = "Tahoma"
            .Font.Size = "14"
        End With
        objLogger.WriteLog ("Extension : merge titel range")
        lxlWorkSheet.Range(GetCellName(vntData, kaStart, 2), Mid$(GetCellName(vntData, kaEnd, 2), 1, 1) & "1").Merge
        If (Len(SubTitel) > 0) Then
            objLogger.WriteLog ("Extension : add subtitle")
            With lxlWorkSheet.Range("A2")
                .value = "(" + SubTitel + ")"
                .HorizontalAlignment = xlLeft
                .Font.Bold = False
                .Font.Name = "Tahoma"
                .Font.Size = "7"
            End With
            objLogger.WriteLog ("Extension : merge subtitle range")
            lxlWorkSheet.Range("A2", Mid$(GetCellName(vntData, kaEnd, 3), 1, 1) & "2").Merge
            objLogger.WriteLog ("Extension : fillexcelsheet")
            llngExtraInsertedHeaders = FillExcelSheet(vntData, lxlWorkSheet, 3, 0, Status)
            Range_Cell1 = GetCellName(vntData, kaStart, 3)
            Range_Cell2 = GetCellName(vntData, kaEnd, 3 + llngExtraInsertedHeaders)
        Else
            objLogger.WriteLog ("Extension : fillexcelsheet")
            llngExtraInsertedHeaders = FillExcelSheet(vntData, lxlWorkSheet, 2, 0, Status)
            Range_Cell1 = GetCellName(vntData, kaStart, 2)
            Range_Cell2 = GetCellName(vntData, kaEnd, 2 + llngExtraInsertedHeaders)
        End If
    Else
        objLogger.WriteLog ("Extension : fillexcelsheet")
        llngExtraInsertedHeaders = FillExcelSheet(vntData, lxlWorkSheet, , , Status)
        Range_Cell1 = GetCellName(vntData, kaStart)
        Range_Cell2 = GetCellName(vntData, kaEnd, llngExtraInsertedHeaders)
    End If

    If (Not mCancelled) Then
        Set DisplayDataInSheet = lxlWorkSheet
    
        objLogger.WriteLog ("Extension : show excel")
        'Show Excel, the user must handle excel from here.
        mobjApp.Application.Visible = ShowExcel
    Else
        Set DisplayDataInSheet = Nothing
    End If
    Set lxlWorkBook = Nothing
End Function

Public Function DisplayDataInChart(vntData As Variant, ChartType As Excel.XlChartType, Optional Status As Boolean = False, Optional ShowExcel As Boolean = False) As Excel.Chart
Dim lxlWorkBook As Excel.Workbook
Dim lxlWorkSheet As Excel.Worksheet
Dim lxlChart As Excel.Chart
Dim lxlChartObject As Excel.ChartObject

    Set lxlWorkBook = GetWorkbook(False)
    Set lxlWorkSheet = lxlWorkBook.Sheets.Item(1)
    
    FillExcelSheet vntData, lxlWorkSheet, , , Status
    
    lxlWorkSheet.Range(GetCellName(vntData, kaStart), GetCellName(vntData, kaEnd)).Select
    Set lxlChart = lxlWorkBook.Charts.Add
    lxlChart.SizeWithWindow = True
    lxlChart.ChartType = ChartType
    
    'Autofit columns
    If (Not mCancelled) Then
        Set DisplayDataInChart = lxlChart
        
        'Show Excel, the user must handle excel from here.
        mobjApp.Application.Visible = ShowExcel
    Else
        Set DisplayDataInChart = Nothing
        Err.Raise cErrorUserrequestedCancel
    End If
End Function

Private Sub Class_Terminate()
    Set mobjApp = Nothing
    Set mxlCurrentWindow = Nothing
End Sub

Private Sub mfrmStatus_Cancelled()
    mCancelled = True
    If (mblnExcelWasNotRunning) Then
        'Stop using Excel
        If (mobjApp.Workbooks.Count > 0) Then mobjApp.ActiveWorkbook.Close False
        mobjApp.Application.Quit
    Else
        If (Not mxlCurrentWindow Is Nothing) Then mxlCurrentWindow.Visible = True
    End If
End Sub

