VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmStatus 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Kaisho Admin"
   ClientHeight    =   2256
   ClientLeft      =   36
   ClientTop       =   276
   ClientWidth     =   6120
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2256
   ScaleWidth      =   6120
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAnnuleren 
      Cancel          =   -1  'True
      Caption         =   "&Annuleren"
      Height          =   372
      Left            =   4560
      TabIndex        =   2
      Top             =   1800
      Width           =   1452
   End
   Begin MSComctlLib.ProgressBar prgMain 
      Height          =   252
      Left            =   120
      TabIndex        =   0
      Top             =   1320
      Width           =   5892
      _ExtentX        =   10393
      _ExtentY        =   445
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   6000
      Y1              =   1680
      Y2              =   1680
   End
   Begin VB.Label lblText 
      Caption         =   "Label1"
      Height          =   972
      Left            =   120
      TabIndex        =   1
      Top             =   240
      Width           =   5892
   End
End
Attribute VB_Name = "frmStatus"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Event Cancelled()

Public Sub OpenForm(Mode As kaStatusMode, Optional ShowMode As VBRUN.FormShowConstants = vbModeless)
    Select Case Mode
        Case kaMetProgress
            prgMain.Visible = True
            cmdAnnuleren.Visible = True
            Line1.Visible = True
            Me.Height = 2660
        Case kaZonderProgress
            prgMain.Visible = True
            cmdAnnuleren.Top = 600
            cmdAnnuleren.Visible = True
            Line1.Visible = False
            Me.Height = 1400
    End Select
    Me.Show ShowMode
    Me.ZOrder
End Sub

Private Sub cmdAnnuleren_Click()
    RaiseEvent Cancelled
    WaitAWhile 10
End Sub
