VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form frmLidLijst 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Ledenlijst"
   ClientHeight    =   5850
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6045
   Icon            =   "frmLidLijst.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5850
   ScaleWidth      =   6045
   Begin VB.CommandButton cmdVernieuwen 
      Caption         =   "Vern&ieuwen"
      Height          =   375
      Left            =   4200
      TabIndex        =   8
      Top             =   840
      Width           =   1695
   End
   Begin VB.ComboBox cboLidType 
      Height          =   315
      ItemData        =   "frmLidLijst.frx":030A
      Left            =   1080
      List            =   "frmLidLijst.frx":0347
      Style           =   2  'Dropdown List
      TabIndex        =   7
      Top             =   840
      Width           =   1815
   End
   Begin VB.CommandButton cmdVerwijderen 
      Caption         =   "&Verwijderen"
      Height          =   375
      Left            =   1560
      TabIndex        =   2
      Top             =   5400
      Width           =   1335
   End
   Begin VB.CommandButton cmdToevoegen 
      Caption         =   "&Toevoegen"
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   5400
      Width           =   1335
   End
   Begin MSComctlLib.ImageList imlColumnHeadings 
      Left            =   2520
      Top             =   5160
      _ExtentX        =   794
      _ExtentY        =   794
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      UseMaskColor    =   0   'False
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidLijst.frx":0445
            Key             =   "picDown"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidLijst.frx":0761
            Key             =   "picUp"
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdDetail 
      Caption         =   "&Details ..."
      Height          =   372
      Left            =   3000
      TabIndex        =   3
      Top             =   5400
      Width           =   1332
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Sluiten"
      Default         =   -1  'True
      Height          =   375
      Left            =   4560
      TabIndex        =   5
      Top             =   5400
      Width           =   1335
   End
   Begin MSComctlLib.ImageList imlGraden 
      Left            =   3000
      Top             =   5280
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   15
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidLijst.frx":0A7D
            Key             =   "picUnknownKYU"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidLijst.frx":0D97
            Key             =   "picBEGINNER"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidLijst.frx":10B1
            Key             =   "pic8KYU"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidLijst.frx":13CD
            Key             =   "pic7KYU"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidLijst.frx":16E9
            Key             =   "pic6KYU"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidLijst.frx":1A05
            Key             =   "pic5KYU"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidLijst.frx":1D21
            Key             =   "pic4KYU"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidLijst.frx":203D
            Key             =   "pic3KYU"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidLijst.frx":2359
            Key             =   "pic2KYU"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidLijst.frx":2675
            Key             =   "pic1KYU"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidLijst.frx":2991
            Key             =   "pic1DAN"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidLijst.frx":2CAD
            Key             =   "pic2DAN"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidLijst.frx":2FC9
            Key             =   "pic3DAN"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidLijst.frx":32E5
            Key             =   "pic4DAN"
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLidLijst.frx":3601
            Key             =   "pic5DAN"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lvwLeden 
      Height          =   3855
      Left            =   120
      TabIndex        =   0
      Top             =   1320
      Width           =   5775
      _ExtentX        =   10186
      _ExtentY        =   6800
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   12632256
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Naam"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Graad"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "ID"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Label lblLidCount 
      Caption         =   "Aantal: "
      Height          =   375
      Left            =   3000
      TabIndex        =   9
      Top             =   840
      Width           =   1095
   End
   Begin VB.Label lblLidType 
      Caption         =   "Leden :"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   840
      Width           =   855
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   240
      Picture         =   "frmLidLijst.frx":391D
      Top             =   210
      Width           =   480
   End
   Begin VB.Label lblTitel 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Aanwezigheidslijst"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   612
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   5772
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   5880
      Y1              =   5280
      Y2              =   5280
   End
End
Attribute VB_Name = "frmLidLijst"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mOpenMode As kaLedendLijstMode
Private mMode As kaFormMode

Public Property Let Mode(NewMode As kaFormMode)
    mMode = NewMode
    SetButtons mMode
    SetMenu mMode
End Property

Public Property Get Mode() As kaFormMode
    Mode = mMode
End Property

Private Sub cmdAnnuleren_Click()
    Unload Me
End Sub

Private Sub cboLidType_Click()
    FillListView cboLidType.ListIndex
End Sub

Private Sub cmdDetail_Click()
    If (lvwLeden.SelectedItem Is Nothing) Then
        MsgBox "Kan geen details tonen. Selecteer een Lid door in deze lijst te klikken en druk dan op de knop details.", vbInformation
    Else
        ShowDetail lvwLeden.SelectedItem.ListSubItems(2)
    End If
End Sub

Private Sub cmdOk_Click()
    Unload Me
End Sub

Private Sub cmdToevoegen_Click()
    If (mOpenMode = kaAlleLeden) Then
        Mode = kaFormVerwerking
        Set mfrmLidDetail = New frmLidDetail
        mfrmLidDetail.OpenForm mclngNieuwLid
        
        mfrmLidDetail.Show vbModal
        Set mfrmLidDetail = Nothing
        FillListView mOpenMode
        Mode = kaFormOpen
    End If
End Sub

Private Sub cmdVernieuwen_Click()
    FillListView cboLidType.ListIndex
End Sub

Private Sub cmdVerwijderen_Click()
On Error GoTo ErrorHandler

    If (lvwLeden.SelectedItem Is Nothing) Then
        MsgBox "Je moet eerst een lid aanduiden om het te kunnen verwijderen", vbExclamation
    Else
        Mode = kaFormVerwerking
        With DataEnvironment1.rsLeden
            If (Not (.EOF And .BOF)) Then
                .MoveFirst
                .Find "LidID=" + CStr(lvwLeden.SelectedItem.ListSubItems(2))
                If (Not .EOF) Then
                    If (MsgBox("Ben je zeker dat je lid '" & lvwLeden.SelectedItem & "' wilt verwijderen?", vbQuestion + vbYesNo) = vbYes) Then
                        .Delete
                        .Requery
                    End If
                End If
            End If
        End With
        FillListView mOpenMode
        Mode = kaFormOpen
        lvwLeden.SetFocus
    End If
    
    Exit Sub
    
ErrorHandler:
    Select Case Err.Number
        Case Else
            MsgBox "Er is een fout opgetreden, sluit het scherm af en probeer opnieuw. Indien de fout zich blijft herhalen, contacteer dan " & mcstrAdministrator & " en geef volgende informatie : " & vbCrLf & vbCrLf & Err.Description & vbCrLf & Err.Source & vbCrLf & Err.Number, vbExclamation
    End Select
End Sub

Private Sub Form_Activate()
    lvwLeden.SetFocus
End Sub

Public Sub OpenForm(OpenMode As kaLedendLijstMode)
    Mode = kaFormLaden
    Select Case OpenMode
        Case kaLedendLijstMode.kaAlleLeden
            Me.Caption = "LedenLijst (Alle leden)"
        Case kaLedendLijstMode.kaBestaandeLeden
            Me.Caption = "LedenLijst (Verzekerde leden)"
'            Me.cmdVerwijderen.Enabled = False
'            Me.cmdToevoegen.Enabled = False
'            Me.cmdToevoegen.Left = Me.cmdOk.Left
        Case kaLedendLijstMode.kaNieuweLeden
            Me.Caption = "LedenLijst (Nieuwe leden)"
'            Me.cmdVerwijderen.Visible = False
'            Me.cmdOk.Visible = False
'            Me.cmdToevoegen.Left = Me.cmdOk.Left
    End Select
    lblTitel.Caption = vbCrLf + Me.Caption
    FillListView OpenMode
    mOpenMode = OpenMode
    Me.ZOrder
    Mode = kaFormOpen
    lvwLeden.SetFocus
End Sub

Private Sub Form_Load()
    cboLidType.ListIndex = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Mode = kaFormOntLaden

'    EndUsingRecordset DataEnvironment1.rsLeden
    Set mfrmLidLijst = Nothing
End Sub

Private Sub ShowDetail(lngLidID As Long)
    Mode = kaFormVerwerking

    Set mfrmLidDetail = New frmLidDetail
    mfrmLidDetail.OpenForm lngLidID
    
    mfrmLidDetail.Show vbModal
    Set mfrmLidDetail = Nothing
    lvwLeden.SetFocus
    Mode = kaFormOpen
End Sub

Private Sub lvwLeden_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    SortListViewColumn Me.lvwLeden, ColumnHeader
End Sub

Private Sub lvwLeden_DblClick()
    ShowDetail lvwLeden.SelectedItem.ListSubItems(2)
End Sub

Private Sub FillListView(OpenMode As kaLedendLijstMode)
Dim itmX As ListItem
Dim lvntGraad As Variant
   
    Mode = kaFormVerwerking
    lvwLeden.Visible = False
   
    Set lvwLeden.SmallIcons = imlGraden
    lvwLeden.View = lvwReport   ' Set view to Report.
    lvwLeden.BorderStyle = ccNone
    lvwLeden.ColumnHeaderIcons = imlColumnHeadings
    lvwLeden.ListItems.Clear
   
    lvwLeden.ColumnHeaders(1).Width = (lvwLeden.Width / 6) * 4
    lvwLeden.ColumnHeaders(2).Width = (lvwLeden.Width / 5) * 1
    lvwLeden.ColumnHeaders(3).Width = 0
   
    StartUsingRecordset DataEnvironment1.rsLeden
    With DataEnvironment1.rsLeden
        Select Case OpenMode
            Case kaLedendLijstMode.kaAlleLeden
                .Filter = ""
                lvwLeden.CheckBoxes = False
            Case kaLedendLijstMode.kaBestaandeLeden
                .Filter = "Nieuw=0"
                lvwLeden.CheckBoxes = False
            Case kaLedendLijstMode.kaNieuweLeden
                .Filter = "Nieuw=1"
                lvwLeden.CheckBoxes = False
            Case kaLedendLijstMode.kaInactieveLeden
                .Filter = "InActief=1"
                lvwLeden.CheckBoxes = False
            Case kaLedendLijstMode.kaActieveLeden
                .Filter = "InActief=0"
                lvwLeden.CheckBoxes = False
            Case kaLedendLijstMode.ka9Kyu
                .Filter = "Graad='Beginner'"
                lvwLeden.CheckBoxes = False
            Case kaLedendLijstMode.ka8Kyu
                .Filter = "Graad='8 Kyu'"
                lvwLeden.CheckBoxes = False
            Case kaLedendLijstMode.ka7Kyu
                .Filter = "Graad='7 Kyu'"
                lvwLeden.CheckBoxes = False
            Case kaLedendLijstMode.ka6Kyu
                .Filter = "Graad='6 Kyu'"
                lvwLeden.CheckBoxes = False
            Case kaLedendLijstMode.ka5Kyu
                .Filter = "Graad='5 Kyu'"
                lvwLeden.CheckBoxes = False
            Case kaLedendLijstMode.ka4Kyu
                .Filter = "Graad='4 Kyu'"
                lvwLeden.CheckBoxes = False
            Case kaLedendLijstMode.ka3Kyu
                .Filter = "Graad='3 Kyu'"
                lvwLeden.CheckBoxes = False
            Case kaLedendLijstMode.ka2Kyu
                .Filter = "Graad='2 Kyu'"
                lvwLeden.CheckBoxes = False
            Case kaLedendLijstMode.ka1Kyu
                .Filter = "Graad='1 Kyu'"
                lvwLeden.CheckBoxes = False
            Case kaLedendLijstMode.ka1Dan
                .Filter = "Graad='1 Dan'"
                lvwLeden.CheckBoxes = False
            Case kaLedendLijstMode.ka2Dan
                .Filter = "Graad='2 Dan'"
                lvwLeden.CheckBoxes = False
            Case kaLedendLijstMode.ka3Dan
                .Filter = "Graad='3 Dan'"
                lvwLeden.CheckBoxes = False
            Case kaLedendLijstMode.ka4Dan
                .Filter = "Graad='4 Dan'"
                lvwLeden.CheckBoxes = False
            Case kaLedendLijstMode.ka5Dan
                .Filter = "Graad='5 Dan'"
                lvwLeden.CheckBoxes = False
        End Select
        While Not .EOF
            lvntGraad = .Fields("Graad").value
            Set itmX = lvwLeden.ListItems.Add(, , IIf(IsNull(.Fields("Naam").value), "", .Fields("Naam").value))
            If (Not IsNull(lvntGraad)) Then
                itmX.ListSubItems.Add , , , "pic" + UCase(Join(Split(CStr(lvntGraad), " "), ""))
                'Set itmX = lvwLeden.ListItems.Add(, , CStr(lvntGraad), , "pic" + UCase(Join(Split(CStr(lvntGraad), " "), "")))
            Else
                itmX.ListSubItems.Add , , , "picUnknownKYU"
                'Set itmX = lvwLeden.ListItems.Add(, , , , "picUnknownKYU")
            End If
            itmX.ListSubItems.Add , "ID" & .Fields("LidID").value, .Fields("LidID").value
            .MoveNext
        Wend
    End With
    
    If (lvwLeden.ListItems.Count > 0) Then
        lvwLeden.ListItems(1).Selected = True
    End If
    
    lvwLeden.Visible = True
    lvwLeden.Refresh
    Mode = kaFormOpen
    
    'Sort
    lvwLeden.SortKey = lvwLeden.ColumnHeaders(1).Index - 1
    lvwLeden.Sorted = True
    lvwLeden.SortOrder = lvwAscending
    lblLidCount.Caption = "Aantal: " & DataEnvironment1.rsLeden.RecordCount
End Sub
         
Public Sub SetButtons(Mode As kaFormMode)
    Select Case Mode
        Case kaFormMode.kaFormLaden, kaFormMode.kaFormOntLaden
            Me.cmdDetail.Enabled = False
            Me.cmdToevoegen.Enabled = False
            Me.cmdOk.Enabled = False
            Me.cmdVerwijderen.Enabled = False
        Case kaFormMode.kaFormOpen
            Me.cmdDetail.Enabled = True
            Me.cmdToevoegen.Enabled = True
            Me.cmdOk.Enabled = True
            Me.cmdVerwijderen.Enabled = True
        Case kaFormMode.kaFormVerversen, kaFormMode.kaFormVerwerking
            Me.cmdDetail.Enabled = False
            Me.cmdToevoegen.Enabled = False
            Me.cmdOk.Enabled = True
            Me.cmdVerwijderen.Enabled = False
    End Select
    DoEvents
End Sub

Public Sub SetMenu(Mode As kaFormMode)
    Select Case Mode
        Case kaFormMode.kaFormLaden
            mfrmMDIMain.mnuLidLijst.Enabled = False
            mfrmMDIMain.mnuLidNieuw.Enabled = False
            mfrmMDIMain.mnuLeden.Enabled = False
        Case kaFormMode.kaFormOpen
            mfrmMDIMain.mnuLidLijst.Enabled = False
            mfrmMDIMain.mnuLidNieuw.Enabled = True
            mfrmMDIMain.mnuLeden.Enabled = True
        Case kaFormMode.kaFormVerversen, kaFormMode.kaFormVerwerking
            mfrmMDIMain.mnuLidLijst.Enabled = False
            mfrmMDIMain.mnuLidNieuw.Enabled = False
            mfrmMDIMain.mnuLeden.Enabled = True
        Case kaFormMode.kaFormOntLaden
            mfrmMDIMain.mnuLidLijst.Enabled = True
            mfrmMDIMain.mnuLidNieuw.Enabled = True
            mfrmMDIMain.mnuLeden.Enabled = True
    End Select
    DoEvents
End Sub

