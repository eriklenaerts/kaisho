using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace KA
{
    class ToolStripColors : ProfessionalColorTable
    {  
        public override Color ToolStripGradientBegin
        { get { return Color.FromArgb(203, 225, 252); } }

        public override Color ToolStripGradientMiddle
        { get { return Color.FromArgb(203, 225, 252); } }

        public override Color ToolStripGradientEnd
        { get { return Color.FromArgb(126, 148, 178); } }

        public override Color MenuStripGradientBegin
            { get { return Color.FromArgb(203, 225, 252); } }

        public override Color MenuStripGradientEnd
            { get { return Color.FromArgb(49, 106, 197); } }
        
             //{ get { return Color.FromArgb(126, 148, 178); } }
              //{ get { return Color.FromArgb(203, 225, 252); } }
        
    }

    

}
