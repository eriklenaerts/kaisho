using System;
using System.Windows.Forms;
using System.Reflection;
using KA.Forms;


namespace KA
{
    static class Program
    {
        private static Assembly assembly = Assembly.GetExecutingAssembly();
        private static string assemblyPath = Program.assembly.GetName().Name.Replace(' ', '_');

        public static System.IO.Stream GetResource(string fileName)
        {
            return Program.assembly.GetManifestResourceStream(Program.assemblyPath + '.' + fileName);
        }


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            new SplashScreen().Show();
            
            DataStorageHalper dsHelper = new DataStorageHalper();
            if (dsHelper.IsDataStorageReady())
            {
                
                BackupProposal backupProposal = dsHelper.GetBackupProposal();
                if (backupProposal != null)
                {
                    BackupDataStorageDialog backupDialog = new BackupDataStorageDialog();
                    string backupFolder = backupDialog.ShowDialog(backupProposal);
                    if (!string.IsNullOrEmpty(backupFolder))
                    {
                        dsHelper.BackupDatabase(backupFolder);

                        MessageBox.Show("Database backup geslaagd. Systeem start Kaisho admin nu op...", "Backup", MessageBoxButtons.OK,
                                        MessageBoxIcon.Information);
                    }
                }

                Application.Run(new Form1());
            }
            else
                MessageBox.Show(dsHelper.GetProblemDescription(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            if (MessageCenterController._messageCenter == null)
                MessageBox.Show("An error occured during the startup of Kaisho Admin. Please review the following error trace : " + e.Exception.Message, "Error");
            else
                MessageCenterController.ShowMessageCenter(new MessageCenterMessage("Error", e.Exception.Message));
        }
    }
}