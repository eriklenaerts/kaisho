using System;
using System.Collections.Generic;
using System.Text;
using KA.Forms;
using KA_Controller;
using System.Windows.Forms;
using KA_Controller.Interfaces;
using KA.CustomControls;

namespace KA
{
    public class ViewFactory : IViewFactory
    {

        public ViewFactory(Form1 mdiParent)
        {
            _MDIParent = mdiParent;
        }

        private Form1 _MDIParent;
        public Form1 MDIParent
        {
            set { _MDIParent = value; }
        }

        #region TRASH

        //private IListViewBase _MemberListView;
        //public IListViewBase MemberListView
        //{
        //    get
        //    {
        //        if (_MemberListView == null || _MemberListView.IsDisposed)
        //        {
        //            _MemberListView = new MemberList();
        //            _MemberListView.Init(_MDIParent);
        //        }
        //        return _MemberListView;
        //    }
        //}

        //private IMemberDetailsView _MemberDetailView;
        //public IMemberDetailsView MemberDetailView
        //{
        //    get
        //    {
        //        if (_MemberDetailView == null || _MemberDetailView.IsDisposed)
        //        {
        //            _MemberDetailView = new MemberDetail();
        //        }
        //        return _MemberDetailView;
        //    }
        //}

        //private IListViewBase _ExamListView;
        //public IListViewBase ExamListView
        //{
        //    get
        //    {
        //        if (_ExamListView == null || _ExamListView.IsDisposed)
        //        {
        //            _ExamListView = new Exams();
        //            _ExamListView.Init(_MDIParent);
        //        }
        //        return _ExamListView;
        //    }
        //}

        //private IListViewBase _StatisticsView;
        //public IListViewBase StatisticsView
        //{
        //    get
        //    {
        //        if (_StatisticsView == null || _StatisticsView.IsDisposed)
        //        {
        //            _StatisticsView = new Statistics();
        //            _StatisticsView.Init(_MDIParent);
        //        }
        //        return _StatisticsView;
        //    }
        //} 
        #endregion

        #region IViewFactory Members

        IViewBase IViewFactory.CreateView(KA_Common.Enums.Controller controller)
        {
            switch (controller)
            {
                case KA_Common.Enums.Controller.MemberListController:
                    MemberList memberlist = new MemberList();
                    memberlist.Init(_MDIParent);
                    return memberlist;
                case KA_Common.Enums.Controller.MemberDetailController:
                    return new MemberDetail();
                case KA_Common.Enums.Controller.ExamListController:
                    Exams exams = new Exams();
                    exams.Init(_MDIParent);
                    return exams;
                case KA_Common.Enums.Controller.StatisticsController:
                    Statistics statistics = new Statistics();
                    statistics.Init(_MDIParent);
                    return statistics;
                case KA_Common.Enums.Controller.StatisticsViewerController:
                    StatisticsViewer statisticsViewer = new StatisticsViewer();
                    statisticsViewer.Init(_MDIParent);
                    return statisticsViewer;
                case KA_Common.Enums.Controller.FamilyListController:
                    FamilieBeheer familyManagement = new FamilieBeheer();
                    familyManagement.Init(_MDIParent);
                    return familyManagement;
                case KA_Common.Enums.Controller.FamilyDetailController:
                    return new FamilyDetail();
                case KA_Common.Enums.Controller.MemberHistoryController:
                    return new MemberHistory();
                case KA_Common.Enums.Controller.OptionsController:
                    Options options = new Options();
                    options.Init(_MDIParent);
                    return options;
                case KA_Common.Enums.Controller.TrainingDaysDetailController:
                    return new TrainingDaysDetail();
                case KA_Common.Enums.Controller.TrainingDaysExceptionsDetailController:
                    return new TrainingDaysExceptionDetail();
                case KA_Common.Enums.Controller.AddHistory:
                    return new AddHistory();
                default:
                    break;
            }
            return null;
            
        }

        #endregion
    }
}
