using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using KA.Forms;
using System.Threading;
using KA_Controller;
using KA_Common;
using KA.app_code;
using KA.CustomControls;
using System.Configuration;
using NLog;

namespace KA
{
    public partial class Form1 : Form
    {
        private MainController _mdiController;
        public MainController MDIController
        {
            get { return _mdiController; }
            set { _mdiController = value; }
        }
	

        public Form1()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            MessageCenterController.SetMessageCenter(this.messageCenter1);
            ApplicationWaitCursor.Cursor = Cursors.WaitCursor;
            ApplicationWaitCursor.Delay = new TimeSpan(0, 0, 0, 0, 250);

            try
            {
                this.Text += " v. " + ConfigurationManager.AppSettings["version"].ToString();
                MDIController = new MainController(new ViewFactory(this));
                //this.optionsMenu1.ActionClicked += new KA.CustomControls.ActionClick(optionsMenu1_ActionClicked);
                this.optionsMenu1.ActionClicked += new EventHandler<KA.app_code.EventArgs.ActionEventArgs>(optionsMenu1_ActionClicked);
                MDIController.ExecuteAction(Enums.Controller.MemberListController, Enums.Actions.showview, null);
            }
            catch (Exception e)
            {
                MessageCenterController.ShowMessageCenter(new MessageCenterMessage("error", e.Message));
                LogManager.GetCurrentClassLogger().ErrorException("Error in Kaisho Admin " + ConfigurationManager.AppSettings["version"].ToString(), e);
            }
        }

        void optionsMenu1_ActionClicked(object sender, KA.app_code.EventArgs.ActionEventArgs e)
        {
            try
            {
                MDIController.ExecuteAction(e.Action.Controller, e.Action.ActionName, e.Action.Context); //null
            }
            catch (Exception ex)
            {
                MessageCenterController.ShowMessageCenter(new MessageCenterMessage("Exception", ex.Message));
            }
        }

        private void outlookBar1_Click(object sender, OutlookStyleControls.OutlookBar.ButtonClickEventArgs e)
        {
                int idx = outlookBar1.Buttons.IndexOf(e.SelectedButton);
                switch (idx)
                {
                    case 0: // ledenlijst
                        MDIController.ExecuteAction(Enums.Controller.MemberListController, Enums.Actions.showview, null);
                        break;
                    case 1: // familiebeheer
                        MDIController.ExecuteAction(Enums.Controller.FamilyListController, Enums.Actions.showview, null);
                        break;
                    case 2: // examens
                        MDIController.ExecuteAction(Enums.Controller.ExamListController, Enums.Actions.showview, null);
                        //MessageBox.Show("Examen beheer wordt geimplementeerd in versie 2.0.2", "Opgelet", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    case 3: // statistieken
                        MDIController.ExecuteAction(Enums.Controller.StatisticsController, Enums.Actions.showview, null);
                        break;
                    case 4: // exit
                        CloseForm close = new CloseForm();
                        close.ShowDialog();
                        break;
                    default: // notes
                        break;
                }
        }

        public void SetSelectedOutlookButton(int index)
        {
            outlookBar1.SelectedButton = outlookBar1.Buttons[index];
        }

        public void SetActions(ActionList<Action> actionList)
        {
            this.optionsMenu1.SetActions(actionList);
        }

        public void DisableAction(string action, bool disable)
        {
            this.optionsMenu1.DisableAction(action, disable);
        }

        private void horizontaalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void verticaalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileVertical);
        }

        private void cascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.Cascade);
        }

        private void optiesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MDIController.ExecuteAction(Enums.Controller.OptionsController, Enums.Actions.showview, null);
        }       
    }
}