﻿namespace KA.Forms
{
    partial class Options
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Options));
            this.tcOptions = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lstTrainingDays = new System.Windows.Forms.ListView();
            this.periode = new System.Windows.Forms.ColumnHeader();
            this.locatie = new System.Windows.Forms.ColumnHeader();
            this.dag = new System.Windows.Forms.ColumnHeader();
            this.beginuur = new System.Windows.Forms.ColumnHeader();
            this.einduur = new System.Windows.Forms.ColumnHeader();
            this.publiek = new System.Windows.Forms.ColumnHeader();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.LabelEx = new KA.CustomControls.Extensions.LabelEx();
            this.tabHolidays = new System.Windows.Forms.TabPage();
            this.lstTrainingDaysExceptions = new System.Windows.Forms.ListView();
            this.Date = new System.Windows.Forms.ColumnHeader();
            this.imageList3 = new System.Windows.Forms.ImageList(this.components);
            this.lblVacation = new KA.CustomControls.Extensions.LabelEx();
            this.datum = new System.Windows.Forms.ColumnHeader("(none)");
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.lblOpties = new KA.CustomControls.Extensions.LabelEx();
            this.tcOptions.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabHolidays.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcOptions
            // 
            this.tcOptions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tcOptions.Controls.Add(this.tabPage2);
            this.tcOptions.Controls.Add(this.tabHolidays);
            this.tcOptions.ItemSize = new System.Drawing.Size(58, 25);
            this.tcOptions.Location = new System.Drawing.Point(-2, 38);
            this.tcOptions.Name = "tcOptions";
            this.tcOptions.SelectedIndex = 0;
            this.tcOptions.ShowToolTips = true;
            this.tcOptions.Size = new System.Drawing.Size(788, 547);
            this.tcOptions.TabIndex = 0;
            this.tcOptions.SelectedIndexChanged += new System.EventHandler(this.tcOptions_SelectedIndexChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(245)))), ((int)(((byte)(248)))));
            this.tabPage2.Controls.Add(this.lstTrainingDays);
            this.tabPage2.Controls.Add(this.LabelEx);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(780, 514);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Trainingsdagen";
            // 
            // lstTrainingDays
            // 
            this.lstTrainingDays.AllowColumnReorder = true;
            this.lstTrainingDays.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstTrainingDays.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.periode,
            this.locatie,
            this.dag,
            this.beginuur,
            this.einduur,
            this.publiek});
            this.lstTrainingDays.FullRowSelect = true;
            this.lstTrainingDays.Location = new System.Drawing.Point(0, 41);
            this.lstTrainingDays.MultiSelect = false;
            this.lstTrainingDays.Name = "lstTrainingDays";
            this.lstTrainingDays.Size = new System.Drawing.Size(780, 468);
            this.lstTrainingDays.SmallImageList = this.imageList1;
            this.lstTrainingDays.TabIndex = 7;
            this.lstTrainingDays.UseCompatibleStateImageBehavior = false;
            this.lstTrainingDays.View = System.Windows.Forms.View.Details;
            this.lstTrainingDays.SelectedIndexChanged += new System.EventHandler(this.lstTrainingDays_SelectedIndexChanged);
            this.lstTrainingDays.DoubleClick += new System.EventHandler(this.lstTrainingDays_DoubleClick);
            this.lstTrainingDays.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstTrainingDays_ColumnClick);
            // 
            // periode
            // 
            this.periode.Text = "        Periode";
            this.periode.Width = 127;
            // 
            // locatie
            // 
            this.locatie.Text = "Locatie";
            this.locatie.Width = 152;
            // 
            // dag
            // 
            this.dag.Text = "Dag";
            this.dag.Width = 126;
            // 
            // beginuur
            // 
            this.beginuur.Text = "Begin uur";
            this.beginuur.Width = 101;
            // 
            // einduur
            // 
            this.einduur.Text = "Eind uur";
            this.einduur.Width = 122;
            // 
            // publiek
            // 
            this.publiek.Text = "Publiek";
            this.publiek.Width = 139;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "date-time.png");
            // 
            // LabelEx
            // 
            this.LabelEx.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelEx.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelEx.ForeColor = System.Drawing.Color.Black;
            this.LabelEx.HighColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252)))));
            this.LabelEx.Image = global::KA.Properties.Resources.boxing_gloves_red;
            this.LabelEx.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.LabelEx.Location = new System.Drawing.Point(3, 3);
            this.LabelEx.LowColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178)))));
            this.LabelEx.Name = "LabelEx";
            this.LabelEx.Size = new System.Drawing.Size(780, 35);
            this.LabelEx.TabIndex = 6;
            this.LabelEx.Text = "Trainingsdagen";
            this.LabelEx.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabHolidays
            // 
            this.tabHolidays.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(245)))), ((int)(((byte)(248)))));
            this.tabHolidays.Controls.Add(this.lstTrainingDaysExceptions);
            this.tabHolidays.Controls.Add(this.lblVacation);
            this.tabHolidays.Location = new System.Drawing.Point(4, 29);
            this.tabHolidays.Name = "tabHolidays";
            this.tabHolidays.Padding = new System.Windows.Forms.Padding(3);
            this.tabHolidays.Size = new System.Drawing.Size(780, 514);
            this.tabHolidays.TabIndex = 2;
            this.tabHolidays.Text = "Vakantiedagen";
            // 
            // lstTrainingDaysExceptions
            // 
            this.lstTrainingDaysExceptions.AllowColumnReorder = true;
            this.lstTrainingDaysExceptions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstTrainingDaysExceptions.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Date});
            this.lstTrainingDaysExceptions.FullRowSelect = true;
            this.lstTrainingDaysExceptions.Location = new System.Drawing.Point(3, 41);
            this.lstTrainingDaysExceptions.MultiSelect = false;
            this.lstTrainingDaysExceptions.Name = "lstTrainingDaysExceptions";
            this.lstTrainingDaysExceptions.Size = new System.Drawing.Size(774, 470);
            this.lstTrainingDaysExceptions.SmallImageList = this.imageList3;
            this.lstTrainingDaysExceptions.TabIndex = 9;
            this.lstTrainingDaysExceptions.UseCompatibleStateImageBehavior = false;
            this.lstTrainingDaysExceptions.View = System.Windows.Forms.View.Details;
            this.lstTrainingDaysExceptions.SelectedIndexChanged += new System.EventHandler(this.lstTrainingDaysExceptions_SelectedIndexChanged);
            this.lstTrainingDaysExceptions.DoubleClick += new System.EventHandler(this.lstTrainingDaysExceptions_DoubleClick);
            // 
            // Date
            // 
            this.Date.Text = "        Datum";
            this.Date.Width = 758;
            // 
            // imageList3
            // 
            this.imageList3.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList3.ImageStream")));
            this.imageList3.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList3.Images.SetKeyName(0, "calendar_3.png");
            // 
            // lblVacation
            // 
            this.lblVacation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVacation.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVacation.ForeColor = System.Drawing.Color.Black;
            this.lblVacation.HighColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252)))));
            this.lblVacation.Image = global::KA.Properties.Resources.palm;
            this.lblVacation.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblVacation.Location = new System.Drawing.Point(3, 3);
            this.lblVacation.LowColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178)))));
            this.lblVacation.Name = "lblVacation";
            this.lblVacation.Size = new System.Drawing.Size(774, 35);
            this.lblVacation.TabIndex = 8;
            this.lblVacation.Text = "Vakantiedagen";
            this.lblVacation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // datum
            // 
            this.datum.Text = "        Datum";
            this.datum.Width = 404;
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "add.png");
            this.imageList2.Images.SetKeyName(1, "delete.png");
            this.imageList2.Images.SetKeyName(2, "refresh.png");
            this.imageList2.Images.SetKeyName(3, "document_into.png");
            // 
            // lblOpties
            // 
            this.lblOpties.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblOpties.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOpties.ForeColor = System.Drawing.Color.Black;
            this.lblOpties.HighColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252)))));
            this.lblOpties.Image = global::KA.Properties.Resources.control_panel;
            this.lblOpties.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblOpties.Location = new System.Drawing.Point(0, 0);
            this.lblOpties.LowColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178)))));
            this.lblOpties.Name = "lblOpties";
            this.lblOpties.Size = new System.Drawing.Size(786, 35);
            this.lblOpties.TabIndex = 3;
            this.lblOpties.Text = "Opties";
            this.lblOpties.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Options
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(106)))), ((int)(((byte)(197)))));
            this.ClientSize = new System.Drawing.Size(786, 583);
            this.Controls.Add(this.lblOpties);
            this.Controls.Add(this.tcOptions);
            this.Name = "Options";
            this.Text = "Options";
            this.Load += new System.EventHandler(this.Options_Load);
            this.Activated += new System.EventHandler(this.Options_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Options_FormClosed);
            this.tcOptions.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabHolidays.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcOptions;
        private System.Windows.Forms.TabPage tabHolidays;
        private KA.CustomControls.Extensions.LabelEx lblOpties;
        private System.Windows.Forms.ColumnHeader datum;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListView lstTrainingDays;
        private System.Windows.Forms.ColumnHeader periode;
        private System.Windows.Forms.ColumnHeader locatie;
        private System.Windows.Forms.ColumnHeader dag;
        private System.Windows.Forms.ColumnHeader beginuur;
        private System.Windows.Forms.ColumnHeader einduur;
        private System.Windows.Forms.ColumnHeader publiek;
        private KA.CustomControls.Extensions.LabelEx LabelEx;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.ListView lstTrainingDaysExceptions;
        private System.Windows.Forms.ColumnHeader Date;
        private KA.CustomControls.Extensions.LabelEx lblVacation;
        private System.Windows.Forms.ImageList imageList3;
    }
}