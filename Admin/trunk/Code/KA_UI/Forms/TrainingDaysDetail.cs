﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using KA_Controller.Interfaces;
using BL = KA.BusinessLogicLayer;
using System.Globalization;
using SL = KA.ServiceLayer;
using KA.BusinessLogicLayer;
using KA.app_code;

namespace KA.Forms
{
    public partial class TrainingDaysDetail : Form, ITrainingDaysDetailView
    {
       
        private BL.TrainingTime _trainingTime;
        private bool _new = true;

        public TrainingDaysDetail()
        {
            InitializeComponent();

            
        }

        #region ITrainingDaysDetailView Members

        public void SetContext(KA.BusinessLogicLayer.TrainingTime trainingTime)
        {
            if (trainingTime != null)
            {
                _trainingTime = trainingTime;
                _new = false;

                CultureInfo dutch = CultureInfo.GetCultureInfo("nl-BE");
                lblTitle.Text += " " + dutch.DateTimeFormat.DayNames[trainingTime.DayOfTheWeek] + " " + trainingTime.Audience;

                txtAudience.Text = trainingTime.Audience;
                txtEndHour.Text = trainingTime.EndOfTraining.Split(new char[] { ':' })[0];
                txtEndMinute.Text = trainingTime.EndOfTraining.Split(new char[] { ':' })[1];
                txtStartHour.Text = trainingTime.StartOfTraining.Split(new char[] { ':' })[0];
                txtStartMinute.Text = trainingTime.StartOfTraining.Split(new char[] { ':' })[1];

                cmbDayOfWeek.SelectedIndex = trainingTime.DayOfTheWeek;
                cmbLocation.SelectedValue = trainingTime.LocationID;
                cmbPeriod.SelectedValue = trainingTime.TrainingPeriodID;
            }
            else
            {
                _new = true;
                CleanFormData();
            }
        }

        void ITrainingDaysDetailView.LoadData(TList<Location> locations, TList<TrainingPeriod> periods)
        {
            LoadDaysOfTheWeek();
            LoadLocations(locations);
            LoadPeriods(periods);
        }
        #endregion

        #region IViewBase Members


        public event EventHandler ViewClosed;

        public event EventHandler<KA_Controller.EventArgs.ActionEventArgs> ActionInvoked;

        public void Init(Form mdiParent)
        {
            
        }

        public void ShowBrokenRules(KA.BusinessLogicLayer.Validation.BrokenRulesList brokenRules)
        {
            MessageCenterController.ShowMessageCenter(brokenRules);
        }

        #endregion

        private void btnOK_Click(object sender, EventArgs e)
        {
            if(_trainingTime == null) //new
                _trainingTime = new TrainingTime();

            _trainingTime.Audience = txtAudience.Text;
            _trainingTime.EndOfTraining = txtEndHour.Text + ":" + txtEndMinute.Text;
            _trainingTime.StartOfTraining = txtStartHour.Text + ":" + txtStartMinute.Text;

            _trainingTime.DayOfTheWeek = (byte)cmbDayOfWeek.SelectedIndex;
            _trainingTime.LocationID = (int)cmbLocation.SelectedValue;
            _trainingTime.TrainingPeriodID = (int)cmbPeriod.SelectedValue;

            if(!_new) //update
            {
                if(ActionInvoked != null)
                    ActionInvoked(this, new KA_Controller.EventArgs.ActionEventArgs(KA_Common.Enums.Actions.update, KA_Common.Enums.Controller.TrainingDaysDetailController, _trainingTime));
            }
            else //new
            {
                if(ActionInvoked != null)
                    ActionInvoked(this, new KA_Controller.EventArgs.ActionEventArgs(KA_Common.Enums.Actions.add, KA_Common.Enums.Controller.TrainingDaysDetailController, _trainingTime));
            }
        }

        private void CleanFormData()
        {
            txtAudience.Text = string.Empty;
            txtEndHour.Text = string.Empty;
            txtEndMinute.Text = string.Empty;
            txtStartHour.Text = string.Empty;
            txtStartMinute.Text = string.Empty;

            cmbDayOfWeek.SelectedIndex = 0;
            cmbLocation.SelectedValue = 0;
            cmbPeriod.SelectedValue = 0;
        }

        private void TrainingDaysDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (ViewClosed != null)
                ViewClosed(this, new EventArgs());
        }

        private void LoadPeriods(TList<BL.TrainingPeriod> periods)
        {
            cmbPeriod.DataSource = periods;
            cmbPeriod.ValueMember = "ID";
            cmbPeriod.DisplayMember = "Name";
        }

        private void LoadLocations(TList<BL.Location> locations)
        {
            cmbLocation.DataSource = locations;
            cmbLocation.ValueMember = "ID";
            cmbLocation.DisplayMember = "Name";
        }

        private void LoadDaysOfTheWeek()
        {
            CultureInfo dutch = CultureInfo.GetCultureInfo("nl-BE");
            for (int i = 0; i < 7; i++)
            {
                cmbDayOfWeek.Items.Add(dutch.DateTimeFormat.DayNames[i]);
            }
        }

        
    }
}
