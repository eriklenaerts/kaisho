using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;

namespace KA
{
    public partial class SplashScreen : Form
    {
        private int tick = 0;
        public SplashScreen()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            //  Do nothing here!
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            Graphics gfx = e.Graphics;

            // EL: gewijzigd omdat deze regel niet werkte na de setup van KA.
        //    Bitmap splahImage = new Bitmap(Application.StartupPath + @"\gfx\Splash_Screen.png");
            Bitmap splahImage = new Bitmap(this.BackgroundImage);

            gfx.DrawImage(splahImage, new Rectangle(0, 0, this.Width, this.Height));
            gfx.DrawString("v. " + ConfigurationManager.AppSettings["version"].ToString(), new Font(new FontFamily("Verdana"), 12, FontStyle.Bold), Brushes.Black, new PointF(this.Width - 110, this.Height - 60));
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            tick += 1;
            if (tick == 1)
            {
                timer1.Stop();
                this.Hide();
            }
        }

        private void SplashScreen_Load(object sender, EventArgs e)
        {
            timer1.Interval = 1000;
            timer1.Start();
            
        }
    }
}