namespace KA.Forms
{
    partial class FamilieBeheer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FamilieBeheer));
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBoxInfo = new System.Windows.Forms.GroupBox();
            this.cmbFilter = new System.Windows.Forms.ComboBox();
            this.lblFamily = new System.Windows.Forms.Label();
            this.lblNumberOfFamilies = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lstFamilies = new System.Windows.Forms.ListView();
            this.FamilieNaam = new System.Windows.Forms.ColumnHeader();
            this.FamilieLeden = new System.Windows.Forms.ColumnHeader();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.lblExFamilieBeheer = new KA.CustomControls.Extensions.LabelEx();
            this.panel1.SuspendLayout();
            this.groupBoxInfo.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(245)))), ((int)(((byte)(248)))));
            this.panel1.Controls.Add(this.groupBoxInfo);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(10, 40);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(772, 61);
            this.panel1.TabIndex = 1;
            // 
            // groupBoxInfo
            // 
            this.groupBoxInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxInfo.Controls.Add(this.cmbFilter);
            this.groupBoxInfo.Controls.Add(this.lblFamily);
            this.groupBoxInfo.Controls.Add(this.lblNumberOfFamilies);
            this.groupBoxInfo.Location = new System.Drawing.Point(4, 3);
            this.groupBoxInfo.Name = "groupBoxInfo";
            this.groupBoxInfo.Size = new System.Drawing.Size(765, 48);
            this.groupBoxInfo.TabIndex = 0;
            this.groupBoxInfo.TabStop = false;
            this.groupBoxInfo.Text = "Filter";
            // 
            // cmbFilter
            // 
            this.cmbFilter.FormattingEnabled = true;
            this.cmbFilter.Items.AddRange(new object[] {
            "Alle families",
            "Minstens 1 actief lid",
            "Minstens 1 inactief lid"});
            this.cmbFilter.Location = new System.Drawing.Point(52, 17);
            this.cmbFilter.Name = "cmbFilter";
            this.cmbFilter.Size = new System.Drawing.Size(198, 21);
            this.cmbFilter.TabIndex = 2;
            this.cmbFilter.Text = "Kies uw filter";
            this.cmbFilter.SelectedIndexChanged += new System.EventHandler(this.cmbFilter_SelectedIndexChanged);
            // 
            // lblFamily
            // 
            this.lblFamily.AutoSize = true;
            this.lblFamily.Location = new System.Drawing.Point(7, 20);
            this.lblFamily.Name = "lblFamily";
            this.lblFamily.Size = new System.Drawing.Size(39, 13);
            this.lblFamily.TabIndex = 1;
            this.lblFamily.Text = "Familie";
            // 
            // lblNumberOfFamilies
            // 
            this.lblNumberOfFamilies.AutoSize = true;
            this.lblNumberOfFamilies.Location = new System.Drawing.Point(315, 21);
            this.lblNumberOfFamilies.Name = "lblNumberOfFamilies";
            this.lblNumberOfFamilies.Size = new System.Drawing.Size(80, 13);
            this.lblNumberOfFamilies.TabIndex = 0;
            this.lblNumberOfFamilies.Text = "Aantal families :";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lstFamilies);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(10, 101);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(772, 455);
            this.panel2.TabIndex = 2;
            // 
            // lstFamilies
            // 
            this.lstFamilies.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.lstFamilies.AllowDrop = true;
            this.lstFamilies.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(245)))), ((int)(((byte)(248)))));
            this.lstFamilies.CausesValidation = false;
            this.lstFamilies.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.FamilieNaam,
            this.FamilieLeden});
            this.lstFamilies.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lstFamilies.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstFamilies.FullRowSelect = true;
            this.lstFamilies.HideSelection = false;
            this.lstFamilies.Location = new System.Drawing.Point(0, 0);
            this.lstFamilies.MultiSelect = false;
            this.lstFamilies.Name = "lstFamilies";
            this.lstFamilies.Size = new System.Drawing.Size(772, 455);
            this.lstFamilies.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lstFamilies.StateImageList = this.imageList2;
            this.lstFamilies.TabIndex = 15;
            this.lstFamilies.UseCompatibleStateImageBehavior = false;
            this.lstFamilies.View = System.Windows.Forms.View.Details;
            this.lstFamilies.DoubleClick += new System.EventHandler(this.lstFamilies_DoubleClick);
            this.lstFamilies.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstFamilies_ColumnClick);
            // 
            // FamilieNaam
            // 
            this.FamilieNaam.Text = "        Familie naam";
            this.FamilieNaam.Width = 170;
            // 
            // FamilieLeden
            // 
            this.FamilieLeden.Text = "Familieleden";
            this.FamilieLeden.Width = 550;
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "users4.png");
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = global::KA.Properties.Resources.users4;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Width = 385;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "add.png");
            this.imageList1.Images.SetKeyName(1, "delete.png");
            this.imageList1.Images.SetKeyName(2, "refresh.png");
            this.imageList1.Images.SetKeyName(3, "document_into.png");
            // 
            // lblExFamilieBeheer
            // 
            this.lblExFamilieBeheer.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblExFamilieBeheer.Font = new System.Drawing.Font("Tahoma", 14.25F);
            this.lblExFamilieBeheer.HighColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252)))));
            this.lblExFamilieBeheer.Image = global::KA.Properties.Resources.users_family;
            this.lblExFamilieBeheer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblExFamilieBeheer.Location = new System.Drawing.Point(10, 10);
            this.lblExFamilieBeheer.LowColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178)))));
            this.lblExFamilieBeheer.Name = "lblExFamilieBeheer";
            this.lblExFamilieBeheer.Size = new System.Drawing.Size(772, 30);
            this.lblExFamilieBeheer.TabIndex = 0;
            this.lblExFamilieBeheer.Text = "Familie Beheer";
            this.lblExFamilieBeheer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FamilieBeheer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(106)))), ((int)(((byte)(197)))));
            this.ClientSize = new System.Drawing.Size(792, 566);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblExFamilieBeheer);
            this.Name = "FamilieBeheer";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Text = "Familie Beheer";
            this.Activated += new System.EventHandler(this.FamilieBeheer_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FamilieBeheer_FormClosed);
            this.panel1.ResumeLayout(false);
            this.groupBoxInfo.ResumeLayout(false);
            this.groupBoxInfo.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private KA.CustomControls.Extensions.LabelEx lblExFamilieBeheer;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.GroupBox groupBoxInfo;
        private System.Windows.Forms.Label lblNumberOfFamilies;
        private System.Windows.Forms.ComboBox cmbFilter;
        private System.Windows.Forms.Label lblFamily;
        private System.Windows.Forms.ListView lstFamilies;
        private System.Windows.Forms.ColumnHeader FamilieNaam;
        private System.Windows.Forms.ColumnHeader FamilieLeden;
        private System.Windows.Forms.ImageList imageList2;
    }
}