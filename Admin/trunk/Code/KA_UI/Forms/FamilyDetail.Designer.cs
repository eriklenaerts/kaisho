namespace KA.Forms
{
    partial class FamilyDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitle = new KA.CustomControls.Extensions.LabelEx();
            this.btnOK = new MTControls.MTButtons.MTMultiGradiantButton();
            this.btnCancel = new MTControls.MTButtons.MTMultiGradiantButton();
            this.familyGroupbox = new System.Windows.Forms.GroupBox();
            this.btnLedenBeheer = new MTControls.MTButtons.MTMultiGradiantButton();
            this.lblFamilieleden = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFamilyName = new System.Windows.Forms.TextBox();
            this.lblFamilieNaam = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdBtnWhithoutFamily = new System.Windows.Forms.RadioButton();
            this.rdBtnSameFamilyName = new System.Windows.Forms.RadioButton();
            this.rdbtnAllMembers = new System.Windows.Forms.RadioButton();
            this.lblTeKiezenLeden = new KA.CustomControls.Extensions.LabelEx();
            this.lstMembers = new System.Windows.Forms.ListView();
            this.Achternaam = new System.Windows.Forms.ColumnHeader();
            this.Voornaam = new System.Windows.Forms.ColumnHeader();
            this.labelEx2 = new KA.CustomControls.Extensions.LabelEx();
            this.btnAddFamilyMember = new MTControls.MTButtons.MTMultiGradiantButton();
            this.lstFamilyMembers = new System.Windows.Forms.ListView();
            this.FamilyMemberLastName = new System.Windows.Forms.ColumnHeader();
            this.FamilyMemberFirstName = new System.Windows.Forms.ColumnHeader();
            this.btnRemoveFamilyMember = new MTControls.MTButtons.MTMultiGradiantButton();
            this.familyGroupbox.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.HighColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252)))));
            this.lblTitle.Image = global::KA.Properties.Resources.users_family;
            this.lblTitle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.LowColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178)))));
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(809, 30);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "Familie :";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Angle = 180F;
            this.btnOK.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252))))));
            this.btnOK.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178))))));
            this.btnOK.FlatAppearance.BorderSize = 0;
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOK.Location = new System.Drawing.Point(552, 426);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(114, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "OK";
            this.btnOK.Transparency = 100;
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Angle = 180F;
            this.btnCancel.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252))))));
            this.btnCancel.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178))))));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancel.Location = new System.Drawing.Point(691, 426);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(114, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Transparency = 100;
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // familyGroupbox
            // 
            this.familyGroupbox.Controls.Add(this.btnLedenBeheer);
            this.familyGroupbox.Controls.Add(this.lblFamilieleden);
            this.familyGroupbox.Controls.Add(this.label1);
            this.familyGroupbox.Controls.Add(this.txtFamilyName);
            this.familyGroupbox.Controls.Add(this.lblFamilieNaam);
            this.familyGroupbox.Controls.Add(this.panel1);
            this.familyGroupbox.Location = new System.Drawing.Point(12, 37);
            this.familyGroupbox.Name = "familyGroupbox";
            this.familyGroupbox.Size = new System.Drawing.Size(794, 387);
            this.familyGroupbox.TabIndex = 0;
            this.familyGroupbox.TabStop = false;
            this.familyGroupbox.Text = "Familie";
            // 
            // btnLedenBeheer
            // 
            this.btnLedenBeheer.Angle = 180F;
            this.btnLedenBeheer.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252))))));
            this.btnLedenBeheer.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178))))));
            this.btnLedenBeheer.FlatAppearance.BorderSize = 0;
            this.btnLedenBeheer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnLedenBeheer.Location = new System.Drawing.Point(342, 54);
            this.btnLedenBeheer.Name = "btnLedenBeheer";
            this.btnLedenBeheer.Size = new System.Drawing.Size(114, 23);
            this.btnLedenBeheer.TabIndex = 1;
            this.btnLedenBeheer.Text = "Leden beheer";
            this.btnLedenBeheer.Transparency = 100;
            this.btnLedenBeheer.UseVisualStyleBackColor = true;
            this.btnLedenBeheer.Click += new System.EventHandler(this.btnLedenBeheer_Click);
            // 
            // lblFamilieleden
            // 
            this.lblFamilieleden.AutoSize = true;
            this.lblFamilieleden.Location = new System.Drawing.Point(102, 56);
            this.lblFamilieleden.Name = "lblFamilieleden";
            this.lblFamilieleden.Size = new System.Drawing.Size(0, 13);
            this.lblFamilieleden.TabIndex = 24;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "Familieleden :";
            // 
            // txtFamilyName
            // 
            this.txtFamilyName.Location = new System.Drawing.Point(103, 28);
            this.txtFamilyName.Name = "txtFamilyName";
            this.txtFamilyName.Size = new System.Drawing.Size(233, 20);
            this.txtFamilyName.TabIndex = 0;
            this.txtFamilyName.TextChanged += new System.EventHandler(this.txtFamilyName_TextChanged);
            // 
            // lblFamilieNaam
            // 
            this.lblFamilieNaam.AutoSize = true;
            this.lblFamilieNaam.Location = new System.Drawing.Point(31, 31);
            this.lblFamilieNaam.Name = "lblFamilieNaam";
            this.lblFamilieNaam.Size = new System.Drawing.Size(71, 13);
            this.lblFamilieNaam.TabIndex = 0;
            this.lblFamilieNaam.Text = "Familienaam :";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(245)))), ((int)(((byte)(248)))));
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.lblTeKiezenLeden);
            this.panel1.Controls.Add(this.lstMembers);
            this.panel1.Controls.Add(this.labelEx2);
            this.panel1.Controls.Add(this.btnAddFamilyMember);
            this.panel1.Controls.Add(this.lstFamilyMembers);
            this.panel1.Controls.Add(this.btnRemoveFamilyMember);
            this.panel1.Location = new System.Drawing.Point(6, 83);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(782, 293);
            this.panel1.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdBtnWhithoutFamily);
            this.groupBox2.Controls.Add(this.rdBtnSameFamilyName);
            this.groupBox2.Controls.Add(this.rdbtnAllMembers);
            this.groupBox2.Location = new System.Drawing.Point(28, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(302, 47);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Filter";
            // 
            // rdBtnWhithoutFamily
            // 
            this.rdBtnWhithoutFamily.AutoSize = true;
            this.rdBtnWhithoutFamily.Location = new System.Drawing.Point(204, 19);
            this.rdBtnWhithoutFamily.Name = "rdBtnWhithoutFamily";
            this.rdBtnWhithoutFamily.Size = new System.Drawing.Size(91, 17);
            this.rdBtnWhithoutFamily.TabIndex = 2;
            this.rdBtnWhithoutFamily.TabStop = true;
            this.rdBtnWhithoutFamily.Text = "Zonder familie";
            this.rdBtnWhithoutFamily.UseVisualStyleBackColor = true;
            this.rdBtnWhithoutFamily.CheckedChanged += new System.EventHandler(this.rdBtnWhithoutFamily_CheckedChanged);
            // 
            // rdBtnSameFamilyName
            // 
            this.rdBtnSameFamilyName.AutoSize = true;
            this.rdBtnSameFamilyName.Location = new System.Drawing.Point(84, 19);
            this.rdBtnSameFamilyName.Name = "rdBtnSameFamilyName";
            this.rdBtnSameFamilyName.Size = new System.Drawing.Size(113, 17);
            this.rdBtnSameFamilyName.TabIndex = 1;
            this.rdBtnSameFamilyName.Text = "Zelfde familienaam";
            this.rdBtnSameFamilyName.UseVisualStyleBackColor = true;
            this.rdBtnSameFamilyName.CheckedChanged += new System.EventHandler(this.rdBtnSameFamilyName_CheckedChanged);
            // 
            // rdbtnAllMembers
            // 
            this.rdbtnAllMembers.AutoSize = true;
            this.rdbtnAllMembers.Checked = true;
            this.rdbtnAllMembers.Location = new System.Drawing.Point(6, 19);
            this.rdbtnAllMembers.Name = "rdbtnAllMembers";
            this.rdbtnAllMembers.Size = new System.Drawing.Size(71, 17);
            this.rdbtnAllMembers.TabIndex = 0;
            this.rdbtnAllMembers.TabStop = true;
            this.rdbtnAllMembers.Text = "Alle leden";
            this.rdbtnAllMembers.UseVisualStyleBackColor = true;
            this.rdbtnAllMembers.CheckedChanged += new System.EventHandler(this.rdbtnAllMembers_CheckedChanged);
            // 
            // lblTeKiezenLeden
            // 
            this.lblTeKiezenLeden.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTeKiezenLeden.ForeColor = System.Drawing.Color.Black;
            this.lblTeKiezenLeden.HighColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252)))));
            this.lblTeKiezenLeden.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTeKiezenLeden.Location = new System.Drawing.Point(28, 53);
            this.lblTeKiezenLeden.LowColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178)))));
            this.lblTeKiezenLeden.Name = "lblTeKiezenLeden";
            this.lblTeKiezenLeden.Size = new System.Drawing.Size(302, 22);
            this.lblTeKiezenLeden.TabIndex = 3;
            this.lblTeKiezenLeden.Text = "Te kiezen leden";
            this.lblTeKiezenLeden.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lstMembers
            // 
            this.lstMembers.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.lstMembers.AllowDrop = true;
            this.lstMembers.CausesValidation = false;
            this.lstMembers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Achternaam,
            this.Voornaam});
            this.lstMembers.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lstMembers.FullRowSelect = true;
            this.lstMembers.HideSelection = false;
            this.lstMembers.Location = new System.Drawing.Point(28, 75);
            this.lstMembers.MultiSelect = false;
            this.lstMembers.Name = "lstMembers";
            this.lstMembers.Size = new System.Drawing.Size(302, 208);
            this.lstMembers.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lstMembers.TabIndex = 0;
            this.lstMembers.UseCompatibleStateImageBehavior = false;
            this.lstMembers.View = System.Windows.Forms.View.Details;
            this.lstMembers.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.List_ColumnClick);
            // 
            // Achternaam
            // 
            this.Achternaam.Text = "Achternaam";
            this.Achternaam.Width = 140;
            // 
            // Voornaam
            // 
            this.Voornaam.Text = "Voornaam";
            this.Voornaam.Width = 140;
            // 
            // labelEx2
            // 
            this.labelEx2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEx2.ForeColor = System.Drawing.Color.Black;
            this.labelEx2.HighColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252)))));
            this.labelEx2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelEx2.Location = new System.Drawing.Point(458, 53);
            this.labelEx2.LowColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178)))));
            this.labelEx2.Name = "labelEx2";
            this.labelEx2.Size = new System.Drawing.Size(299, 22);
            this.labelEx2.TabIndex = 18;
            this.labelEx2.Text = "Familieleden";
            this.labelEx2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAddFamilyMember
            // 
            this.btnAddFamilyMember.Angle = 180F;
            this.btnAddFamilyMember.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252))))));
            this.btnAddFamilyMember.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178))))));
            this.btnAddFamilyMember.FlatAppearance.BorderSize = 0;
            this.btnAddFamilyMember.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAddFamilyMember.Location = new System.Drawing.Point(336, 148);
            this.btnAddFamilyMember.Name = "btnAddFamilyMember";
            this.btnAddFamilyMember.Size = new System.Drawing.Size(114, 23);
            this.btnAddFamilyMember.TabIndex = 1;
            this.btnAddFamilyMember.Text = "Voeg lid toe";
            this.btnAddFamilyMember.Transparency = 100;
            this.btnAddFamilyMember.UseVisualStyleBackColor = true;
            this.btnAddFamilyMember.Click += new System.EventHandler(this.btnAddFamilyMember_Click);
            // 
            // lstFamilyMembers
            // 
            this.lstFamilyMembers.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.lstFamilyMembers.AllowDrop = true;
            this.lstFamilyMembers.CausesValidation = false;
            this.lstFamilyMembers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.FamilyMemberLastName,
            this.FamilyMemberFirstName});
            this.lstFamilyMembers.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lstFamilyMembers.FullRowSelect = true;
            this.lstFamilyMembers.HideSelection = false;
            this.lstFamilyMembers.Location = new System.Drawing.Point(457, 75);
            this.lstFamilyMembers.MultiSelect = false;
            this.lstFamilyMembers.Name = "lstFamilyMembers";
            this.lstFamilyMembers.Size = new System.Drawing.Size(300, 208);
            this.lstFamilyMembers.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lstFamilyMembers.TabIndex = 3;
            this.lstFamilyMembers.UseCompatibleStateImageBehavior = false;
            this.lstFamilyMembers.View = System.Windows.Forms.View.Details;
            this.lstFamilyMembers.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.List_ColumnClick);
            // 
            // FamilyMemberLastName
            // 
            this.FamilyMemberLastName.Text = "Achternaam";
            this.FamilyMemberLastName.Width = 140;
            // 
            // FamilyMemberFirstName
            // 
            this.FamilyMemberFirstName.Text = "Naam";
            this.FamilyMemberFirstName.Width = 140;
            // 
            // btnRemoveFamilyMember
            // 
            this.btnRemoveFamilyMember.Angle = 180F;
            this.btnRemoveFamilyMember.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252))))));
            this.btnRemoveFamilyMember.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178))))));
            this.btnRemoveFamilyMember.FlatAppearance.BorderSize = 0;
            this.btnRemoveFamilyMember.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRemoveFamilyMember.Location = new System.Drawing.Point(336, 177);
            this.btnRemoveFamilyMember.Name = "btnRemoveFamilyMember";
            this.btnRemoveFamilyMember.Size = new System.Drawing.Size(114, 23);
            this.btnRemoveFamilyMember.TabIndex = 2;
            this.btnRemoveFamilyMember.Text = "Verwijder lid";
            this.btnRemoveFamilyMember.Transparency = 100;
            this.btnRemoveFamilyMember.UseVisualStyleBackColor = true;
            this.btnRemoveFamilyMember.Click += new System.EventHandler(this.btnRemoveFamilyMember_Click);
            // 
            // FamilyDetail
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(245)))), ((int)(((byte)(248)))));
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(809, 455);

            this.Controls.Add(this.familyGroupbox);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblTitle);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FamilyDetail";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Familie : detail";
            this.Load += new System.EventHandler(this.FamilyDetail_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FamilyDetail_FormClosing);
            this.familyGroupbox.ResumeLayout(false);
            this.familyGroupbox.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private KA.CustomControls.Extensions.LabelEx lblTitle;
        private MTControls.MTButtons.MTMultiGradiantButton btnOK;
        private MTControls.MTButtons.MTMultiGradiantButton btnCancel;
        private System.Windows.Forms.GroupBox familyGroupbox;
        private KA.CustomControls.Extensions.LabelEx lblTeKiezenLeden;
        private System.Windows.Forms.Label lblFamilieNaam;
        private System.Windows.Forms.ListView lstMembers;
        private System.Windows.Forms.ColumnHeader Voornaam;
        private MTControls.MTButtons.MTMultiGradiantButton btnRemoveFamilyMember;
        private MTControls.MTButtons.MTMultiGradiantButton btnAddFamilyMember;
        private System.Windows.Forms.ColumnHeader Achternaam;
        private System.Windows.Forms.ListView lstFamilyMembers;
        private System.Windows.Forms.ColumnHeader FamilyMemberFirstName;
        private KA.CustomControls.Extensions.LabelEx labelEx2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rdBtnWhithoutFamily;
        private System.Windows.Forms.RadioButton rdBtnSameFamilyName;
        private System.Windows.Forms.RadioButton rdbtnAllMembers;
        private System.Windows.Forms.TextBox txtFamilyName;
        private System.Windows.Forms.Panel panel1;
        private MTControls.MTButtons.MTMultiGradiantButton btnLedenBeheer;
        private System.Windows.Forms.Label lblFamilieleden;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ColumnHeader FamilyMemberLastName;

    }
}
