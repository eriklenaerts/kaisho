﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using KA_Common;
using KA_Controller.EventArgs;
using KA_Controller.Interfaces;
using BL = KA.BusinessLogicLayer;

namespace KA.Forms
{
    public partial class AddHistory : Form, IAddHistoryViewBase
    {
        private BL.Examination _examination;
        private BL.Member _member;
        public AddHistory()
        {
            InitializeComponent();
            dtpDate.Value = DateTime.Now;
        }

        public event EventHandler ViewClosed;
        public event EventHandler<KA_Controller.EventArgs.ActionEventArgs> ActionInvoked;

        #region IAddHistoryViewBase Members

        public void SetContext(KA.BusinessLogicLayer.Member member)
        {
            _member = member;
            int memberId = member.ID;
            int LocationId = 0;
            
            DateTime date = DateTime.Now;

            _examination = new KA.BusinessLogicLayer.Examination();
            //examination.DegreeID = degreeId;
            _examination.LocationID = LocationId;
            _examination.MemberID = memberId;
            _examination.Date = new DateTime?(date);
            
        }

        #endregion

        #region IViewBase Members


        public void Init(Form mdiParent)
        {
            
        }

        public void ShowBrokenRules(KA.BusinessLogicLayer.Validation.BrokenRulesList brokenRules)
        {
            
        }

        #endregion

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int degreeId = rdb8Kyu.Checked ? 2 : rdb7Kyu.Checked ? 3 : rdb6Kyu.Checked ? 4 : rdb5Kyu.Checked ? 5 : rdb4Kyu.Checked ? 6 : rdb3Kyu.Checked ? 7 : rdb2Kyu.Checked ? 8 : rdb1Kyu.Checked ? 9 : rdbShodan.Checked ? 10 : rdbNiDan.Checked ? 11 : rdbSanDan.Checked ? 12 : rdbYonDan.Checked ? 13 : rdbGoDan.Checked ? 14 : 15;

            _examination.DegreeID = degreeId;
            _examination.Date = dtpDate.Value;

            if (ActionInvoked != null)
            {
                ActionInvoked(this, new ActionEventArgs(Enums.Actions.Apply, Enums.Controller.ExamListController, new AddHistoryEventArgs{Exam = _examination, Member = _member}));
            }
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AddHistory_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ViewClosed != null)
                ViewClosed(this, new EventArgs());
        }
    }
}
