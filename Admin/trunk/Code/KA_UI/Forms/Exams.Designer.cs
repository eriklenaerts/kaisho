namespace KA.Forms
{
    partial class Exams
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Exams));
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdbAllMembers = new System.Windows.Forms.RadioButton();
            this.ddlLocations = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rdbGoDan = new System.Windows.Forms.RadioButton();
            this.rdbYonDan = new System.Windows.Forms.RadioButton();
            this.rdbSanDan = new System.Windows.Forms.RadioButton();
            this.rdbNiDan = new System.Windows.Forms.RadioButton();
            this.rdbShodan = new System.Windows.Forms.RadioButton();
            this.rdb1Kyu = new System.Windows.Forms.RadioButton();
            this.rdb2Kyu = new System.Windows.Forms.RadioButton();
            this.rdb3Kyu = new System.Windows.Forms.RadioButton();
            this.rdb4Kyu = new System.Windows.Forms.RadioButton();
            this.rdb5Kyu = new System.Windows.Forms.RadioButton();
            this.rdb6Kyu = new System.Windows.Forms.RadioButton();
            this.rdb7Kyu = new System.Windows.Forms.RadioButton();
            this.rdb8Kyu = new System.Windows.Forms.RadioButton();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.listView1 = new System.Windows.Forms.ListView();
            this.Naam = new System.Windows.Forms.ColumnHeader();
            this.Graad = new System.Windows.Forms.ColumnHeader();
            this.Straat = new System.Windows.Forms.ColumnHeader();
            this.Postcode = new System.Windows.Forms.ColumnHeader();
            this.Gemeente = new System.Windows.Forms.ColumnHeader();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnApply = new xpButton.xpGlowButton();
            this.LabelEx = new KA.CustomControls.Extensions.LabelEx();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(245)))), ((int)(((byte)(248)))));
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(10, 40);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(772, 209);
            this.panel1.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.AutoSize = true;
            this.groupBox1.Controls.Add(this.rdbAllMembers);
            this.groupBox1.Controls.Add(this.btnApply);
            this.groupBox1.Controls.Add(this.ddlLocations);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.rdbGoDan);
            this.groupBox1.Controls.Add(this.rdbYonDan);
            this.groupBox1.Controls.Add(this.rdbSanDan);
            this.groupBox1.Controls.Add(this.rdbNiDan);
            this.groupBox1.Controls.Add(this.rdbShodan);
            this.groupBox1.Controls.Add(this.rdb1Kyu);
            this.groupBox1.Controls.Add(this.rdb2Kyu);
            this.groupBox1.Controls.Add(this.rdb3Kyu);
            this.groupBox1.Controls.Add(this.rdb4Kyu);
            this.groupBox1.Controls.Add(this.rdb5Kyu);
            this.groupBox1.Controls.Add(this.rdb6Kyu);
            this.groupBox1.Controls.Add(this.rdb7Kyu);
            this.groupBox1.Controls.Add(this.rdb8Kyu);
            this.groupBox1.Controls.Add(this.dtpDate);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 18);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.groupBox1.Size = new System.Drawing.Size(752, 178);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Examen info";
            // 
            // rdbAllMembers
            // 
            this.rdbAllMembers.AutoSize = true;
            this.rdbAllMembers.Location = new System.Drawing.Point(463, 80);
            this.rdbAllMembers.Name = "rdbAllMembers";
            this.rdbAllMembers.Size = new System.Drawing.Size(78, 17);
            this.rdbAllMembers.TabIndex = 22;
            this.rdbAllMembers.Text = "Alle graden";
            this.rdbAllMembers.UseVisualStyleBackColor = true;
            this.rdbAllMembers.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // ddlLocations
            // 
            this.ddlLocations.FormattingEnabled = true;
            this.ddlLocations.Items.AddRange(new object[] {
            "Wommelgem",
            "Braschaat"});
            this.ddlLocations.Location = new System.Drawing.Point(340, 22);
            this.ddlLocations.Name = "ddlLocations";
            this.ddlLocations.Size = new System.Drawing.Size(216, 21);
            this.ddlLocations.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(292, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Plaats :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Graad :";
            // 
            // rdbGoDan
            // 
            this.rdbGoDan.AutoSize = true;
            this.rdbGoDan.Image = global::KA.Properties.Resources._5dan;
            this.rdbGoDan.Location = new System.Drawing.Point(463, 53);
            this.rdbGoDan.Name = "rdbGoDan";
            this.rdbGoDan.Size = new System.Drawing.Size(93, 24);
            this.rdbGoDan.TabIndex = 15;
            this.rdbGoDan.UseVisualStyleBackColor = true;
            this.rdbGoDan.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // rdbYonDan
            // 
            this.rdbYonDan.AutoSize = true;
            this.rdbYonDan.Image = global::KA.Properties.Resources._4dan;
            this.rdbYonDan.Location = new System.Drawing.Point(364, 99);
            this.rdbYonDan.Name = "rdbYonDan";
            this.rdbYonDan.Size = new System.Drawing.Size(93, 24);
            this.rdbYonDan.TabIndex = 14;
            this.rdbYonDan.UseVisualStyleBackColor = true;
            this.rdbYonDan.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // rdbSanDan
            // 
            this.rdbSanDan.AutoSize = true;
            this.rdbSanDan.Image = global::KA.Properties.Resources._3dan;
            this.rdbSanDan.Location = new System.Drawing.Point(364, 76);
            this.rdbSanDan.Name = "rdbSanDan";
            this.rdbSanDan.Size = new System.Drawing.Size(93, 24);
            this.rdbSanDan.TabIndex = 13;
            this.rdbSanDan.UseVisualStyleBackColor = true;
            this.rdbSanDan.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // rdbNiDan
            // 
            this.rdbNiDan.AutoSize = true;
            this.rdbNiDan.Image = global::KA.Properties.Resources._2dan;
            this.rdbNiDan.Location = new System.Drawing.Point(364, 53);
            this.rdbNiDan.Name = "rdbNiDan";
            this.rdbNiDan.Size = new System.Drawing.Size(93, 24);
            this.rdbNiDan.TabIndex = 12;
            this.rdbNiDan.UseVisualStyleBackColor = true;
            this.rdbNiDan.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // rdbShodan
            // 
            this.rdbShodan.AutoSize = true;
            this.rdbShodan.Image = global::KA.Properties.Resources._1dan;
            this.rdbShodan.Location = new System.Drawing.Point(265, 99);
            this.rdbShodan.Name = "rdbShodan";
            this.rdbShodan.Size = new System.Drawing.Size(93, 24);
            this.rdbShodan.TabIndex = 11;
            this.rdbShodan.UseVisualStyleBackColor = true;
            this.rdbShodan.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // rdb1Kyu
            // 
            this.rdb1Kyu.AutoSize = true;
            this.rdb1Kyu.Image = global::KA.Properties.Resources._1kyu;
            this.rdb1Kyu.Location = new System.Drawing.Point(265, 76);
            this.rdb1Kyu.Name = "rdb1Kyu";
            this.rdb1Kyu.Size = new System.Drawing.Size(93, 24);
            this.rdb1Kyu.TabIndex = 10;
            this.rdb1Kyu.UseVisualStyleBackColor = true;
            this.rdb1Kyu.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // rdb2Kyu
            // 
            this.rdb2Kyu.AutoSize = true;
            this.rdb2Kyu.Image = global::KA.Properties.Resources._2kyu;
            this.rdb2Kyu.Location = new System.Drawing.Point(265, 53);
            this.rdb2Kyu.Name = "rdb2Kyu";
            this.rdb2Kyu.Size = new System.Drawing.Size(93, 24);
            this.rdb2Kyu.TabIndex = 9;
            this.rdb2Kyu.UseVisualStyleBackColor = true;
            this.rdb2Kyu.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // rdb3Kyu
            // 
            this.rdb3Kyu.AutoSize = true;
            this.rdb3Kyu.Image = global::KA.Properties.Resources._3kyu;
            this.rdb3Kyu.Location = new System.Drawing.Point(166, 99);
            this.rdb3Kyu.Name = "rdb3Kyu";
            this.rdb3Kyu.Size = new System.Drawing.Size(93, 24);
            this.rdb3Kyu.TabIndex = 8;
            this.rdb3Kyu.UseVisualStyleBackColor = true;
            this.rdb3Kyu.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // rdb4Kyu
            // 
            this.rdb4Kyu.AutoSize = true;
            this.rdb4Kyu.Image = global::KA.Properties.Resources._4kyu;
            this.rdb4Kyu.Location = new System.Drawing.Point(166, 76);
            this.rdb4Kyu.Name = "rdb4Kyu";
            this.rdb4Kyu.Size = new System.Drawing.Size(93, 24);
            this.rdb4Kyu.TabIndex = 7;
            this.rdb4Kyu.UseVisualStyleBackColor = true;
            this.rdb4Kyu.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // rdb5Kyu
            // 
            this.rdb5Kyu.AutoSize = true;
            this.rdb5Kyu.Image = global::KA.Properties.Resources._5kyu;
            this.rdb5Kyu.Location = new System.Drawing.Point(166, 53);
            this.rdb5Kyu.Name = "rdb5Kyu";
            this.rdb5Kyu.Size = new System.Drawing.Size(93, 24);
            this.rdb5Kyu.TabIndex = 6;
            this.rdb5Kyu.UseVisualStyleBackColor = true;
            this.rdb5Kyu.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // rdb6Kyu
            // 
            this.rdb6Kyu.AutoSize = true;
            this.rdb6Kyu.Image = global::KA.Properties.Resources._6kyu;
            this.rdb6Kyu.Location = new System.Drawing.Point(67, 99);
            this.rdb6Kyu.Name = "rdb6Kyu";
            this.rdb6Kyu.Size = new System.Drawing.Size(93, 24);
            this.rdb6Kyu.TabIndex = 5;
            this.rdb6Kyu.UseVisualStyleBackColor = true;
            this.rdb6Kyu.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // rdb7Kyu
            // 
            this.rdb7Kyu.AutoSize = true;
            this.rdb7Kyu.Image = global::KA.Properties.Resources._7kyu;
            this.rdb7Kyu.Location = new System.Drawing.Point(67, 76);
            this.rdb7Kyu.Name = "rdb7Kyu";
            this.rdb7Kyu.Size = new System.Drawing.Size(93, 24);
            this.rdb7Kyu.TabIndex = 4;
            this.rdb7Kyu.UseVisualStyleBackColor = true;
            this.rdb7Kyu.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // rdb8Kyu
            // 
            this.rdb8Kyu.AutoSize = true;
            this.rdb8Kyu.Checked = true;
            this.rdb8Kyu.Image = global::KA.Properties.Resources._8kyu;
            this.rdb8Kyu.Location = new System.Drawing.Point(67, 53);
            this.rdb8Kyu.Name = "rdb8Kyu";
            this.rdb8Kyu.Size = new System.Drawing.Size(93, 24);
            this.rdb8Kyu.TabIndex = 3;
            this.rdb8Kyu.TabStop = true;
            this.rdb8Kyu.UseVisualStyleBackColor = true;
            this.rdb8Kyu.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // dtpDate
            // 
            this.dtpDate.Location = new System.Drawing.Point(67, 23);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(200, 20);
            this.dtpDate.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Datum :";
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewImageColumn1.FillWeight = 1F;
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewImageColumn1.Image")));
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Width = 24;
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "clipboard.png");
            this.imageList2.Images.SetKeyName(1, "document_into.png");
            this.imageList2.Images.SetKeyName(2, "document_exchange.png");
            this.imageList2.Images.SetKeyName(3, "id_card.png");
            // 
            // listView1
            // 
            this.listView1.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.listView1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(245)))), ((int)(((byte)(248)))));
            this.listView1.CausesValidation = false;
            this.listView1.CheckBoxes = true;
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Naam,
            this.Graad,
            this.Straat,
            this.Postcode,
            this.Gemeente});
            this.listView1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.FullRowSelect = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(10, 249);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.OwnerDraw = true;
            this.listView1.Size = new System.Drawing.Size(772, 307);
            this.listView1.SmallImageList = this.imageList1;
            this.listView1.TabIndex = 5;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.listView1_DrawColumnHeader);
            this.listView1.DrawSubItem += new System.Windows.Forms.DrawListViewSubItemEventHandler(this.listView1_DrawSubItem);
            // 
            // Naam
            // 
            this.Naam.Text = "Naam";
            this.Naam.Width = 170;
            // 
            // Graad
            // 
            this.Graad.Text = "Graad";
            this.Graad.Width = 100;
            // 
            // Straat
            // 
            this.Straat.Text = "Straat";
            this.Straat.Width = 150;
            // 
            // Postcode
            // 
            this.Postcode.Text = "Postcode";
            this.Postcode.Width = 200;
            // 
            // Gemeente
            // 
            this.Gemeente.Text = "Gemeente";
            this.Gemeente.Width = 148;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "id_card.png");
            // 
            // btnApply
            // 
            this.btnApply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252)))));
            this.btnApply.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252)))));
            this.btnApply.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178)))));
            this.btnApply.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnApply.HoverForeColor = System.Drawing.Color.White;
            this.btnApply.Location = new System.Drawing.Point(20, 140);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(115, 22);
            this.btnApply.TabIndex = 21;
            this.btnApply.Text = "Toepassen";
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // LabelEx
            // 
            this.LabelEx.Dock = System.Windows.Forms.DockStyle.Top;
            this.LabelEx.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelEx.ForeColor = System.Drawing.Color.Black;
            this.LabelEx.HighColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252)))));
            this.LabelEx.Image = global::KA.Properties.Resources.graduation_hat1;
            this.LabelEx.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.LabelEx.Location = new System.Drawing.Point(10, 10);
            this.LabelEx.LowColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178)))));
            this.LabelEx.Name = "LabelEx";
            this.LabelEx.Size = new System.Drawing.Size(772, 30);
            this.LabelEx.TabIndex = 1;
            this.LabelEx.Text = "Examens";
            this.LabelEx.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Exams
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(106)))), ((int)(((byte)(197)))));
            this.ClientSize = new System.Drawing.Size(792, 566);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.LabelEx);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Exams";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.ShowInTaskbar = false;
            this.Text = "Examens";
            this.Activated += new System.EventHandler(this.Exams_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Exams_FormClosed);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private KA.CustomControls.Extensions.LabelEx LabelEx;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rdbGoDan;
        private System.Windows.Forms.RadioButton rdbYonDan;
        private System.Windows.Forms.RadioButton rdbSanDan;
        private System.Windows.Forms.RadioButton rdbNiDan;
        private System.Windows.Forms.RadioButton rdbShodan;
        private System.Windows.Forms.RadioButton rdb1Kyu;
        private System.Windows.Forms.RadioButton rdb2Kyu;
        private System.Windows.Forms.RadioButton rdb3Kyu;
        private System.Windows.Forms.RadioButton rdb4Kyu;
        private System.Windows.Forms.RadioButton rdb5Kyu;
        private System.Windows.Forms.RadioButton rdb6Kyu;
        private System.Windows.Forms.RadioButton rdb7Kyu;
        private System.Windows.Forms.RadioButton rdb8Kyu;
        private System.Windows.Forms.ComboBox ddlLocations;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ImageList imageList2;
        private xpButton.xpGlowButton btnApply;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader Naam;
        private System.Windows.Forms.ColumnHeader Graad;
        private System.Windows.Forms.ColumnHeader Straat;
        private System.Windows.Forms.ColumnHeader Postcode;
        private System.Windows.Forms.ColumnHeader Gemeente;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.RadioButton rdbAllMembers;
    }
}