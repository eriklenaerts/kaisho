namespace KA.Forms
{
    partial class CloseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.colorWithAlpha1 = new BlueActivity.Controls.ColorWithAlpha();
            this.colorWithAlpha2 = new BlueActivity.Controls.ColorWithAlpha();
            this.alphaGradientPanel1 = new BlueActivity.Controls.AlphaGradientPanel();
            this.colorWithAlpha3 = new BlueActivity.Controls.ColorWithAlpha();
            this.colorWithAlpha4 = new BlueActivity.Controls.ColorWithAlpha();
            this.xpGlowButton3 = new xpButton.xpGlowButton();
            this.xpGlowButton2 = new xpButton.xpGlowButton();
            this.label1 = new System.Windows.Forms.Label();
            this.alphaGradientPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // colorWithAlpha1
            // 
            this.colorWithAlpha1.Alpha = 255;
            this.colorWithAlpha1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252)))));
            this.colorWithAlpha1.Parent = null;
            // 
            // colorWithAlpha2
            // 
            this.colorWithAlpha2.Alpha = 255;
            this.colorWithAlpha2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178)))));
            this.colorWithAlpha2.Parent = null;
            // 
            // alphaGradientPanel1
            // 
            this.alphaGradientPanel1.BackColor = System.Drawing.Color.Transparent;
            this.alphaGradientPanel1.Border = true;
            this.alphaGradientPanel1.BorderColor = System.Drawing.SystemColors.ControlText;
            this.alphaGradientPanel1.Colors.Add(this.colorWithAlpha3);
            this.alphaGradientPanel1.Colors.Add(this.colorWithAlpha4);
            this.alphaGradientPanel1.ContentPadding = new System.Windows.Forms.Padding(0);
            this.alphaGradientPanel1.Controls.Add(this.xpGlowButton3);
            this.alphaGradientPanel1.Controls.Add(this.xpGlowButton2);
            this.alphaGradientPanel1.Controls.Add(this.label1);
            this.alphaGradientPanel1.CornerRadius = 20;
            this.alphaGradientPanel1.Corners = BlueActivity.Controls.Corner.None;
            this.alphaGradientPanel1.Gradient = true;
            this.alphaGradientPanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.alphaGradientPanel1.GradientOffset = 1F;
            this.alphaGradientPanel1.GradientSize = new System.Drawing.Size(0, 0);
            this.alphaGradientPanel1.GradientWrapMode = System.Drawing.Drawing2D.WrapMode.Tile;
            this.alphaGradientPanel1.Grayscale = false;
            this.alphaGradientPanel1.Image = global::KA.Properties.Resources.exit;
            this.alphaGradientPanel1.ImageAlpha = 100;
            this.alphaGradientPanel1.ImagePadding = new System.Windows.Forms.Padding(5);
            this.alphaGradientPanel1.ImagePosition = BlueActivity.Controls.ImagePosition.TopLeft;
            this.alphaGradientPanel1.ImageSize = new System.Drawing.Size(48, 48);
            this.alphaGradientPanel1.Location = new System.Drawing.Point(0, 0);
            this.alphaGradientPanel1.Name = "alphaGradientPanel1";
            this.alphaGradientPanel1.Rounded = true;
            this.alphaGradientPanel1.Size = new System.Drawing.Size(322, 137);
            this.alphaGradientPanel1.TabIndex = 0;
            // 
            // colorWithAlpha3
            // 
            this.colorWithAlpha3.Alpha = 255;
            this.colorWithAlpha3.Color = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252)))));
            this.colorWithAlpha3.Parent = this.alphaGradientPanel1;
            // 
            // colorWithAlpha4
            // 
            this.colorWithAlpha4.Alpha = 255;
            this.colorWithAlpha4.Color = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178)))));
            this.colorWithAlpha4.Parent = this.alphaGradientPanel1;
            // 
            // xpGlowButton3
            // 
            this.xpGlowButton3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252)))));
            this.xpGlowButton3.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252)))));
            this.xpGlowButton3.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178)))));
            this.xpGlowButton3.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.xpGlowButton3.HoverForeColor = System.Drawing.Color.White;
            this.xpGlowButton3.Location = new System.Drawing.Point(182, 85);
            this.xpGlowButton3.Name = "xpGlowButton3";
            this.xpGlowButton3.Size = new System.Drawing.Size(115, 22);
            this.xpGlowButton3.TabIndex = 20;
            this.xpGlowButton3.Text = "Cancel";
            this.xpGlowButton3.Click += new System.EventHandler(this.xpGlowButton3_Click);
            // 
            // xpGlowButton2
            // 
            this.xpGlowButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252)))));
            this.xpGlowButton2.BackColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252)))));
            this.xpGlowButton2.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178)))));
            this.xpGlowButton2.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.xpGlowButton2.HoverForeColor = System.Drawing.Color.White;
            this.xpGlowButton2.Location = new System.Drawing.Point(23, 85);
            this.xpGlowButton2.Name = "xpGlowButton2";
            this.xpGlowButton2.Size = new System.Drawing.Size(115, 22);
            this.xpGlowButton2.TabIndex = 19;
            this.xpGlowButton2.Text = "OK";
            this.xpGlowButton2.Click += new System.EventHandler(this.xpGlowButton2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(81, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bent u zeker dat u wilt afsluiten ?";
            // 
            // CloseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::KA.Properties.Resources.transparantBack;
            this.ClientSize = new System.Drawing.Size(322, 137);
            this.ControlBox = false;
            this.Controls.Add(this.alphaGradientPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CloseForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CloseForm";
            this.TopMost = true;
            this.alphaGradientPanel1.ResumeLayout(false);
            this.alphaGradientPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private BlueActivity.Controls.ColorWithAlpha colorWithAlpha1;
        private BlueActivity.Controls.ColorWithAlpha colorWithAlpha2;
        private BlueActivity.Controls.AlphaGradientPanel alphaGradientPanel1;
        private BlueActivity.Controls.ColorWithAlpha colorWithAlpha3;
        private BlueActivity.Controls.ColorWithAlpha colorWithAlpha4;
        private System.Windows.Forms.Label label1;
        private xpButton.xpGlowButton xpGlowButton3;
        private xpButton.xpGlowButton xpGlowButton2;
    }
}