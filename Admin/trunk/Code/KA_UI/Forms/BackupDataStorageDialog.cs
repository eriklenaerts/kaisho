﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace KA.Forms
{
    public partial class BackupDataStorageDialog : Form
    {
        private string _backupFolder = string.Empty;

        public BackupDataStorageDialog()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Shows the backup dialog
        /// </summary>
        /// <returns>An non empty string with the location of the backup</returns>
        public string ShowDialog(BackupProposal backupProposal)
        {
            lblDataStorageFilename.Text = backupProposal.DataStorageFilename;
            if (backupProposal.LastBackupDate == DateTime.MinValue)
                lblLastBackupDate.Text = "Nog geen backup genomen!";
            else
            {
                lblLastBackupDate.Text = backupProposal.LastBackupDate.ToString();
                lblLastBackupDate.Text += " (" + DateTime.Now.Subtract(backupProposal.LastBackupDate).Days + " dagen geleden)";
            }
            txtBackupFolder.Text = backupProposal.LastBackupLocation;
            this.ShowDialog();
            return _backupFolder;
        }


        private void btnBrowseForBackupFolder_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtBackupFolder.Text = folderBrowserDialog.SelectedPath;
            }
        }

        private void btnBackup_Click(object sender, EventArgs e)
        {
            if (IsValidPath(txtBackupFolder.Text))
            {
                _backupFolder = txtBackupFolder.Text;
                this.Close();
            }
            else
                MessageBox.Show("Gelieve een geldig locatie op te geven voor uw backup", "Backup", MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Ben je zeker dat je geen backup wil nemen deze keer?", "Backup", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
            if (dialogResult == System.Windows.Forms.DialogResult.Yes)
                this.Close();
        }

         /// <summary>  
         /// Gets whether the specified path is a valid absolute file path.  
         /// </summary>  
         /// <param name="path">Any path. OK if null or empty.</param>  
         static public bool IsValidPath( string path )  
         {  
             Regex r = new Regex( @"^(([a-zA-Z]\:)|(\\))(\\{1}|((\\{1})[^\\]([^/:*?<>""|]*))+)$" );  
             return r.IsMatch( path );  
         }  
    }
}
