﻿namespace KA.Forms
{
    partial class BackupDataStorageDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BackupDataStorageDialog));
            this.lblTitle = new KA.CustomControls.Extensions.LabelEx();
            this.btnCancel = new MTControls.MTButtons.MTMultiGradiantButton();
            this.btnBackup = new MTControls.MTButtons.MTMultiGradiantButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBackupFolder = new System.Windows.Forms.TextBox();
            this.btnBrowseForBackupFolder = new MTControls.MTButtons.MTMultiGradiantButton();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.lblDataStorageFilenameLabel = new System.Windows.Forms.Label();
            this.lblDataStorageFilename = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblLastBackupDate = new System.Windows.Forms.Label();
            this.lblLastBackupDateLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.HighColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252)))));
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.LowColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178)))));
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(502, 30);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "Databank backup";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Angle = 180F;
            this.btnCancel.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252))))));
            this.btnCancel.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178))))));
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancel.Location = new System.Drawing.Point(402, 266);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(88, 23);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Annuleer";
            this.btnCancel.Transparency = 100;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnBackup
            // 
            this.btnBackup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBackup.Angle = 180F;
            this.btnBackup.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252))))));
            this.btnBackup.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178))))));
            this.btnBackup.FlatAppearance.BorderSize = 0;
            this.btnBackup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBackup.Location = new System.Drawing.Point(308, 266);
            this.btnBackup.Name = "btnBackup";
            this.btnBackup.Size = new System.Drawing.Size(88, 23);
            this.btnBackup.TabIndex = 10;
            this.btnBackup.Text = "Neem backup ";
            this.btnBackup.Transparency = 100;
            this.btnBackup.UseVisualStyleBackColor = true;
            this.btnBackup.Click += new System.EventHandler(this.btnBackup_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 138);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Backup folder:";
            // 
            // txtBackupFolder
            // 
            this.txtBackupFolder.Location = new System.Drawing.Point(90, 135);
            this.txtBackupFolder.Name = "txtBackupFolder";
            this.txtBackupFolder.Size = new System.Drawing.Size(306, 20);
            this.txtBackupFolder.TabIndex = 8;
            // 
            // btnBrowseForBackupFolder
            // 
            this.btnBrowseForBackupFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowseForBackupFolder.Angle = 180F;
            this.btnBrowseForBackupFolder.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252))))));
            this.btnBrowseForBackupFolder.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178))))));
            this.btnBrowseForBackupFolder.FlatAppearance.BorderSize = 0;
            this.btnBrowseForBackupFolder.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBrowseForBackupFolder.Location = new System.Drawing.Point(402, 133);
            this.btnBrowseForBackupFolder.Name = "btnBrowseForBackupFolder";
            this.btnBrowseForBackupFolder.Size = new System.Drawing.Size(88, 23);
            this.btnBrowseForBackupFolder.TabIndex = 9;
            this.btnBrowseForBackupFolder.Text = "Kies ...";
            this.btnBrowseForBackupFolder.Transparency = 100;
            this.btnBrowseForBackupFolder.UseVisualStyleBackColor = true;
            this.btnBrowseForBackupFolder.Click += new System.EventHandler(this.btnBrowseForBackupFolder_Click);
            // 
            // folderBrowserDialog
            // 
            this.folderBrowserDialog.Description = "Backup folder";
            this.folderBrowserDialog.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // lblDataStorageFilenameLabel
            // 
            this.lblDataStorageFilenameLabel.AutoSize = true;
            this.lblDataStorageFilenameLabel.Location = new System.Drawing.Point(12, 87);
            this.lblDataStorageFilenameLabel.Name = "lblDataStorageFilenameLabel";
            this.lblDataStorageFilenameLabel.Size = new System.Drawing.Size(57, 13);
            this.lblDataStorageFilenameLabel.TabIndex = 3;
            this.lblDataStorageFilenameLabel.Text = "Databank:";
            // 
            // lblDataStorageFilename
            // 
            this.lblDataStorageFilename.AutoSize = true;
            this.lblDataStorageFilename.Location = new System.Drawing.Point(87, 87);
            this.lblDataStorageFilename.Name = "lblDataStorageFilename";
            this.lblDataStorageFilename.Size = new System.Drawing.Size(10, 13);
            this.lblDataStorageFilename.TabIndex = 4;
            this.lblDataStorageFilename.Text = "-";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(478, 32);
            this.label2.TabIndex = 2;
            this.label2.Text = "Kies een folder waar je een backup van de gegevens wil opslaan. Gebruik hiervoor " +
                "best een externe schijf of USB stick";
            // 
            // lblLastBackupDate
            // 
            this.lblLastBackupDate.AutoSize = true;
            this.lblLastBackupDate.Location = new System.Drawing.Point(87, 112);
            this.lblLastBackupDate.Name = "lblLastBackupDate";
            this.lblLastBackupDate.Size = new System.Drawing.Size(10, 13);
            this.lblLastBackupDate.TabIndex = 6;
            this.lblLastBackupDate.Text = "-";
            // 
            // lblLastBackupDateLabel
            // 
            this.lblLastBackupDateLabel.AutoSize = true;
            this.lblLastBackupDateLabel.Location = new System.Drawing.Point(12, 112);
            this.lblLastBackupDateLabel.Name = "lblLastBackupDateLabel";
            this.lblLastBackupDateLabel.Size = new System.Drawing.Size(79, 13);
            this.lblLastBackupDateLabel.TabIndex = 5;
            this.lblLastBackupDateLabel.Text = "Vorige backup:";
            // 
            // BackupDataStorageDialog
            // 
            this.AcceptButton = this.btnBackup;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(502, 301);
            this.ControlBox = false;
            this.Controls.Add(this.lblLastBackupDate);
            this.Controls.Add(this.lblLastBackupDateLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblDataStorageFilename);
            this.Controls.Add(this.lblDataStorageFilenameLabel);
            this.Controls.Add(this.btnBrowseForBackupFolder);
            this.Controls.Add(this.txtBackupFolder);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnBackup);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblTitle);
            this.Name = "BackupDataStorageDialog";
            this.Text = "2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private KA.CustomControls.Extensions.LabelEx lblTitle;
        private MTControls.MTButtons.MTMultiGradiantButton btnCancel;
        private MTControls.MTButtons.MTMultiGradiantButton btnBackup;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBackupFolder;
        private MTControls.MTButtons.MTMultiGradiantButton btnBrowseForBackupFolder;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Label lblDataStorageFilenameLabel;
        private System.Windows.Forms.Label lblDataStorageFilename;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblLastBackupDate;
        private System.Windows.Forms.Label lblLastBackupDateLabel;
    }
}