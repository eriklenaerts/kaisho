using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace KA.Forms
{
    public partial class CloseForm : Form
    {
        public CloseForm()
        {
            InitializeComponent();
        }


        private void xpGlowButton2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void xpGlowButton3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}