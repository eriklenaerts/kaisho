using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using KA_Controller.Interfaces;
using KA.app_code;
using KA_Common;
using BL = KA.BusinessLogicLayer;
using KA_Controller.EventArgs;


namespace KA.Forms
{
    public partial class Exams : Form, IExamsListViewBase
    {
        public Exams()
        {
            InitializeComponent();
        }
        private void Exams_Activated(object sender, EventArgs e)
        {
            ActionList<Action> actionList = new ActionList<Action>();
            //actionList.ImageList = this.imageList2;
            //actionList.Add(new Action("Examen lijst", 0, Enums.Actions.ExamList, Enums.Controller.ExamListController, null));
            //actionList.Add(new Action("Details", 1, Enums.Actions.detail, Enums.Controller.ExamListController, null));
            //actionList.Add(new Action("Vernieuwen", 2, Enums.Actions.refresh, Enums.Controller.ExamListController, null));
            ((Form1)this.MdiParent).SetActions(actionList);
        }

        #region IViewBase Members

        public void FillGrid(BL.TList<BL.Member> MemberList)
        {
            MemberList.Sort(LastNameSort);
            List<MemberView> members = new List<MemberView>();
            listView1.Items.Clear();
            foreach (BL.Member member in MemberList)
            {
                MemberView memberview = new MemberView(member);
                members.Add(memberview);
                ListViewItem lvi = new ListViewItem(new string[6] { memberview.FullName, memberview.DegreeImage.ToString(), memberview.Street, memberview.ZipCode, memberview.City, memberview.DegreeName });
                lvi.Tag = memberview;
                lvi.ImageIndex = 0;
                listView1.Items.Add(lvi);

            }
        }

        public void FillLocations(KA.BusinessLogicLayer.TList<KA.BusinessLogicLayer.Location> LocationList)
        {
            ddlLocations.DataSource = LocationList;
            ddlLocations.DisplayMember = "Name";
            ddlLocations.ValueMember = "ID";   
        }

        private int LastNameSort(BL.Member m1, BL.Member m2)
        {
            return m1.Lastname.CompareTo(m2.Lastname);
        }

        private void listView1_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            if (e.Item.SubItems[1] == e.SubItem)
            {

                e.DrawDefault = false;

                e.Graphics.DrawImage(((MemberView)e.Item.Tag).DegreeImage, e.SubItem.Bounds.Left, e.SubItem.Bounds.Top, 73, 24);
            }
            else
            {
                e.DrawDefault = true;
            }
        }

        private void listView1_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            e.DrawDefault = true;
            e.DrawBackground();
        }

        public void Init(Form mdiParent)
        {
            this.MdiParent = mdiParent;
            this.WindowState = FormWindowState.Maximized;
        }

        public void ShowBrokenRules(KA.BusinessLogicLayer.Validation.BrokenRulesList brokenRules)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public event EventHandler ViewClosed;
        public event EventHandler<KA_Controller.EventArgs.ActionEventArgs> ActionInvoked;

        #endregion

        private void Exams_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (ViewClosed != null)
                ViewClosed(this, new EventArgs());
        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                int degreeId = 0;
                switch (((RadioButton)sender).Name.ToString())
                {
                    case "rdb8Kyu": degreeId = 1; break;
                    case "rdb7Kyu": degreeId = 2; break;
                    case "rdb6Kyu": degreeId = 3; break;
                    case "rdb5Kyu": degreeId = 4; break;
                    case "rdb4Kyu": degreeId = 5; break;
                    case "rdb3Kyu": degreeId = 6; break;
                    case "rdb2Kyu": degreeId = 7; break;
                    case "rdb1Kyu": degreeId = 8; break;
                    case "rdbShodan": degreeId = 9; break;
                    case "rdbNiDan": degreeId = 10; break;
                    case "rdbSanDan": degreeId = 11; break;
                    case "rdbYonDan": degreeId = 12; break;
                    case "rdbGoDan": degreeId = 13; break;
                    case "rdbAllMembers": degreeId = 14; break;
                    default:
                        break;
                }
                if (ActionInvoked != null)
                    ActionInvoked(this, new ActionEventArgs(Enums.Actions.refresh, Enums.Controller.ExamListController, degreeId));
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bent u zeker dat u deze examenresultaten wilt toepassen?", "Toepassen", MessageBoxButtons.OKCancel)== DialogResult.OK)
            {
                BL.TList<BL.Examination> exams = new KA.BusinessLogicLayer.TList<KA.BusinessLogicLayer.Examination>();
                foreach (ListViewItem item in listView1.CheckedItems)
                {
                    int memberId = ((MemberView)item.Tag).Member.ID;
                    int LocationId = (int)ddlLocations.SelectedValue;
                    int degreeId = rdb8Kyu.Checked ? 2 : rdb7Kyu.Checked ? 3 : rdb6Kyu.Checked ? 4 : rdb5Kyu.Checked ? 5 : rdb4Kyu.Checked ? 6 : rdb3Kyu.Checked ? 7 : rdb2Kyu.Checked ? 8 : rdb1Kyu.Checked ? 9 : rdbShodan.Checked ? 10 : rdbNiDan.Checked ? 11 : rdbSanDan.Checked ? 12 : rdbYonDan.Checked ? 13 : rdbGoDan.Checked ? 14 : 15;
                    DateTime date = dtpDate.Value;
                    BL.Examination examination = new KA.BusinessLogicLayer.Examination();
                    examination.DegreeID = degreeId;
                    examination.LocationID = LocationId;
                    examination.MemberID = memberId;
                    examination.Date = new DateTime?(date);
                    exams.Add(examination);
                }
                if (ActionInvoked != null)
                {
                    ActionInvoked(this, new ActionEventArgs(Enums.Actions.Apply, Enums.Controller.ExamListController, exams));
                    int degree = rdb8Kyu.Checked ? 1 : rdb7Kyu.Checked ? 2 : rdb6Kyu.Checked ? 3 : rdb5Kyu.Checked ? 4 : rdb4Kyu.Checked ? 5 : rdb3Kyu.Checked ? 6 : rdb2Kyu.Checked ? 7 : rdb1Kyu.Checked ? 8 : rdbShodan.Checked ? 9 : rdbNiDan.Checked ? 10 : rdbSanDan.Checked ? 11 : rdbYonDan.Checked ? 12 : rdbGoDan.Checked ? 13 : 14;
                    ActionInvoked(this, new ActionEventArgs(Enums.Actions.refresh, Enums.Controller.ExamListController, degree));
                }
            }
        }
    }
}