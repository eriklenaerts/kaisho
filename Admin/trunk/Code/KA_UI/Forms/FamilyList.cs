using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using KA_Controller;
using KA.app_code;
using KA_Common;
using KA_Controller.BaseClasses;
using KA_Domain;
using SL = KA.ServiceLayer;
using BL = KA.BusinessLogicLayer;
using KA_Controller.EventArgs;
using KA_Controller.Interfaces;

namespace KA.Forms
{
    public partial class FamilieBeheer : Form, IFamilyListViewBase
    {
        private ListViewItemSorter lviSorter;

        public FamilieBeheer()
        {
            InitializeComponent();

            lviSorter = new ListViewItemSorter();
            lstFamilies.ListViewItemSorter = lviSorter;
        }

        #region IViewBase Members

        public event EventHandler ViewClosed;
        public event EventHandler<KA_Controller.EventArgs.ActionEventArgs> ActionInvoked;

        public void Init(Form mdiParent)
        {
            this.MdiParent = mdiParent;
            this.WindowState = FormWindowState.Maximized;
        }

        public void ShowBrokenRules(KA.BusinessLogicLayer.Validation.BrokenRulesList brokenRules)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region IFamilyManagementView Members

        public void FillGrid(BL.TList<BL.Group> groupList)
        {
            lstFamilies.Items.Clear();

            groupList.Sort(GroupNameSort);
            List<GroupView> groups = new List<GroupView>();
            foreach (BL.Group group in groupList)
            {
                GroupView groupView = new GroupView(group);
                ListViewItem lvi = new ListViewItem(new string[2] { groupView.Name, groupView.Members });
                lvi.Tag = new GroupView(group);
                lvi.StateImageIndex = 0;
                lstFamilies.Items.Add(lvi);
            }
            lblNumberOfFamilies.Text = "Aantal families : " + " " + lstFamilies.Items.Count.ToString();


        }

        public KA.BusinessLogicLayer.Group GetSelectedGroup()
        {
            if (lstFamilies.SelectedItems.Count > 0 && ((GroupView)lstFamilies.SelectedItems[0].Tag).Group is BL.Group)
            {
                return ((GroupView)lstFamilies.SelectedItems[0].Tag).Group;
            }
            return null;
        }

        #endregion

        private void FamilieBeheer_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (ViewClosed != null)
                ViewClosed(this, new EventArgs());
        }

        private void FamilieBeheer_Activated(object sender, EventArgs e)
        {
            //Here we instantiate which actions are visible in the small menu on the left hand side called "Opties"
            ((Form1)this.MdiParent).SetSelectedOutlookButton(1);
            ActionList<Action> actionList = new ActionList<Action>();
            actionList.ImageList = this.imageList1;
            actionList.Add(new Action("Toevoegen", 0, Enums.Actions.add, Enums.Controller.FamilyListController, null));
            actionList.Add(new Action("Verwijderen", 1, Enums.Actions.delete, Enums.Controller.FamilyListController, null));
            actionList.Add(new Action("Vernieuwen", 2, Enums.Actions.refresh, Enums.Controller.FamilyListController, null));
            actionList.Add(new Action("Details", 3, Enums.Actions.detail, Enums.Controller.FamilyListController, null));
            ((Form1)this.MdiParent).SetActions(actionList);
        }

        private int GroupNameSort(BL.Group g1, BL.Group g2)
        {
            return g1.Name.CompareTo(g2.Name);
        }

        private void cmbFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            Enums.Actions action = Enums.Actions.refresh;
            string filter = string.Empty; ;
            switch (cmbFilter.SelectedIndex)
            {
                case 0: action = Enums.Actions.refresh; filter = string.Empty; break;
                case 1: action = Enums.Actions.GetFilteredFamilies; filter = "Active"; break;
                case 2: action = Enums.Actions.GetFilteredFamilies; filter = "NonActive"; break;
                default: break;
            }

            if (ActionInvoked != null)
            {
                ActionInvoked(this, new ActionEventArgs(action, Enums.Controller.FamilyListController, filter));
            }
        }

        private void lstFamilies_DoubleClick(object sender, EventArgs e)
        {
            if (ActionInvoked != null && lstFamilies.SelectedItems.Count > 0 && lstFamilies.SelectedItems[0].Tag is GroupView)
            {
                ActionInvoked(this, new ActionEventArgs(Enums.Actions.detail, Enums.Controller.FamilyListController, ((GroupView)lstFamilies.SelectedItems[0].Tag).Group));
            }
        }

        private void lstFamilies_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            ListViewItemSorter.SortListView(sender, e);
        }
    }
}