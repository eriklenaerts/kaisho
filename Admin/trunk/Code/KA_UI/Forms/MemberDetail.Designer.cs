namespace KA.Forms
{
    partial class MemberDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MemberDetail));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ddlPaymentType = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.lblFamilyOftxt = new System.Windows.Forms.Label();
            this.lblFamilyOf = new System.Windows.Forms.Label();
            this.TxtLicenseNr = new System.Windows.Forms.MaskedTextBox();
            this.dtpBirthDate = new UIComponent.DateTimePicker();
            this.ddlSexe = new System.Windows.Forms.ComboBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtTelephone = new System.Windows.Forms.TextBox();
            this.txtProvince = new System.Windows.Forms.TextBox();
            this.txtCountry = new System.Windows.Forms.TextBox();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.txtZipCode = new System.Windows.Forms.TextBox();
            this.txtStreet = new System.Windows.Forms.TextBox();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ddlTrainingLocation = new System.Windows.Forms.ComboBox();
            this.lblTrainingLocation = new System.Windows.Forms.Label();
            this.chkInactive = new System.Windows.Forms.CheckBox();
            this.btnDegreeHistoric = new MTControls.MTButtons.MTMultiGradiantButton();
            this.chkTrainer = new System.Windows.Forms.CheckBox();
            this.picDegree = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.chkNewMember = new System.Windows.Forms.CheckBox();
            this.lblTitle = new KA.CustomControls.Extensions.LabelEx();
            this.btnCancel = new MTControls.MTButtons.MTMultiGradiantButton();
            this.btnOK = new MTControls.MTButtons.MTMultiGradiantButton();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picDegree)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.ddlPaymentType);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.lblFamilyOftxt);
            this.groupBox1.Controls.Add(this.lblFamilyOf);
            this.groupBox1.Controls.Add(this.TxtLicenseNr);
            this.groupBox1.Controls.Add(this.dtpBirthDate);
            this.groupBox1.Controls.Add(this.ddlSexe);
            this.groupBox1.Controls.Add(this.txtEmail);
            this.groupBox1.Controls.Add(this.txtTelephone);
            this.groupBox1.Controls.Add(this.txtProvince);
            this.groupBox1.Controls.Add(this.txtCountry);
            this.groupBox1.Controls.Add(this.txtCity);
            this.groupBox1.Controls.Add(this.txtZipCode);
            this.groupBox1.Controls.Add(this.txtStreet);
            this.groupBox1.Controls.Add(this.txtLastName);
            this.groupBox1.Controls.Add(this.txtFirstName);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 49);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(636, 320);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Persoonlijk";
            // 
            // ddlPaymentType
            // 
            this.ddlPaymentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPaymentType.FormattingEnabled = true;
            this.ddlPaymentType.Items.AddRange(new object[] {
            "",
            "Man",
            "Vrouw"});
            this.ddlPaymentType.Location = new System.Drawing.Point(97, 288);
            this.ddlPaymentType.Name = "ddlPaymentType";
            this.ddlPaymentType.Size = new System.Drawing.Size(160, 21);
            this.ddlPaymentType.TabIndex = 27;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 291);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(84, 13);
            this.label14.TabIndex = 28;
            this.label14.Text = "Betaalmethode :";
            // 
            // lblFamilyOftxt
            // 
            this.lblFamilyOftxt.AutoSize = true;
            this.lblFamilyOftxt.Location = new System.Drawing.Point(94, 119);
            this.lblFamilyOftxt.Name = "lblFamilyOftxt";
            this.lblFamilyOftxt.Size = new System.Drawing.Size(0, 13);
            this.lblFamilyOftxt.TabIndex = 4;
            // 
            // lblFamilyOf
            // 
            this.lblFamilyOf.AutoSize = true;
            this.lblFamilyOf.Location = new System.Drawing.Point(6, 119);
            this.lblFamilyOf.Name = "lblFamilyOf";
            this.lblFamilyOf.Size = new System.Drawing.Size(66, 13);
            this.lblFamilyOf.TabIndex = 26;
            this.lblFamilyOf.Text = "Familie van :";
            // 
            // TxtLicenseNr
            // 
            this.TxtLicenseNr.HidePromptOnLeave = true;
            this.TxtLicenseNr.Location = new System.Drawing.Point(97, 19);
            this.TxtLicenseNr.Mask = "00000000000000000000000000000000000000000000000";
            this.TxtLicenseNr.Name = "TxtLicenseNr";
            this.TxtLicenseNr.Size = new System.Drawing.Size(275, 20);
            this.TxtLicenseNr.TabIndex = 0;
            // 
            // dtpBirthDate
            // 
            this.dtpBirthDate.Location = new System.Drawing.Point(356, 227);
            this.dtpBirthDate.Name = "dtpBirthDate";
            this.dtpBirthDate.Size = new System.Drawing.Size(200, 20);
            this.dtpBirthDate.TabIndex = 10;
            this.dtpBirthDate.Value = new System.DateTime(2007, 7, 31, 14, 14, 26, 934);
            // 
            // ddlSexe
            // 
            this.ddlSexe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlSexe.FormattingEnabled = true;
            this.ddlSexe.Items.AddRange(new object[] {
            "",
            "Man",
            "Vrouw"});
            this.ddlSexe.Location = new System.Drawing.Point(97, 227);
            this.ddlSexe.Name = "ddlSexe";
            this.ddlSexe.Size = new System.Drawing.Size(160, 21);
            this.ddlSexe.TabIndex = 9;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(97, 258);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(275, 20);
            this.txtEmail.TabIndex = 11;
            // 
            // txtTelephone
            // 
            this.txtTelephone.Location = new System.Drawing.Point(97, 201);
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Size = new System.Drawing.Size(160, 20);
            this.txtTelephone.TabIndex = 8;
            // 
            // txtProvince
            // 
            this.txtProvince.Location = new System.Drawing.Point(97, 168);
            this.txtProvince.Name = "txtProvince";
            this.txtProvince.Size = new System.Drawing.Size(160, 20);
            this.txtProvince.TabIndex = 6;
            // 
            // txtCountry
            // 
            this.txtCountry.Location = new System.Drawing.Point(355, 168);
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.Size = new System.Drawing.Size(160, 20);
            this.txtCountry.TabIndex = 7;
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(355, 142);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(160, 20);
            this.txtCity.TabIndex = 5;
            // 
            // txtZipCode
            // 
            this.txtZipCode.Location = new System.Drawing.Point(97, 142);
            this.txtZipCode.Name = "txtZipCode";
            this.txtZipCode.Size = new System.Drawing.Size(160, 20);
            this.txtZipCode.TabIndex = 4;
            // 
            // txtStreet
            // 
            this.txtStreet.Location = new System.Drawing.Point(97, 92);
            this.txtStreet.Name = "txtStreet";
            this.txtStreet.Size = new System.Drawing.Size(275, 20);
            this.txtStreet.TabIndex = 3;
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(97, 68);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(275, 20);
            this.txtLastName.TabIndex = 2;
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(97, 45);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(275, 20);
            this.txtFirstName.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 261);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "Email :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(263, 227);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Geboortedatum :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 230);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Geslacht :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 204);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Telefoon :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 171);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Provincie :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(263, 169);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Land :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(263, 145);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Woonplaats :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Voornaam :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Achternaam :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Straat :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 145);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Postcode :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Licentie nr :";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.ddlTrainingLocation);
            this.groupBox2.Controls.Add(this.lblTrainingLocation);
            this.groupBox2.Controls.Add(this.chkInactive);
            this.groupBox2.Controls.Add(this.btnDegreeHistoric);
            this.groupBox2.Controls.Add(this.chkTrainer);
            this.groupBox2.Controls.Add(this.picDegree);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.chkNewMember);
            this.groupBox2.Location = new System.Drawing.Point(13, 375);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(636, 207);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Algemeen";
            // 
            // ddlTrainingLocation
            // 
            this.ddlTrainingLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlTrainingLocation.FormattingEnabled = true;
            this.ddlTrainingLocation.Items.AddRange(new object[] {
            "",
            "Man",
            "Vrouw"});
            this.ddlTrainingLocation.Location = new System.Drawing.Point(124, 77);
            this.ddlTrainingLocation.Name = "ddlTrainingLocation";
            this.ddlTrainingLocation.Size = new System.Drawing.Size(160, 21);
            this.ddlTrainingLocation.TabIndex = 29;
            // 
            // lblTrainingLocation
            // 
            this.lblTrainingLocation.AutoSize = true;
            this.lblTrainingLocation.Location = new System.Drawing.Point(6, 80);
            this.lblTrainingLocation.Name = "lblTrainingLocation";
            this.lblTrainingLocation.Size = new System.Drawing.Size(112, 13);
            this.lblTrainingLocation.TabIndex = 30;
            this.lblTrainingLocation.Text = "Hoofd trainingsplaats :";
            // 
            // chkInactive
            // 
            this.chkInactive.AutoSize = true;
            this.chkInactive.Location = new System.Drawing.Point(239, 35);
            this.chkInactive.Name = "chkInactive";
            this.chkInactive.Size = new System.Drawing.Size(61, 17);
            this.chkInactive.TabIndex = 2;
            this.chkInactive.Text = "Inactief";
            this.chkInactive.UseVisualStyleBackColor = true;
            // 
            // btnDegreeHistoric
            // 
            this.btnDegreeHistoric.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDegreeHistoric.Angle = 180F;
            this.btnDegreeHistoric.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252))))));
            this.btnDegreeHistoric.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178))))));
            this.btnDegreeHistoric.FlatAppearance.BorderSize = 0;
            this.btnDegreeHistoric.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDegreeHistoric.Location = new System.Drawing.Point(516, 178);
            this.btnDegreeHistoric.Name = "btnDegreeHistoric";
            this.btnDegreeHistoric.Size = new System.Drawing.Size(114, 23);
            this.btnDegreeHistoric.TabIndex = 3;
            this.btnDegreeHistoric.Text = "GraadHistoriek";
            this.btnDegreeHistoric.Transparency = 100;
            this.btnDegreeHistoric.UseVisualStyleBackColor = true;
            this.btnDegreeHistoric.Click += new System.EventHandler(this.btnDegreeHistoric_Click);
            // 
            // chkTrainer
            // 
            this.chkTrainer.AutoSize = true;
            this.chkTrainer.Location = new System.Drawing.Point(132, 35);
            this.chkTrainer.Name = "chkTrainer";
            this.chkTrainer.Size = new System.Drawing.Size(59, 17);
            this.chkTrainer.TabIndex = 1;
            this.chkTrainer.Text = "Trainer";
            this.chkTrainer.UseVisualStyleBackColor = true;
            // 
            // picDegree
            // 
            this.picDegree.Location = new System.Drawing.Point(58, 164);
            this.picDegree.Name = "picDegree";
            this.picDegree.Size = new System.Drawing.Size(79, 24);
            this.picDegree.TabIndex = 13;
            this.picDegree.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(10, 169);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Graad :";
            // 
            // chkNewMember
            // 
            this.chkNewMember.AutoSize = true;
            this.chkNewMember.Location = new System.Drawing.Point(9, 35);
            this.chkNewMember.Name = "chkNewMember";
            this.chkNewMember.Size = new System.Drawing.Size(69, 17);
            this.chkNewMember.TabIndex = 0;
            this.chkNewMember.Text = "Nieuw lid";
            this.chkNewMember.UseVisualStyleBackColor = true;
            // 
            // lblTitle
            // 
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.HighColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252)))));
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.LowColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178)))));
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(661, 30);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "Lid :";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Angle = 180F;
            this.btnCancel.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252))))));
            this.btnCancel.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178))))));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancel.Location = new System.Drawing.Point(529, 587);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(114, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Transparency = 100;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Angle = 180F;
            this.btnOK.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252))))));
            this.btnOK.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178))))));
            this.btnOK.FlatAppearance.BorderSize = 0;
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOK.Location = new System.Drawing.Point(391, 587);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(114, 23);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "OK";
            this.btnOK.Transparency = 100;
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // MemberDetail
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(245)))), ((int)(((byte)(248)))));
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(661, 618);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblTitle);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(670, 531);
            this.Name = "MemberDetail";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lid : detail";
            this.Load += new System.EventHandler(this.MemberDetail_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MemberDetail_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picDegree)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private KA.CustomControls.Extensions.LabelEx lblTitle;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCountry;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.TextBox txtZipCode;
        private System.Windows.Forms.TextBox txtStreet;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtTelephone;
        private System.Windows.Forms.TextBox txtProvince;
        private System.Windows.Forms.PictureBox picDegree;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox chkNewMember;
        private System.Windows.Forms.CheckBox chkInactive;
        private System.Windows.Forms.CheckBox chkTrainer;
        private System.Windows.Forms.ComboBox ddlSexe;
        private MTControls.MTButtons.MTMultiGradiantButton btnCancel;
        private MTControls.MTButtons.MTMultiGradiantButton btnOK;
        private MTControls.MTButtons.MTMultiGradiantButton btnDegreeHistoric;
        private UIComponent.DateTimePicker dtpBirthDate;
        private System.Windows.Forms.MaskedTextBox TxtLicenseNr;
        private System.Windows.Forms.Label lblFamilyOf;
        private System.Windows.Forms.Label lblFamilyOftxt;
        private System.Windows.Forms.ComboBox ddlPaymentType;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox ddlTrainingLocation;
        private System.Windows.Forms.Label lblTrainingLocation;
    }
}
