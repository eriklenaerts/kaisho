using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using KA_Controller.Interfaces;
using KA.app_code;
using KA_Common;
using Host.Types;
using KA_Controller.EventArgs;


namespace KA.Forms
{
    public partial class Statistics : Form, IStatisticsViewBase
    {
        public Statistics()
        {
            InitializeComponent();
        }

        #region IStatisticsViewBase Members

        public void  FillGrid(AvailablePlugins plugins)
        {
            List<StatisticView> statistics = new List<StatisticView>();
            foreach (AvailablePlugin plugin in plugins)
            {
                statistics.Add(new StatisticView(plugin));
            }
 	        dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = statistics;
        }

        public AvailablePlugin GetSelectedReport()
        {
            StatisticView plugin = (StatisticView)dataGridView1.SelectedRows[0].DataBoundItem;
            return plugin.Plugin;
        }

        #endregion

        #region IViewBase Members
  
        public event EventHandler ViewClosed;
        public event EventHandler<KA_Controller.EventArgs.ActionEventArgs> ActionInvoked;
        
        
        public void Init(Form mdiParent)
        {
            MdiParent = mdiParent;
            WindowState = FormWindowState.Maximized;
        }


        public void ShowBrokenRules(KA.BusinessLogicLayer.Validation.BrokenRulesList brokenRules)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        private void Statistics_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (ViewClosed != null)
                ViewClosed(this, new EventArgs());
        }

        private void Statistics_Activated(object sender, EventArgs e)
        {
            ((Form1)this.MdiParent).SetSelectedOutlookButton(3);
            ActionList<Action> actionList = new ActionList<Action>();
            actionList.ImageList = this.imageList1;
            actionList.Add(new Action("Rapport invoegen", 0, Enums.Actions.ReportPlugin, Enums.Controller.StatisticsController, null));
            //actionList.Add(new Action("Rapport verwijderen",1, Enums.Actions.deleteReport, Enums.Controller.StatisticsController, null));
            
            ((Form1)this.MdiParent).SetActions(actionList);
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (ActionInvoked != null && dataGridView1.SelectedRows.Count > 0 && dataGridView1.SelectedRows[0].DataBoundItem is StatisticView)
            {
                ActionInvoked(this, new ActionEventArgs(Enums.Actions.ReportClicked, Enums.Controller.StatisticsController, ((StatisticView)dataGridView1.SelectedRows[0].DataBoundItem).Plugin));
            }
        }











    }
}