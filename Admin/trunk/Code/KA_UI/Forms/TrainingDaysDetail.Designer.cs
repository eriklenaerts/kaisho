﻿namespace KA.Forms
{
    partial class TrainingDaysDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxData = new System.Windows.Forms.GroupBox();
            this.lblSemicolonEnd = new System.Windows.Forms.Label();
            this.lblSemicolonStart = new System.Windows.Forms.Label();
            this.txtAudience = new System.Windows.Forms.TextBox();
            this.txtEndMinute = new System.Windows.Forms.TextBox();
            this.txtEndHour = new System.Windows.Forms.TextBox();
            this.txtStartMinute = new System.Windows.Forms.TextBox();
            this.cmbLocation = new System.Windows.Forms.ComboBox();
            this.cmbDayOfWeek = new System.Windows.Forms.ComboBox();
            this.cmbPeriod = new System.Windows.Forms.ComboBox();
            this.txtStartHour = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPeriode = new System.Windows.Forms.Label();
            this.btnOK = new MTControls.MTButtons.MTMultiGradiantButton();
            this.btnCancel = new MTControls.MTButtons.MTMultiGradiantButton();
            this.lblTitle = new KA.CustomControls.Extensions.LabelEx();
            this.groupBoxData.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxData
            // 
            this.groupBoxData.Controls.Add(this.lblSemicolonEnd);
            this.groupBoxData.Controls.Add(this.lblSemicolonStart);
            this.groupBoxData.Controls.Add(this.txtAudience);
            this.groupBoxData.Controls.Add(this.txtEndMinute);
            this.groupBoxData.Controls.Add(this.txtEndHour);
            this.groupBoxData.Controls.Add(this.txtStartMinute);
            this.groupBoxData.Controls.Add(this.cmbLocation);
            this.groupBoxData.Controls.Add(this.cmbDayOfWeek);
            this.groupBoxData.Controls.Add(this.cmbPeriod);
            this.groupBoxData.Controls.Add(this.txtStartHour);
            this.groupBoxData.Controls.Add(this.label3);
            this.groupBoxData.Controls.Add(this.label4);
            this.groupBoxData.Controls.Add(this.label5);
            this.groupBoxData.Controls.Add(this.label2);
            this.groupBoxData.Controls.Add(this.label1);
            this.groupBoxData.Controls.Add(this.lblPeriode);
            this.groupBoxData.Location = new System.Drawing.Point(13, 49);
            this.groupBoxData.Name = "groupBoxData";
            this.groupBoxData.Size = new System.Drawing.Size(701, 198);
            this.groupBoxData.TabIndex = 1;
            this.groupBoxData.TabStop = false;
            this.groupBoxData.Text = "Data";
            // 
            // lblSemicolonEnd
            // 
            this.lblSemicolonEnd.AutoSize = true;
            this.lblSemicolonEnd.Location = new System.Drawing.Point(180, 138);
            this.lblSemicolonEnd.Name = "lblSemicolonEnd";
            this.lblSemicolonEnd.Size = new System.Drawing.Size(10, 13);
            this.lblSemicolonEnd.TabIndex = 10;
            this.lblSemicolonEnd.Text = ":";
            // 
            // lblSemicolonStart
            // 
            this.lblSemicolonStart.AutoSize = true;
            this.lblSemicolonStart.Location = new System.Drawing.Point(180, 112);
            this.lblSemicolonStart.Name = "lblSemicolonStart";
            this.lblSemicolonStart.Size = new System.Drawing.Size(10, 13);
            this.lblSemicolonStart.TabIndex = 9;
            this.lblSemicolonStart.Text = ":";
            // 
            // txtAudience
            // 
            this.txtAudience.Location = new System.Drawing.Point(127, 161);
            this.txtAudience.Name = "txtAudience";
            this.txtAudience.Size = new System.Drawing.Size(232, 20);
            this.txtAudience.TabIndex = 8;
            // 
            // txtEndMinute
            // 
            this.txtEndMinute.Location = new System.Drawing.Point(190, 135);
            this.txtEndMinute.Name = "txtEndMinute";
            this.txtEndMinute.Size = new System.Drawing.Size(52, 20);
            this.txtEndMinute.TabIndex = 7;
            // 
            // txtEndHour
            // 
            this.txtEndHour.Location = new System.Drawing.Point(127, 135);
            this.txtEndHour.Name = "txtEndHour";
            this.txtEndHour.Size = new System.Drawing.Size(52, 20);
            this.txtEndHour.TabIndex = 6;
            // 
            // txtStartMinute
            // 
            this.txtStartMinute.Location = new System.Drawing.Point(190, 109);
            this.txtStartMinute.Name = "txtStartMinute";
            this.txtStartMinute.Size = new System.Drawing.Size(52, 20);
            this.txtStartMinute.TabIndex = 5;
            // 
            // cmbLocation
            // 
            this.cmbLocation.FormattingEnabled = true;
            this.cmbLocation.Location = new System.Drawing.Point(127, 55);
            this.cmbLocation.Name = "cmbLocation";
            this.cmbLocation.Size = new System.Drawing.Size(232, 21);
            this.cmbLocation.TabIndex = 2;
            // 
            // cmbDayOfWeek
            // 
            this.cmbDayOfWeek.FormattingEnabled = true;
            this.cmbDayOfWeek.Location = new System.Drawing.Point(127, 82);
            this.cmbDayOfWeek.Name = "cmbDayOfWeek";
            this.cmbDayOfWeek.Size = new System.Drawing.Size(232, 21);
            this.cmbDayOfWeek.TabIndex = 3;
            // 
            // cmbPeriod
            // 
            this.cmbPeriod.FormattingEnabled = true;
            this.cmbPeriod.Location = new System.Drawing.Point(127, 28);
            this.cmbPeriod.Name = "cmbPeriod";
            this.cmbPeriod.Size = new System.Drawing.Size(232, 21);
            this.cmbPeriod.TabIndex = 1;
            // 
            // txtStartHour
            // 
            this.txtStartHour.Location = new System.Drawing.Point(127, 109);
            this.txtStartHour.Name = "txtStartHour";
            this.txtStartHour.Size = new System.Drawing.Size(52, 20);
            this.txtStartHour.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 164);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Publiek : ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 138);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Eind uur training : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Start uur training";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Dag van de week : ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Locatie :";
            // 
            // lblPeriode
            // 
            this.lblPeriode.AutoSize = true;
            this.lblPeriode.Location = new System.Drawing.Point(7, 31);
            this.lblPeriode.Name = "lblPeriode";
            this.lblPeriode.Size = new System.Drawing.Size(49, 13);
            this.lblPeriode.TabIndex = 0;
            this.lblPeriode.Text = "Periode :";
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Angle = 180F;
            this.btnOK.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252))))));
            this.btnOK.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178))))));
            this.btnOK.FlatAppearance.BorderSize = 0;
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOK.Location = new System.Drawing.Point(463, 253);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(114, 23);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "OK";
            this.btnOK.Transparency = 100;
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Angle = 180F;
            this.btnCancel.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252))))));
            this.btnCancel.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178))))));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancel.Location = new System.Drawing.Point(600, 253);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(114, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Transparency = 100;
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // lblTitle
            // 
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.HighColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252)))));
            this.lblTitle.Image = global::KA.Properties.Resources.date_time32;
            this.lblTitle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.LowColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178)))));
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(726, 35);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Trainingsdag :";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TrainingDaysDetail
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(245)))), ((int)(((byte)(248)))));
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(726, 295);
            this.ControlBox = false;
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.groupBoxData);
            this.Controls.Add(this.lblTitle);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(742, 331);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(742, 331);
            this.Name = "TrainingDaysDetail";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Trainings dag detail";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.TrainingDaysDetail_FormClosed);
            this.groupBoxData.ResumeLayout(false);
            this.groupBoxData.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private KA.CustomControls.Extensions.LabelEx lblTitle;
        private System.Windows.Forms.GroupBox groupBoxData;
        private System.Windows.Forms.Label lblPeriode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbDayOfWeek;
        private System.Windows.Forms.ComboBox cmbPeriod;
        private System.Windows.Forms.TextBox txtStartHour;
        private System.Windows.Forms.TextBox txtEndMinute;
        private System.Windows.Forms.TextBox txtEndHour;
        private System.Windows.Forms.TextBox txtStartMinute;
        private System.Windows.Forms.ComboBox cmbLocation;
        private System.Windows.Forms.TextBox txtAudience;
        private MTControls.MTButtons.MTMultiGradiantButton btnOK;
        private MTControls.MTButtons.MTMultiGradiantButton btnCancel;
        private System.Windows.Forms.Label lblSemicolonStart;
        private System.Windows.Forms.Label lblSemicolonEnd;

    }
}