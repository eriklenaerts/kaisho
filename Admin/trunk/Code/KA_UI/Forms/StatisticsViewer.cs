using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using KA_Controller.Interfaces;
using System.Configuration;

namespace KA.Forms
{
    public partial class StatisticsViewer : Form, IStatisticsViewerBase
    {
        public StatisticsViewer()
        {
            InitializeComponent();
        }
      
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region IstatisticsViewerBase Members

        public void LoadReport(Host.Types.AvailablePlugin selectedPlugin)
        {
            this.Text = "Rapporten : " + selectedPlugin.Instance.PluginName;
            this.lblTitle.Text = "Rapporten : " + selectedPlugin.Instance.PluginName;
            this.pnlPlugin.Controls.Clear();
            selectedPlugin.Instance.MainInterface.Dock = DockStyle.Fill;
            selectedPlugin.Instance.Initialize(ConfigurationManager.ConnectionStrings["netTiersConnectionString"].ConnectionString);
            this.pnlPlugin.Controls.Add(selectedPlugin.Instance.MainInterface);
        }

        #endregion

        #region IViewBase Members


        public event EventHandler ViewClosed;

        public event EventHandler<KA_Controller.EventArgs.ActionEventArgs> ActionInvoked;

        public void Init(Form mdiParent){
            this.MdiParent = mdiParent;
        }

        public void ShowBrokenRules(KA.BusinessLogicLayer.Validation.BrokenRulesList brokenRules){}

        #endregion

        private void StatisticsViewer_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ViewClosed != null)
                ViewClosed(this, new EventArgs());
        }
    }
}