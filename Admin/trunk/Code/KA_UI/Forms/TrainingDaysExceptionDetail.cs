﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using KA_Controller.Interfaces;
using KA.BusinessLogicLayer;

namespace KA.Forms
{
    public partial class TrainingDaysExceptionDetail : Form, ITrainingDaysExceptionsDetailView
    {
        private bool _new = true;
        private TrainingExceptions _trainingException;

        public TrainingDaysExceptionDetail()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (_trainingException == null)
                _trainingException = new TrainingExceptions();

            _trainingException.Day = dateTimePicker1.Value;

            if (_new)
            {
                if (ActionInvoked != null)
                    ActionInvoked(this, new KA_Controller.EventArgs.ActionEventArgs(KA_Common.Enums.Actions.add, KA_Common.Enums.Controller.TrainingDaysExceptionsDetailController, _trainingException));
            }
            else
            {
                if (ActionInvoked != null)
                    ActionInvoked(this, new KA_Controller.EventArgs.ActionEventArgs(KA_Common.Enums.Actions.update, KA_Common.Enums.Controller.TrainingDaysExceptionsDetailController, _trainingException));
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region ITrainingDaysExceptionsDetailView Members

        public void SetContext(KA.BusinessLogicLayer.TrainingExceptions trainingEx)
        {
            if (trainingEx != null)
            {
                _new = false;
                _trainingException = trainingEx;
                dateTimePicker1.Value = trainingEx.Day;
            }
            else
                _new = true;
        }

        #endregion

        #region IViewBase Members


        public event EventHandler ViewClosed;

        public event EventHandler<KA_Controller.EventArgs.ActionEventArgs> ActionInvoked;

        public void Init(Form mdiParent)
        {
            throw new NotImplementedException();
        }

        public void ShowBrokenRules(KA.BusinessLogicLayer.Validation.BrokenRulesList brokenRules)
        {
            throw new NotImplementedException();
        }

        private void TrainingDaysExceptionDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (ViewClosed != null)
                ViewClosed(this, new EventArgs());
        }
        #endregion
    }
}
