﻿namespace KA.Forms
{
    partial class AddHistory
    {
        /// <summary>²
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddHistory));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.rdbGoDan = new System.Windows.Forms.RadioButton();
            this.rdbYonDan = new System.Windows.Forms.RadioButton();
            this.rdbSanDan = new System.Windows.Forms.RadioButton();
            this.rdbNiDan = new System.Windows.Forms.RadioButton();
            this.rdbShodan = new System.Windows.Forms.RadioButton();
            this.rdb1Kyu = new System.Windows.Forms.RadioButton();
            this.rdb2Kyu = new System.Windows.Forms.RadioButton();
            this.rdb3Kyu = new System.Windows.Forms.RadioButton();
            this.rdb4Kyu = new System.Windows.Forms.RadioButton();
            this.rdb5Kyu = new System.Windows.Forms.RadioButton();
            this.rdb6Kyu = new System.Windows.Forms.RadioButton();
            this.rdb7Kyu = new System.Windows.Forms.RadioButton();
            this.rdb8Kyu = new System.Windows.Forms.RadioButton();
            this.btnCancel = new MTControls.MTButtons.MTMultiGradiantButton();
            this.btnAdd = new MTControls.MTButtons.MTMultiGradiantButton();
            this.lblTitle = new KA.CustomControls.Extensions.LabelEx();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.AutoSize = true;
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dtpDate);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.rdbGoDan);
            this.groupBox1.Controls.Add(this.rdbYonDan);
            this.groupBox1.Controls.Add(this.rdbSanDan);
            this.groupBox1.Controls.Add(this.rdbNiDan);
            this.groupBox1.Controls.Add(this.rdbShodan);
            this.groupBox1.Controls.Add(this.rdb1Kyu);
            this.groupBox1.Controls.Add(this.rdb2Kyu);
            this.groupBox1.Controls.Add(this.rdb3Kyu);
            this.groupBox1.Controls.Add(this.rdb4Kyu);
            this.groupBox1.Controls.Add(this.rdb5Kyu);
            this.groupBox1.Controls.Add(this.rdb6Kyu);
            this.groupBox1.Controls.Add(this.rdb7Kyu);
            this.groupBox1.Controls.Add(this.rdb8Kyu);
            this.groupBox1.Location = new System.Drawing.Point(12, 42);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.groupBox1.Size = new System.Drawing.Size(555, 156);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Graad :";
            // 
            // rdbGoDan
            // 
            this.rdbGoDan.AutoSize = true;
            this.rdbGoDan.Image = global::KA.Properties.Resources._5dan;
            this.rdbGoDan.Location = new System.Drawing.Point(456, 15);
            this.rdbGoDan.Name = "rdbGoDan";
            this.rdbGoDan.Size = new System.Drawing.Size(93, 24);
            this.rdbGoDan.TabIndex = 15;
            this.rdbGoDan.UseVisualStyleBackColor = true;
            // 
            // rdbYonDan
            // 
            this.rdbYonDan.AutoSize = true;
            this.rdbYonDan.Image = global::KA.Properties.Resources._4dan;
            this.rdbYonDan.Location = new System.Drawing.Point(357, 61);
            this.rdbYonDan.Name = "rdbYonDan";
            this.rdbYonDan.Size = new System.Drawing.Size(93, 24);
            this.rdbYonDan.TabIndex = 14;
            this.rdbYonDan.UseVisualStyleBackColor = true;
            // 
            // rdbSanDan
            // 
            this.rdbSanDan.AutoSize = true;
            this.rdbSanDan.Image = global::KA.Properties.Resources._3dan;
            this.rdbSanDan.Location = new System.Drawing.Point(357, 38);
            this.rdbSanDan.Name = "rdbSanDan";
            this.rdbSanDan.Size = new System.Drawing.Size(93, 24);
            this.rdbSanDan.TabIndex = 13;
            this.rdbSanDan.UseVisualStyleBackColor = true;
            // 
            // rdbNiDan
            // 
            this.rdbNiDan.AutoSize = true;
            this.rdbNiDan.Image = global::KA.Properties.Resources._2dan;
            this.rdbNiDan.Location = new System.Drawing.Point(357, 15);
            this.rdbNiDan.Name = "rdbNiDan";
            this.rdbNiDan.Size = new System.Drawing.Size(93, 24);
            this.rdbNiDan.TabIndex = 12;
            this.rdbNiDan.UseVisualStyleBackColor = true;
            // 
            // rdbShodan
            // 
            this.rdbShodan.AutoSize = true;
            this.rdbShodan.Image = global::KA.Properties.Resources._1dan;
            this.rdbShodan.Location = new System.Drawing.Point(258, 61);
            this.rdbShodan.Name = "rdbShodan";
            this.rdbShodan.Size = new System.Drawing.Size(93, 24);
            this.rdbShodan.TabIndex = 11;
            this.rdbShodan.UseVisualStyleBackColor = true;
            // 
            // rdb1Kyu
            // 
            this.rdb1Kyu.AutoSize = true;
            this.rdb1Kyu.Image = global::KA.Properties.Resources._1kyu;
            this.rdb1Kyu.Location = new System.Drawing.Point(258, 38);
            this.rdb1Kyu.Name = "rdb1Kyu";
            this.rdb1Kyu.Size = new System.Drawing.Size(93, 24);
            this.rdb1Kyu.TabIndex = 10;
            this.rdb1Kyu.UseVisualStyleBackColor = true;
            // 
            // rdb2Kyu
            // 
            this.rdb2Kyu.AutoSize = true;
            this.rdb2Kyu.Image = global::KA.Properties.Resources._2kyu;
            this.rdb2Kyu.Location = new System.Drawing.Point(258, 15);
            this.rdb2Kyu.Name = "rdb2Kyu";
            this.rdb2Kyu.Size = new System.Drawing.Size(93, 24);
            this.rdb2Kyu.TabIndex = 9;
            this.rdb2Kyu.UseVisualStyleBackColor = true;
            // 
            // rdb3Kyu
            // 
            this.rdb3Kyu.AutoSize = true;
            this.rdb3Kyu.Image = global::KA.Properties.Resources._3kyu;
            this.rdb3Kyu.Location = new System.Drawing.Point(159, 61);
            this.rdb3Kyu.Name = "rdb3Kyu";
            this.rdb3Kyu.Size = new System.Drawing.Size(93, 24);
            this.rdb3Kyu.TabIndex = 8;
            this.rdb3Kyu.UseVisualStyleBackColor = true;
            // 
            // rdb4Kyu
            // 
            this.rdb4Kyu.AutoSize = true;
            this.rdb4Kyu.Image = global::KA.Properties.Resources._4kyu;
            this.rdb4Kyu.Location = new System.Drawing.Point(159, 38);
            this.rdb4Kyu.Name = "rdb4Kyu";
            this.rdb4Kyu.Size = new System.Drawing.Size(93, 24);
            this.rdb4Kyu.TabIndex = 7;
            this.rdb4Kyu.UseVisualStyleBackColor = true;
            // 
            // rdb5Kyu
            // 
            this.rdb5Kyu.AutoSize = true;
            this.rdb5Kyu.Image = global::KA.Properties.Resources._5kyu;
            this.rdb5Kyu.Location = new System.Drawing.Point(159, 15);
            this.rdb5Kyu.Name = "rdb5Kyu";
            this.rdb5Kyu.Size = new System.Drawing.Size(93, 24);
            this.rdb5Kyu.TabIndex = 6;
            this.rdb5Kyu.UseVisualStyleBackColor = true;
            // 
            // rdb6Kyu
            // 
            this.rdb6Kyu.AutoSize = true;
            this.rdb6Kyu.Image = global::KA.Properties.Resources._6kyu;
            this.rdb6Kyu.Location = new System.Drawing.Point(60, 61);
            this.rdb6Kyu.Name = "rdb6Kyu";
            this.rdb6Kyu.Size = new System.Drawing.Size(93, 24);
            this.rdb6Kyu.TabIndex = 5;
            this.rdb6Kyu.UseVisualStyleBackColor = true;
            // 
            // rdb7Kyu
            // 
            this.rdb7Kyu.AutoSize = true;
            this.rdb7Kyu.Image = global::KA.Properties.Resources._7kyu;
            this.rdb7Kyu.Location = new System.Drawing.Point(60, 38);
            this.rdb7Kyu.Name = "rdb7Kyu";
            this.rdb7Kyu.Size = new System.Drawing.Size(93, 24);
            this.rdb7Kyu.TabIndex = 4;
            this.rdb7Kyu.UseVisualStyleBackColor = true;
            // 
            // rdb8Kyu
            // 
            this.rdb8Kyu.AutoSize = true;
            this.rdb8Kyu.Checked = true;
            this.rdb8Kyu.Image = global::KA.Properties.Resources._8kyu;
            this.rdb8Kyu.Location = new System.Drawing.Point(60, 15);
            this.rdb8Kyu.Name = "rdb8Kyu";
            this.rdb8Kyu.Size = new System.Drawing.Size(93, 24);
            this.rdb8Kyu.TabIndex = 3;
            this.rdb8Kyu.TabStop = true;
            this.rdb8Kyu.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Angle = 180F;
            this.btnCancel.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252))))));
            this.btnCancel.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178))))));
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancel.Location = new System.Drawing.Point(438, 216);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(114, 23);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Annuleren";
            this.btnCancel.Transparency = 100;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Angle = 180F;
            this.btnAdd.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252))))));
            this.btnAdd.Colors.Add(System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178))))));
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAdd.Location = new System.Drawing.Point(304, 216);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(114, 23);
            this.btnAdd.TabIndex = 6;
            this.btnAdd.Text = "Toevoegen";
            this.btnAdd.Transparency = 100;
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.HighColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252)))));
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.LowColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178)))));
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(581, 30);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "Historiek toevoegen";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpDate
            // 
            this.dtpDate.Location = new System.Drawing.Point(60, 105);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(200, 20);
            this.dtpDate.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Datum :";
            // 
            // AddHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(581, 250);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblTitle);
            this.Name = "AddHistory";
            this.Text = "AddHistory";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AddHistory_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private KA.CustomControls.Extensions.LabelEx lblTitle;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rdbGoDan;
        private System.Windows.Forms.RadioButton rdbYonDan;
        private System.Windows.Forms.RadioButton rdbSanDan;
        private System.Windows.Forms.RadioButton rdbNiDan;
        private System.Windows.Forms.RadioButton rdbShodan;
        private System.Windows.Forms.RadioButton rdb1Kyu;
        private System.Windows.Forms.RadioButton rdb2Kyu;
        private System.Windows.Forms.RadioButton rdb3Kyu;
        private System.Windows.Forms.RadioButton rdb4Kyu;
        private System.Windows.Forms.RadioButton rdb5Kyu;
        private System.Windows.Forms.RadioButton rdb6Kyu;
        private System.Windows.Forms.RadioButton rdb7Kyu;
        private System.Windows.Forms.RadioButton rdb8Kyu;
        private MTControls.MTButtons.MTMultiGradiantButton btnAdd;
        private MTControls.MTButtons.MTMultiGradiantButton btnCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpDate;
    }
}