using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using KA_Controller;
using KA_Controller.Interfaces;
using KA_Domain;
using BL = KA.BusinessLogicLayer;
using KA_Controller.EventArgs;
using System.Configuration;
using KA.Properties;
using KA.BusinessLogicLayer;


namespace KA.Forms
{
    public partial class MemberDetail : Form, IMemberDetailsView
    {
        private BL.Member _member;

        public MemberDetail()
        {
            InitializeComponent();
        }

        public void RefreshDegree(Image degreePic)
        {
            picDegree.Image = degreePic;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (_member == null)
                _member = new BL.Member();      
      
            //FormData Saven
            if (TxtLicenseNr.Text != string.Empty)
                _member.LicenseNumber = Int32.Parse(TxtLicenseNr.Text);
            _member.Firstname = txtFirstName.Text;
            _member.Lastname = txtLastName.Text;
            _member.Address = txtStreet.Text;
            _member.ZipCode = txtZipCode.Text;
            _member.City = txtCity.Text;
            _member.Province = txtProvince.Text;
            _member.Country = txtCountry.Text;
            _member.Telephone = txtTelephone.Text;
            _member.Sex = ddlSexe.Text.ToLower() == "man" ? "M" : "V"; ;
            if (dtpBirthDate.Value != DateTime.MinValue)
                _member.Birthdate = dtpBirthDate.Value;
            else
                _member.Birthdate = null;
            _member.Email = txtEmail.Text;
            _member.IsNew = chkNewMember.Checked;
            _member.IsTrainer = chkTrainer.Checked;
            _member.IsActive = !chkInactive.Checked;
            if (((PaymentType)ddlPaymentType.SelectedItem).Id != -1)
                _member.PaymentId = ((PaymentType)ddlPaymentType.SelectedItem).Id;
            else
                _member.PaymentId = null;

            _member.TrainingLocationId = ((Location)ddlTrainingLocation.SelectedItem).ID;

            if (ActionInvoked != null)
            {
                ActionInvoked(this, new ActionEventArgs(KA_Common.Enums.Actions.update,KA_Common.Enums.Controller.MemberDetailController, _member));
                //ActionInvoked(this, KA_Common.Enums.Controller.MemberDetailController, KA_Common.Enums.Actions.refresh, null);
            }
        }

        private void MemberDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ViewClosed != null)
                ViewClosed(this, new EventArgs());
        }
        
        private void CleanFormData()
        {
            lblTitle.Text = "Lid :";
            TxtLicenseNr.Text = string.Empty;
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtStreet.Text = string.Empty;
            txtZipCode.Text = string.Empty;
            txtCity.Text = string.Empty;
            txtProvince.Text = ConfigurationManager.AppSettings["defaultProvince"].ToString();
            txtCountry.Text = ConfigurationManager.AppSettings["defaultCountry"].ToString(); ;
            txtTelephone.Text = string.Empty;
            ddlSexe.Text = string.Empty;
            dtpBirthDate.Value = DateTime.MinValue;
            txtEmail.Text = string.Empty;
            chkNewMember.Checked = true;
            chkTrainer.Checked = false;
            chkInactive.Checked = false;
            picDegree.Image = null;
            _member = null;
        }

        #region IViewBase Members

        public void Init(Form mdiParent){}

        public void ShowBrokenRules(KA.BusinessLogicLayer.Validation.BrokenRulesList brokenRules)
        {
            MessageCenterController.ShowMessageCenter(brokenRules);
            
        }

        public event EventHandler ViewClosed;
        public  event EventHandler<KA_Controller.EventArgs.ActionEventArgs> ActionInvoked;
        
        #endregion

        #region IMemberDetailsView Members

        public void SetContext(KA.BusinessLogicLayer.Member member, Image degreeImage, TList<PaymentType> paymentTypes, TList<Location> locations)
        {
            if (member != null)
            {
                _member = member;

                lblTitle.Text += " " + member.FullName;
                TxtLicenseNr.Text = member.LicenseNumber.ToString();
                txtFirstName.Text = member.Firstname;
                txtLastName.Text = member.Lastname;
                lblFamilyOftxt.Text = GetFamilyMembers(member);
                txtStreet.Text = member.Address;
                txtZipCode.Text = member.ZipCode;
                txtCity.Text = member.City;
                txtProvince.Text = member.Province;
                txtCountry.Text = member.Country;
                txtTelephone.Text = member.Telephone;
                if (member.Sex != null)
                    ddlSexe.Text = member.Sex.ToLower() == "m" ? "Man" : "Vrouw";
                dtpBirthDate.Value = member.Birthdate.HasValue ? member.Birthdate.Value : DateTime.MinValue;
                txtEmail.Text = member.Email;
                chkNewMember.Checked = member.IsNew;
                chkTrainer.Checked = member.IsTrainer;
                chkInactive.Checked = !member.IsActive;
                picDegree.Image = degreeImage != null ? degreeImage : Resources.undefined;

                PaymentType empty = new PaymentType();
                empty.Id = -1;
                empty.Code = "NA";
                empty.Description = "Onbekend";
                paymentTypes.Insert(0, empty);

                ddlPaymentType.ValueMember = "ID";
                ddlPaymentType.DisplayMember = "Description";
                ddlPaymentType.DataSource = paymentTypes;

                ddlTrainingLocation.ValueMember = "ID";
                ddlTrainingLocation.DisplayMember = "Name";
                ddlTrainingLocation.DataSource = locations;

                if (member.PaymentId != null)
                    ddlPaymentType.SelectedValue = member.PaymentId;

                if (member.TrainingLocationId != null)
                    ddlTrainingLocation.SelectedValue = member.TrainingLocationId;

            }
            else
            {
                PaymentType empty = new PaymentType();
                empty.Id = -1;
                empty.Code = "NA";
                empty.Description = "Onbekend";
                paymentTypes.Insert(0, empty);

                ddlPaymentType.ValueMember = "ID";
                ddlPaymentType.DisplayMember = "Description";
                ddlPaymentType.DataSource = paymentTypes;

                ddlTrainingLocation.ValueMember = "ID";
                ddlTrainingLocation.DisplayMember = "Name";
                ddlTrainingLocation.DataSource = locations;

                CleanFormData();
            }
        }

        private string GetFamilyMembers(KA.BusinessLogicLayer.Member member)
        {
            StringBuilder sbMembers = new StringBuilder();
            bool hasFamily = false;
            if (member.GroupIDSource !=null)
	        {
                foreach (KA.BusinessLogicLayer.Member groupMember in member.GroupIDSource.MemberCollection)
                {
                    if (groupMember != member)
                    {
                        sbMembers.Append(groupMember.Firstname);
                        sbMembers.Append(", ");
                    }
                    else
                    {
                        hasFamily = true;
                    }
                }
	        }

            if (hasFamily && sbMembers.Length == 0)
                return "Enigste lid in de familie";
            else if (sbMembers.Length > 0)
                return sbMembers.ToString().Remove(sbMembers.Length - 2, 2);
            else
                return "Behoort niet tot een familie";
        }

        #endregion

        private void btnDegreeHistoric_Click(object sender, EventArgs e)
        {
            if (ActionInvoked != null && _member != null)
                ActionInvoked(this, new ActionEventArgs(KA_Common.Enums.Actions.ShowHistory, KA_Common.Enums.Controller.MemberHistoryController, _member));
        }

        private void MemberDetail_Load(object sender, EventArgs e)
        {
            TxtLicenseNr.Focus();
        }
    }
}