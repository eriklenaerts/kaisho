using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using KA_Controller;
using KA_Controller.Interfaces;
using KA_Domain;
using BL = KA.BusinessLogicLayer;
using KA_Controller.EventArgs;
using SL = KA.ServiceLayer;
using KA_Common;
using KA.app_code;

namespace KA.Forms
{
    public partial class FamilyDetail : Form, IFamilyDetailsView
    {
        private BL.TList<BL.Member> _allMembers = new KA.BusinessLogicLayer.TList<KA.BusinessLogicLayer.Member>();
        private BL.TList<BL.Member> _familyMembers = new KA.BusinessLogicLayer.TList<KA.BusinessLogicLayer.Member>();
        private BL.Group _currentGroup = new KA.BusinessLogicLayer.Group();
        private ListViewItemSorter lviSorter;

        public BL.TList<BL.Member> FamilyMembers
        {
            get { return _familyMembers; }
            set { _familyMembers = value; }
        }

        public FamilyDetail()
        {
            InitializeComponent();
        }

        #region IFamilyDetailsView Members

        public void SetContext(KA.BusinessLogicLayer.Group group)
        {
            _currentGroup = group;
            lviSorter = new ListViewItemSorter();
            lstMembers.ListViewItemSorter = lviSorter;
            lstFamilyMembers.ListViewItemSorter = lviSorter;

            if (group != null)
            {
                txtFamilyName.Text = group.Name;
                lblTitle.Text = "Familie : " + group.Name;
                lblFamilieleden.Text = GetFamilyMembers();
                FillFamilyMembers();
            }
            else
                CleanFormData();

            if (!rdbtnAllMembers.Checked)
                rdbtnAllMembers.Checked = true;
            else
                FillAllMembers();

            HideMemberManagement(true);
        }
        #endregion

        #region IViewBase Members


        public event EventHandler ViewClosed;

        public event EventHandler<ActionEventArgs> ActionInvoked;

        public void Init(Form mdiParent)
        {        }

        public void ShowBrokenRules(KA.BusinessLogicLayer.Validation.BrokenRulesList brokenRules)
        {
            MessageCenterController.ShowMessageCenter(brokenRules);
        }

        #endregion

        #region fillLists /clear form
        private void CleanFormData()
        {
            lblTitle.Text = "Familie";
            txtFamilyName.Text = string.Empty;
            lstFamilyMembers.Items.Clear();
            lstMembers.Items.Clear();
        }

        private void FillFamilyMembers()
        {
            bool fillList = (_familyMembers.Count == 0);
            lstFamilyMembers.Items.Clear();

            foreach (KA.BusinessLogicLayer.Member member in _currentGroup.MemberCollection)
            {
                ListViewItem lvi = new ListViewItem(new string[2] { member.Lastname, member.Firstname });
                lvi.Tag = member;
                lvi.Name = member.ID.ToString();
                lstFamilyMembers.Items.Add(lvi);

                if (fillList)
                    _familyMembers.Add(member);
            }

            SortColumn(lstFamilyMembers, 0, SortOrder.Ascending);
        }

        private void FillAllMembers()
        {
            SL.MemberService memberService = new KA.ServiceLayer.MemberService();
            bool fillList = (_allMembers.Count == 0);
            lstMembers.Items.Clear();

            if (fillList)
            {
                string tmpFilter = _allMembers.Filter;
                _allMembers = memberService.GetAll();
                _allMembers.Filter = tmpFilter;
            }

            foreach (KA.BusinessLogicLayer.Member member in _allMembers)
            {
                BL.Member foundMember = null;
                if(_currentGroup != null)
                    foundMember = _currentGroup.MemberCollection.Find("ID", member.ID);

                if (foundMember == null)
                {
                    ListViewItem lvi = new ListViewItem(new string[2] { member.Lastname, member.Firstname });
                    lvi.Tag = member;
                    lvi.Name = member.ID.ToString();
                    lstMembers.Items.Add(lvi);
                }
            }

            lblTeKiezenLeden.Text = "Te kiezen leden (" + lstMembers.Items.Count + ")";
            SortColumn(lstMembers, 0, SortOrder.Ascending);
        }
        #endregion

        #region add/remove
        private void btnAddFamilyMember_Click(object sender, EventArgs e)
        {
            if(lstMembers.SelectedIndices.Count > 0)
            {
                foreach (int selectedIndex in lstMembers.SelectedIndices)
                {
                    //add to the familyMembers listview
                    ListViewItem lvi = new ListViewItem(new string[2] { lstMembers.Items[selectedIndex].SubItems[0].Text, lstMembers.Items[selectedIndex].SubItems[1].Text }); 
                    lvi.Tag = lstMembers.Items[selectedIndex].Tag;
                    lvi.Name = ((KA.BusinessLogicLayer.Member)lstMembers.Items[selectedIndex].Tag).ID.ToString();
                    lstFamilyMembers.Items.Add(lvi);
                    
                    //delete from the AllMembers listview
                    lstMembers.Items[selectedIndex].Remove();
                }
                lstFamilyMembers.Sort();
            }
        }

        private void btnRemoveFamilyMember_Click(object sender, EventArgs e)
        {
            if (lstFamilyMembers.SelectedIndices.Count > 0)
            {
                foreach (int selectedIndex in lstFamilyMembers.SelectedIndices)
                {
                    //add to the AllMembers listview
                    ListViewItem lvi = new ListViewItem(new string[2] { ((KA.BusinessLogicLayer.Member)lstFamilyMembers.Items[selectedIndex].Tag).Lastname, ((KA.BusinessLogicLayer.Member)lstFamilyMembers.Items[selectedIndex].Tag).Firstname }); //add the firstname to the Listbox
                    lvi.Tag = lstFamilyMembers.Items[selectedIndex].Tag;
                    lvi.Name = ((KA.BusinessLogicLayer.Member)lstFamilyMembers.Items[selectedIndex].Tag).ID.ToString();
                    lstMembers.Items.Add(lvi);

                    //delete from the familyMembers listview
                    //this has to happen after the add to the allmembers list, otherwise the object wil not be found to do the 'add'
                    lstFamilyMembers.Items[selectedIndex].Remove();
                }
                lstMembers.Sort();
            }
        }
        #endregion

        #region btnOK
        private void btnOK_Click(object sender, EventArgs e)
        {
            SL.GroupService groupService = new KA.ServiceLayer.GroupService();
            SL.MemberService memberService = new KA.ServiceLayer.MemberService();

            if (_currentGroup != null)
            {
                #region familymembers
                //Find all added members by comparing the original list with the currentFamilylist
                //everything that is in the currentFamilyList but not in the original list is an extra item
                for (int i = 0; i < lstFamilyMembers.Items.Count; i++)
                {
                    if (!_familyMembers.Contains((KA.BusinessLogicLayer.Member)lstFamilyMembers.Items[i].Tag))
                    {
                        KA.BusinessLogicLayer.Member tmpMember = (KA.BusinessLogicLayer.Member)lstFamilyMembers.Items[i].Tag;
                        tmpMember.GroupID = _currentGroup.ID;
                        memberService.Update(tmpMember);
                    }
                }

                //Find all deleted members by comparing the currentFamilylist with the original list
                //everything that is in the original list but not in the currentFamilyList is a deleted item
                for (int i = 0; i < FamilyMembers.Count; i++)
                {
                    ListViewItem[] tmpLvi = lstFamilyMembers.Items.Find(_familyMembers[i].ID.ToString(), false);
                    if (tmpLvi.Length == 0)
                    {
                        KA.BusinessLogicLayer.Member tmpMember = FamilyMembers[i];
                        tmpMember.GroupID = null;
                        memberService.Update(tmpMember);
                    }
                }
                #endregion

                #region familyName
                if (txtFamilyName.Text.Trim() != _currentGroup.Name.Trim())
                {
                    _currentGroup.Name = txtFamilyName.Text.Trim();
                    groupService.Update(_currentGroup);
                }
                #endregion
            }
            else
            {
                #region new group
                _currentGroup = new KA.BusinessLogicLayer.Group();
                _currentGroup.Name = txtFamilyName.Text.Trim();
                groupService.Insert(_currentGroup);

                for (int i = 0; i < lstFamilyMembers.Items.Count; i++)
                {
                    KA.BusinessLogicLayer.Member tmpMember = (KA.BusinessLogicLayer.Member)lstFamilyMembers.Items[i].Tag;
                    tmpMember.GroupID = _currentGroup.ID;
                    memberService.Update(tmpMember);
                }
                #endregion
            }

            //refresh the main familyMembers list
            if (ActionInvoked != null)
                ActionInvoked(this, new ActionEventArgs(Enums.Actions.refresh, Enums.Controller.FamilyListController, null));

            this.Close();
        }
        #endregion

        #region radiobuttons Filter
        private void rdbtnAllMembers_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbtnAllMembers.Checked)
                FillAllMembers();
        }

        private void rdBtnSameFamilyName_CheckedChanged(object sender, EventArgs e)
        {
            if (rdBtnSameFamilyName.Checked)
            {
                _allMembers.Filter = "Lastname like " + txtFamilyName.Text.Trim();
                FillAllMembers();
                _allMembers.Filter = string.Empty; 
            }
        }

        private void rdBtnWhithoutFamily_CheckedChanged(object sender, EventArgs e)
        {
            if (rdBtnWhithoutFamily.Checked)
            {
                _allMembers.Filter = "groupID = NULL";
                FillAllMembers();
                _allMembers.Filter = string.Empty;
            }
        }
        #endregion

        #region columnClick (sort)
        private void List_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            ListViewItemSorter.SortListView(sender, e);
        }

        private void SortColumn(ListView sender, int columnToSort, SortOrder sortOrder)
        {
            // Set the column number that is to be sorted; default to ascending.
            lviSorter.SortColumn = columnToSort;
            lviSorter.Order = sortOrder;

            sender.Sort();
        }
        #endregion

       

        private string GetFamilyMembers()
        {
            StringBuilder sbMembers = new StringBuilder();
            if (_currentGroup != null)
            {
                foreach (KA.BusinessLogicLayer.Member groupMember in _currentGroup.MemberCollection)
                {
                    sbMembers.Append(groupMember.Firstname);
                    sbMembers.Append(", ");
                }
            }

            if (sbMembers.Length > 0)
                return sbMembers.ToString().Remove(sbMembers.Length - 2, 2);
            else
                return "Geen leden in deze familie.";
        }

        private void FamilyDetail_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ViewClosed != null)
                ViewClosed(this, new EventArgs());
        }

        private void txtFamilyName_TextChanged(object sender, EventArgs e)
        {
            //we set it to false first so it will trigger the CheckChangedEvent whatsoever
            rdBtnSameFamilyName.Checked = false;
            rdBtnSameFamilyName.Checked = true;
        }

        private void btnLedenBeheer_Click(object sender, EventArgs e)
        {
            HideMemberManagement(panel1.Visible);
        }

        private void HideMemberManagement(bool hide)
        {
            if (hide)
            {
                panel1.Visible = false;
                familyGroupbox.Height = 90;
                Height = 195;
                this.Location = new Point(this.Location.X, this.Location.Y + 200);
            }
            else
            {
                panel1.Visible = true;
                familyGroupbox.Height = 387;
                Height = 493;
                this.Location = new Point(this.Location.X, this.Location.Y - 200);
            }
        }

        private void FamilyDetail_Load(object sender, EventArgs e)
        {
            txtFamilyName.Focus();
        }

    }
}