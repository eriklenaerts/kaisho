﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using KA_Common;
using KA_Controller.EventArgs;
using KA_Controller.Interfaces;
using BL = KA.BusinessLogicLayer;
using System.IO;
using KA.app_code;

namespace KA.Forms
{
    public partial class MemberHistory : Form, IMemberHistoryViewBase
    {
        private BL.TList<BL.Examination> _examinations;
        private BL.Member _member;

        public MemberHistory()
        {
            InitializeComponent();
        }

        #region IMemberHistoryViewBase Members

        public void SetContext(KA.BusinessLogicLayer.TList<KA.BusinessLogicLayer.Examination> examinations, BL.Member member)
        {
            _examinations = examinations;
            _member = member;
            FillGrid(member, _examinations);
        }

        public void FillGrid(BL.Member member, BL.TList<BL.Examination> examinationList)
        {
            examinationList.Sort(DegreeSort);
            List<ExaminationHistoryView> examsView = new List<ExaminationHistoryView>();
            //listView1.Items.Clear();
            foreach (BL.Examination ex in examinationList)
            {
                ExaminationHistoryView exView = new ExaminationHistoryView(ex);
                examsView.Add(exView);

                //ListViewItem lvi = new ListViewItem(new string[3] {ex.DegreeIDSource.Title,ex.Date.Value.ToString("dd/MM/yyyy"), ex.LocationIDSource.Name});
                //lvi.Tag = ex;
                //lvi.ImageIndex = 0;
                //listView1.Items.Add(lvi);
            }
            dgvExaminations.DataSource = examsView;

            if (examinationList.Count > 0)
                lblTitle.Text = "Historiek voor : " + examinationList[0].MemberIDSource.Firstname + " " + examinationList[0].MemberIDSource.Lastname;
        }

        private int DegreeSort(BL.Examination ex1, BL.Examination ex2)
        {
            return ex1.DegreeIDSource.Rank.CompareTo(ex2.DegreeIDSource.Rank);
        }

        #endregion

        #region IViewBase Members


        public event EventHandler ViewClosed;

        public event EventHandler<KA_Controller.EventArgs.ActionEventArgs> ActionInvoked;

        public void Init(Form mdiParent){}//THIS FORM WILL NOT BE ATTACHED TO AN MDI

        public void ShowBrokenRules(KA.BusinessLogicLayer.Validation.BrokenRulesList brokenRules){}

        #endregion

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MemberHistory_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ViewClosed != null)
                ViewClosed(this, new EventArgs());
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (ActionInvoked != null)
                ActionInvoked(this, new ActionEventArgs(Enums.Actions.add, Enums.Controller.AddHistory, _member));
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvExaminations.SelectedRows.Count > 0)
            {
                ExaminationHistoryView selectedExamView = dgvExaminations.SelectedRows[0].DataBoundItem as ExaminationHistoryView;
                if (ActionInvoked != null && selectedExamView != null)
                    ActionInvoked(this, new ActionEventArgs(Enums.Actions.delete, Enums.Controller.MemberHistoryController, new AddHistoryEventArgs{Exam = selectedExamView._ex, Member = _member}));
            }
        }
    }
}
