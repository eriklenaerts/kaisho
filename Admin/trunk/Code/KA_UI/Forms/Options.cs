﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using KA_Common;
using KA_Controller.Interfaces;
using KA.app_code;
using SL = KA.ServiceLayer;
using BL = KA.BusinessLogicLayer;
using KA_Controller.EventArgs;
using KA.CustomControls;
using KA_Controller;
using KA.BusinessLogicLayer;
using KA_Domain;

namespace KA.Forms
{
    public partial class Options : Form, IOptionsViewBase
    {
        private ListViewItemSorter lviSorter;

        public Options()
        {
            InitializeComponent();
        }

        #region IViewBase Members
        public event EventHandler ViewClosed;
        public event EventHandler<KA_Controller.EventArgs.ActionEventArgs> ActionInvoked;

        public void Init(Form mdiParent)
        {
            this.MdiParent = mdiParent;
            this.WindowState = FormWindowState.Maximized;
        }

        public void ShowBrokenRules(KA.BusinessLogicLayer.Validation.BrokenRulesList brokenRules)
        {
            
        }

        #endregion

        #region methods
        private void Options_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (ViewClosed != null)
                ViewClosed(this, new EventArgs());
        }

        private void tcOptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            Enums.Actions action = Enums.Actions.refresh;
            Enums.Controller controller = Enums.Controller.TrainingDaysController;
            IViewBase controllerView = null;
            switch (((TabControl)sender).SelectedIndex)
            {
                case 0: 
                    if(ActionInvoked != null)
                        ActionInvoked(this, new ActionEventArgs(Enums.Actions.LoadTrainingDays, Enums.Controller.OptionsController, null));    
                    break;
                case 1: 
                    if (ActionInvoked != null)
                        ActionInvoked(this, new ActionEventArgs(Enums.Actions.LoadTrainingExceptions, Enums.Controller.OptionsController, null));    
                    break;
                default:
                    break;
            }

            if (ActionInvoked != null)
                ActionInvoked(this, new ActionEventArgs(action, controller, controllerView));
        }

        private void Options_Load(object sender, EventArgs e)
        {
            //load the trainingdays when the options window is opened
            if (ActionInvoked != null)
            {
                if (ActionInvoked != null)
                    ActionInvoked(this, new ActionEventArgs(Enums.Actions.LoadTrainingDays, Enums.Controller.OptionsController, null));
            }
        }
        #endregion

        #region trainingdays
        private void lstTrainingDays_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            ListViewItemSorter.SortListView(sender, e);
        }

        public void FillTrainingDaysGrid(KA.BusinessLogicLayer.TList<KA.BusinessLogicLayer.TrainingTime> TrainingTimeList)
        {
            TrainingTimeList.Sort("DayOfTheWeek");
            lstTrainingDays.Items.Clear();
            foreach (BL.TrainingTime trainingTime in TrainingTimeList)
            {
                TrainingView trainingview = new TrainingView(trainingTime);
                ListViewItem lvi = new ListViewItem(new string[6] { trainingview.TrainingSeason, trainingview.Location, trainingview.DayOfWeek, trainingview.StartOfTraining, trainingview.EndOfTraining, trainingview.Audience });
                lvi.Tag = trainingTime;
                lvi.ImageIndex = 0;
                lstTrainingDays.Items.Add(lvi);
            }

            //--------------------------------------------

            lviSorter = new ListViewItemSorter();
            lstTrainingDays.ListViewItemSorter = lviSorter;

            ActivateTrainingDays();
        }

        private void ActivateTrainingDays()
        {
            //Here we instantiate which actions are visible in the small menu on the left hand side called "Opties"
            ((Form1)this.MdiParent).SetSelectedOutlookButton(1);
            ActionList<Action> actionList = new ActionList<Action>();
            actionList.ImageList = this.imageList2;
            DomainBase trainingDays = new DomainBase();
            trainingDays.mode = 1;
            actionList.Add(new Action("Toevoegen", 0, Enums.Actions.add, Enums.Controller.OptionsController, trainingDays));
            actionList.Add(new Action("Verwijderen", 1, Enums.Actions.delete, Enums.Controller.OptionsController, trainingDays));
            actionList.Add(new Action("Details", 3, Enums.Actions.detail, Enums.Controller.OptionsController, trainingDays));
            ((Form1)this.MdiParent).SetActions(actionList);

            EnableDisableMenu();
        }

        public TrainingTime GetSelectedTrainingTime()
        {
            if (lstTrainingDays.SelectedItems.Count > 0)
                return (TrainingTime)lstTrainingDays.SelectedItems[0].Tag;
            else
                return null;
        }

        private void lstTrainingDays_DoubleClick(object sender, EventArgs e)
        {
            if (lstTrainingDays.SelectedItems.Count > 0)
            {
                if (ActionInvoked != null)
                    ActionInvoked(this, new ActionEventArgs(Enums.Actions.detail, Enums.Controller.OptionsController, lstTrainingDays.SelectedItems[0].Tag));
            }
        }

        private void lstTrainingDays_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableMenu();
        }

        private void EnableDisableMenu()
        {
            if (lstTrainingDays.SelectedItems.Count == 0)
            {
                ((Form1)this.MdiParent).DisableAction("Verwijderen", true);
                ((Form1)this.MdiParent).DisableAction("Details", true);
            }
            else
            {
                ((Form1)this.MdiParent).DisableAction("Verwijderen", false);
                ((Form1)this.MdiParent).DisableAction("Details", false);
            }
        }
        #endregion

        #region vakantiedagen
        public void FillTrainingDaysExceptionsGrid(TList<TrainingExceptions> trainingExceptions)
        {
            trainingExceptions.Sort("Day");
            lstTrainingDaysExceptions.Items.Clear();
            foreach (BL.TrainingExceptions trainingException in trainingExceptions)
            {
                ListViewItem lvi = new ListViewItem(new string[1] { trainingException.Day.ToLongDateString() });
                lvi.Tag = trainingException;
                lvi.ImageIndex = 0;
                lstTrainingDaysExceptions.Items.Add(lvi);
            }

            lviSorter = new ListViewItemSorter();
            lstTrainingDaysExceptions.ListViewItemSorter = lviSorter;

            ActivateHolidays();
        }

        private void ActivateHolidays()
        {
            //Here we instantiate which actions are visible in the small menu on the left hand side called "Opties"
            ((Form1)this.MdiParent).SetSelectedOutlookButton(1);
            ActionList<Action> actionList = new ActionList<Action>();
            actionList.ImageList = this.imageList2;
            DomainBase trainingDays = new DomainBase();
            trainingDays.mode = 2;
            actionList.Add(new Action("Toevoegen", 0, Enums.Actions.add, Enums.Controller.OptionsController, trainingDays));
            actionList.Add(new Action("Verwijderen", 1, Enums.Actions.delete, Enums.Controller.OptionsController, trainingDays));
            actionList.Add(new Action("Details", 3, Enums.Actions.detail, Enums.Controller.OptionsController, trainingDays));
            ((Form1)this.MdiParent).SetActions(actionList);

            EnableDisableExceptionsMenu();
        }

        public TrainingExceptions GetSelectedTrainingException()
        {
            if (lstTrainingDaysExceptions.SelectedItems.Count > 0)
                return (TrainingExceptions)lstTrainingDaysExceptions.SelectedItems[0].Tag;
            else
                return null;
        }

        private void lstTrainingDaysExceptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableExceptionsMenu();
        }

        private void EnableDisableExceptionsMenu()
        {
            if (lstTrainingDaysExceptions.SelectedItems.Count == 0)
            {
                ((Form1)this.MdiParent).DisableAction("Verwijderen", true);
                ((Form1)this.MdiParent).DisableAction("Details", true);
            }
            else
            {
                ((Form1)this.MdiParent).DisableAction("Verwijderen", false);
                ((Form1)this.MdiParent).DisableAction("Details", false);
            }
        }

        private void lstTrainingDaysExceptions_DoubleClick(object sender, EventArgs e)
        {
            if (lstTrainingDaysExceptions.SelectedItems.Count > 0)
            {
                if (ActionInvoked != null)
                    ActionInvoked(this, new ActionEventArgs(Enums.Actions.detail, Enums.Controller.OptionsController, lstTrainingDaysExceptions.SelectedItems[0].Tag));
            }
        }
        #endregion

        private void Options_Activated(object sender, EventArgs e)
        {
            if(tcOptions.SelectedIndex == 0)
                ActivateTrainingDays();
            else
                ActivateHolidays();
        }
    }
}