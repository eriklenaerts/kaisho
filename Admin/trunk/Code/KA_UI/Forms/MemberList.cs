using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using KA_Controller;
using KA.app_code;
using KA_Common;
using KA_Controller.BaseClasses;
using KA_Domain;
using SL = KA.ServiceLayer;
using BL = KA.BusinessLogicLayer;
using KA_Controller.EventArgs;



namespace KA.Forms
{
    public partial class MemberList : Form, IMemberListViewBase
    {
        private ListViewItemSorter lviSorter;

        public MemberList()
        {
            InitializeComponent();

            lviSorter = new ListViewItemSorter();
            listView1.ListViewItemSorter = lviSorter;
        }

        private void MemberList_Activated(object sender, EventArgs e)
        {
            ((Form1)this.MdiParent).SetSelectedOutlookButton(0);
            ActionList<Action> actionList = new ActionList<Action>();
            actionList.ImageList = this.imageList1;
            actionList.Add(new Action("Toevoegen", 0, Enums.Actions.add, Enums.Controller.MemberListController, null));
            actionList.Add(new Action("Verwijderen", 1, Enums.Actions.delete, Enums.Controller.MemberListController, null));
            actionList.Add(new Action("Vernieuwen", 2, Enums.Actions.refresh, Enums.Controller.MemberListController, null));
            actionList.Add(new Action("Details", 3, Enums.Actions.detail, Enums.Controller.MemberListController, null));
            actionList.Add(new Action("Kopieren", 4, Enums.Actions.copy, Enums.Controller.MemberListController, null));
            ((Form1)this.MdiParent).SetActions(actionList);
        }

        public void FillGrid(BL.TList<BL.Member> MemberList)
        {
            MemberList.Sort(LastNameSort);
            List<MemberView> members = new List<MemberView>();
            listView1.Items.Clear();
            foreach (BL.Member member in MemberList)
            {
               
                    MemberView memberview = new MemberView(member);
                    members.Add(memberview);
                    ListViewItem lvi = new ListViewItem(new string[7] { memberview.FullName, memberview.DegreeImage.ToString(), memberview.Street, memberview.ZipCode, memberview.City, memberview.Telephone, memberview.Active});
                    lvi.Tag = memberview;
                    lvi.ImageIndex = 0;
                    listView1.Items.Add(lvi);
               
            }

            lblNumberOfMembers.Text = "Aantal leden : " + " " + MemberList.Count.ToString();
        }

        private int LastNameSort(BL.Member m1, BL.Member m2)
        {
            return m1.Lastname.CompareTo(m2.Lastname);
        }


        private void MemberList_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (ViewClosed != null)
                ViewClosed(this, new EventArgs());
        }

        #region IViewBase Members

        public void Init(Form mdiParent)
        {
            this.MdiParent = mdiParent;
            this.WindowState = FormWindowState.Maximized;
            comboBox1.SelectedIndex = 0;
        }

        public BL.Member GetSelectedMember()
        {
            if (listView1.SelectedItems.Count > 0 && listView1.SelectedItems[0].Tag is MemberView)
                return ((MemberView)listView1.SelectedItems[0].Tag).Member;
            return null;
        }

        public void ShowBrokenRules(KA.BusinessLogicLayer.Validation.BrokenRulesList brokenRules)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public event EventHandler ViewClosed;
        public event EventHandler<KA_Controller.EventArgs.ActionEventArgs> ActionInvoked;

        #endregion

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (ActionInvoked != null && listView1.SelectedItems.Count > 0 && listView1.SelectedItems[0].Tag is MemberView)
            {
                ActionInvoked(this, new ActionEventArgs(Enums.Actions.detail, Enums.Controller.MemberListController, ((MemberView)listView1.SelectedItems[0].Tag).Member));
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Enums.Actions action = Enums.Actions.refresh;
            string filter = string.Empty; ;
            switch (comboBox1.SelectedIndex)
            {
                case 0: action = Enums.Actions.refresh; filter = string.Empty; break;
                case 1: action = Enums.Actions.GetFilteredMembers; filter = "IsActive = True"; break;
                case 2: action = Enums.Actions.GetFilteredMembers; filter = "IsActive = False"; break;
                // case 3: action = Enums.Actions.GetFilteredMembers; filter = "licenseNumber <> 0 AND licenseNumber is not null"; break;
            }
            if (ActionInvoked != null)
            {
                ActionInvoked(this, new ActionEventArgs(action, Enums.Controller.MemberListController, filter));
            }
        }

        private void listView1_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            if (e.Item.SubItems[1] == e.SubItem)
            {

                e.DrawDefault = false;
                
                e.Graphics.DrawImage(((MemberView)e.Item.Tag).DegreeImage, e.SubItem.Bounds.Left, e.SubItem.Bounds.Top, 73, 24);
            }
            else 
            {
                e.DrawDefault = true;
            }
        }

        private void listView1_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            e.DrawDefault = true;
            e.DrawBackground();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
            {
                ((Form1)this.MdiParent).DisableAction("Details", true);
                ((Form1)this.MdiParent).DisableAction("Verwijderen", true);
            }
            else
            {
                ((Form1)this.MdiParent).DisableAction("Details", false);
                ((Form1)this.MdiParent).DisableAction("Verwijderen", false);
            }
        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            if (ActionInvoked != null && listView1.SelectedItems.Count > 0 && listView1.SelectedItems[0].Tag is MemberView)
            {
                ActionInvoked(this, new ActionEventArgs(Enums.Actions.detail, Enums.Controller.MemberListController, ((MemberView)listView1.SelectedItems[0].Tag).Member));
            }
        }

        private void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Determine if clicked column is already the column that is being sorted.
            if (e.Column == lviSorter.SortColumn)
            {
                // Reverse the current sort direction for this column.
                if (lviSorter.Order == SortOrder.Ascending)
                {
                    lviSorter.Order = SortOrder.Descending;
                }
                else
                {
                    lviSorter.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                lviSorter.SortColumn = e.Column;
                lviSorter.Order = SortOrder.Ascending;
            }

            // Perform the sort with these new sort options.
            listView1.Sort();
        }

        #region IMemberListViewBase Members


        public string GetSelectedFilter()
        {
            switch (comboBox1.SelectedIndex)
            {
                case 0: return string.Empty; 
                case 1: return "IsActive = True"; 
                case 2: return "IsActive = False"; 
                // case 3: action = Enums.Actions.GetFilteredMembers; filter = "licenseNumber <> 0 AND licenseNumber is not null"; break;
                default: return string.Empty;
            }
        }

        #endregion
    }
}