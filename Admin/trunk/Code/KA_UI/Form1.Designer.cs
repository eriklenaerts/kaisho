namespace KA
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            OutlookStyleControls.OutlookBarButton outlookBarButton1 = new OutlookStyleControls.OutlookBarButton();
            OutlookStyleControls.OutlookBarButton outlookBarButton2 = new OutlookStyleControls.OutlookBarButton();
            OutlookStyleControls.OutlookBarButton outlookBarButton3 = new OutlookStyleControls.OutlookBarButton();
            OutlookStyleControls.OutlookBarButton outlookBarButton4 = new OutlookStyleControls.OutlookBarButton();
            OutlookStyleControls.OutlookBarButton outlookBarButton5 = new OutlookStyleControls.OutlookBarButton();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.optionsMenu1 = new KA.CustomControls.OptionsMenu();
            this.outlookBar1 = new OutlookStyleControls.OutlookBar();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.kaishoAdminToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sluitenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.venstersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uitlijnenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.horizontaalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verticaalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cascadeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.messageCenter1 = new KA.CustomControls.MessageCenter();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(106)))), ((int)(((byte)(197)))));
            this.panel1.Controls.Add(this.optionsMenu1);
            this.panel1.Controls.Add(this.outlookBar1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5);
            this.panel1.Size = new System.Drawing.Size(190, 657);
            this.panel1.TabIndex = 1;
            // 
            // optionsMenu1
            // 
            this.optionsMenu1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(106)))), ((int)(((byte)(197)))));
            this.optionsMenu1.Dock = System.Windows.Forms.DockStyle.Top;
            this.optionsMenu1.Location = new System.Drawing.Point(5, 219);
            this.optionsMenu1.Name = "optionsMenu1";
            this.optionsMenu1.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.optionsMenu1.Size = new System.Drawing.Size(180, 392);
            this.optionsMenu1.TabIndex = 1;
            // 
            // outlookBar1
            // 
            this.outlookBar1.BackColor = System.Drawing.SystemColors.Highlight;
            this.outlookBar1.ButtonHeight = 40;
            outlookBarButton1.Enabled = true;
            outlookBarButton1.Image = global::KA.Properties.Resources.id_cards;
            outlookBarButton1.Tag = null;
            outlookBarButton1.Text = "Ledenlijst";
            outlookBarButton2.Enabled = true;
            outlookBarButton2.Image = global::KA.Properties.Resources.users_family;
            outlookBarButton2.Tag = null;
            outlookBarButton2.Text = "Familiebeheer";
            outlookBarButton3.Enabled = true;
            outlookBarButton3.Image = global::KA.Properties.Resources.graduation_hat1;
            outlookBarButton3.Tag = null;
            outlookBarButton3.Text = "Examens";
            outlookBarButton4.Enabled = true;
            outlookBarButton4.Image = global::KA.Properties.Resources.column_chart;
            outlookBarButton4.Tag = null;
            outlookBarButton4.Text = "Rapporten";
            outlookBarButton5.Enabled = true;
            outlookBarButton5.Image = global::KA.Properties.Resources.exit;
            outlookBarButton5.Tag = null;
            outlookBarButton5.Text = "Sluiten";
            this.outlookBar1.Buttons.Add(outlookBarButton1);
            this.outlookBar1.Buttons.Add(outlookBarButton2);
            this.outlookBar1.Buttons.Add(outlookBarButton3);
            this.outlookBar1.Buttons.Add(outlookBarButton4);
            this.outlookBar1.Buttons.Add(outlookBarButton5);
            this.outlookBar1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.outlookBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.outlookBar1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.outlookBar1.GradientButtonHoverDark = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(192)))), ((int)(((byte)(91)))));
            this.outlookBar1.GradientButtonHoverLight = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(220)))));
            this.outlookBar1.GradientButtonNormalDark = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(148)))), ((int)(((byte)(178)))));
            this.outlookBar1.GradientButtonNormalLight = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(225)))), ((int)(((byte)(252)))));
            this.outlookBar1.GradientButtonSelectedDark = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(150)))), ((int)(((byte)(21)))));
            this.outlookBar1.GradientButtonSelectedLight = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(230)))), ((int)(((byte)(148)))));
            this.outlookBar1.Location = new System.Drawing.Point(5, 5);
            this.outlookBar1.Name = "outlookBar1";
            this.outlookBar1.SelectedButton = null;
            this.outlookBar1.Size = new System.Drawing.Size(180, 214);
            this.outlookBar1.TabIndex = 0;
            this.outlookBar1.Click += new OutlookStyleControls.OutlookBar.ButtonClickEventHandler(this.outlookBar1_Click);
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(114)))), ((int)(((byte)(148)))));
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(190, 680);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(1049, 1);
            this.splitter1.TabIndex = 5;
            this.splitter1.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kaishoAdminToolStripMenuItem,
            this.venstersToolStripMenuItem});
            this.menuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.MdiWindowListItem = this.venstersToolStripMenuItem;
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1239, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // kaishoAdminToolStripMenuItem
            // 
            this.kaishoAdminToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sluitenToolStripMenuItem,
            this.optiesToolStripMenuItem});
            this.kaishoAdminToolStripMenuItem.Name = "kaishoAdminToolStripMenuItem";
            this.kaishoAdminToolStripMenuItem.Size = new System.Drawing.Size(82, 20);
            this.kaishoAdminToolStripMenuItem.Text = "Kaisho Admin";
            // 
            // sluitenToolStripMenuItem
            // 
            this.sluitenToolStripMenuItem.Image = global::KA.Properties.Resources.exit;
            this.sluitenToolStripMenuItem.Name = "sluitenToolStripMenuItem";
            this.sluitenToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.sluitenToolStripMenuItem.Text = "Sluiten";
            // 
            // optiesToolStripMenuItem
            // 
            this.optiesToolStripMenuItem.Image = global::KA.Properties.Resources.options;
            this.optiesToolStripMenuItem.Name = "optiesToolStripMenuItem";
            this.optiesToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.optiesToolStripMenuItem.Text = "Opties";
            this.optiesToolStripMenuItem.Click += new System.EventHandler(this.optiesToolStripMenuItem_Click);
            // 
            // venstersToolStripMenuItem
            // 
            this.venstersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.uitlijnenToolStripMenuItem});
            this.venstersToolStripMenuItem.Name = "venstersToolStripMenuItem";
            this.venstersToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.venstersToolStripMenuItem.Text = "Vensters";
            // 
            // uitlijnenToolStripMenuItem
            // 
            this.uitlijnenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.horizontaalToolStripMenuItem,
            this.verticaalToolStripMenuItem,
            this.cascadeToolStripMenuItem});
            this.uitlijnenToolStripMenuItem.Name = "uitlijnenToolStripMenuItem";
            this.uitlijnenToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.uitlijnenToolStripMenuItem.Text = "Uitlijnen";
            // 
            // horizontaalToolStripMenuItem
            // 
            this.horizontaalToolStripMenuItem.Name = "horizontaalToolStripMenuItem";
            this.horizontaalToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.horizontaalToolStripMenuItem.Text = "Horizontaal";
            this.horizontaalToolStripMenuItem.Click += new System.EventHandler(this.horizontaalToolStripMenuItem_Click);
            // 
            // verticaalToolStripMenuItem
            // 
            this.verticaalToolStripMenuItem.Name = "verticaalToolStripMenuItem";
            this.verticaalToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.verticaalToolStripMenuItem.Text = "Verticaal";
            this.verticaalToolStripMenuItem.Click += new System.EventHandler(this.verticaalToolStripMenuItem_Click);
            // 
            // cascadeToolStripMenuItem
            // 
            this.cascadeToolStripMenuItem.Name = "cascadeToolStripMenuItem";
            this.cascadeToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.cascadeToolStripMenuItem.Text = "Cascade";
            this.cascadeToolStripMenuItem.Click += new System.EventHandler(this.cascadeToolStripMenuItem_Click);
            // 
            // messageCenter1
            // 
            this.messageCenter1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(106)))), ((int)(((byte)(197)))));
            this.messageCenter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.messageCenter1.Location = new System.Drawing.Point(190, 681);
            this.messageCenter1.Name = "messageCenter1";
            this.messageCenter1.Size = new System.Drawing.Size(1049, 0);
            this.messageCenter1.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1239, 681);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.messageCenter1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Kaisho Admin";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private OutlookStyleControls.OutlookBar outlookBar1;
        private KA.CustomControls.MessageCenter messageCenter1;
        private System.Windows.Forms.Splitter splitter1;
        private KA.CustomControls.OptionsMenu optionsMenu1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem kaishoAdminToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sluitenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem venstersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uitlijnenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem horizontaalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verticaalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cascadeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optiesToolStripMenuItem;
    }
}

