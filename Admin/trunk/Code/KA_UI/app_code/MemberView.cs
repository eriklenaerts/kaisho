using System;
using System.Collections.Generic;
using System.Text;
using BL = KA.BusinessLogicLayer;
using System.Drawing;
using System.IO;
using KA.Properties;

namespace KA.app_code
{
    public class MemberView
    {
        public MemberView(BL.Member member)
        {
            _member = member;
        }

        private BL.Member _member;

        public BL.Member Member
        {
            get { return _member; }
            set { _member = value; }
        }

        public string FirstName
        {
            get { return _member.Firstname; }
        }

        public string LastName
        {
            get { return _member.Lastname; }
        }

        public string FullName
        {
            get { return _member.Lastname + " " + _member.Firstname; }
        }

        public string Street
        {
            get { return _member.Address; }
        }

        public string City
        {
            get { return _member.City; }
        }

        public string ZipCode
        {
            get { return _member.ZipCode; }
        }

        public string Country
        {
            get { return _member.Country; }
        }

        public string Telephone
        {
            get { return _member.Telephone; }
        }

        public string Active
        {
            get { return _member.IsActive ? "ja" : "nee"; }
        }

        public string DegreeName
        {
            get
            {
                if (_member.ExaminationCollection.Count > 0)
                {
                    _member.ExaminationCollection.Sort(Sort);
                    if (_member.ExaminationCollection[0].DegreeIDSource != null)
                    {
                        return _member.ExaminationCollection[0].DegreeIDSource.Title;
                    }
                }
                else
                    return "";
                
                return null;
            }
        }

        private Image _degreeImage;
        public Image DegreeImage
        {
            get
            {
                if (_degreeImage == null)
                {
                    if (_member.ExaminationCollection.Count > 0)
                    {
                        _member.ExaminationCollection.Sort(Sort);
                        if (_member.ExaminationCollection[0].DegreeIDSource.Picture != null)
                        {
                            MemoryStream stream = new MemoryStream(_member.ExaminationCollection[0].DegreeIDSource.Picture);
                            _degreeImage = Image.FromStream(stream);
                            return _degreeImage;
                        }
                    }
                    else
                        return Resources.undefined;
                }
                else
                    return _degreeImage;
                return null;
            }
        }


        private int Sort(BL.Examination m1, BL.Examination m2)
        {
            return m2.DegreeIDSource.Rank.CompareTo(m1.DegreeIDSource.Rank);
        }

    }
}
