using System;
using System.Collections.Generic;
using System.Text;
using Host.Types;

namespace KA.app_code
{
    public class StatisticView
    {
        public StatisticView(AvailablePlugin plugin)
        {
            _plugin = plugin;
        }

        private AvailablePlugin _plugin;

        public AvailablePlugin Plugin
        {
            get { return _plugin; }
            set { _plugin = value; }
        }

        public string PluginName
        {
            get { return _plugin.Instance.PluginName; }
        }

        public string Description
        {
            get { return _plugin.Instance.Description; }
        }

        public string Version
        {
            get { return _plugin.Instance.Version; }
        }
	
	
	
    }
}
