using System;
using System.Collections.Generic;
using System.Text;

namespace KA.app_code.EventArgs
{
    public class ActionEventArgs : System.EventArgs
    {
        public ActionEventArgs(Action action)
        {
            _action = action;
        }
        private Action _action;

        public Action Action
        {
            get { return _action; }
            set { _action = value; }
        }
	
    }
}
