﻿using System;
using System.Collections.Generic;
using System.Text;
using BL = KA.BusinessLogicLayer;
using SL = KA.ServiceLayer;
using KA.Properties;
using System.Globalization;

namespace KA.app_code
{
    public class TrainingView
    {
        public TrainingView(BL.TrainingTime trainingTime)
        {
            _trainingTime = trainingTime;
        }

        private BL.TrainingTime _trainingTime;

        public BL.TrainingTime TrainingTime
        {
            get { return _trainingTime; }
        }

        public string TrainingSeason
        {
            get 
            { return _trainingTime.TrainingPeriodIDSource.Name; }
        }

        public string Location
        {
            get { return _trainingTime.LocationIDSource.Name; }
        }

        public string DayOfWeek
        {
            get
            {
                CultureInfo dutch = CultureInfo.GetCultureInfo("nl-BE");
                return dutch.DateTimeFormat.DayNames[_trainingTime.DayOfTheWeek];
            }
        }

        public string StartOfTraining
        {
            get { return _trainingTime.StartOfTraining; }
        }

        public string EndOfTraining
        {
            get { return _trainingTime.EndOfTraining; }
        }

        public string Audience
        {
            get { return _trainingTime.Audience; }
        }
    }
}
