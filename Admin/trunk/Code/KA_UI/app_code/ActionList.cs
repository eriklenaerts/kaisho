using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace KA.app_code
{
    public class ActionList<T> : List<T> where T : Action
    {
        private ImageList _imageList;
        public ImageList ImageList
        {
            get { return _imageList; }
            set { _imageList = value; }
        }
	
    }
}
