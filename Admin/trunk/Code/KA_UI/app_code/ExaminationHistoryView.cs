﻿using System;
using System.Collections.Generic;
using System.Text;
using bl = KA.BusinessLogicLayer;
using System.IO;
using System.Drawing;

namespace KA.app_code
{
    public class ExaminationHistoryView
    {
        public bl.Examination _ex;

        public Image Picture 
        { 
            get
            {
                MemoryStream stream = new MemoryStream(_ex.DegreeIDSource.Picture);
                return Image.FromStream(stream);     
            }
        }

        public string Degree
        {
            get 
            {
                return _ex.DegreeIDSource.Title;
            }
        }

        public string Date
        {
            get
            {
                return _ex.Date.Value.ToString("dd/MM/yyyy");
            }
        }

        public string Location
        {
            get
            {
                return _ex.LocationIDSource.Name;
            }
        }

        public ExaminationHistoryView(bl.Examination ex)
        {
            _ex = ex;
        }
    }
}
