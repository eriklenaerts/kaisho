using System;
using System.Collections.Generic;
using System.Text;

using BL = KA.BusinessLogicLayer;
using System.Drawing;
using System.IO;
using KA.BusinessLogicLayer;

namespace KA.app_code
{
    public class GroupView
    {
        private BL.Group _group;

        public BL.Group Group
        {
            get { return _group; }
            set { _group = value; }
        }

        //ctor
        public GroupView (BL.Group group)
	    {
            _group = group;
	    }

        public string Name
        {
            get { return _group.Name; }
        }

        public int ID
        {
            get { return _group.ID; }
        }

        public string Members
        {
            get { return MemberCollectionToString(_group.MemberCollection); }
        }

        private string MemberCollectionToString(TList<Member> memberCollection)
        {
            StringBuilder sbMembers = new StringBuilder();
            foreach (Member member in memberCollection)
            {
                sbMembers.Append(member.Firstname);
                sbMembers.Append(", ");
            }
            if (sbMembers.Length > 0)
                return sbMembers.ToString().Remove(sbMembers.Length - 2, 2);
            else
                return string.Empty;
        }
    }
}
