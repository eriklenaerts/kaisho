using System;
using System.Collections.Generic;
using System.Text;
using KA_Common;
using System.Windows.Forms;
using KA_Domain;

namespace KA.app_code
{
    public class Action : TreeNode
    {

        public Action(string screenName, int imageIndex, Enums.Actions action, Enums.Controller controller, DomainBase context)
        {
            Text = screenName;
            ImageIndex = imageIndex;
            SelectedImageIndex = imageIndex;
            _actionName = action;
            _context = context;
            _controller = controller;
        }

        private Enums.Actions _actionName;
        public Enums.Actions ActionName
        {
            get { return _actionName; }
            set { _actionName = value; }
        }

        private Enums.Controller _controller;
        public Enums.Controller Controller
        {
            get { return _controller; }
            set { _controller = value; }
        }

        private DomainBase _context;
        public DomainBase Context
        {
            get { return _context; }
            set { _context = value; }
        }
	
    }
}
