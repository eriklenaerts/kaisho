﻿using System;
using System.IO;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.ServiceProcess;
using Microsoft.Win32;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;

namespace KA
{
    internal class DataStorageHalper
    {
        private string _problemDescription;
        private readonly string _connectionString;

        internal DataStorageHalper()
        {
            ConnectionStringSettings cnSettings = ConfigurationManager.ConnectionStrings["netTiersConnectionString"];
            if (cnSettings != null)
                _connectionString = cnSettings.ConnectionString;
            else
            {
                _problemDescription = "Kijk de configuratie na en verifier dat er een ConnectionString 'netTiersConnectionString' bestaat.";
            }
        }

        internal bool IsDataStorageReady()
        {
            bool result = false;
            string databaseFilePath = GetDatabaseFilePathFromConnectionstring(_connectionString);

            if (File.Exists(databaseFilePath))
            {
                SqlConnection conn = new SqlConnection(_connectionString + ";Connect Timeout=15");
                try
                {
                    conn.Open();
                    if (conn.State == System.Data.ConnectionState.Open)
                    {
                        conn.Close();
                        SqlConnection.ClearPool(conn);  // avoid the mdf to get locked for later backup 
                        result = true;
                    }
                }
                catch
                {
                    _problemDescription = "De database voor Kaisho admin kan niet worden gevonden, contacteer uw administrator om dit probleem op te lossen";
                }
            }
            else
            {
                _problemDescription = "Pad van de databank in de configuratie is niet juist. (" + databaseFilePath + ")";
            }

            return result;
        }

        internal string GetProblemDescription()
        {
            return _problemDescription;
        }

        internal void BackupDatabase(string backupFolder)
        {
            SqlConnection conn = new SqlConnection(_connectionString);
            try
            {
                conn.Open();
                if (conn.State == System.Data.ConnectionState.Open)
                {
                    ServerConnection serverConnection = new ServerConnection(conn);
                    Server server = new Server(serverConnection);

                    if (!Directory.Exists(backupFolder))
                        Directory.CreateDirectory(backupFolder);
                    string backupFilePath = Path.Combine(backupFolder, "KaishoAdmin_" + DateTime.Now.ToString("dd_MM_yyyy")) + ".backup";

                    Backup backup = new Backup();
                    backup.Devices.Add(new BackupDeviceItem(backupFilePath, DeviceType.File));
                    backup.Database = conn.Database;
                    backup.Action = BackupActionType.Database;
                    backup.BackupSetDescription = "Backup Kaisho Admin";
                    backup.BackupSetName = "Backup Kaisho Admin Set";
                    backup.LogTruncation = BackupTruncateLogType.Truncate;
                    backup.Incremental = false;
                    backup.SqlBackup(server);

                    SetLastBackupDate(DateTime.Now);
                    SetLastBackupLocation(new FileInfo(backupFilePath).Directory.FullName);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("De backup is niet gelukt vanweg de volgende fout:" + ex.Message, ex);
            }
            finally
            {
                if (conn.State == System.Data.ConnectionState.Open)
                    conn.Close();

            }
        }


        private static string GetDatabaseFilePathFromConnectionstring(string connectionString)
        {
            Regex regex = new Regex("AttachDbFilename=(?<dbFilename>[^;]+);");
            string dbFilePath = regex.Match(connectionString).Groups["dbFilename"].Value;
            return dbFilePath;
        }

        internal BackupProposal GetBackupProposal()
        {
            DateTime lastBackupDate = GetLastBackupDate();
            string lastBackupLocation = GetLastBackupLocation();
            if (lastBackupDate.AddMonths(+1) < DateTime.Now)
                return new BackupProposal(GetDatabaseFilePathFromConnectionstring(_connectionString), lastBackupDate, lastBackupLocation);

            return null;
        }

        private static string GetLastBackupLocation()
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\Kaisho Admin", true);
            if (key == null)
                key = Registry.CurrentUser.CreateSubKey(@"Software\Kaisho Admin");

            if (key != null)
            {
                if (key.GetValue("LastBackupLocation") == null)
                    key.SetValue("LastBackupLocation", string.Empty, RegistryValueKind.String);
                else
                    return key.GetValue("LastBackupLocation").ToString();
            }

            return string.Empty;
        }

        private static void SetLastBackupLocation(string lastBackupLocation)
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\Kaisho Admin", true);
            if (key == null)
                key = Registry.CurrentUser.CreateSubKey(@"Software\Kaisho Admin");

            if (key != null)
            {
                key.SetValue("LastBackupLocation", lastBackupLocation, RegistryValueKind.String);
            }
        }

        private static DateTime GetLastBackupDate()
        {
            DateTime lastBackupDate = DateTime.MinValue;

            RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\Kaisho Admin", true);
            if (key == null)
                key = Registry.CurrentUser.CreateSubKey(@"Software\Kaisho Admin");

            if (key != null)
            {
                if (key.GetValue("LastBackupDate") == null)
                {
                    // When no key is found, no backup has been taken yet, and therefore we set the min 
                    // value to trigger an initial backup
                    key.SetValue("LastBackupDate", DateTime.MinValue, RegistryValueKind.String);
                }
                else
                {
                    string lastBackupDateString = key.GetValue("LastBackupDate").ToString();
                    if (!string.IsNullOrEmpty(lastBackupDateString))
                        DateTime.TryParse(lastBackupDateString, out lastBackupDate);
                }
            }

            return lastBackupDate;
        }

        private static void SetLastBackupDate(DateTime lastBackupDate)
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\Kaisho Admin", true);
            if (key == null)
                key = Registry.CurrentUser.CreateSubKey(@"Software\Kaisho Admin");

            if (key != null)
            {
                key.SetValue("LastBackupDate", lastBackupDate, RegistryValueKind.String);
            }
        }
    }
}
