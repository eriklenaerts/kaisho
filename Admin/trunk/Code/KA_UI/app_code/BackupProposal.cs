﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KA
{
    public class BackupProposal
    {
        public string DataStorageFilename { get; private set; }
        public DateTime LastBackupDate { get; private set; }
        public string LastBackupLocation { get; private set; }


        public BackupProposal(string dataStorageFilename, DateTime lastBackupDate, string lastBackupLocation)
        {
            DataStorageFilename = dataStorageFilename;
            LastBackupDate = lastBackupDate;
            LastBackupLocation = lastBackupLocation;
        }

    }
}
