using System;
using System.Collections.Generic;
using System.Text;
using KA.CustomControls;
using KA.BusinessLogicLayer.Validation;

namespace KA
{
    class MessageCenterController
    {
        public static MessageCenter _messageCenter;

        public static void ShowMessageCenter(List<MessageCenterMessage> messages)
        {
            _messageCenter.SetMessage(messages);
            _messageCenter.Height = 200;
        }

        public static void ShowMessageCenter(MessageCenterMessage message)
        {
            _messageCenter.SetMessage(message);
            _messageCenter.Height = 200;
        }

        public static void ShowMessageCenter(BrokenRulesList brokenRules)
        {
            List<MessageCenterMessage> messages = new List<MessageCenterMessage>();
            foreach (BrokenRule rule in brokenRules)
            {
                MessageCenterMessage message = new MessageCenterMessage("Validation", rule.Description);
                messages.Add(message);
            }
            _messageCenter.SetMessage(messages);
            _messageCenter.Height = 200;
        }

        public static void HideMessageCenter()
        {
            _messageCenter.Height = 0;
        }

        public static void SetMessageCenter(MessageCenter messageCenter)
        {
            _messageCenter = messageCenter;
        }
    }
}
