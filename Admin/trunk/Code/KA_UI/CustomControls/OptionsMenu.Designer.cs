namespace KA.CustomControls
{
    partial class OptionsMenu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tvActions = new System.Windows.Forms.TreeView();
            this.labelEx1 = new KA.CustomControls.Extensions.LabelEx();
            this.SuspendLayout();
            // 
            // tvActions
            // 
            this.tvActions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(199)))), ((int)(((byte)(216)))));
            this.tvActions.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tvActions.Dock = System.Windows.Forms.DockStyle.Top;
            this.tvActions.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvActions.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(113)))), ((int)(((byte)(147)))));
            this.tvActions.FullRowSelect = true;
            this.tvActions.HideSelection = false;
            this.tvActions.Location = new System.Drawing.Point(0, 40);
            this.tvActions.Name = "tvActions";
            this.tvActions.Size = new System.Drawing.Size(187, 117);
            this.tvActions.TabIndex = 10;
            this.tvActions.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvActions_NodeMouseClick);
            // 
            // labelEx1
            // 
            this.labelEx1.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEx1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEx1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelEx1.HighColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(230)))), ((int)(((byte)(148)))));
            this.labelEx1.Location = new System.Drawing.Point(0, 10);
            this.labelEx1.LowColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(150)))), ((int)(((byte)(21)))));
            this.labelEx1.Name = "labelEx1";
            this.labelEx1.Size = new System.Drawing.Size(187, 30);
            this.labelEx1.TabIndex = 7;
            this.labelEx1.Text = "Opties";
            this.labelEx1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // OptionsMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tvActions);
            this.Controls.Add(this.labelEx1);
            this.Name = "OptionsMenu";
            this.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.Size = new System.Drawing.Size(187, 157);
            this.Load += new System.EventHandler(this.OptionsMenu_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private KA.CustomControls.Extensions.LabelEx labelEx1;
        private System.Windows.Forms.TreeView tvActions;

    }
}
