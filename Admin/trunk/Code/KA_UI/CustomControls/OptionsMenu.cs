using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using KA.Forms;
using KA.app_code;
using KA.app_code.EventArgs;

namespace KA.CustomControls
{
    //public delegate void ActionClick(object sender, Action action);



    public partial class OptionsMenu : UserControl
    {
        //public event ActionClick ActionClicked;

        public event EventHandler<ActionEventArgs> ActionClicked;

        public OptionsMenu()
        {
            InitializeComponent();
        }

        public void SetActions(ActionList<Action> actionList)
        {
            tvActions.Nodes.Clear();
            tvActions.ImageList = actionList.ImageList;
            foreach (Action action in actionList)
	        {
		        tvActions.Nodes.Add(action);
	        }       
        }

        private void tvActions_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Tag == null)
            {
                if (ActionClicked != null)
                {
                    ActionClicked(this, new ActionEventArgs(((Action)e.Node)));
                }
            }
        }

        private void OptionsMenu_Load(object sender, EventArgs e)
        {
            tvActions.Nodes.Clear();
            tvActions.Nodes.Add("geen opties");
        }

        internal void DisableAction(string action, bool disable)
        {
            TreeNode searchedNode = null;
            foreach (TreeNode node in tvActions.Nodes)
	        {
		        if (node.Text == action)
                {
                    searchedNode = node;
                    if (disable)
                    {
                        searchedNode.ForeColor = Color.LightGray;
                        searchedNode.Tag = "disabled";
                        break;
                    }
                    else 
                    {
                        searchedNode.ForeColor = Color.FromArgb(87, 113, 147);
                        searchedNode.Tag = null;
                        break;
                    }
                }
	        }
        }

       
    }
}
