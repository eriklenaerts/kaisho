using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace KA.CustomControls
{
    public partial class MessageCenter : UserControl
    {
        public MessageCenter()
        {
            InitializeComponent();
        }

        private void buttonEx1_Click(object sender, EventArgs e)
        {
            this.Height = 0;
        }

        public void SetMessage(MessageCenterMessage message)
        {
            List<MessageCenterMessage> list = new List<MessageCenterMessage>();
            list.Add(message);
            SetMessage(list);
        }

        public void SetMessage(List<MessageCenterMessage> messages)
        {
            dataGridView1.DataSource = messages;
        }
    }
}
