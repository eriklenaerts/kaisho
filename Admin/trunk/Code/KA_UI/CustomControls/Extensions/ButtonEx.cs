using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;

namespace KA.CustomControls.Extensions
{
    class ButtonEx : Button
    {
        public ButtonEx()
        {
            this.BackColor = System.Drawing.Color.Transparent;
            this.Font = new Font("Arial", 6, FontStyle.Bold, GraphicsUnit.Point);
            this.FlatStyle = FlatStyle.Flat;
        }

        #region Variables
        private Color _HighColor;
        private Color _LowColor;
        #endregion

        #region Properties

        [Category("MSP")]
        public Color HighColor
        {
            get { return _HighColor; }
            set
            {
                _HighColor = value;
                Invalidate();
            }
        }

        [Category("MSP")]
        public Color LowColor
        {
            get { return _LowColor; }
            set
            {
                _LowColor = value;
                Invalidate();
            }
        }

        #endregion
        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
           base.OnPaintBackground(pevent);

           Rectangle lRectangle = base.DisplayRectangle;
            Graphics lGraphics = pevent.Graphics;
            LinearGradientBrush lBrush = new LinearGradientBrush(pevent.ClipRectangle, _HighColor, _LowColor, LinearGradientMode.Vertical);

            lGraphics.FillRectangle(lBrush, pevent.ClipRectangle);
        }
    
    }
}
