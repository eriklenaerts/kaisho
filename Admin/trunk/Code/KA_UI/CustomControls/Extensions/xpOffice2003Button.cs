/*
 *
 * Author: Amit Bhandari
 *
 * Purpose: 
 *  Implementation of Office2003Button class
 * Version:
 *  1.0
 */

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace xpButton
{
	#region ColorScheme ENUM
	/// <summary>
	/// Type for handling the BackColorScheme
	/// </summary>
	public enum BackColorSchemeType
	{
		/// <summary>
		/// Office 2003 Blue Color
		/// </summary>
		Office2003Blue,
		/// <summary>
		/// Office 2003 Orange
		/// </summary>
		Office2003Orange,
		/// <summary>
		/// Office 2003 Silver
		/// </summary>
		Office2003Silver,
		/// <summary>
		/// Office 2003 Green
		/// </summary>
		Office2003Green
	}
	#endregion
	
	/// <summary>
	/// Implementation of Office2003Button control
	/// </summary>
	[ToolboxBitmap(@"d:\OfficeButton.bmp")]
	public class xpOffice2003Button : ButtonBase
	{
		#region Custom Variables
		private BackColorSchemeType _bkgColorScheme = BackColorSchemeType.Office2003Blue;
		private bool _shadow = false;
		
		private Color _downColor1;
		private Color _downColor2;
		private Color _hoverColor;
		private StringFormat _txtFormat;
		#endregion
			
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
				
		public xpOffice2003Button()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			SetStyle(ControlStyles.AllPaintingInWmPaint, true);
			SetStyle(ControlStyles.DoubleBuffer, true);
			SetStyle(ControlStyles.ResizeRedraw, true);
			SetStyle(ControlStyles.SupportsTransparentBackColor, false);
			
			ChangeColorScheme();
			
			_txtFormat = new StringFormat();
			_txtFormat.Alignment = StringAlignment.Center;
			_txtFormat.LineAlignment = StringAlignment.Center;
		}
		
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if( components != null )
					components.Dispose();
			}
			base.Dispose( disposing );
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// xpOffice2003Button
			// 
			this.Name = "xpOffice2003Button";
			this.Size = new System.Drawing.Size(150, 100);
		}
		#endregion
		
		#region Extended Properties	
		/// <summary>
		/// BackColorScheme: To use pre-defined color scheme.
		/// </summary>
		[Description("Color scheme to be used")]
		public BackColorSchemeType BackColorScheme
		{
			get{ return _bkgColorScheme; }
			set
			{
				if(value != _bkgColorScheme)
				{
					_bkgColorScheme = value;
					ChangeColorScheme();
					Invalidate();
				}
			}
		}
		
		/// <summary>
		/// Start color of background when the mouse button is down
		/// </summary>
		[Description("Starting color of Gradient when mouse button is down"),
		Category("Gradient Colors")]
		public Color DownColor1
		{
			get{ return _downColor1; }
			set
			{ 
				if( (_downColor1 != value) )
				{
					_downColor1 = value; 
					ChangeColorScheme();
					Invalidate();
				}
			}
		}
		
		/// <summary>
		/// Ending color of background when the mouse button is down
		/// </summary>
		[Description("Ending color of Gradient when mouse button is down"),
		Category("Gradient Colors")]
		public Color DownColor2
		{
			get{ return _downColor2; }
			set
			{ 
				if( (_downColor2 != value) )
				{
					_downColor2 = value; 
					ChangeColorScheme();
					Invalidate();
				}
			}
		}
		
		/// <summary>
		/// Backgroun color when the mouse button is hovered
		/// </summary>
		[Description("Hover color of button")]
		public Color HoverColor
		{
			get{ return _hoverColor; }
			set
			{ 
				if( (_hoverColor != value) )
				{
					_hoverColor = value; 
					ChangeColorScheme();
					Invalidate();
				}
			}
		}
		
		/// <summary>
		/// Drop shadow for the text.
		/// </summary>
		[Description("Show Shadow for the text")]
		public bool ShowShadow
		{
			get{ return _shadow; }
			set
			{
				_shadow = value;
				Invalidate();
			}
		}
		#endregion
		
		protected override void	PaintNormalState(Graphics g)
		{
			g.FillRectangle(new LinearGradientBrush(ClientRectangle,
				BackColor1, BackColor2, 90, false), ClientRectangle);
			g.DrawString(Text, Font, new SolidBrush(ForeColor), 
				ClientRectangle, _txtFormat);
			
			if( ShowShadow )
			{
				SolidBrush tb1 = new SolidBrush(ForeColor);
				g.DrawString(Text, Font, tb1, ClientRectangle, _txtFormat);
				tb1.Dispose();
			}
		}
		
		protected override void PaintHoverState(Graphics g)
		{
			g.FillRectangle(new SolidBrush(HoverColor), ClientRectangle);
			g.DrawRectangle(new Pen(Color.Gray, 0.8f), ClientRectangle);
			g.DrawString(Text, Font, new SolidBrush(ForeColor), 
				ClientRectangle, _txtFormat);
			
			if( ShowShadow )
			{
				SolidBrush tb1 = new SolidBrush(ForeColor);
				g.DrawString(Text, Font, tb1, ClientRectangle, _txtFormat);
				tb1.Dispose();
			}
		}
		
		protected override void PaintClickedState(Graphics g)
		{
			g.FillRectangle(new LinearGradientBrush(ClientRectangle,
				DownColor1, DownColor2, 90, false), ClientRectangle);
			g.DrawString(Text, Font, new SolidBrush(ForeColor), 
				ClientRectangle, _txtFormat);
			
			if( ShowShadow )
			{
				SolidBrush tb1 = new SolidBrush(ForeColor);
				g.DrawString(Text, Font, tb1, ClientRectangle, _txtFormat);
				tb1.Dispose();
			}
		}
		
		protected override void PaintDisabledState(Graphics g)
		{
			g.FillRectangle(new SolidBrush(Color.Gray), ClientRectangle);
			g.DrawString(Text, Font, new SolidBrush(Color.White), 
				ClientRectangle, _txtFormat);
			
			if( ShowShadow )
			{
				SolidBrush tb1 = new SolidBrush(ForeColor);
				g.DrawString(Text, Font, tb1, ClientRectangle, _txtFormat);
				tb1.Dispose();
			}
		}
		
		private void ChangeColorScheme()
		{
			switch(BackColorScheme)
			{
				case BackColorSchemeType.Office2003Blue:
					BackColor = Color.FromArgb(159, 191, 236);
					BackColor1 = Color.FromArgb(159, 191, 236);
					BackColor2 = Color.FromArgb(54, 102, 187);
					DownColor1 = Color.FromArgb(251, 230, 148);
					DownColor2 = Color.FromArgb(239, 150, 21);
					HoverColor = Color.FromArgb(255, 153, 0);
					break;
				case BackColorSchemeType.Office2003Orange:
					BackColor = Color.FromArgb(251, 230, 148);
					BackColor1 = Color.FromArgb(251, 230, 148);
					BackColor2 = Color.FromArgb(239, 150, 21);
					DownColor1 = Color.FromArgb(31, 143, 255);
					DownColor2 = Color.FromArgb(0, 105, 209);
					HoverColor = Color.FromArgb(255, 191, 128);
					break;
				case BackColorSchemeType.Office2003Silver:
					BackColor = Color.FromArgb(225, 226, 236);
					BackColor1 = Color.FromArgb(225, 226, 236);
					BackColor2 = Color.FromArgb(150, 148, 178);
					DownColor1 = Color.FromArgb(165, 165, 165);
					DownColor2 = Color.FromArgb(214, 214, 214);
					HoverColor = Color.FromArgb(180, 178, 199);
					break;
				case BackColorSchemeType.Office2003Green:
					BackColor = Color.FromArgb(234, 240, 207);
					BackColor1 = Color.FromArgb(234, 240, 207);
					BackColor2 = Color.FromArgb(178, 193, 140);
					DownColor1 = Color.FromArgb(143, 166, 94);
					DownColor2 = Color.FromArgb(102, 118, 65);
					HoverColor = Color.FromArgb(175, 192, 140);
					break;
			}
			
			Invalidate();
		}
	}
}
