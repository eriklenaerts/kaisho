using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;

namespace KA.CustomControls.Extensions
{
    public class LabelEx : Label
    {
        #region Variables
            private Color _HighColor;
            private Color _LowColor; 
        #endregion

        #region Properties

        [Category("MSP")]
        public Color HighColor
        {
            get { return _HighColor; }
            set
            {
                _HighColor = value;
                Invalidate();
            }
        }

        [Category("MSP")]
        public Color LowColor
        {
            get { return _LowColor; }
            set
            {
                _LowColor = value;
                Invalidate();
            }
        }
        
        #endregion

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            base.OnPaintBackground(pevent);

            Rectangle lRectangle = new Rectangle(0, 0, this.Width, this.Height);
            Graphics lGraphics = pevent.Graphics;
            LinearGradientBrush lBrush = new LinearGradientBrush(lRectangle, _HighColor, _LowColor, LinearGradientMode.Vertical);

            lGraphics.FillRectangle(lBrush, lRectangle);
            ControlPaint.DrawBorder3D(lGraphics, lRectangle, Border3DStyle.RaisedInner);
        }
    }
}
