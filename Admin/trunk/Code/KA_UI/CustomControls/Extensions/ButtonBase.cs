/*
 *
 * Author: Amit Bhandari
 *
 * Purpose: 
 *  Base class of Button control
 * Version:
 *  1.0
 */
 
using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Drawing;

namespace xpButton
{
	#region ButtonStates ENUM
	/// <summary>
	/// Enumeration of Button states
	/// </summary>
	public enum ButtonStates
	{
		/// <summary>
		/// Normal button state
		/// </summary>
		Normal,
		/// <summary>
		/// Mouse is hovered over button
		/// </summary>
		Hover,
		/// <summary>
		/// Button is clicked
		/// </summary>
		Pushed,
		/// <summary>
		/// The enabled property of button set to false
		/// </summary>
		Disabled
	}
	#endregion
	
	/// <summary>
	/// Button Base class implementation
	/// </summary>
	public abstract class ButtonBase : Control
	{
		#region Custom Variables
		protected abstract void PaintNormalState(Graphics g);
		protected abstract void PaintHoverState(Graphics g);
		protected abstract void PaintClickedState(Graphics g);
		protected abstract void PaintDisabledState(Graphics g);
		
		private ButtonStates _state = ButtonStates.Normal;
		private DialogResult _dlgResult = DialogResult.OK;
		
		private Color _bkgcolor1 = Color.FromArgb(214, 214, 214);
		private Color _bkgcolor2 = Color.FromArgb(165, 165, 165);
		#endregion
		
		#region Extended Properties
		///<summary>
		/// Start background color
		///</summary>
		[Description("Starting color of Gradient"),
		Category("Gradient Colors")]
		public Color BackColor1
		{
			get{ return _bkgcolor1; }
			set
			{ 
				if( (_bkgcolor1 != value) )
				{
					_bkgcolor1 = value; 
					BackColor = value;
					Invalidate();
				}
			}
		}
		
		///<summary>
		/// End background color
		///</summary>
		[Description("Ending color of Gradient"),
		Category("Gradient Colors")]
		public Color BackColor2
		{
			get{ return _bkgcolor2; }
			set
			{ 
				if( (_bkgcolor2 != value) )
				{
					_bkgcolor2 = value; 
					Invalidate();
				}
			}
		}
		
		protected ButtonStates StateOfButton
		{
			get{ return _state; }
			set
			{
				if(value != _state)
				{
					_state = value;
					Invalidate();
				}
			}
		}
		
		///<summary>
		/// DialogResult of the button
		///</summary>
		[Description("Result of dialog to be returned"),
		Category("Dialog Result")]
		public DialogResult DialogResult
		{
			get{ return _dlgResult; }
			set{ _dlgResult = value; }
		}
		
		public override string Text
		{
			get{ return base.Text; }
			set
			{
				if(value != base.Text)
				{
					base.Text = value;
					Invalidate();
				}
			}
		}
		
		#endregion
		
		#region Virtual Methods
		/// <summary>
		/// Over-ride this method, if a portion of the control
		/// is to made clickable.
		/// </summary>
		/// <param name="X">X co-ordinate</param>
		/// <param name="Y">Y co-ordinate</param>
		/// <returns></returns>
		protected virtual bool HitTest(int X, int Y)
		{
			return true;
		}

		protected virtual void PaintFocusRectangle(Graphics g)
		{
			ControlPaint.DrawFocusRectangle(g, this.ClientRectangle);
		}

		/// <summary>
		/// Initiates the window a message for custom
		/// rendering
		/// </summary>
		/// <param name="value">boolean value</param>
		public virtual void OnNotifyDefault(bool value)
		{
			// Override this method to perform any custom
			// rendering
		}
		
		/// <summary>
		/// Used to specify the WM_NOTIFY message in
		/// WndProc
		/// </summary>
		/// <param name="value">boolean value</param>
		public void NotifyDefault(bool value)
		{
			OnNotifyDefault(value);
		}
		#endregion
		
		#region Methods over-riden
		
		protected override void OnEnabledChanged(EventArgs e)
		{
			if(!Enabled)
				StateOfButton = ButtonStates.Disabled;
			else if(Enabled && StateOfButton == ButtonStates.Disabled)
				StateOfButton = ButtonStates.Normal;

			Invalidate();
			//base.OnEnabledChanged (e);
		}
		
		protected override void OnPaint(PaintEventArgs e)
		{			
			switch(StateOfButton)
			{
				case ButtonStates.Normal:
					PaintNormalState(e.Graphics);
					break;
				case ButtonStates.Hover:
					PaintHoverState(e.Graphics);
					break;
				case ButtonStates.Pushed:
					PaintClickedState(e.Graphics);
					break;
				case ButtonStates.Disabled:
					PaintDisabledState(e.Graphics);
					break;
			}

			if(this.Focused && StateOfButton != ButtonStates.Disabled)
				PaintFocusRectangle(e.Graphics);
		}
		
		protected override void OnClick(EventArgs e)
		{
			if(StateOfButton == ButtonStates.Pushed)
				base.OnClick(e);
		}
		
		protected override void OnMouseMove(MouseEventArgs e)
		{
			if(StateOfButton == ButtonStates.Disabled)
				return;

			if(HitTest(e.X, e.Y))
			{
				if(StateOfButton != ButtonStates.Pushed)
					StateOfButton = ButtonStates.Hover;
			}
			else
				StateOfButton = ButtonStates.Normal;
			
			Invalidate();
		}
		
		protected override void OnMouseLeave(EventArgs e)
		{
			if(StateOfButton == ButtonStates.Disabled)
				return;
			else
				StateOfButton = ButtonStates.Normal;
			
			Invalidate();
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			if(StateOfButton == ButtonStates.Disabled)
				return;
			else
				StateOfButton = ButtonStates.Normal;


			if(HitTest(e.X, e.Y))
			{
				if((e.Button & MouseButtons.Left) == MouseButtons.Left)
				{
					StateOfButton = ButtonStates.Pushed;
					Focus();
				}
			}
			
			Invalidate();
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			if(StateOfButton == ButtonStates.Disabled)
				return;
			else
				StateOfButton = ButtonStates.Normal;

			if((e.Button & MouseButtons.Left) == MouseButtons.Left)
			{
				if(HitTest(e.X, e.Y))
					StateOfButton = ButtonStates.Hover;
				else
					StateOfButton = ButtonStates.Normal;
			}
			
			Invalidate();
		}
		
		#endregion
	}
}
