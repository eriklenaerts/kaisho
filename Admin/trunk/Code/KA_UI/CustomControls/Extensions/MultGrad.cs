/*
 * Created by SharpDevelop.
 * User: Mark
 * Date: 12/12/2004
 * Time: 6:31 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
// This is my namespace of collections and contains the ColorCollection used in the multigradiant button
using MTControls.MTCollections;

namespace MTControls
{
	/// <summary>
	/// 	
	/// </summary>
	namespace MTButtons
	{
		/*
		 * MultiGraniantButton class
		 */
		public class MTMultiGradiantButton : System.Windows.Forms.Button
		{
			// Constructors
			// default constructor
			public MTMultiGradiantButton()
			{
				
			}
			
			// overridden constructor takes 2 colors
			public MTMultiGradiantButton(Color c1, Color c2)
			{
				m_clrColors.Add(c1);
				m_clrColors.Add(c2);
			}
			
			
			// encapsulated data
			private MTColorCollection m_clrColors = new MTColorCollection();
			private int m_nTransparency = 64;
			private float m_fAngle = 0.0f;
			
			
			
			// The properties
			// transparency value for the button gradiants
			public int Transparency
			{
				get
				{
					return m_nTransparency;
				}
				set
				{
					m_nTransparency = value;
					Invalidate();
				}
			}
			
			// angle for the gradiant from the x-axis
			public float Angle
			{
				get
				{
					return m_fAngle;
				}
				set
				{
					m_fAngle = value;
					Invalidate();
				}
			}
			
			// This is for the properties interface so you can add colors at design time
			// it tells the interface to use the follows MTColorColletion Colors property
			// as a template to create the interface for adding colors to the control
			[DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
			EditorAttribute("typeof(CollectionEditor)","typeof(System.Drawing.Design.UITypeEditor)")]
			public MTColorCollection Colors
			{
				get
				{
					return m_clrColors;
				}
				
			}
			
			// the overridden OnPaint method for the control
			protected override void OnPaint(PaintEventArgs pe)
			{
				base.OnPaint(pe);
				// We need atleast 2 colors for a gradiant so if 2 colors aren't defined just draw the
				// regular button
				if(m_clrColors.Count > 1)
				{
					// Make sure a get a good single percision percentage of the client rectangle
					float percentage = Convert.ToSingle(ClientRectangle.Width) * ( 1.0f / Convert.ToSingle(m_clrColors.Count - 1));
					Brush b;
					RectangleF rectF;
					Color c1;
					Color c2;
					// create the initial rectangle
					rectF = new RectangleF (new PointF (0.0f,0.0f), new SizeF (percentage, ClientRectangle.Height));
					for(int i=0; i<m_clrColors.Count-1; i++)
					{
						
						// create the colors
						c1 = Color.FromArgb(m_nTransparency, m_clrColors[i]);
						c2 = Color.FromArgb(m_nTransparency, m_clrColors[i+1]);
						// create the brush
						b = new System.Drawing.Drawing2D.LinearGradientBrush(rectF, c1, c2, m_fAngle);
						// fill the segment
						pe.Graphics.FillRectangle(b, rectF);
						// reset the rectangle
						rectF.Offset(percentage, 0);
						// dispose of the brush
						b.Dispose ();	
					}
				}
			}
		}
	}
}
