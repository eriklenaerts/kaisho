﻿using System;
using System.Collections.Generic;
using System.Text;
using BL = KA.BusinessLogicLayer;

namespace KA_Common
{
    public class AddHistoryEventArgs
    {
        public BL.Examination Exam { get; set; }
        public BL.Member Member { get; set; }
    }
}
