using System;
using System.Collections.Generic;
using System.Text;
using KA_Common;

namespace KA_Controller.EventArgs
{
    public class ActionEventArgs : System.EventArgs
    {
        public ActionEventArgs(Enums.Actions action, Enums.Controller controller, object context)
        {
            _context = context;
            _controller = controller;
            _action = action;
        }

        private Enums.Actions _action;
        public Enums.Actions Action
        {
            get { return _action; }
            set { _action = value; }
        }

        private Enums.Controller _controller;
        public Enums.Controller Controller
        {
            get { return _controller; }
            set { _controller = value; }
        }
	
        private object _context;
        public object Context
        {
            get { return _context; }
            set { _context = value; }
        }
    }
}
