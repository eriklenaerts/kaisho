﻿using System;
using System.Collections.Generic;
using System.Text;
using KA_Domain;
using SL = KA.ServiceLayer;
using BL = KA.BusinessLogicLayer;

namespace KA_Controller.Interfaces
{
    public interface ITrainingDaysListViewBase : IViewBase
    {
        void FillTrainingDaysGrid(BL.TList<BL.TrainingTime> TrainingTimeList);
        BL.TrainingTime GetSelectedTrainingTime();
    }
}
