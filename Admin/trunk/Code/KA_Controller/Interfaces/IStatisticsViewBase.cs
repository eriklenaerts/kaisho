using System;
using System.Collections.Generic;
using System.Text;
using KA_Domain;
using Host.Types;

namespace KA_Controller.Interfaces
{
    public interface IStatisticsViewBase : IListViewBase
    {
        void FillGrid(AvailablePlugins plugins);
        AvailablePlugin GetSelectedReport();
    }
}
