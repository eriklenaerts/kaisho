﻿using System;
using System.Collections.Generic;
using System.Text;
using BL = KA.BusinessLogicLayer;
using KA.BusinessLogicLayer;

namespace KA_Controller.Interfaces
{
    public interface ITrainingDaysDetailView : IViewBase
    {
        void SetContext(BL.TrainingTime trainingTime);
        void LoadData(TList<Location> locations, TList<TrainingPeriod> periods);
    }
}
