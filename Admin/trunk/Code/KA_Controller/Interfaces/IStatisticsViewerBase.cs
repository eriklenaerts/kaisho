using System;
using System.Collections.Generic;
using System.Text;
using Host.Types;

namespace KA_Controller.Interfaces
{
    public interface IStatisticsViewerBase : IViewBase
    {
        void LoadReport(AvailablePlugin selectedPlugin);
    }
}
