using System;
using System.Collections.Generic;
using System.Text;
using BL = KA.BusinessLogicLayer;

namespace KA_Controller.Interfaces
{
    public interface IFamilyDetailsView : IViewBase
    {
        void SetContext(BL.Group group);
    }
}
