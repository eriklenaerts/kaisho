using System;
using System.Collections.Generic;
using System.Text;
using KA_Domain;
using SL = KA.ServiceLayer;
using BL = KA.BusinessLogicLayer;

namespace KA_Controller.BaseClasses
{
    public interface IMemberListViewBase : IListViewBase
    {
        void FillGrid(BL.TList<BL.Member> MemberList);
        BL.Member GetSelectedMember();
        string GetSelectedFilter();
    }
}
