using System;
using System.Collections.Generic;
using System.Text;
using BL = KA.BusinessLogicLayer;

namespace KA_Controller.Interfaces
{
    public interface IFamilyListViewBase : IViewBase
    {
        void FillGrid(BL.TList<BL.Group> groupList);
        BL.Group GetSelectedGroup();
    }
}
