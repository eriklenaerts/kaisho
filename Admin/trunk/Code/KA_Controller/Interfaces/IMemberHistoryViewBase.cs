﻿using System;
using System.Collections.Generic;
using System.Text;
using BL = KA.BusinessLogicLayer;


namespace KA_Controller.Interfaces
{
    public interface IMemberHistoryViewBase : IViewBase
    {
        void SetContext(BL.TList<BL.Examination> examinations, BL.Member member);
    }
}
