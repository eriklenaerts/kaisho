﻿using System;
using System.Collections.Generic;
using System.Text;
using BL = KA.BusinessLogicLayer;

namespace KA_Controller.Interfaces
{
    public interface ITrainingDaysExceptionsListViewBase : IListViewBase
    {
        void FillTrainingDaysExceptionsGrid(BL.TList<BL.TrainingExceptions> TrainingDaysExceptionsList);
        BL.TrainingTime GetSelectedTrainingDaysException();
    }
}
