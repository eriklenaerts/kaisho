using System;
using System.Collections.Generic;
using System.Text;
using KA_Domain;
using BL = KA.BusinessLogicLayer;
using System.Drawing;
using KA.BusinessLogicLayer;

namespace KA_Controller.Interfaces
{
    public interface IMemberDetailsView : IViewBase
    {
        void SetContext(BL.Member member, Image degreeImage, TList<PaymentType> paymentTypes, TList<Location> locations);
        void RefreshDegree(Image degreePic);
    }
}
