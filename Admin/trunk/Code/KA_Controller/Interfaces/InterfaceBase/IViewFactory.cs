using System;
using System.Collections.Generic;
using System.Text;
using KA_Common;
using System.Windows.Forms;

namespace KA_Controller
{
    public interface IViewFactory
    {
        IViewBase CreateView(Enums.Controller controller);

    }
}
