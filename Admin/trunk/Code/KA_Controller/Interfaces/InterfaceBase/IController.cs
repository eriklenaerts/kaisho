using System;
using System.Collections.Generic;
using System.Text;
using KA_Common;
using KA_Domain;
using KA.BusinessLogicLayer;
using KA_Controller.EventArgs;

namespace KA_Controller
{
    //public delegate void ActionClick(Object sender, Enums.Actions action, Enums.Controller controller, object context);
    public interface IController
    {
        event EventHandler<ActionEventArgs> ActionClicked;
        //event ActionClick ActionClicked;
        void ExecuteAction(Enums.Actions Action, object context);

        void SetView(IViewBase view);
    }
}
