using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using KA_Domain;
using KA_Common;
using KA.BusinessLogicLayer;
using KA.BusinessLogicLayer.Validation;
using KA_Controller.EventArgs;

namespace KA_Controller
{
    public interface IViewBase
    {
        bool IsDisposed
        {
            get;
        }

        event EventHandler ViewClosed;
        event EventHandler<ActionEventArgs> ActionInvoked;
        void Show();
        void Close();
        DialogResult ShowDialog();
        void Init(Form mdiParent);
        void Activate();
        void ShowBrokenRules(BrokenRulesList brokenRules);
    }
}
