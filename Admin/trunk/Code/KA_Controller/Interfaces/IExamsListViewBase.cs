using System;
using System.Collections.Generic;
using System.Text;
using KA_Domain;
using BL = KA.BusinessLogicLayer;

namespace KA_Controller.Interfaces
{
    public interface IExamsListViewBase : IListViewBase
    {
        void FillGrid(BL.TList<BL.Member> MemberList);
        void FillLocations(BL.TList<BL.Location> LocationList);
    }
}
