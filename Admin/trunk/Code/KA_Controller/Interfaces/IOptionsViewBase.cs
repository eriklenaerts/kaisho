﻿using System;
using System.Collections.Generic;
using System.Text;
using KA.BusinessLogicLayer;

namespace KA_Controller.Interfaces
{
    public interface IOptionsViewBase : IViewBase
    {
        void FillTrainingDaysGrid(TList<TrainingTime> trainingTimeList);
        KA.BusinessLogicLayer.TrainingTime GetSelectedTrainingTime();

        void FillTrainingDaysExceptionsGrid(TList<TrainingExceptions> trainingExceptions);
        KA.BusinessLogicLayer.TrainingExceptions GetSelectedTrainingException();
    }
}
