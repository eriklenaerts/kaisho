﻿using System;
using System.Collections.Generic;
using System.Text;
using KA.BusinessLogicLayer;

namespace KA_Controller.Interfaces
{
    public interface ITrainingDaysExceptionsDetailView : IViewBase
    {
        void SetContext(TrainingExceptions trainingEx);
    }
}
