﻿using System;
using System.Collections.Generic;
using System.Text;
using BL = KA.BusinessLogicLayer;
using System.Drawing;
using KA.BusinessLogicLayer;

namespace KA_Controller.Interfaces
{
    public interface IAddHistoryViewBase : IListViewBase
    {
        void SetContext(BL.Member member);
    }
}
