using System;
using System.Collections.Generic;
using System.Text;

using KA_Controller.Interfaces;
using KA_Domain;
using BL = KA.BusinessLogicLayer;
using SL = KA.ServiceLayer;
using KA_Controller.EventArgs;

namespace KA_Controller.Controllers
{
    public class FamilyDetailController : IController
    {
        private IFamilyDetailsView _familyDetailsView;
        private SL.GroupService _groupService;

        public IFamilyDetailsView FamilyDetailsView
        {
            get { return _familyDetailsView; }
            set { _familyDetailsView = value;}
        }

        public FamilyDetailController(IFamilyDetailsView familyDetailsView)
        {
            SetView(familyDetailsView);
            _groupService = new KA.ServiceLayer.GroupService();
        }

        #region IController Members

        public event EventHandler<KA_Controller.EventArgs.ActionEventArgs> ActionClicked;

        public void ExecuteAction(KA_Common.Enums.Actions Action, object context)
        {
            switch (Action)
            {
                case KA_Common.Enums.Actions.showview:
                    _familyDetailsView.SetContext((BL.Group)context);
                    _familyDetailsView.ShowDialog();
                    break;
                case KA_Common.Enums.Actions.update:
                    ((BL.Group)context).Validate();
                    if (((BL.Group)context).BrokenRulesList.Count == 0)
                    {
                        _groupService.Save((BL.Group)context);
                        ActionClicked(this, new ActionEventArgs(KA_Common.Enums.Actions.refresh, KA_Common.Enums.Controller.FamilyListController, null));
                        _familyDetailsView.Close();
                    }
                    else
                        _familyDetailsView.ShowBrokenRules(((BL.Group)context).BrokenRulesList);
                    break;
                case KA_Common.Enums.Actions.refresh:
                    if (ActionClicked != null)
                        ActionClicked(this, new ActionEventArgs(KA_Common.Enums.Actions.refresh, KA_Common.Enums.Controller.FamilyListController, null));
                    break;
                default:
                    break;
            }
        }

        public void SetView(IViewBase view)
        {
            if (_familyDetailsView == null)
            {
                _familyDetailsView = (IFamilyDetailsView)view;
                _familyDetailsView.ViewClosed += new EventHandler(_familyDetailsView_ViewClosed);
                _familyDetailsView.ActionInvoked += new EventHandler<ActionEventArgs>(_familyDetailsView_ActionInvoked);
            }
        }

        void _familyDetailsView_ActionInvoked(object sender, ActionEventArgs e)
        {
            ExecuteAction(e.Action, e.Context);
        }

        void _familyDetailsView_ViewClosed(object sender, System.EventArgs e)
        {
            FamilyDetailsView = null;
        }
        #endregion
    }
}
