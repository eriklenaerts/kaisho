using System;
using System.Collections.Generic;
using System.Text;
using KA_Common;
using KA_Domain;
using KA_Controller.EventArgs;

namespace KA_Controller
{
    public class MainController
    {
        public MainController(IViewFactory viewFactory)
        {
            _ViewFactory = viewFactory;
            _ControllerCollection = new Dictionary<Enums.Controller, IController>();
        }

        private IViewFactory _ViewFactory;
        public IViewFactory ViewFactory
        {
            get { return _ViewFactory; }
            set { _ViewFactory = value; }
        }

        private Dictionary<Enums.Controller, IController> _ControllerCollection;
	    public Dictionary<Enums.Controller, IController> ControllerCollection
	    {
		    get { return _ControllerCollection;}
		    set { _ControllerCollection = value;}
	    }

        public void ExecuteAction(Enums.Controller controller, Enums.Actions Action, Object Context)
        {   
            IController controllerInstance;
            if (ControllerCollection.ContainsKey(controller))
            {
                ControllerCollection.TryGetValue(controller, out controllerInstance);
                if (controllerInstance != null)
                {
                    controllerInstance.SetView(ViewFactory.CreateView(controller));
                    controllerInstance.ExecuteAction(Action, Context);
                }
            }
            else
            {
                controllerInstance = ControllerFactory.CreateController(controller, ViewFactory.CreateView(controller));
                controllerInstance.ActionClicked += new EventHandler<ActionEventArgs>(controllerInstance_ActionClicked);
                ControllerCollection.Add(controller, controllerInstance);
                controllerInstance.ExecuteAction(Action, Context);
            }
        }

        void controllerInstance_ActionClicked(object sender, ActionEventArgs e)
        {
            ExecuteAction(e.Controller, e.Action, e.Context);
        }

        //void controllerInstance_ActionClicked(object sender, Enums.Actions action, Enums.Controller controller, Object context)
        //{
            
        //}
    }
}
