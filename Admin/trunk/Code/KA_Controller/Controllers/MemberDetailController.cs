using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using KA_Controller.Interfaces;
using KA_Domain;
using BL = KA.BusinessLogicLayer;
using SL = KA.ServiceLayer;
using KA_Controller.EventArgs;
using System.IO;
using System.Drawing;
using KA.ServiceLayer;

namespace KA_Controller
{
    public class MemberDetailController : IController
    {
        private IMemberDetailsView _MemberDetailView;
        private SL.MemberService _memberService;
        public IMemberDetailsView MemberDetailView
        {
            get { return _MemberDetailView; }
            set { _MemberDetailView = value;}
        }

        public MemberDetailController(IMemberDetailsView memberDetailView)
        {
            SetView(memberDetailView);
            _memberService = new KA.ServiceLayer.MemberService();
        }

        #region IController Members

        public event EventHandler<KA_Controller.EventArgs.ActionEventArgs> ActionClicked;

        public void ExecuteAction(KA_Common.Enums.Actions Action, object context)
        {
            switch (Action)
            {
                case KA_Common.Enums.Actions.showview:
                    PaymentTypeService paymentTypeService = new PaymentTypeService();
                    LocationService locationService = new LocationService();
                    _MemberDetailView.SetContext((BL.Member)context, GetDegreePicture((BL.Member)context), paymentTypeService.GetAll(), locationService.GetAll());
                    _MemberDetailView.ShowDialog();
                    //_MemberDetailView.Activate();
                    break;
                case KA_Common.Enums.Actions.update:
                    ((BL.Member)context).Validate();
                    if (((BL.Member)context).BrokenRulesList.Count == 0)
                    {
                        _memberService.Save((BL.Member)context);
                        ActionClicked(this, new ActionEventArgs(KA_Common.Enums.Actions.refresh, KA_Common.Enums.Controller.MemberListController, null));
                        _MemberDetailView.Close();
                    }
                    else
                        _MemberDetailView.ShowBrokenRules(((BL.Member)context).BrokenRulesList);
                  break;
                case KA_Common.Enums.Actions.refresh:
                    if (ActionClicked != null)
                        ActionClicked(this, new ActionEventArgs(KA_Common.Enums.Actions.refresh, KA_Common.Enums.Controller.MemberListController, null));
                    break;
                case KA_Common.Enums.Actions.ShowHistory:
                    if(ActionClicked != null)
                        ActionClicked(this, new ActionEventArgs(KA_Common.Enums.Actions.showview, KA_Common.Enums.Controller.MemberHistoryController, (BL.Member)context));
                    break;
                case KA_Common.Enums.Actions.RefreshDegree :
                    _MemberDetailView.RefreshDegree(GetDegreePicture((BL.Member)context));
                    break;
                default:
                    break;
            }
        }

        private Image GetDegreePicture(BL.Member member)
        {
            if (member != null)
            {
                Image _degreeImage = null;
                if (member.ExaminationCollection.Count > 0)
                {
                    member.ExaminationCollection.Sort(Sort);
                    if (member.ExaminationCollection[0].DegreeIDSource.Picture != null)
                    {
                        MemoryStream stream = new MemoryStream(member.ExaminationCollection[0].DegreeIDSource.Picture);
                        _degreeImage = Image.FromStream(stream);
                        return _degreeImage;
                    }
                }
                else
                    return null;
            }
            return null;
        }

        private int Sort(BL.Examination m1, BL.Examination m2)
        {
            return m2.DegreeIDSource.Rank.CompareTo(m1.DegreeIDSource.Rank);
        }

        #endregion

        #region IController Members


        public void SetView(IViewBase view)
        {
            if (_MemberDetailView == null)
            {
                _MemberDetailView = (IMemberDetailsView)view;
                _MemberDetailView.ViewClosed +=new EventHandler(_MemberDetailView_ViewClosed);
                _MemberDetailView.ActionInvoked += new EventHandler<ActionEventArgs>(_MemberDetailView_ActionInvoked);
            }
        }

        void _MemberDetailView_ActionInvoked(object sender, ActionEventArgs e)
        {
            ExecuteAction(e.Action, e.Context);
        }

        void _MemberDetailView_ViewClosed(object sender, System.EventArgs e)
        {
            MemberDetailView = null;
        }

        #endregion

    }
}
