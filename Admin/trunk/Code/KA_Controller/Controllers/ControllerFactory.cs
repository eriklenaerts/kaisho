using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using KA_Common;
using KA_Controller.BaseClasses;
using KA_Controller.Controllers;
using KA_Controller.Interfaces;

namespace KA_Controller
{
    public class ControllerFactory
    {
        public static IController CreateController(Enums.Controller controller, IViewBase view)
        {
            switch (controller)
            {
                case Enums.Controller.MemberListController:
                    return new MemberListController((IMemberListViewBase)view);
                case Enums.Controller.MemberDetailController:
                    return new MemberDetailController((IMemberDetailsView)view);
                case Enums.Controller.ExamListController :
                    return new ExamListController((IExamsListViewBase)view);
                case Enums.Controller.StatisticsController:
                    return new StatisticsController((IStatisticsViewBase)view);
                case Enums.Controller.StatisticsViewerController:
                    return new StatisticsViewerController((IStatisticsViewerBase)view);
                case Enums.Controller.FamilyListController:
                    return new FamilyListController((IFamilyListViewBase)view);
                case Enums.Controller.FamilyDetailController:
                    return new FamilyDetailController((IFamilyDetailsView)view);
                case Enums.Controller.MemberHistoryController:
                    return new MemberHistoryController((IMemberHistoryViewBase)view);
                case Enums.Controller.AddHistory:
                    return new AddHistoryController((IAddHistoryViewBase)view);
                case Enums.Controller.OptionsController:
                    return new OptionsController((IOptionsViewBase)view);
                case Enums.Controller.TrainingDaysDetailController:
                    return new TrainingDaysDetailController((ITrainingDaysDetailView)view);
                case Enums.Controller.TrainingDaysExceptionsDetailController:
                    return new TrainingDaysExceptionsDetailController((ITrainingDaysExceptionsDetailView)view);
                default:
                    break;
            }
            return null;
        }
    }
}
