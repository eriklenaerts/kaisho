using System;
using System.Collections.Generic;
using System.Text;
using KA_Controller.Interfaces;
using KA_Domain.DomainController;
using bl = KA.BusinessLogicLayer;
using SL = KA.ServiceLayer;

namespace KA_Controller.Controllers
{
    public class ExamListController : IController
    {
        private IExamsListViewBase _ExamsListView;
        private SL.MemberService _memberServcice = new KA.ServiceLayer.MemberService();
        private SL.ExaminationService _examinatonService = new KA.ServiceLayer.ExaminationService();
        private SL.DegreeService _degreeService = new KA.ServiceLayer.DegreeService();
        private SL.LocationService _locationService = new KA.ServiceLayer.LocationService();

        public IExamsListViewBase ExamsListView
        {
            get { return _ExamsListView; }
            set { _ExamsListView = value;
            
            }
        }

        public ExamListController(IExamsListViewBase examsListView)
        {
            SetView(examsListView);
        }
	

        #region IController Members

        public event EventHandler<KA_Controller.EventArgs.ActionEventArgs> ActionClicked;

        public void ExecuteAction(KA_Common.Enums.Actions Action, object context)
        {
            switch (Action)
            {
                case KA_Common.Enums.Actions.showview :
                    ExamsListView.FillGrid(GetMembersByDegree(1));
                    ExamsListView.FillLocations(_locationService.GetAll());
                    ExamsListView.Show();
                    ExamsListView.Activate();
                    break;
                case KA_Common.Enums.Actions.refresh:
                    ExamsListView.FillGrid(GetMembersByDegree((int)context));
                    break;
                case KA_Common.Enums.Actions.detail:
                    break;              
                case KA_Common.Enums.Actions.ExamList:
                    break;
                case KA_Common.Enums.Actions.Apply:
                        _examinatonService.Insert((bl.TList<bl.Examination>)context);
                    break;
                default:
                    break;
            }
        }
      
        public bl.TList<bl.Member> GetMembersByDegree(int degreeId)
        {
            bl.TList<bl.Member> membersByRank = new KA.BusinessLogicLayer.TList<KA.BusinessLogicLayer.Member>();
            if (degreeId == 1)
            {
                bl.TList<bl.Member> members = _memberServcice.GetAll();
                bl.TList<bl.Examination> Examinations = _examinatonService.GetAll();
                foreach (bl.Member member in members)
                {
                    bl.TList<bl.Examination> exams = _examinatonService.GetByMemberID(member.ID);
                    if (exams.Count == 0)
                        membersByRank.Add(member);
                }
            }
            if (degreeId != 14)
            {
                bl.TList<bl.Member> members = _memberServcice.GetAll();
                foreach (bl.Member member in members)
                {
                    bl.TList<bl.Examination> Examinations = _examinatonService.GetByMemberID(member.ID);
                    _examinatonService.DeepLoad(Examinations);
                    Examinations.Sort(ExaminationSortOnDegree);
                    if (Examinations.Count > 0 && Examinations[0].DegreeIDSource.ID == degreeId)
                    {
                        _memberServcice.DeepLoad(Examinations[0].MemberIDSource);
                        membersByRank.Add(Examinations[0].MemberIDSource);
                    }
                }
                return membersByRank;
            }
            else
                return _memberServcice.GetAll();
        }

        private int ExaminationSortOnDegree(bl.Examination m1, bl.Examination m2)
        {
            return m2.DegreeIDSource.Rank.CompareTo(m1.DegreeIDSource.Rank);
        }

        #endregion

        #region IController Members


        public void SetView(IViewBase view)
        {
            if (_ExamsListView == null)
            {
                _ExamsListView = (IExamsListViewBase)view;
                _ExamsListView.ViewClosed += new EventHandler(_ExamsListView_ViewClosed);
                _ExamsListView.ActionInvoked += new EventHandler<KA_Controller.EventArgs.ActionEventArgs>(_ExamsListView_ActionInvoked);
            }
        }

        void _ExamsListView_ActionInvoked(object sender, KA_Controller.EventArgs.ActionEventArgs e)
        {
            ExecuteAction(e.Action, e.Context);
        }

        void _ExamsListView_ViewClosed(object sender, System.EventArgs e)
        {
            ExamsListView = null;
        }

        #endregion

      
    }
}
