﻿using System;
using System.Collections.Generic;
using System.Text;
using KA_Controller.Interfaces;
using SL = KA.ServiceLayer;
using BL = KA.BusinessLogicLayer;
using KA_Controller.EventArgs;
using KA.BusinessLogicLayer;

namespace KA_Controller.Controllers
{
    public class TrainingDaysExceptionsDetailController : IController
    {
        private ITrainingDaysExceptionsDetailView _trainingExceptionsDetailView;
        private SL.TrainingExceptionsService _trainingExceptionsService = new SL.TrainingExceptionsService();

        public ITrainingDaysExceptionsDetailView TrainingExceptionsDetail
        {
            get { return _trainingExceptionsDetailView; }
            set { _trainingExceptionsDetailView = value; }
        }

        public TrainingDaysExceptionsDetailController(ITrainingDaysExceptionsDetailView trainingExceptionsDetailView)
        {
            SetView(trainingExceptionsDetailView);
        }

        #region IController Members

        public event EventHandler<KA_Controller.EventArgs.ActionEventArgs> ActionClicked;

        public void ExecuteAction(KA_Common.Enums.Actions Action, object context)
        {
            switch (Action)
            {
                case KA_Common.Enums.Actions.showview:
                    TrainingExceptionsDetail.SetContext((TrainingExceptions)context);
                    TrainingExceptionsDetail.ShowDialog();
                    break;
                case KA_Common.Enums.Actions.update:
                    TrainingExceptions trainingExceptions = (TrainingExceptions)context;
                    //Het uur moet 00:00:00 zijn in de DB anders worden ze niet in de rapporten getoond.
                    DateTime tmpDate = new DateTime(trainingExceptions.Day.Year, trainingExceptions.Day.Month, trainingExceptions.Day.Day, 0, 0, 0, 0);
                    trainingExceptions.Day = tmpDate;
                    _trainingExceptionsService.Save(trainingExceptions);
                    ActionClicked(this, new KA_Controller.EventArgs.ActionEventArgs(KA_Common.Enums.Actions.LoadTrainingExceptions, KA_Common.Enums.Controller.OptionsController, null));
                    TrainingExceptionsDetail.Close();
                    break;
                case KA_Common.Enums.Actions.add:
                    TrainingExceptions trainingExceptionsAdd = (TrainingExceptions)context;
                    DateTime tmpDateAdd = new DateTime(trainingExceptionsAdd.Day.Year, trainingExceptionsAdd.Day.Month, trainingExceptionsAdd.Day.Day, 0, 0, 0, 0);
                    trainingExceptionsAdd.Day = tmpDateAdd;
                    _trainingExceptionsService.Insert(trainingExceptionsAdd);
                    ActionClicked(this, new KA_Controller.EventArgs.ActionEventArgs(KA_Common.Enums.Actions.LoadTrainingExceptions, KA_Common.Enums.Controller.OptionsController, null));
                    TrainingExceptionsDetail.Close();
                    break;
                default:
                    break;
            }
        }

        public void SetView(IViewBase view)
        {
            if (_trainingExceptionsDetailView == null)
            {
                _trainingExceptionsDetailView = (ITrainingDaysExceptionsDetailView)view;
                _trainingExceptionsDetailView.ViewClosed += new EventHandler(_TrainingExceptionsDetailView_ViewClosed);
                _trainingExceptionsDetailView.ActionInvoked += new EventHandler<ActionEventArgs>(_TrainingExceptionsDetailView_ActionInvoked);
            }
        }
        #endregion

        void _TrainingExceptionsDetailView_ActionInvoked(object sender, ActionEventArgs e)
        {
            ExecuteAction(e.Action, e.Context);
        }

        void _TrainingExceptionsDetailView_ViewClosed(object sender, System.EventArgs e)
        {
            TrainingExceptionsDetail = null;
        }
    }
}
