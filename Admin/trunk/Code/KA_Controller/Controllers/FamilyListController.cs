using System;
using System.Collections.Generic;
using System.Text;

using KA_Controller.Interfaces;
using KA_Domain;
using BL = KA.BusinessLogicLayer;
using SL = KA.ServiceLayer;
using KA_Controller.EventArgs;
using System.Windows.Forms;
using KA.ServiceLayer;

namespace KA_Controller.Controllers
{
    public class FamilyListController : IController
    {
        private IFamilyListViewBase _FamilyListView;
        private SL.GroupService _groupService = new KA.ServiceLayer.GroupService();

        public IFamilyListViewBase FamilyListView
        {
            get { return _FamilyListView; }
            set { _FamilyListView = value;}
        }


        //ctor
        public FamilyListController(IViewBase familyListView)
        {
            SetView(familyListView);
        }

        #region IController Members

        public event EventHandler<KA_Controller.EventArgs.ActionEventArgs> ActionClicked;

        public void ExecuteAction(KA_Common.Enums.Actions Action, object context)
        {
            switch (Action)
            {
                case KA_Common.Enums.Actions.showview:
                    BL.TList<BL.Group> allGroups = _groupService.GetAll();
                    _groupService.DeepLoad(allGroups, true);
                    _FamilyListView.FillGrid(allGroups);
                    _FamilyListView.Show();
                    _FamilyListView.Activate();
                    break;
                case KA_Common.Enums.Actions.add:
                    if (ActionClicked != null)
                        ActionClicked(this, new ActionEventArgs(KA_Common.Enums.Actions.showview, KA_Common.Enums.Controller.FamilyDetailController, null));
                    break;
                case KA_Common.Enums.Actions.delete:
                    if (MessageBox.Show("Bent u zeker dat u deze familie/groep wilt verwijderen?", "Verwijderen", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        BL.Group group = (BL.Group)FamilyListView.GetSelectedGroup();
                        MemberService memberService = new MemberService();

                        //First we need to kick every member of this group out (set groupID to NULL)
                        //then we can delete the actual group. (because of foreign key constraints)
                        foreach (KA.BusinessLogicLayer.Member member in group.MemberCollection)
                        {
                            member.GroupID = null;
                            member.AcceptChanges();
                            memberService.Update(member);
                        }
                        
                        _groupService.Delete(group);
                        FamilyListView.FillGrid(_groupService.GetAll());
                    }
                    break;
                case KA_Common.Enums.Actions.refresh:
                    BL.TList<BL.Group> groups = _groupService.GetAll();
                    _groupService.DeepLoad(groups, true);
                    FamilyListView.FillGrid(groups);
                    break;
                case KA_Common.Enums.Actions.detail:
                    if (ActionClicked != null && context != null)
                        ActionClicked(this, new ActionEventArgs(KA_Common.Enums.Actions.showview, KA_Common.Enums.Controller.FamilyDetailController, (BL.Group)context));
                    else
                        ActionClicked(this, new ActionEventArgs(KA_Common.Enums.Actions.showview, KA_Common.Enums.Controller.FamilyDetailController, FamilyListView.GetSelectedGroup()));
                    break;
                case KA_Common.Enums.Actions.GetFilteredFamilies:
                    BL.TList<BL.Group> FilteredFamilies = GetFilteredFamilies((string)context);
                    _groupService.DeepLoad(FilteredFamilies, true);
                    FamilyListView.FillGrid(FilteredFamilies);
                    break;
                default:
                    break;
            }
        }

        
        private BL.TList<BL.Group> GetFilteredFamilies(string filter)
        {
            BL.TList<BL.Group> allFamilies = _groupService.GetAll();

            //following code filters for active or non active families. 
            //A family is active when it has at least one active member
            if (!string.IsNullOrEmpty(filter))
            {
                BL.TList<BL.Group> filteredFamilies = new KA.BusinessLogicLayer.TList<KA.BusinessLogicLayer.Group>();

                foreach (BL.Group group in allFamilies)
                {
                    foreach (KA.BusinessLogicLayer.Member member in group.MemberCollection)
                    {
                        if (member.IsActive == (filter.ToLower() == "active"))
                        {
                            if (!filteredFamilies.Contains(group))
                                filteredFamilies.Add(group);
                        }
                    }
                }

                return filteredFamilies;
            }
            else
                return allFamilies;
        }
        #endregion

        #region IController Members
        public void SetView(IViewBase view)
        {
            if (_FamilyListView == null)
            {
                _FamilyListView = (IFamilyListViewBase)view;
                _FamilyListView.ViewClosed += new EventHandler(_FamilyManagementView_ViewClosed);
                _FamilyListView.ActionInvoked += new EventHandler<ActionEventArgs>(_FamilyManagementView_ActionInvoked);
            }
        }

        void _FamilyManagementView_ActionInvoked(object sender, ActionEventArgs e)
        {
            ExecuteAction(e.Action, e.Context);
        }

        void _FamilyManagementView_ViewClosed(object sender, System.EventArgs e)
        {
            FamilyListView = null;
        }
        #endregion
    }
}
