using System;
using System.Collections.Generic;
using System.Text;
using KA_Controller.Interfaces;
using KA_Domain.DomainController;
using System.Windows.Forms;
using Host.Types;
using KA_Controller.EventArgs;
using KA_Common;
using System.IO;

namespace KA_Controller.Controllers
{
    public class StatisticsController : IController
    {
        private IStatisticsViewBase _StatisticsView;

        public IStatisticsViewBase StatisticsView
        {
            get { return _StatisticsView; }
            set { _StatisticsView = value;
            
            }
        }

        void _StatisticsView_ActionInvoked(object sender, ActionEventArgs e)
        {
            ExecuteAction(e.Action, e.Context);
        }

        void _StatisticsView_ViewClosed(object sender, System.EventArgs e)
        {
            StatisticsView = null;
        }

        
        public StatisticsController(IStatisticsViewBase statisticsView)
        {
            SetView(statisticsView);
        }

        public AvailablePlugins LoadPlugins()
        {
            Plugins.Global.Plugins.FindPlugins(Application.StartupPath + @"\Plugins");
            return Plugins.Global.Plugins.AvailablePlugins;

        }

        public void ImportPlugin(string path)
        {
            if (Plugins.Global.Plugins.isPluginKaishoPlugin(path))
            {
                File.Copy(path, Application.StartupPath + @"\Plugins\" + Path.GetFileName(path));
                StatisticsView.FillGrid(LoadPlugins());
                StatisticsView.Show();
                MessageBox.Show("Kaisho rapport succesvol geimporteerd.", "Import", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                throw new Exception("De geselecteerde file is geen geldig Kaisho rapport...");
        }

        private void DeleteReportFile(string path)
        {
            if (File.Exists(path))
                File.Delete(path);
        }


        #region IController Members

        public event EventHandler<KA_Controller.EventArgs.ActionEventArgs> ActionClicked;

        public void ExecuteAction(KA_Common.Enums.Actions Action, object context)
        {
            switch (Action)
            {
                case KA_Common.Enums.Actions.ReportClicked:
                    ActionClicked(this, new ActionEventArgs(Enums.Actions.ReportPlugin, Enums.Controller.StatisticsViewerController, context));
                    break;
                case KA_Common.Enums.Actions.showview:
                    StatisticsView.FillGrid(LoadPlugins());
                    StatisticsView.Show();
                    StatisticsView.Activate();
                    break;
                case Enums.Actions.LoadReports:
                    StatisticsView.FillGrid(LoadPlugins());
                    StatisticsView.Activate();
                    break;
                case Enums.Actions.ReportPlugin:
                    OpenFileDialog ofd = new OpenFileDialog();
                    ofd.Filter = "Kaisho Report|*.dll";
                    ofd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        ImportPlugin(ofd.FileName);
                    }
                    break;
                case Enums.Actions.deleteReport:
                    AvailablePlugin pluginToDelete = StatisticsView.GetSelectedReport();
                    if (MessageBox.Show("Bent u zeker dat u het rapport '" + pluginToDelete.Instance.PluginName + "' wilt verwijderen?", "Verwijderen", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        DeleteReportFile(pluginToDelete.AssemblyPath);
                        StatisticsView.FillGrid(LoadPlugins());
                    }
                    break;
                default:
                    break;
            }
        }

        

        #endregion

        #region IController Members


        public void SetView(IViewBase view)
        {
            if (_StatisticsView == null)
            {
                _StatisticsView = (IStatisticsViewBase)view;
                _StatisticsView.ViewClosed += new EventHandler(_StatisticsView_ViewClosed);
                _StatisticsView.ActionInvoked += new EventHandler<ActionEventArgs>(_StatisticsView_ActionInvoked);
            }
        }

        #endregion
    }

}
