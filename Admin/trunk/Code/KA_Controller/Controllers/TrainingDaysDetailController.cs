﻿using System;
using System.Collections.Generic;
using System.Text;
using KA_Controller.Interfaces;
using KA.BusinessLogicLayer;
using KA.ServiceLayer;

namespace KA_Controller.Controllers
{
    public class TrainingDaysDetailController : IController
    {
        private ITrainingDaysDetailView _trainingDaysDetailView;
        public ITrainingDaysDetailView TrainingListView
        {
            get { return _trainingDaysDetailView; }
            set { _trainingDaysDetailView = value; }
        }

        public TrainingDaysDetailController(ITrainingDaysDetailView trainingDaysDetail)
        {
            SetView(trainingDaysDetail);
        }

        #region IController Members

        public event EventHandler<KA_Controller.EventArgs.ActionEventArgs> ActionClicked;

        public void ExecuteAction(KA_Common.Enums.Actions Action, object context)
        {
            switch (Action)
            {
                case KA_Common.Enums.Actions.showview:
                    TList<Location> locations = new TList<Location>();
                    LocationService locationService = new KA.ServiceLayer.LocationService();
                    locations = locationService.GetAll();

                    TList<TrainingPeriod> periods = new TList<TrainingPeriod>();
                    TrainingPeriodService trainingPeriodService = new KA.ServiceLayer.TrainingPeriodService();
                    periods = trainingPeriodService.GetAll();

                    TrainingListView.LoadData(locations, periods);
                    if (context != null)
                        TrainingListView.SetContext((TrainingTime)context);
                    TrainingListView.ShowDialog();
                    break;
                case KA_Common.Enums.Actions.update:
                    ((TrainingTime)context).Validate();
                    if (((TrainingTime)context).BrokenRulesList.Count == 0)
                    {
                        TrainingTimeService trainingTimeService = new TrainingTimeService();
                        trainingTimeService.Save((TrainingTime)context);
                        ActionClicked(this, new KA_Controller.EventArgs.ActionEventArgs(KA_Common.Enums.Actions.LoadTrainingDays, KA_Common.Enums.Controller.OptionsController, null));
                        _trainingDaysDetailView.Close();
                    }
                    else
                        _trainingDaysDetailView.ShowBrokenRules(((TrainingTime)context).BrokenRulesList);
                    break;
                case KA_Common.Enums.Actions.add:
                    ((TrainingTime)context).Validate();
                    if (((TrainingTime)context).BrokenRulesList.Count == 0)
                    {
                        TrainingTimeService trainingTimeService = new TrainingTimeService();
                        trainingTimeService.Insert((TrainingTime)context);
                        ActionClicked(this, new KA_Controller.EventArgs.ActionEventArgs(KA_Common.Enums.Actions.LoadTrainingDays, KA_Common.Enums.Controller.OptionsController, null));
                        _trainingDaysDetailView.Close();
                    }
                    else
                        _trainingDaysDetailView.ShowBrokenRules(((TrainingTime)context).BrokenRulesList);
                    break;
                default:
                    break;
            }
        }

        public void SetView(IViewBase view)
        {
            if (_trainingDaysDetailView == null)
            {
                _trainingDaysDetailView = (ITrainingDaysDetailView)view;
                _trainingDaysDetailView.ViewClosed += new EventHandler(_trainingDaysDetailView_ViewClosed);
                _trainingDaysDetailView.ActionInvoked += new EventHandler<KA_Controller.EventArgs.ActionEventArgs>(_trainingDaysDetailView_ActionInvoked);
            }
        }

        void _trainingDaysDetailView_ActionInvoked(object sender, KA_Controller.EventArgs.ActionEventArgs e)
        {
            ExecuteAction(e.Action, e.Context);
        }

        void _trainingDaysDetailView_ViewClosed(object sender, System.EventArgs e)
        {
            _trainingDaysDetailView = null;
        }

        #endregion
    }
}
