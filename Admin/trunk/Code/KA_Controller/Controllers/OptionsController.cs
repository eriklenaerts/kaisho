﻿using System;
using System.Collections.Generic;
using System.Text;
using KA_Controller.Interfaces;
using KA_Controller.EventArgs;
using KA_Common;
using KA.ServiceLayer;
using KA.BusinessLogicLayer;
using KA_Domain;

namespace KA_Controller.Controllers
{
    public class OptionsController : IController
    {
        private TrainingTimeService _trainingService = new KA.ServiceLayer.TrainingTimeService();
        private TrainingExceptionsService _trainingExceptionService = new KA.ServiceLayer.TrainingExceptionsService();

        public OptionsController(IOptionsViewBase optionsView)
        {
            SetView(optionsView);
            _ControllerCollection = new Dictionary<Enums.Controller, IController>();
        }

        private IOptionsViewBase _optionsView;
        public IOptionsViewBase OptionsView
        {
            get { return _optionsView; }
            set { _optionsView = value; }
        }

        private Dictionary<Enums.Controller, IController> _ControllerCollection;
        public Dictionary<Enums.Controller, IController> ControllerCollection
        {
            get { return _ControllerCollection; }
            set { _ControllerCollection = value; }
        }

        #region IController Members
        public event EventHandler<KA_Controller.EventArgs.ActionEventArgs> ActionClicked;

        public void ExecuteAction(Enums.Actions Action, object context)
        {
            switch (Action)
            {
                case KA_Common.Enums.Actions.showview:
                    OptionsView.Show();
                    OptionsView.Activate();
                    break;
                case Enums.Actions.add:
                    if (((KA_Domain.DomainBase)context).mode == 1)// trainingdays
                        ActionClicked(this, new ActionEventArgs(KA_Common.Enums.Actions.showview, KA_Common.Enums.Controller.TrainingDaysDetailController, null));
                    else if (((DomainBase)context).mode == 2)// trainingexceptiondays
                        ActionClicked(this, new ActionEventArgs(KA_Common.Enums.Actions.showview, KA_Common.Enums.Controller.TrainingDaysExceptionsDetailController, null));
                    break;
                case Enums.Actions.delete:
                    if (((DomainBase)context).mode == 1)// trainingdays
                    {
                        TrainingTime time = OptionsView.GetSelectedTrainingTime();
                        _trainingService.Delete(time.ID);
                        ExecuteAction(Enums.Actions.LoadTrainingDays, null);
                    }
                    else if (((DomainBase)context).mode == 2)// trainingexceptiondays
                    {
                        TrainingExceptions exception = OptionsView.GetSelectedTrainingException();
                        _trainingExceptionService.Delete(exception.ID);
                        ExecuteAction(Enums.Actions.LoadTrainingExceptions, null);
                    }
                    break;
                case KA_Common.Enums.Actions.LoadTrainingDays:
                    TList<TrainingTime> allTrainings = _trainingService.GetAll();
                    _trainingService.DeepLoad(allTrainings, true);
                    OptionsView.FillTrainingDaysGrid(allTrainings);
                    break;
                case Enums.Actions.LoadTrainingExceptions:
                    TList<TrainingExceptions> trainingExceptions = _trainingExceptionService.GetAll();
                    OptionsView.FillTrainingDaysExceptionsGrid(trainingExceptions);
                    break;
                case Enums.Actions.detail:
                    if (context is DomainBase)
                    {
                        if (((DomainBase)context).mode == 1) //trainingDays
                            ActionClicked(this, new ActionEventArgs(KA_Common.Enums.Actions.showview, KA_Common.Enums.Controller.TrainingDaysDetailController, OptionsView.GetSelectedTrainingTime()));
                        else
                            ActionClicked(this, new ActionEventArgs(KA_Common.Enums.Actions.showview, KA_Common.Enums.Controller.TrainingDaysExceptionsDetailController, OptionsView.GetSelectedTrainingException()));
                    }
                    else
                    {
                        if (ActionClicked != null && context is TrainingTime)
                            ActionClicked(this, new ActionEventArgs(KA_Common.Enums.Actions.showview, KA_Common.Enums.Controller.TrainingDaysDetailController, context));
                        else if (ActionClicked != null && context is TrainingExceptions)
                            ActionClicked(this, new ActionEventArgs(KA_Common.Enums.Actions.showview, KA_Common.Enums.Controller.TrainingDaysExceptionsDetailController, context));
                    }
                    break;
                default:
                    break;
            }
        }

        public void SetView(IViewBase view)
        {
            if (_optionsView == null)
            {
                _optionsView = (IOptionsViewBase)view;
                _optionsView.ViewClosed += new EventHandler(_optionsView_ViewClosed);
                _optionsView.ActionInvoked += new EventHandler<ActionEventArgs>(_optionsView_ActionInvoked);
            }
        }

        #endregion

        #region methods
        void _optionsView_ActionInvoked(object sender, ActionEventArgs e)
        {
            ExecuteAction(e.Action, e.Context);
        }

        void _optionsView_ViewClosed(object sender, System.EventArgs e)
        {
            _optionsView = null;
        }
        #endregion methods
    }
}

