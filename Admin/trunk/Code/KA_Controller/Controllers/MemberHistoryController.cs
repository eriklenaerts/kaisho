﻿using System;
using System.Collections.Generic;
using System.Text;
using KA_Common;
using KA_Controller.EventArgs;
using KA_Controller.Interfaces;
using SL = KA.ServiceLayer;
using BL = KA.BusinessLogicLayer;

namespace KA_Controller.Controllers
{

    public class MemberHistoryController : IController
    {
        private IMemberHistoryViewBase _memberHistoryView;
        private SL.ExaminationService _examinationService;
        public IMemberHistoryViewBase MemberHistoryView
        {
            get { return _memberHistoryView; }
            set { _memberHistoryView = value; }
        }

        public MemberHistoryController(IMemberHistoryViewBase memberHistoryView)
        {
            SetView(memberHistoryView);
            _examinationService = new KA.ServiceLayer.ExaminationService();
        }
        
        #region IController Members

        public event EventHandler<KA_Controller.EventArgs.ActionEventArgs> ActionClicked;

        public void ExecuteAction(KA_Common.Enums.Actions Action, object context)
        {
            switch (Action)
            {
                case KA_Common.Enums.Actions.showview:
                    BL.TList<BL.Examination> examinations = _examinationService.GetByMemberID(((BL.Member)context).ID);
                    _examinationService.DeepLoad(examinations);
                    _memberHistoryView.SetContext(examinations, (BL.Member)context);
                    _memberHistoryView.ShowDialog();
                    break;
                case KA_Common.Enums.Actions.refresh:
                    BL.TList<BL.Examination> examinations2 = _examinationService.GetByMemberID(((BL.Member)context).ID);
                    _examinationService.DeepLoad(examinations2);
                    _memberHistoryView.SetContext(examinations2, (BL.Member)context);
                    break;
                case KA_Common.Enums.Actions.add :
                    if (ActionClicked != null && context != null)
                        ActionClicked(this, new ActionEventArgs(KA_Common.Enums.Actions.showview, KA_Common.Enums.Controller.AddHistory, (BL.Member)context));
                    break;
                case KA_Common.Enums.Actions.delete :
                    _examinationService.Delete(((AddHistoryEventArgs)context).Exam);

                    BL.TList<BL.Examination> examinations3 = _examinationService.GetByMemberID(((AddHistoryEventArgs)context).Member.ID);
                    _examinationService.DeepLoad(examinations3);
                    _memberHistoryView.SetContext(examinations3, ((AddHistoryEventArgs)context).Member);

                    ActionClicked(this, new ActionEventArgs(Enums.Actions.refresh, Enums.Controller.MemberListController, null));
                    ActionClicked(this, new ActionEventArgs(Enums.Actions.RefreshDegree, Enums.Controller.MemberDetailController, ((AddHistoryEventArgs)context).Member));
                    break;
                default:
                    break;
            }
        }

        public void SetView(IViewBase view)
        {
            if (_memberHistoryView == null)
            {
                _memberHistoryView = (IMemberHistoryViewBase)view;
                _memberHistoryView.ViewClosed += new EventHandler(_memberHistoryView_ViewClosed);
                _memberHistoryView.ActionInvoked += new EventHandler<KA_Controller.EventArgs.ActionEventArgs>(_memberHistoryView_ActionInvoked);
            }
        }

        void _memberHistoryView_ActionInvoked(object sender, KA_Controller.EventArgs.ActionEventArgs e)
        {
            ExecuteAction(e.Action, e.Context);
        }

        void _memberHistoryView_ViewClosed(object sender, System.EventArgs e)
        {
            MemberHistoryView = null;
        }

        #endregion
    }
}
