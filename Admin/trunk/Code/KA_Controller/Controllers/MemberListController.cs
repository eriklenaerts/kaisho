using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using KA_Controller.BaseClasses;
using KA_Domain.DomainController;
using SL = KA.ServiceLayer;
using BL = KA.BusinessLogicLayer;
using KA.BusinessLogicLayer;
using KA_Controller.EventArgs;

namespace KA_Controller
{
    public class MemberListController : IController
    {

        private IMemberListViewBase _MemberListView;
        private SL.MemberService _memberServcice = new KA.ServiceLayer.MemberService();

        public IMemberListViewBase MemberListView
        {
            get { return _MemberListView; }
            set { _MemberListView = value; }
        }



        public MemberListController(IMemberListViewBase memberListView)
        {
            SetView(memberListView);
        }

        #region IController Members

        public event EventHandler<KA_Controller.EventArgs.ActionEventArgs> ActionClicked;

        public void ExecuteAction(KA_Common.Enums.Actions Action, object context)
        {
            switch (Action)
            {
                case KA_Common.Enums.Actions.add:
                    if (ActionClicked != null)
                        ActionClicked(this, new ActionEventArgs(KA_Common.Enums.Actions.showview, KA_Common.Enums.Controller.MemberDetailController, null));
                    break;
                case KA_Common.Enums.Actions.delete:
                    BL.Member member = (BL.Member)MemberListView.GetSelectedMember();
                    if (member != null)
                    {
                        if (MessageBox.Show("Bent u zeker dat u " + member.Firstname + " " + member.Lastname + " wilt verwijderen?", "Verwijderen", MessageBoxButtons.OKCancel) == DialogResult.OK)
                        {
                            _memberServcice.Delete(member);
                            string filter1 = MemberListView.GetSelectedFilter();
                            BL.TList<BL.Member> membersList = null;
                            if (filter1 == string.Empty)
                                membersList = _memberServcice.GetAll();
                            else
                                membersList = _memberServcice.Find(filter1);

                            MemberListView.FillGrid(membersList);
                        }
                    }
                    break;
                case KA_Common.Enums.Actions.copy :
                    BL.Member memberToCopy = (BL.Member) MemberListView.GetSelectedMember();
                    BL.Member newMember = new BL.Member();
                    if (memberToCopy != null)
                    {
                        newMember.Lastname = memberToCopy.Lastname;
                        newMember.Address = memberToCopy.Address;
                        newMember.GroupID = memberToCopy.GroupID;
                        newMember.GroupIDSource = memberToCopy.GroupIDSource;
                        newMember.ZipCode = memberToCopy.ZipCode;
                        newMember.City = memberToCopy.City;
                        newMember.Province = memberToCopy.Province;
                        newMember.Country = memberToCopy.Country;
                        newMember.Telephone = memberToCopy.Telephone;
                        newMember.Birthdate = DateTime.Now;
                        newMember.PaymentId = memberToCopy.PaymentId;
                        newMember.IsNew = true;
                        newMember.IsTrainer = memberToCopy.IsTrainer;
                        newMember.IsActive = true;
                        newMember.TrainingLocationId = memberToCopy.TrainingLocationId;
                        newMember.TrainingLocationIdSource = memberToCopy.TrainingLocationIdSource;
                        if (ActionClicked != null)
                            ActionClicked(this, new ActionEventArgs(KA_Common.Enums.Actions.showview, KA_Common.Enums.Controller.MemberDetailController, newMember));
                                                
                    }
                    break;
                case KA_Common.Enums.Actions.refresh:
                    string filter = MemberListView.GetSelectedFilter();
                    BL.TList<BL.Member> members  = null;
                    if (filter == string.Empty)
                        members = _memberServcice.GetAll();
                    else
                        members = _memberServcice.Find(filter);
                    
                    _memberServcice.DeepLoad(members, true);
                    _memberServcice.DeepLoad(members, true);
                    MemberListView.FillGrid(members);
                    break;
                case KA_Common.Enums.Actions.detail:
                    if (ActionClicked != null && context != null)
                        ActionClicked(this, new ActionEventArgs(KA_Common.Enums.Actions.showview, KA_Common.Enums.Controller.MemberDetailController, (BL.Member)context));
                    else
                    {
                        ActionClicked(this, new ActionEventArgs(KA_Common.Enums.Actions.showview, KA_Common.Enums.Controller.MemberDetailController, MemberListView.GetSelectedMember()));
                    }

                    break;
                case KA_Common.Enums.Actions.showview:
                    string filter3 = MemberListView.GetSelectedFilter();
                    BL.TList<BL.Member> memberslist2 = null;
                    if (filter3 == string.Empty)
                        memberslist2 = _memberServcice.GetAll();
                    else
                        memberslist2 = _memberServcice.Find(filter3);
                    _memberServcice.DeepLoad(memberslist2, true);
                    _memberServcice.DeepLoad(memberslist2, true);
                    MemberListView.FillGrid(memberslist2);
                    MemberListView.Show();
                    MemberListView.Activate();
                    break;
                case KA_Common.Enums.Actions.GetFilteredMembers:
                    BL.TList<BL.Member> FilteredMembers = _memberServcice.Find((string)context);
                    _memberServcice.DeepLoad(FilteredMembers, true);
                    _memberServcice.DeepLoad(FilteredMembers, true);
                    MemberListView.FillGrid(FilteredMembers);
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region IController Members


        public void SetView(IViewBase view)
        {
            if (_MemberListView == null)
            {
                _MemberListView = (IMemberListViewBase)view;
                _MemberListView.ViewClosed += new EventHandler(_MemberListView_ViewClosed);
                _MemberListView.ActionInvoked += new EventHandler<ActionEventArgs>(_MemberListView_ActionInvoked);
            }
        }

        void _MemberListView_ActionInvoked(object sender, ActionEventArgs e)
        {
            ExecuteAction(e.Action, e.Context);
        }

        void _MemberListView_ViewClosed(object sender, System.EventArgs e)
        {
            MemberListView = null;
        }

        #endregion


    }
}
