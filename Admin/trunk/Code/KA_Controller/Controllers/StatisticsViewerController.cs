using System;
using System.Collections.Generic;
using System.Text;
using KA_Controller.Interfaces;
using KA_Common;
using Host.Types;

namespace KA_Controller.Controllers
{
    class StatisticsViewerController : IController
    {
        public StatisticsViewerController(IViewBase statisticsViewer)
        {
            SetView(statisticsViewer);
        }
        private IStatisticsViewerBase _StatisticsViewer;

        public IStatisticsViewerBase StatisticsViewer
        {
            get { return _StatisticsViewer; }
            set
            {
                _StatisticsViewer = value;
                
            }
        }

        void _StatisticsViewer_ViewClosed(object sender, System.EventArgs e)
        {
            _StatisticsViewer = null;
            if (ActionClicked != null)
                ActionClicked(this, new KA_Controller.EventArgs.ActionEventArgs(Enums.Actions.LoadReports, Enums.Controller.StatisticsController, null));
        }

        #region IController Members

        public event EventHandler<KA_Controller.EventArgs.ActionEventArgs> ActionClicked;

        public void ExecuteAction(KA_Common.Enums.Actions Action, object context)
        {

            switch (Action)
            {
                case Enums.Actions.ReportPlugin:
                    _StatisticsViewer.LoadReport((AvailablePlugin)context);
                    _StatisticsViewer.Show();
                    _StatisticsViewer.Activate();
                    break;
                default:
                    break;
            }
        }

        public void SetView(IViewBase view)
        {
            if (_StatisticsViewer == null)
            {
                _StatisticsViewer = (IStatisticsViewerBase)view;
                _StatisticsViewer.ViewClosed += new EventHandler(_StatisticsViewer_ViewClosed);
            }
        }

        #endregion
    }
}
