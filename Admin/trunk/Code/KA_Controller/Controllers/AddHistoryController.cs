﻿using System;
using System.Collections.Generic;
using System.Text;
using KA_Controller.Interfaces;
using BL = KA.BusinessLogicLayer;
using SL = KA.ServiceLayer;
using KA_Controller.EventArgs;
using KA_Common;

namespace KA_Controller.Controllers
{
    public class AddHistoryController : IController
    {
        private IAddHistoryViewBase _AddHistoryView;
        private SL.ExaminationService _examinatonService = new KA.ServiceLayer.ExaminationService();

        public IAddHistoryViewBase AddHistoryView 
        {
            get { return _AddHistoryView; }
            set { _AddHistoryView = value;}
        }

        public AddHistoryController(IAddHistoryViewBase addHistoryView)
        {
            SetView(addHistoryView);
        }

        #region IController Members

        public event EventHandler<KA_Controller.EventArgs.ActionEventArgs> ActionClicked;

        public void ExecuteAction(KA_Common.Enums.Actions Action, object context)
        {
            switch (Action)
            {
                case KA_Common.Enums.Actions.showview:
                    AddHistoryView.SetContext((BL.Member)context);
                    AddHistoryView.Show();
                    AddHistoryView.Activate();
                    break;
                case KA_Common.Enums.Actions.Apply:
                    _examinatonService.Insert(((AddHistoryEventArgs)context).Exam);
                    ActionClicked(this, new ActionEventArgs(Enums.Actions.refresh, Enums.Controller.MemberHistoryController, ((AddHistoryEventArgs)context).Member));
                    ActionClicked(this, new ActionEventArgs(Enums.Actions.refresh,  Enums.Controller.MemberListController, null));
                    ActionClicked(this, new ActionEventArgs(Enums.Actions.RefreshDegree, Enums.Controller.MemberDetailController, ((AddHistoryEventArgs)context).Member));
                    break;
                default:
                    break;
            }
        }

        public void SetView(IViewBase view)
        {
            if (_AddHistoryView == null)
            {
                _AddHistoryView = (IAddHistoryViewBase)view;
                _AddHistoryView.ViewClosed += new EventHandler(_AddHistoryView_ViewClosed);
                _AddHistoryView.ActionInvoked += new EventHandler<KA_Controller.EventArgs.ActionEventArgs>(_AddHistoryView_ActionInvoked);
            }
        }

        void _AddHistoryView_ViewClosed(object sender, System.EventArgs e)
        {
            AddHistoryView = null;
        }

        void _AddHistoryView_ActionInvoked(object sender, KA_Controller.EventArgs.ActionEventArgs e)
        {
            ExecuteAction(e.Action, e.Context);
        }

        #endregion
    }
}
