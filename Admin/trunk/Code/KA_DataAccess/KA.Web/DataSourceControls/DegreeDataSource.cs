﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KA.BusinessLogicLayer;
using KA.DataAccessLayer;
using KA.DataAccessLayer.Bases;
using KA.ServiceLayer;
#endregion

namespace KA.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.DegreeProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(DegreeDataSourceDesigner))]
	public class DegreeDataSource : ProviderDataSource<Degree, DegreeKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DegreeDataSource class.
		/// </summary>
		public DegreeDataSource() : base(new DegreeService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the DegreeDataSourceView used by the DegreeDataSource.
		/// </summary>
		protected DegreeDataSourceView DegreeView
		{
			get { return ( View as DegreeDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DegreeDataSource control invokes to retrieve data.
		/// </summary>
		public DegreeSelectMethod SelectMethod
		{
			get
			{
				DegreeSelectMethod selectMethod = DegreeSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (DegreeSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the DegreeDataSourceView class that is to be
		/// used by the DegreeDataSource.
		/// </summary>
		/// <returns>An instance of the DegreeDataSourceView class.</returns>
		protected override BaseDataSourceView<Degree, DegreeKey> GetNewDataSourceView()
		{
			return new DegreeDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the DegreeDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class DegreeDataSourceView : ProviderDataSourceView<Degree, DegreeKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DegreeDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the DegreeDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public DegreeDataSourceView(DegreeDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal DegreeDataSource DegreeOwner
		{
			get { return Owner as DegreeDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal DegreeSelectMethod SelectMethod
		{
			get { return DegreeOwner.SelectMethod; }
			set { DegreeOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal DegreeService DegreeProvider
		{
			get { return Provider as DegreeService; }
		}

		#endregion Properties
		
		#region Methods
		
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<Degree> GetSelectData(out int count)
		{
			Hashtable values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<Degree> results = null;
			Degree item;
			count = 0;
			
			System.Int32 id;

			switch ( SelectMethod )
			{
				case DegreeSelectMethod.Get:
					DegreeKey entityKey  = new DegreeKey();
					entityKey.Load(values);
					item = DegreeProvider.Get(entityKey);
					results = new TList<Degree>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case DegreeSelectMethod.GetAll:
                    results = DegreeProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case DegreeSelectMethod.GetPaged:
					results = DegreeProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case DegreeSelectMethod.Find:
					if ( FilterParameters != null )
						results = DegreeProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = DegreeProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case DegreeSelectMethod.GetByID:
					id = ( values["ID"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ID"], typeof(System.Int32)) : (int)0;
					item = DegreeProvider.GetByID(id);
					results = new TList<Degree>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == DegreeSelectMethod.Get || SelectMethod == DegreeSelectMethod.GetByID )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				Degree entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					DegreeProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<Degree> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			DegreeProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region DegreeDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the DegreeDataSource class.
	/// </summary>
	public class DegreeDataSourceDesigner : ProviderDataSourceDesigner<Degree, DegreeKey>
	{
		/// <summary>
		/// Initializes a new instance of the DegreeDataSourceDesigner class.
		/// </summary>
		public DegreeDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public DegreeSelectMethod SelectMethod
		{
			get { return ((DegreeDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new DegreeDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region DegreeDataSourceActionList

	/// <summary>
	/// Supports the DegreeDataSourceDesigner class.
	/// </summary>
	internal class DegreeDataSourceActionList : DesignerActionList
	{
		private DegreeDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the DegreeDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public DegreeDataSourceActionList(DegreeDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public DegreeSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion DegreeDataSourceActionList
	
	#endregion DegreeDataSourceDesigner
	
	#region DegreeSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the DegreeDataSource.SelectMethod property.
	/// </summary>
	public enum DegreeSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByID method.
		/// </summary>
		GetByID
	}
	
	#endregion DegreeSelectMethod

	#region DegreeFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Degree"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DegreeFilter : SqlFilter<DegreeColumn>
	{
	}
	
	#endregion DegreeFilter

	#region DegreeProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;DegreeChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="Degree"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DegreeProperty : ChildEntityProperty<DegreeChildEntityTypes>
	{
	}
	
	#endregion DegreeProperty
}

