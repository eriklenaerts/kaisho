﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KA.BusinessLogicLayer;
using KA.DataAccessLayer;
using KA.DataAccessLayer.Bases;
using KA.ServiceLayer;
#endregion

namespace KA.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.PaymentTypeProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(PaymentTypeDataSourceDesigner))]
	public class PaymentTypeDataSource : ProviderDataSource<PaymentType, PaymentTypeKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PaymentTypeDataSource class.
		/// </summary>
		public PaymentTypeDataSource() : base(new PaymentTypeService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the PaymentTypeDataSourceView used by the PaymentTypeDataSource.
		/// </summary>
		protected PaymentTypeDataSourceView PaymentTypeView
		{
			get { return ( View as PaymentTypeDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the PaymentTypeDataSource control invokes to retrieve data.
		/// </summary>
		public PaymentTypeSelectMethod SelectMethod
		{
			get
			{
				PaymentTypeSelectMethod selectMethod = PaymentTypeSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (PaymentTypeSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the PaymentTypeDataSourceView class that is to be
		/// used by the PaymentTypeDataSource.
		/// </summary>
		/// <returns>An instance of the PaymentTypeDataSourceView class.</returns>
		protected override BaseDataSourceView<PaymentType, PaymentTypeKey> GetNewDataSourceView()
		{
			return new PaymentTypeDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the PaymentTypeDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class PaymentTypeDataSourceView : ProviderDataSourceView<PaymentType, PaymentTypeKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PaymentTypeDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the PaymentTypeDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public PaymentTypeDataSourceView(PaymentTypeDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal PaymentTypeDataSource PaymentTypeOwner
		{
			get { return Owner as PaymentTypeDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal PaymentTypeSelectMethod SelectMethod
		{
			get { return PaymentTypeOwner.SelectMethod; }
			set { PaymentTypeOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal PaymentTypeService PaymentTypeProvider
		{
			get { return Provider as PaymentTypeService; }
		}

		#endregion Properties
		
		#region Methods
		
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<PaymentType> GetSelectData(out int count)
		{
			Hashtable values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<PaymentType> results = null;
			PaymentType item;
			count = 0;
			
			System.Int32 id;

			switch ( SelectMethod )
			{
				case PaymentTypeSelectMethod.Get:
					PaymentTypeKey entityKey  = new PaymentTypeKey();
					entityKey.Load(values);
					item = PaymentTypeProvider.Get(entityKey);
					results = new TList<PaymentType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case PaymentTypeSelectMethod.GetAll:
                    results = PaymentTypeProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case PaymentTypeSelectMethod.GetPaged:
					results = PaymentTypeProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case PaymentTypeSelectMethod.Find:
					if ( FilterParameters != null )
						results = PaymentTypeProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = PaymentTypeProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case PaymentTypeSelectMethod.GetById:
					id = ( values["Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Id"], typeof(System.Int32)) : (int)0;
					item = PaymentTypeProvider.GetById(id);
					results = new TList<PaymentType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == PaymentTypeSelectMethod.Get || SelectMethod == PaymentTypeSelectMethod.GetById )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				PaymentType entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					PaymentTypeProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<PaymentType> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			PaymentTypeProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region PaymentTypeDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the PaymentTypeDataSource class.
	/// </summary>
	public class PaymentTypeDataSourceDesigner : ProviderDataSourceDesigner<PaymentType, PaymentTypeKey>
	{
		/// <summary>
		/// Initializes a new instance of the PaymentTypeDataSourceDesigner class.
		/// </summary>
		public PaymentTypeDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public PaymentTypeSelectMethod SelectMethod
		{
			get { return ((PaymentTypeDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new PaymentTypeDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region PaymentTypeDataSourceActionList

	/// <summary>
	/// Supports the PaymentTypeDataSourceDesigner class.
	/// </summary>
	internal class PaymentTypeDataSourceActionList : DesignerActionList
	{
		private PaymentTypeDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the PaymentTypeDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public PaymentTypeDataSourceActionList(PaymentTypeDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public PaymentTypeSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion PaymentTypeDataSourceActionList
	
	#endregion PaymentTypeDataSourceDesigner
	
	#region PaymentTypeSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the PaymentTypeDataSource.SelectMethod property.
	/// </summary>
	public enum PaymentTypeSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetById method.
		/// </summary>
		GetById
	}
	
	#endregion PaymentTypeSelectMethod

	#region PaymentTypeFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PaymentType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PaymentTypeFilter : SqlFilter<PaymentTypeColumn>
	{
	}
	
	#endregion PaymentTypeFilter

	#region PaymentTypeProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;PaymentTypeChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="PaymentType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PaymentTypeProperty : ChildEntityProperty<PaymentTypeChildEntityTypes>
	{
	}
	
	#endregion PaymentTypeProperty
}

