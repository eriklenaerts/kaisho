﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KA.BusinessLogicLayer;
using KA.DataAccessLayer;
using KA.DataAccessLayer.Bases;
using KA.ServiceLayer;
#endregion

namespace KA.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.PriceSchemeProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(PriceSchemeDataSourceDesigner))]
	public class PriceSchemeDataSource : ProviderDataSource<PriceScheme, PriceSchemeKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PriceSchemeDataSource class.
		/// </summary>
		public PriceSchemeDataSource() : base(new PriceSchemeService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the PriceSchemeDataSourceView used by the PriceSchemeDataSource.
		/// </summary>
		protected PriceSchemeDataSourceView PriceSchemeView
		{
			get { return ( View as PriceSchemeDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the PriceSchemeDataSource control invokes to retrieve data.
		/// </summary>
		public PriceSchemeSelectMethod SelectMethod
		{
			get
			{
				PriceSchemeSelectMethod selectMethod = PriceSchemeSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (PriceSchemeSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the PriceSchemeDataSourceView class that is to be
		/// used by the PriceSchemeDataSource.
		/// </summary>
		/// <returns>An instance of the PriceSchemeDataSourceView class.</returns>
		protected override BaseDataSourceView<PriceScheme, PriceSchemeKey> GetNewDataSourceView()
		{
			return new PriceSchemeDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the PriceSchemeDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class PriceSchemeDataSourceView : ProviderDataSourceView<PriceScheme, PriceSchemeKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PriceSchemeDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the PriceSchemeDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public PriceSchemeDataSourceView(PriceSchemeDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal PriceSchemeDataSource PriceSchemeOwner
		{
			get { return Owner as PriceSchemeDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal PriceSchemeSelectMethod SelectMethod
		{
			get { return PriceSchemeOwner.SelectMethod; }
			set { PriceSchemeOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal PriceSchemeService PriceSchemeProvider
		{
			get { return Provider as PriceSchemeService; }
		}

		#endregion Properties
		
		#region Methods
		
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<PriceScheme> GetSelectData(out int count)
		{
			Hashtable values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<PriceScheme> results = null;
			PriceScheme item;
			count = 0;
			
			System.Int32 id;
			System.Int32 priceTypeID;

			switch ( SelectMethod )
			{
				case PriceSchemeSelectMethod.Get:
					PriceSchemeKey entityKey  = new PriceSchemeKey();
					entityKey.Load(values);
					item = PriceSchemeProvider.Get(entityKey);
					results = new TList<PriceScheme>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case PriceSchemeSelectMethod.GetAll:
                    results = PriceSchemeProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case PriceSchemeSelectMethod.GetPaged:
					results = PriceSchemeProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case PriceSchemeSelectMethod.Find:
					if ( FilterParameters != null )
						results = PriceSchemeProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = PriceSchemeProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case PriceSchemeSelectMethod.GetByID:
					id = ( values["ID"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ID"], typeof(System.Int32)) : (int)0;
					item = PriceSchemeProvider.GetByID(id);
					results = new TList<PriceScheme>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				case PriceSchemeSelectMethod.GetByPriceTypeID:
					priceTypeID = ( values["PriceTypeID"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["PriceTypeID"], typeof(System.Int32)) : (int)0;
					results = PriceSchemeProvider.GetByPriceTypeID(priceTypeID, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == PriceSchemeSelectMethod.Get || SelectMethod == PriceSchemeSelectMethod.GetByID )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				PriceScheme entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					PriceSchemeProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<PriceScheme> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			PriceSchemeProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region PriceSchemeDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the PriceSchemeDataSource class.
	/// </summary>
	public class PriceSchemeDataSourceDesigner : ProviderDataSourceDesigner<PriceScheme, PriceSchemeKey>
	{
		/// <summary>
		/// Initializes a new instance of the PriceSchemeDataSourceDesigner class.
		/// </summary>
		public PriceSchemeDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public PriceSchemeSelectMethod SelectMethod
		{
			get { return ((PriceSchemeDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new PriceSchemeDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region PriceSchemeDataSourceActionList

	/// <summary>
	/// Supports the PriceSchemeDataSourceDesigner class.
	/// </summary>
	internal class PriceSchemeDataSourceActionList : DesignerActionList
	{
		private PriceSchemeDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the PriceSchemeDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public PriceSchemeDataSourceActionList(PriceSchemeDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public PriceSchemeSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion PriceSchemeDataSourceActionList
	
	#endregion PriceSchemeDataSourceDesigner
	
	#region PriceSchemeSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the PriceSchemeDataSource.SelectMethod property.
	/// </summary>
	public enum PriceSchemeSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByID method.
		/// </summary>
		GetByID,
		/// <summary>
		/// Represents the GetByPriceTypeID method.
		/// </summary>
		GetByPriceTypeID
	}
	
	#endregion PriceSchemeSelectMethod

	#region PriceSchemeFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PriceScheme"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PriceSchemeFilter : SqlFilter<PriceSchemeColumn>
	{
	}
	
	#endregion PriceSchemeFilter

	#region PriceSchemeProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;PriceSchemeChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="PriceScheme"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PriceSchemeProperty : ChildEntityProperty<PriceSchemeChildEntityTypes>
	{
	}
	
	#endregion PriceSchemeProperty
}

