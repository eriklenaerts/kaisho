﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KA.BusinessLogicLayer;
using KA.DataAccessLayer;
using KA.DataAccessLayer.Bases;
using KA.ServiceLayer;
#endregion

namespace KA.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.PriceTypeProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(PriceTypeDataSourceDesigner))]
	public class PriceTypeDataSource : ProviderDataSource<PriceType, PriceTypeKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PriceTypeDataSource class.
		/// </summary>
		public PriceTypeDataSource() : base(new PriceTypeService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the PriceTypeDataSourceView used by the PriceTypeDataSource.
		/// </summary>
		protected PriceTypeDataSourceView PriceTypeView
		{
			get { return ( View as PriceTypeDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the PriceTypeDataSource control invokes to retrieve data.
		/// </summary>
		public PriceTypeSelectMethod SelectMethod
		{
			get
			{
				PriceTypeSelectMethod selectMethod = PriceTypeSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (PriceTypeSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the PriceTypeDataSourceView class that is to be
		/// used by the PriceTypeDataSource.
		/// </summary>
		/// <returns>An instance of the PriceTypeDataSourceView class.</returns>
		protected override BaseDataSourceView<PriceType, PriceTypeKey> GetNewDataSourceView()
		{
			return new PriceTypeDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the PriceTypeDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class PriceTypeDataSourceView : ProviderDataSourceView<PriceType, PriceTypeKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PriceTypeDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the PriceTypeDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public PriceTypeDataSourceView(PriceTypeDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal PriceTypeDataSource PriceTypeOwner
		{
			get { return Owner as PriceTypeDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal PriceTypeSelectMethod SelectMethod
		{
			get { return PriceTypeOwner.SelectMethod; }
			set { PriceTypeOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal PriceTypeService PriceTypeProvider
		{
			get { return Provider as PriceTypeService; }
		}

		#endregion Properties
		
		#region Methods
		
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<PriceType> GetSelectData(out int count)
		{
			Hashtable values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<PriceType> results = null;
			PriceType item;
			count = 0;
			
			System.Int32 id;

			switch ( SelectMethod )
			{
				case PriceTypeSelectMethod.Get:
					PriceTypeKey entityKey  = new PriceTypeKey();
					entityKey.Load(values);
					item = PriceTypeProvider.Get(entityKey);
					results = new TList<PriceType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case PriceTypeSelectMethod.GetAll:
                    results = PriceTypeProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case PriceTypeSelectMethod.GetPaged:
					results = PriceTypeProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case PriceTypeSelectMethod.Find:
					if ( FilterParameters != null )
						results = PriceTypeProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = PriceTypeProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case PriceTypeSelectMethod.GetByID:
					id = ( values["ID"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ID"], typeof(System.Int32)) : (int)0;
					item = PriceTypeProvider.GetByID(id);
					results = new TList<PriceType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == PriceTypeSelectMethod.Get || SelectMethod == PriceTypeSelectMethod.GetByID )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				PriceType entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					PriceTypeProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<PriceType> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			PriceTypeProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region PriceTypeDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the PriceTypeDataSource class.
	/// </summary>
	public class PriceTypeDataSourceDesigner : ProviderDataSourceDesigner<PriceType, PriceTypeKey>
	{
		/// <summary>
		/// Initializes a new instance of the PriceTypeDataSourceDesigner class.
		/// </summary>
		public PriceTypeDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public PriceTypeSelectMethod SelectMethod
		{
			get { return ((PriceTypeDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new PriceTypeDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region PriceTypeDataSourceActionList

	/// <summary>
	/// Supports the PriceTypeDataSourceDesigner class.
	/// </summary>
	internal class PriceTypeDataSourceActionList : DesignerActionList
	{
		private PriceTypeDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the PriceTypeDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public PriceTypeDataSourceActionList(PriceTypeDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public PriceTypeSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion PriceTypeDataSourceActionList
	
	#endregion PriceTypeDataSourceDesigner
	
	#region PriceTypeSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the PriceTypeDataSource.SelectMethod property.
	/// </summary>
	public enum PriceTypeSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByID method.
		/// </summary>
		GetByID
	}
	
	#endregion PriceTypeSelectMethod

	#region PriceTypeFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PriceType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PriceTypeFilter : SqlFilter<PriceTypeColumn>
	{
	}
	
	#endregion PriceTypeFilter

	#region PriceTypeProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;PriceTypeChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="PriceType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PriceTypeProperty : ChildEntityProperty<PriceTypeChildEntityTypes>
	{
	}
	
	#endregion PriceTypeProperty
}

