﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KA.BusinessLogicLayer;
using KA.DataAccessLayer;
using KA.DataAccessLayer.Bases;
using KA.ServiceLayer;
#endregion

namespace KA.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.TrainingExceptionsProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(TrainingExceptionsDataSourceDesigner))]
	public class TrainingExceptionsDataSource : ProviderDataSource<TrainingExceptions, TrainingExceptionsKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TrainingExceptionsDataSource class.
		/// </summary>
		public TrainingExceptionsDataSource() : base(new TrainingExceptionsService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the TrainingExceptionsDataSourceView used by the TrainingExceptionsDataSource.
		/// </summary>
		protected TrainingExceptionsDataSourceView TrainingExceptionsView
		{
			get { return ( View as TrainingExceptionsDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the TrainingExceptionsDataSource control invokes to retrieve data.
		/// </summary>
		public TrainingExceptionsSelectMethod SelectMethod
		{
			get
			{
				TrainingExceptionsSelectMethod selectMethod = TrainingExceptionsSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (TrainingExceptionsSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the TrainingExceptionsDataSourceView class that is to be
		/// used by the TrainingExceptionsDataSource.
		/// </summary>
		/// <returns>An instance of the TrainingExceptionsDataSourceView class.</returns>
		protected override BaseDataSourceView<TrainingExceptions, TrainingExceptionsKey> GetNewDataSourceView()
		{
			return new TrainingExceptionsDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the TrainingExceptionsDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class TrainingExceptionsDataSourceView : ProviderDataSourceView<TrainingExceptions, TrainingExceptionsKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TrainingExceptionsDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the TrainingExceptionsDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public TrainingExceptionsDataSourceView(TrainingExceptionsDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal TrainingExceptionsDataSource TrainingExceptionsOwner
		{
			get { return Owner as TrainingExceptionsDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal TrainingExceptionsSelectMethod SelectMethod
		{
			get { return TrainingExceptionsOwner.SelectMethod; }
			set { TrainingExceptionsOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal TrainingExceptionsService TrainingExceptionsProvider
		{
			get { return Provider as TrainingExceptionsService; }
		}

		#endregion Properties
		
		#region Methods
		
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<TrainingExceptions> GetSelectData(out int count)
		{
			Hashtable values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<TrainingExceptions> results = null;
			TrainingExceptions item;
			count = 0;
			
			System.Int32 id;

			switch ( SelectMethod )
			{
				case TrainingExceptionsSelectMethod.Get:
					TrainingExceptionsKey entityKey  = new TrainingExceptionsKey();
					entityKey.Load(values);
					item = TrainingExceptionsProvider.Get(entityKey);
					results = new TList<TrainingExceptions>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case TrainingExceptionsSelectMethod.GetAll:
                    results = TrainingExceptionsProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case TrainingExceptionsSelectMethod.GetPaged:
					results = TrainingExceptionsProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case TrainingExceptionsSelectMethod.Find:
					if ( FilterParameters != null )
						results = TrainingExceptionsProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = TrainingExceptionsProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case TrainingExceptionsSelectMethod.GetByID:
					id = ( values["ID"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ID"], typeof(System.Int32)) : (int)0;
					item = TrainingExceptionsProvider.GetByID(id);
					results = new TList<TrainingExceptions>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == TrainingExceptionsSelectMethod.Get || SelectMethod == TrainingExceptionsSelectMethod.GetByID )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				TrainingExceptions entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					TrainingExceptionsProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<TrainingExceptions> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			TrainingExceptionsProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region TrainingExceptionsDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the TrainingExceptionsDataSource class.
	/// </summary>
	public class TrainingExceptionsDataSourceDesigner : ProviderDataSourceDesigner<TrainingExceptions, TrainingExceptionsKey>
	{
		/// <summary>
		/// Initializes a new instance of the TrainingExceptionsDataSourceDesigner class.
		/// </summary>
		public TrainingExceptionsDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TrainingExceptionsSelectMethod SelectMethod
		{
			get { return ((TrainingExceptionsDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new TrainingExceptionsDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region TrainingExceptionsDataSourceActionList

	/// <summary>
	/// Supports the TrainingExceptionsDataSourceDesigner class.
	/// </summary>
	internal class TrainingExceptionsDataSourceActionList : DesignerActionList
	{
		private TrainingExceptionsDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the TrainingExceptionsDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public TrainingExceptionsDataSourceActionList(TrainingExceptionsDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TrainingExceptionsSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion TrainingExceptionsDataSourceActionList
	
	#endregion TrainingExceptionsDataSourceDesigner
	
	#region TrainingExceptionsSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the TrainingExceptionsDataSource.SelectMethod property.
	/// </summary>
	public enum TrainingExceptionsSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByID method.
		/// </summary>
		GetByID
	}
	
	#endregion TrainingExceptionsSelectMethod

	#region TrainingExceptionsFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TrainingExceptions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TrainingExceptionsFilter : SqlFilter<TrainingExceptionsColumn>
	{
	}
	
	#endregion TrainingExceptionsFilter

	#region TrainingExceptionsProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;TrainingExceptionsChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="TrainingExceptions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TrainingExceptionsProperty : ChildEntityProperty<TrainingExceptionsChildEntityTypes>
	{
	}
	
	#endregion TrainingExceptionsProperty
}

