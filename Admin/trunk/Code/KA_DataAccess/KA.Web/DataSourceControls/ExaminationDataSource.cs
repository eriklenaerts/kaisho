﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KA.BusinessLogicLayer;
using KA.DataAccessLayer;
using KA.DataAccessLayer.Bases;
using KA.ServiceLayer;
#endregion

namespace KA.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.ExaminationProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(ExaminationDataSourceDesigner))]
	public class ExaminationDataSource : ProviderDataSource<Examination, ExaminationKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ExaminationDataSource class.
		/// </summary>
		public ExaminationDataSource() : base(new ExaminationService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the ExaminationDataSourceView used by the ExaminationDataSource.
		/// </summary>
		protected ExaminationDataSourceView ExaminationView
		{
			get { return ( View as ExaminationDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the ExaminationDataSource control invokes to retrieve data.
		/// </summary>
		public ExaminationSelectMethod SelectMethod
		{
			get
			{
				ExaminationSelectMethod selectMethod = ExaminationSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (ExaminationSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the ExaminationDataSourceView class that is to be
		/// used by the ExaminationDataSource.
		/// </summary>
		/// <returns>An instance of the ExaminationDataSourceView class.</returns>
		protected override BaseDataSourceView<Examination, ExaminationKey> GetNewDataSourceView()
		{
			return new ExaminationDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the ExaminationDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class ExaminationDataSourceView : ProviderDataSourceView<Examination, ExaminationKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ExaminationDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the ExaminationDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public ExaminationDataSourceView(ExaminationDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal ExaminationDataSource ExaminationOwner
		{
			get { return Owner as ExaminationDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal ExaminationSelectMethod SelectMethod
		{
			get { return ExaminationOwner.SelectMethod; }
			set { ExaminationOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal ExaminationService ExaminationProvider
		{
			get { return Provider as ExaminationService; }
		}

		#endregion Properties
		
		#region Methods
		
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<Examination> GetSelectData(out int count)
		{
			Hashtable values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<Examination> results = null;
			Examination item;
			count = 0;
			
			System.Int32 id;
			System.Int32 locationID;
			System.Int32 degreeID;
			System.Int32 memberID;

			switch ( SelectMethod )
			{
				case ExaminationSelectMethod.Get:
					ExaminationKey entityKey  = new ExaminationKey();
					entityKey.Load(values);
					item = ExaminationProvider.Get(entityKey);
					results = new TList<Examination>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case ExaminationSelectMethod.GetAll:
                    results = ExaminationProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case ExaminationSelectMethod.GetPaged:
					results = ExaminationProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case ExaminationSelectMethod.Find:
					if ( FilterParameters != null )
						results = ExaminationProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = ExaminationProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case ExaminationSelectMethod.GetByID:
					id = ( values["ID"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ID"], typeof(System.Int32)) : (int)0;
					item = ExaminationProvider.GetByID(id);
					results = new TList<Examination>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				case ExaminationSelectMethod.GetByLocationID:
					locationID = ( values["LocationID"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["LocationID"], typeof(System.Int32)) : (int)0;
					results = ExaminationProvider.GetByLocationID(locationID, this.StartIndex, this.PageSize, out count);
					break;
				case ExaminationSelectMethod.GetByDegreeID:
					degreeID = ( values["DegreeID"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["DegreeID"], typeof(System.Int32)) : (int)0;
					results = ExaminationProvider.GetByDegreeID(degreeID, this.StartIndex, this.PageSize, out count);
					break;
				case ExaminationSelectMethod.GetByMemberID:
					memberID = ( values["MemberID"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["MemberID"], typeof(System.Int32)) : (int)0;
					results = ExaminationProvider.GetByMemberID(memberID, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == ExaminationSelectMethod.Get || SelectMethod == ExaminationSelectMethod.GetByID )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				Examination entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					ExaminationProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<Examination> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			ExaminationProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region ExaminationDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the ExaminationDataSource class.
	/// </summary>
	public class ExaminationDataSourceDesigner : ProviderDataSourceDesigner<Examination, ExaminationKey>
	{
		/// <summary>
		/// Initializes a new instance of the ExaminationDataSourceDesigner class.
		/// </summary>
		public ExaminationDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public ExaminationSelectMethod SelectMethod
		{
			get { return ((ExaminationDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new ExaminationDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region ExaminationDataSourceActionList

	/// <summary>
	/// Supports the ExaminationDataSourceDesigner class.
	/// </summary>
	internal class ExaminationDataSourceActionList : DesignerActionList
	{
		private ExaminationDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the ExaminationDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public ExaminationDataSourceActionList(ExaminationDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public ExaminationSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion ExaminationDataSourceActionList
	
	#endregion ExaminationDataSourceDesigner
	
	#region ExaminationSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the ExaminationDataSource.SelectMethod property.
	/// </summary>
	public enum ExaminationSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByID method.
		/// </summary>
		GetByID,
		/// <summary>
		/// Represents the GetByLocationID method.
		/// </summary>
		GetByLocationID,
		/// <summary>
		/// Represents the GetByDegreeID method.
		/// </summary>
		GetByDegreeID,
		/// <summary>
		/// Represents the GetByMemberID method.
		/// </summary>
		GetByMemberID
	}
	
	#endregion ExaminationSelectMethod

	#region ExaminationFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Examination"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ExaminationFilter : SqlFilter<ExaminationColumn>
	{
	}
	
	#endregion ExaminationFilter

	#region ExaminationProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;ExaminationChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="Examination"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ExaminationProperty : ChildEntityProperty<ExaminationChildEntityTypes>
	{
	}
	
	#endregion ExaminationProperty
}

