﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KA.BusinessLogicLayer;
using KA.DataAccessLayer;
using KA.DataAccessLayer.Bases;
using KA.ServiceLayer;
#endregion

namespace KA.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.MemberProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(MemberDataSourceDesigner))]
	public class MemberDataSource : ProviderDataSource<Member, MemberKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MemberDataSource class.
		/// </summary>
		public MemberDataSource() : base(new MemberService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the MemberDataSourceView used by the MemberDataSource.
		/// </summary>
		protected MemberDataSourceView MemberView
		{
			get { return ( View as MemberDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the MemberDataSource control invokes to retrieve data.
		/// </summary>
		public MemberSelectMethod SelectMethod
		{
			get
			{
				MemberSelectMethod selectMethod = MemberSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (MemberSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the MemberDataSourceView class that is to be
		/// used by the MemberDataSource.
		/// </summary>
		/// <returns>An instance of the MemberDataSourceView class.</returns>
		protected override BaseDataSourceView<Member, MemberKey> GetNewDataSourceView()
		{
			return new MemberDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the MemberDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class MemberDataSourceView : ProviderDataSourceView<Member, MemberKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MemberDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the MemberDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public MemberDataSourceView(MemberDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal MemberDataSource MemberOwner
		{
			get { return Owner as MemberDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal MemberSelectMethod SelectMethod
		{
			get { return MemberOwner.SelectMethod; }
			set { MemberOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal MemberService MemberProvider
		{
			get { return Provider as MemberService; }
		}

		#endregion Properties
		
		#region Methods
		
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<Member> GetSelectData(out int count)
		{
			Hashtable values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<Member> results = null;
			Member item;
			count = 0;
			
			System.Int32 id;
			System.Int32? paymentId;
			System.Int32? groupID;
			System.Int32? trainingLocationId;

			switch ( SelectMethod )
			{
				case MemberSelectMethod.Get:
					MemberKey entityKey  = new MemberKey();
					entityKey.Load(values);
					item = MemberProvider.Get(entityKey);
					results = new TList<Member>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case MemberSelectMethod.GetAll:
                    results = MemberProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case MemberSelectMethod.GetPaged:
					results = MemberProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case MemberSelectMethod.Find:
					if ( FilterParameters != null )
						results = MemberProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = MemberProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case MemberSelectMethod.GetByID:
					id = ( values["ID"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ID"], typeof(System.Int32)) : (int)0;
					item = MemberProvider.GetByID(id);
					results = new TList<Member>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				case MemberSelectMethod.GetByPaymentId:
					paymentId = (System.Int32?) EntityUtil.ChangeType(values["PaymentId"], typeof(System.Int32?));
					results = MemberProvider.GetByPaymentId(paymentId, this.StartIndex, this.PageSize, out count);
					break;
				case MemberSelectMethod.GetByGroupID:
					groupID = (System.Int32?) EntityUtil.ChangeType(values["GroupID"], typeof(System.Int32?));
					results = MemberProvider.GetByGroupID(groupID, this.StartIndex, this.PageSize, out count);
					break;
				case MemberSelectMethod.GetByTrainingLocationId:
					trainingLocationId = (System.Int32?) EntityUtil.ChangeType(values["TrainingLocationId"], typeof(System.Int32?));
					results = MemberProvider.GetByTrainingLocationId(trainingLocationId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == MemberSelectMethod.Get || SelectMethod == MemberSelectMethod.GetByID )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				Member entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					MemberProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<Member> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			MemberProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region MemberDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the MemberDataSource class.
	/// </summary>
	public class MemberDataSourceDesigner : ProviderDataSourceDesigner<Member, MemberKey>
	{
		/// <summary>
		/// Initializes a new instance of the MemberDataSourceDesigner class.
		/// </summary>
		public MemberDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public MemberSelectMethod SelectMethod
		{
			get { return ((MemberDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new MemberDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region MemberDataSourceActionList

	/// <summary>
	/// Supports the MemberDataSourceDesigner class.
	/// </summary>
	internal class MemberDataSourceActionList : DesignerActionList
	{
		private MemberDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the MemberDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public MemberDataSourceActionList(MemberDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public MemberSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion MemberDataSourceActionList
	
	#endregion MemberDataSourceDesigner
	
	#region MemberSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the MemberDataSource.SelectMethod property.
	/// </summary>
	public enum MemberSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByID method.
		/// </summary>
		GetByID,
		/// <summary>
		/// Represents the GetByPaymentId method.
		/// </summary>
		GetByPaymentId,
		/// <summary>
		/// Represents the GetByGroupID method.
		/// </summary>
		GetByGroupID,
		/// <summary>
		/// Represents the GetByTrainingLocationId method.
		/// </summary>
		GetByTrainingLocationId
	}
	
	#endregion MemberSelectMethod

	#region MemberFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Member"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MemberFilter : SqlFilter<MemberColumn>
	{
	}
	
	#endregion MemberFilter

	#region MemberProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;MemberChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="Member"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MemberProperty : ChildEntityProperty<MemberChildEntityTypes>
	{
	}
	
	#endregion MemberProperty
}

