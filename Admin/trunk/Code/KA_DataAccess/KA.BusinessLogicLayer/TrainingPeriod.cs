﻿#region Using directives

using System;

#endregion

namespace KA.BusinessLogicLayer
{	
	///<summary>
	/// An object representation of the 'TrainingPeriod' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class TrainingPeriod : TrainingPeriodBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="TrainingPeriod"/> instance.
		///</summary>
		public TrainingPeriod():base(){}	
		
		#endregion
	}
}
