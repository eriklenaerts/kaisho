﻿#region Using directives

using System;

#endregion

namespace KA.BusinessLogicLayer
{	
	///<summary>
	/// An object representation of the 'Degree' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class Degree : DegreeBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="Degree"/> instance.
		///</summary>
		public Degree():base(){}	
		
		#endregion
	}
}
