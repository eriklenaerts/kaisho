﻿#region Using directives

using System;

#endregion

namespace KA.BusinessLogicLayer
{	
	///<summary>
	/// An object representation of the 'PaymentType' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class PaymentType : PaymentTypeBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="PaymentType"/> instance.
		///</summary>
		public PaymentType():base(){}	
		
		#endregion
	}
}
