﻿#region Using directives

using System;

#endregion

namespace KA.BusinessLogicLayer
{	
	///<summary>
	/// An object representation of the 'TrainingTime' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class TrainingTime : TrainingTimeBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="TrainingTime"/> instance.
		///</summary>
		public TrainingTime():base(){}	
		
		#endregion
	}
}
