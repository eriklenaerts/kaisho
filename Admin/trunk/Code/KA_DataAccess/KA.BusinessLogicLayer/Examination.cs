﻿#region Using directives

using System;

#endregion

namespace KA.BusinessLogicLayer
{	
	///<summary>
	/// An object representation of the 'Examination' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class Examination : ExaminationBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="Examination"/> instance.
		///</summary>
		public Examination():base(){}	
		
		#endregion
	}
}
