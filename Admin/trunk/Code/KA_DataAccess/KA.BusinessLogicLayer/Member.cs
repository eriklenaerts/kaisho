﻿#region Using directives

using System;
using KA.BusinessLogicLayer.Validation;

#endregion

namespace KA.BusinessLogicLayer
{	
	///<summary>
	/// An object representation of the 'Member' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class Member : MemberBase
	{
        private string _fullName;

        public string FullName
        {
            get { return this.Firstname + " " + this.Lastname; }
        }
	
		#region Constructors

		///<summary>
		/// Creates a new <see cref="Member"/> instance.
		///</summary>
		public Member():base(){}


        private void AddValidation()
        {
           
        }

        protected override void AddValidationRules()
        {
            base.AddValidationRules();

            //Add custom validation rules
            Validation.ValidationRuleArgs firstNameArgs = new ValidationRuleArgs("Firstname");
            firstNameArgs.Description = "Voornaam is een verplicht veld";
            this.ValidationRules.AddRule(Validation.CommonRules.StringRequired, firstNameArgs);
            Validation.ValidationRuleArgs LastNameArgs = new ValidationRuleArgs("Lastname");
            LastNameArgs.Description = "Achternaam is een verplicht veld.";
            this.ValidationRules.AddRule(Validation.CommonRules.StringRequired, LastNameArgs);

            //ValidationRules.AddRule(ValidateOrderDate, "OrderDate");
        }




		#endregion
	}
}
