﻿#region Using directives

using System;

#endregion

namespace KA.BusinessLogicLayer
{	
	///<summary>
	/// An object representation of the 'PriceType' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class PriceType : PriceTypeBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="PriceType"/> instance.
		///</summary>
		public PriceType():base(){}	
		
		#endregion
	}
}
