﻿	
/*
	File generated by NetTiers templates [www.nettiers.com]
	Generated on : woensdag 7 mei 2008
	Important: Do not modify this file. Edit the file PriceType.cs instead.
*/

#region using directives

using System;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Runtime.Serialization;

#endregion

namespace KA.BusinessLogicLayer
{
	#region PriceTypeEventArgs class
	/// <summary>
	/// Provides data for the ColumnChanging and ColumnChanged events.
	/// </summary>
	/// <remarks>
	/// The ColumnChanging and ColumnChanged events occur when a change is made to the value 
	/// of a property of a <see cref="PriceType"/> object.
	/// </remarks>
	public class PriceTypeEventArgs : System.EventArgs
	{
		private PriceTypeColumn column;
		private object value;
		
		
		///<summary>
		/// Initalizes a new Instance of the PriceTypeEventArgs class.
		///</summary>
		public PriceTypeEventArgs(PriceTypeColumn column)
		{
			this.column = column;
		}
		
		///<summary>
		/// Initalizes a new Instance of the PriceTypeEventArgs class.
		///</summary>
		public PriceTypeEventArgs(PriceTypeColumn column, object value)
		{
			this.column = column;
			this.value = value;
		}
		
		
		///<summary>
		/// The PriceTypeColumn that was modified, which has raised the event.
		///</summary>
		///<value cref="PriceTypeColumn" />
		public PriceTypeColumn Column { get { return this.column; } }
		
		/// <summary>
        /// Gets the current value of the column.
        /// </summary>
        /// <value>The current value of the column.</value>
		public object Value{ get { return this.value; } }

	}
	#endregion
	
	
	///<summary>
	/// Define a delegate for all PriceType related events.
	///</summary>
	public delegate void PriceTypeEventHandler(object sender, PriceTypeEventArgs e);
	
	///<summary>
	/// An object representation of the 'PriceType' table. [No description found the database]	
	///</summary>
	[Serializable, DataObject]
	[CLSCompliant(true)]
	public abstract partial class PriceTypeBase : EntityBase, IEntityId<PriceTypeKey>, System.IComparable, System.ICloneable, IEditableObject, IComponent, INotifyPropertyChanged
	{		
		#region Variable Declarations
		
		/// <summary>
		///  Hold the inner data of the entity.
		/// </summary>
		private PriceTypeEntityData entityData;
		
		/// <summary>
		/// 	Hold the original data of the entity, as loaded from the repository.
		/// </summary>
		private PriceTypeEntityData _originalData;
		
		/// <summary>
		/// 	Hold a backup of the inner data of the entity.
		/// </summary>
		private PriceTypeEntityData backupData; 
		
		/// <summary>
		/// 	Key used if Tracking is Enabled for the <see cref="EntityLocator" />.
		/// </summary>
		private string entityTrackingKey;
		
		/// <summary>
		/// 	Hold the parent TList&lt;entity&gt; in which this entity maybe contained.
		/// </summary>
		/// <remark>Mostly used for databinding</remark>
		[NonSerialized]
		private TList<PriceType> parentCollection;
		
		private bool inTxn = false;
		
		/// <summary>
		/// Occurs when a value is being changed for the specified column.
		/// </summary>	
		[field:NonSerialized]
		public event PriceTypeEventHandler ColumnChanging;		
		
		/// <summary>
		/// Occurs after a value has been changed for the specified column.
		/// </summary>
		[field:NonSerialized]
		public event PriceTypeEventHandler ColumnChanged;
		
		#endregion Variable Declarations
		
		#region Constructors
		///<summary>
		/// Creates a new <see cref="PriceTypeBase"/> instance.
		///</summary>
		public PriceTypeBase()
		{
			this.entityData = new PriceTypeEntityData();
			this.backupData = null;
		}		
		
		///<summary>
		/// Creates a new <see cref="PriceTypeBase"/> instance.
		///</summary>
		///<param name="priceTypeID"></param>
		///<param name="priceTypeName"></param>
		public PriceTypeBase(System.Int32 priceTypeID, System.String priceTypeName)
		{
			this.entityData = new PriceTypeEntityData();
			this.backupData = null;

			this.ID = priceTypeID;
			this.Name = priceTypeName;
		}
		
		///<summary>
		/// A simple factory method to create a new <see cref="PriceType"/> instance.
		///</summary>
		///<param name="priceTypeID"></param>
		///<param name="priceTypeName"></param>
		public static PriceType CreatePriceType(System.Int32 priceTypeID, System.String priceTypeName)
		{
			PriceType newPriceType = new PriceType();
			newPriceType.ID = priceTypeID;
			newPriceType.Name = priceTypeName;
			return newPriceType;
		}
				
		#endregion Constructors
		
		
		#region Events trigger
		/// <summary>
		/// Raises the <see cref="ColumnChanging" /> event.
		/// </summary>
		/// <param name="column">The <see cref="PriceTypeColumn"/> which has raised the event.</param>
		public void OnColumnChanging(PriceTypeColumn column)
		{
			OnColumnChanging(column, null);
			return;
		}
		
		/// <summary>
		/// Raises the <see cref="ColumnChanged" /> event.
		/// </summary>
		/// <param name="column">The <see cref="PriceTypeColumn"/> which has raised the event.</param>
		public void OnColumnChanged(PriceTypeColumn column)
		{
			OnColumnChanged(column, null);
			return;
		} 
		
		
		/// <summary>
		/// Raises the <see cref="ColumnChanging" /> event.
		/// </summary>
		/// <param name="column">The <see cref="PriceTypeColumn"/> which has raised the event.</param>
		/// <param name="value">The changed value.</param>
		public void OnColumnChanging(PriceTypeColumn column, object value)
		{
			if(IsEntityTracked && EntityState != EntityState.Added)
				EntityManager.StopTracking(EntityTrackingKey);
				
			if (!SuppressEntityEvents)
			{
				PriceTypeEventHandler handler = ColumnChanging;
				if(handler != null)
				{
					handler(this, new PriceTypeEventArgs(column, value));
				}
			}
		}
		
		/// <summary>
		/// Raises the <see cref="ColumnChanged" /> event.
		/// </summary>
		/// <param name="column">The <see cref="PriceTypeColumn"/> which has raised the event.</param>
		/// <param name="value">The changed value.</param>
		public void OnColumnChanged(PriceTypeColumn column, object value)
		{
			if (!SuppressEntityEvents)
			{
				PriceTypeEventHandler handler = ColumnChanged;
				if(handler != null)
				{
					handler(this, new PriceTypeEventArgs(column, value));
				}
			
				// warn the parent list that i have changed
				OnEntityChanged();
			}
		} 
		#endregion
				
		#region Properties	
				
		/// <summary>
		/// 	Gets or sets the ID property. 
		///		
		/// </summary>
		/// <value>This type is int.</value>
		/// <remarks>
		/// This property can not be set to null. 
		/// </remarks>
		[DescriptionAttribute(""), BindableAttribute()]
		[DataObjectField(true, false, false)]
		public virtual System.Int32 ID
		{
			get
			{
				return this.entityData.ID; 
			}
			
			set
			{
				if (this.entityData.ID == value)
					return;
					
					
				OnColumnChanging(PriceTypeColumn.ID, this.entityData.ID);
				this.entityData.ID = value;
				this.EntityId.ID = value;
				if (this.EntityState == EntityState.Unchanged)
				{
					this.EntityState = EntityState.Changed;
				}
				OnColumnChanged(PriceTypeColumn.ID, this.entityData.ID);
				OnPropertyChanged("ID");
			}
		}
		
		/// <summary>
		/// 	Get the original value of the ID property.
		///		
		/// </summary>
		/// <remarks>This is the original value of the ID property.</remarks>
		/// <value>This type is int</value>
		[BrowsableAttribute(false)/*, XmlIgnoreAttribute()*/]
		public  virtual System.Int32 OriginalID
		{
			get { return this.entityData.OriginalID; }
			set { this.entityData.OriginalID = value; }
		}
		
		/// <summary>
		/// 	Gets or sets the Name property. 
		///		
		/// </summary>
		/// <value>This type is nvarchar.</value>
		/// <remarks>
		/// This property can not be set to null. 
		/// </remarks>
		/// <exception cref="ArgumentNullException">If you attempt to set to null.</exception>
		[DescriptionAttribute(""), BindableAttribute()]
		[DataObjectField(false, false, false, 100)]
		public virtual System.String Name
		{
			get
			{
				return this.entityData.Name; 
			}
			
			set
			{
				if (this.entityData.Name == value)
					return;
					
					
				OnColumnChanging(PriceTypeColumn.Name, this.entityData.Name);
				this.entityData.Name = value;
				if (this.EntityState == EntityState.Unchanged)
				{
					this.EntityState = EntityState.Changed;
				}
				OnColumnChanged(PriceTypeColumn.Name, this.entityData.Name);
				OnPropertyChanged("Name");
			}
		}
		

		#region Source Foreign Key Property
				
		#endregion
			
		#region Table Meta Data
		/// <summary>
		///		The name of the underlying database table.
		/// </summary>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public override string TableName
		{
			get { return "PriceType"; }
		}
		
		/// <summary>
		///		The name of the underlying database table's columns.
		/// </summary>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public override string[] TableColumns
		{
			get
			{
				return new string[] {"ID", "Name"};
			}
		}
		#endregion 
		
	
		/// <summary>
		///	Holds a collection of PriceScheme objects
		///	which are related to this object through the relation FK_PriceScheme_PriceType
		/// </summary>	
		[BindableAttribute()]
		public TList<PriceScheme> PriceSchemeCollection
		{
			get { return entityData.PriceSchemeCollection; }
			set { entityData.PriceSchemeCollection = value; }	
		}
		
		#endregion
		
		#region IEditableObject
		
		#region  CancelAddNew Event
		/// <summary>
        /// The delegate for the CancelAddNew event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		public delegate void CancelAddNewEventHandler(object sender, EventArgs e);
    
    	/// <summary>
		/// The CancelAddNew event.
		/// </summary>
		[field:NonSerialized]
		public event CancelAddNewEventHandler CancelAddNew ;

		/// <summary>
        /// Called when [cancel add new].
        /// </summary>
        public void OnCancelAddNew()
        {    
			if (!SuppressEntityEvents)
			{
	            CancelAddNewEventHandler handler = CancelAddNew ;
            	if (handler != null)
	            {    
    	            handler(this, EventArgs.Empty) ;
        	    }
	        }
        }
		#endregion 
		
		/// <summary>
		/// Begins an edit on an object.
		/// </summary>
		void IEditableObject.BeginEdit() 
	    {
	        //Console.WriteLine("Start BeginEdit");
	        if (!inTxn) 
	        {
	            this.backupData = this.entityData.Clone() as PriceTypeEntityData;
	            inTxn = true;
	            //Console.WriteLine("BeginEdit");
	        }
	        //Console.WriteLine("End BeginEdit");
	    }
	
		/// <summary>
		/// Discards changes since the last <c>BeginEdit</c> call.
		/// </summary>
	    void IEditableObject.CancelEdit() 
	    {
	        //Console.WriteLine("Start CancelEdit");
	        if (this.inTxn) 
	        {
	            this.entityData = this.backupData;
	            this.backupData = null;
				this.inTxn = false;

				if (this.bindingIsNew)
	        	//if (this.EntityState == EntityState.Added)
	        	{
					if (this.parentCollection != null)
						this.parentCollection.Remove( (PriceType) this ) ;
				}	            
	        }
	        //Console.WriteLine("End CancelEdit");
	    }
	
		/// <summary>
		/// Pushes changes since the last <c>BeginEdit</c> or <c>IBindingList.AddNew</c> call into the underlying object.
		/// </summary>
	    void IEditableObject.EndEdit() 
	    {
	        //Console.WriteLine("Start EndEdit" + this.custData.id + this.custData.lastName);
	        if (this.inTxn) 
	        {
	            this.backupData = null;
				if (this.IsDirty) 
				{
					if (this.bindingIsNew) {
						this.EntityState = EntityState.Added;
						this.bindingIsNew = false ;
					}
					else
						if (this.EntityState == EntityState.Unchanged) 
							this.EntityState = EntityState.Changed ;
				}

				this.bindingIsNew = false ;
	            this.inTxn = false;	            
	        }
	        //Console.WriteLine("End EndEdit");
	    }
	    
	    /// <summary>
        /// Gets or sets the parent collection.
        /// </summary>
        /// <value>The parent collection.</value>
	    [XmlIgnore]
		[Browsable(false)]
	    public override object ParentCollection
	    {
	        get 
	        {
	            return (object)this.parentCollection;
	        }
	        set 
	        {
	            this.parentCollection = value as TList<PriceType>;
	        }
	    }
	    
	    /// <summary>
        /// Called when the entity is changed.
        /// </summary>
	    private void OnEntityChanged() 
	    {
	        if (!SuppressEntityEvents && !inTxn && this.parentCollection != null) 
	        {
	            this.parentCollection.EntityChanged(this as PriceType);
	        }
	    }


		#endregion
		
		#region Methods	
			
		///<summary>
		///  Revert all changes and restore original values.
		///</summary>
		public override void CancelChanges()
		{
			IEditableObject obj = (IEditableObject) this;
			obj.CancelEdit();

			this.entityData = null;
			this.entityData = this._originalData.Clone() as PriceTypeEntityData;
		}	
		
		/// <summary>
		/// Accepts the changes made to this object.
		/// </summary>
		/// <remarks>
		/// After calling this method, properties: IsDirty, IsNew are false. IsDeleted flag remains unchanged as it is handled by the parent List.
		/// </remarks>
		public override void AcceptChanges()
		{
			base.AcceptChanges();

			// we keep of the original version of the data
			this._originalData = null;
			this._originalData = this.entityData.Clone() as PriceTypeEntityData;
		}
		
		#region Comparision with original data
		
		/// <summary>
		/// Determines whether the property value has changed from the original data.
		/// </summary>
		/// <param name="column">The column.</param>
		/// <returns>
		/// 	<c>true</c> if the property value has changed; otherwise, <c>false</c>.
		/// </returns>
		public bool IsPropertyChanged(PriceTypeColumn column)
		{
			switch(column)
			{
					case PriceTypeColumn.ID:
					return entityData.ID != _originalData.ID;
					case PriceTypeColumn.Name:
					return entityData.Name != _originalData.Name;
			
				default:
					return false;
			}
		}
		
		/// <summary>
		/// Determines whether the data has changed from original.
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [has data changed]; otherwise, <c>false</c>.
		/// </returns>
		public bool HasDataChanged()
		{
			bool result = false;
			result = result || entityData.ID != _originalData.ID;
			result = result || entityData.Name != _originalData.Name;
			return result;
}	
		
		#endregion
		
		#region ICloneable Members
		///<summary>
		///  Returns a Typed PriceType Entity 
		///</summary>
		public virtual PriceType Copy()
		{
			//shallow copy entity
			PriceType copy = new PriceType();
			copy.ID = this.ID;
			copy.OriginalID = this.OriginalID;
			copy.Name = this.Name;
					
			copy.AcceptChanges();
			return (PriceType)copy;
		}
		
		///<summary>
		/// ICloneable.Clone() Member, returns the Shallow Copy of this entity.
		///</summary>
		public object Clone()
		{
			return this.Copy();
		}
		
		///<summary>
		/// Returns a deep copy of the child collection object passed in.
		///</summary>
		public static object MakeCopyOf(object x)
		{
			if (x is ICloneable)
			{
				// Return a deep copy of the object
				return ((ICloneable)x).Clone();
			}
			else
				throw new System.NotSupportedException("Object Does Not Implement the ICloneable Interface.");
		}
		
		///<summary>
		///  Returns a Typed PriceType Entity which is a deep copy of the current entity.
		///</summary>
		public virtual PriceType DeepCopy()
		{
			return EntityHelper.Clone<PriceType>(this as PriceType);	
		}
		#endregion
		
		///<summary>
		/// Returns a value indicating whether this instance is equal to a specified object.
		///</summary>
		///<param name="toObject">An object to compare to this instance.</param>
		///<returns>true if toObject is a <see cref="PriceTypeBase"/> and has the same value as this instance; otherwise, false.</returns>
		public virtual bool Equals(PriceTypeBase toObject)
		{
			if (toObject == null)
				return false;
			return Equals(this, toObject);
		}
		
		
		///<summary>
		/// Determines whether the specified <see cref="PriceTypeBase"/> instances are considered equal.
		///</summary>
		///<param name="Object1">The first <see cref="PriceTypeBase"/> to compare.</param>
		///<param name="Object2">The second <see cref="PriceTypeBase"/> to compare. </param>
		///<returns>true if Object1 is the same instance as Object2 or if both are null references or if objA.Equals(objB) returns true; otherwise, false.</returns>
		public static bool Equals(PriceTypeBase Object1, PriceTypeBase Object2)
		{
			// both are null
			if (Object1 == null && Object2 == null)
				return true;

			// one or the other is null, but not both
			if (Object1 == null ^ Object2 == null)
				return false;
				
			bool equal = true;
			if (Object1.ID != Object2.ID)
				equal = false;
			if (Object1.Name != Object2.Name)
				equal = false;
			return equal;
		}
		
		#endregion
		
		#region IComparable Members
		///<summary>
		/// Compares this instance to a specified object and returns an indication of their relative values.
		///<param name="obj">An object to compare to this instance, or a null reference (Nothing in Visual Basic).</param>
		///</summary>
		///<returns>A signed integer that indicates the relative order of this instance and obj.</returns>
		public virtual int CompareTo(object obj)
		{
			throw new NotImplementedException();
			// TODO -> generate a strongly typed IComparer in the concrete class
			//return this. GetPropertyName(SourceTable.PrimaryKey.MemberColumns[0].Name) .CompareTo(((PriceTypeBase)obj).GetPropertyName(SourceTable.PrimaryKey.MemberColumns[0].Name));
		}
		
		/*
		// static method to get a Comparer object
        public static PriceTypeComparer GetComparer()
        {
            return new PriceTypeComparer();
        }
        */

        // Comparer delegates back to PriceType
        // Employee uses the integer's default
        // CompareTo method
        /*
        public int CompareTo(Item rhs)
        {
            return this.Id.CompareTo(rhs.Id);
        }
        */

/*
        // Special implementation to be called by custom comparer
        public int CompareTo(PriceType rhs, PriceTypeColumn which)
        {
            switch (which)
            {
            	
            	
            	case PriceTypeColumn.ID:
            		return this.ID.CompareTo(rhs.ID);
            		
            		                 
            	
            	
            	case PriceTypeColumn.Name:
            		return this.Name.CompareTo(rhs.Name);
            		
            		                 
            }
            return 0;
        }
        */
	
		#endregion
		
		#region IComponent Members
		
		private ISite _site = null;

		/// <summary>
		/// Gets or Sets the site where this data is located.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public ISite Site
		{
			get{ return this._site; }
			set{ this._site = value; }
		}

		#endregion

		#region IDisposable Members
		
		/// <summary>
		/// Notify those that care when we dispose.
		/// </summary>
		[field:NonSerialized]
		public event System.EventHandler Disposed;

		/// <summary>
		/// Clean up. Nothing here though.
		/// </summary>
		public void Dispose()
		{
			this.parentCollection = null;
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}
		
		/// <summary>
		/// Clean up.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				EventHandler handler = Disposed;
				if (handler != null)
					handler(this, EventArgs.Empty);
			}
		}
		
		#endregion
				
		#region IEntityKey<PriceTypeKey> Members
		
		// member variable for the EntityId property
		private PriceTypeKey _entityId;

		/// <summary>
		/// Gets or sets the EntityId property.
		/// </summary>
		[XmlIgnore]
		public PriceTypeKey EntityId
		{
			get
			{
				if ( _entityId == null )
				{
					_entityId = new PriceTypeKey(this);
				}

				return _entityId;
			}
			set
			{
				if ( value != null )
				{
					value.Entity = this;
				}
				
				_entityId = value;
			}
		}
		
		#endregion
		
		#region EntityTrackingKey
		///<summary>
		/// Provides the tracking key for the <see cref="EntityLocator"/>
		///</summary>
		[XmlIgnore]
		public override string EntityTrackingKey
		{
			get
			{
				if(entityTrackingKey == null)
					entityTrackingKey = @"PriceType" 
					+ this.ID.ToString();
				return entityTrackingKey;
			}
			set
		    {
		        if (value != null)
                    entityTrackingKey = value;
		    }
		}
		#endregion 
		
		#region ToString Method
		
		///<summary>
		/// Returns a String that represents the current object.
		///</summary>
		public override string ToString()
		{
			return string.Format(System.Globalization.CultureInfo.InvariantCulture,
				"{3}{2}- ID: {0}{2}- Name: {1}{2}", 
				this.ID,
				this.Name,
				Environment.NewLine, 
				this.GetType());
		}
		
		#endregion ToString Method
		
		#region Inner data class
		
	/// <summary>
	///		The data structure representation of the 'PriceType' table.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	[EditorBrowsable(EditorBrowsableState.Never)]
	[Serializable]
	internal protected class PriceTypeEntityData : ICloneable
	{
		#region Variable Declarations
		
		#region Primary key(s)
			/// <summary>			
			/// ID : 
			/// </summary>
			/// <remarks>Member of the primary key of the underlying table "PriceType"</remarks>
			public System.Int32 ID;
				
			/// <summary>
			/// keep a copy of the original so it can be used for editable primary keys.
			/// </summary>
			public System.Int32 OriginalID;
			
		#endregion
		
		#region Non Primary key(s)
		
		
		/// <summary>
		/// Name : 
		/// </summary>
		public System.String		  Name = string.Empty;
		#endregion
			
		#endregion Variable Declarations
		
		#region Clone Method

		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public Object Clone()
		{
			PriceTypeEntityData _tmp = new PriceTypeEntityData();
						
			_tmp.ID = this.ID;
			_tmp.OriginalID = this.OriginalID;
			
			_tmp.Name = this.Name;
			
			return _tmp;
		}
		
		#endregion Clone Method
		
		#region Data Properties

		#region PriceSchemeCollection
		
		private TList<PriceScheme> priceSchemePriceTypeID;
		
		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation priceSchemePriceTypeID
		/// </summary>	
		public TList<PriceScheme> PriceSchemeCollection
		{
			get
			{
				if (priceSchemePriceTypeID == null)
				{
				priceSchemePriceTypeID = new TList<PriceScheme>();
				}
	
				return priceSchemePriceTypeID;
			}
			set { priceSchemePriceTypeID = value; }
		}
		
		#endregion

		#endregion Data Properties

	}//End struct


		#endregion
		
		#region Validation
		
		/// <summary>
		/// Assigns validation rules to this object based on model definition.
		/// </summary>
		/// <remarks>This method overrides the base class to add schema related validation.</remarks>
		protected override void AddValidationRules()
		{
			//Validation rules based on database schema.
			ValidationRules.AddRule(Validation.CommonRules.NotNull,"Name");
			ValidationRules.AddRule(Validation.CommonRules.StringMaxLength,new Validation.CommonRules.MaxLengthRuleArgs("Name",100));
		}
   		#endregion
	
	} // End Class
	
	#region PriceTypeComparer
		
	/// <summary>
	///	Strongly Typed IComparer
	/// </summary>
	public class PriceTypeComparer : System.Collections.Generic.IComparer<PriceType>
	{
		PriceTypeColumn whichComparison;
		
		/// <summary>
        /// Initializes a new instance of the <see cref="T:PriceTypeComparer"/> class.
        /// </summary>
		public PriceTypeComparer()
        {            
        }               
        
        /// <summary>
        /// Initializes a new instance of the <see cref="T:%=className%>Comparer"/> class.
        /// </summary>
        /// <param name="column">The column to sort on.</param>
        public PriceTypeComparer(PriceTypeColumn column)
        {
            this.whichComparison = column;
        }

		/// <summary>
        /// Determines whether the specified <c cref="PriceType"/> instances are considered equal.
        /// </summary>
        /// <param name="a">The first <c cref="PriceType"/> to compare.</param>
        /// <param name="b">The second <c>PriceType</c> to compare.</param>
        /// <returns>true if objA is the same instance as objB or if both are null references or if objA.Equals(objB) returns true; otherwise, false.</returns>
        public bool Equals(PriceType a, PriceType b)
        {
            return this.Compare(a, b) == 0;
        }

		/// <summary>
        /// Gets the hash code of the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public int GetHashCode(PriceType entity)
        {
            return entity.GetHashCode();
        }

        /// <summary>
        /// Performs a case-insensitive comparison of two objects of the same type and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <param name="a">The first object to compare.</param>
        /// <param name="b">The second object to compare.</param>
        /// <returns></returns>
        public int Compare(PriceType a, PriceType b)
        {
        	EntityPropertyComparer entityPropertyComparer = new EntityPropertyComparer(this.whichComparison.ToString());
        	return entityPropertyComparer.Compare(a, b);
        }

		/// <summary>
        /// Gets or sets the column that will be used for comparison.
        /// </summary>
        /// <value>The comparison column.</value>
        public PriceTypeColumn WhichComparison
        {
            get { return this.whichComparison; }
            set { this.whichComparison = value; }
        }
	}
	
	#endregion
	
	#region PriceTypeKey Class

	/// <summary>
	/// Wraps the unique identifier values for the <see cref="PriceType"/> object.
	/// </summary>
	[Serializable]
	[CLSCompliant(true)]
	public class PriceTypeKey : EntityKeyBase
	{
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the PriceTypeKey class.
		/// </summary>
		public PriceTypeKey()
		{
		}
		
		/// <summary>
		/// Initializes a new instance of the PriceTypeKey class.
		/// </summary>
		public PriceTypeKey(PriceTypeBase entity)
		{
			Entity = entity;

			#region Init Properties

			if ( entity != null )
			{
				this.id = entity.ID;
			}

			#endregion
		}
		
		/// <summary>
		/// Initializes a new instance of the PriceTypeKey class.
		/// </summary>
		public PriceTypeKey(System.Int32 id)
		{
			#region Init Properties

			this.id = id;

			#endregion
		}
		
		#endregion Constructors

		#region Properties
		
		// member variable for the Entity property
		private PriceTypeBase _entity;
		
		/// <summary>
		/// Gets or sets the Entity property.
		/// </summary>
		public PriceTypeBase Entity
		{
			get { return _entity; }
			set { _entity = value; }
		}
		
		// member variable for the ID property
		private System.Int32 id;
		
		/// <summary>
		/// Gets or sets the ID property.
		/// </summary>
		public System.Int32 ID
		{
			get { return id; }
			set
			{
				if ( Entity != null )
				{
					Entity.ID = value;
				}
				
				id = value;
			}
		}
		
		#endregion

		#region Methods
		
		/// <summary>
		/// Reads values from the supplied <see cref="IDictionary"/> object into
		/// properties of the current object.
		/// </summary>
		/// <param name="values">An <see cref="IDictionary"/> instance that contains
		/// the key/value pairs to be used as property values.</param>
		public override void Load(IDictionary values)
		{
			#region Init Properties

			if ( values != null )
			{
				ID = ( values["ID"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ID"], typeof(System.Int32)) : (int)0;
			}

			#endregion
		}

		/// <summary>
		/// Creates a new <see cref="IDictionary"/> object and populates it
		/// with the property values of the current object.
		/// </summary>
		/// <returns>A collection of name/value pairs.</returns>
		public override IDictionary ToDictionary()
		{
			IDictionary values = new Hashtable();

			#region Init Dictionary

			values.Add("ID", ID);

			#endregion Init Dictionary

			return values;
		}
		
		///<summary>
		/// Returns a String that represents the current object.
		///</summary>
		public override string ToString()
		{
			return String.Format("ID: {0}{1}",
								ID,
								Environment.NewLine);
		}

		#endregion Methods
	}
	
	#endregion	

	#region PriceTypeColumn Enum
	
	/// <summary>
	/// Enumerate the PriceType columns.
	/// </summary>
	[Serializable]
	public enum PriceTypeColumn : int
	{
		/// <summary>
		/// ID : 
		/// </summary>
		[EnumTextValue("ID")]
		[ColumnEnum("ID", typeof(System.Int32), System.Data.DbType.Int32, true, false, false)]
		ID = 1,
		/// <summary>
		/// Name : 
		/// </summary>
		[EnumTextValue("Name")]
		[ColumnEnum("Name", typeof(System.String), System.Data.DbType.String, false, false, false, 100)]
		Name = 2
	}//End enum

	#endregion PriceTypeColumn Enum

} // end namespace
