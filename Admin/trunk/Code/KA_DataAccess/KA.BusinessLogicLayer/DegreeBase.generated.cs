﻿	
/*
	File generated by NetTiers templates [www.nettiers.com]
	Generated on : woensdag 7 mei 2008
	Important: Do not modify this file. Edit the file Degree.cs instead.
*/

#region using directives

using System;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Runtime.Serialization;

#endregion

namespace KA.BusinessLogicLayer
{
	#region DegreeEventArgs class
	/// <summary>
	/// Provides data for the ColumnChanging and ColumnChanged events.
	/// </summary>
	/// <remarks>
	/// The ColumnChanging and ColumnChanged events occur when a change is made to the value 
	/// of a property of a <see cref="Degree"/> object.
	/// </remarks>
	public class DegreeEventArgs : System.EventArgs
	{
		private DegreeColumn column;
		private object value;
		
		
		///<summary>
		/// Initalizes a new Instance of the DegreeEventArgs class.
		///</summary>
		public DegreeEventArgs(DegreeColumn column)
		{
			this.column = column;
		}
		
		///<summary>
		/// Initalizes a new Instance of the DegreeEventArgs class.
		///</summary>
		public DegreeEventArgs(DegreeColumn column, object value)
		{
			this.column = column;
			this.value = value;
		}
		
		
		///<summary>
		/// The DegreeColumn that was modified, which has raised the event.
		///</summary>
		///<value cref="DegreeColumn" />
		public DegreeColumn Column { get { return this.column; } }
		
		/// <summary>
        /// Gets the current value of the column.
        /// </summary>
        /// <value>The current value of the column.</value>
		public object Value{ get { return this.value; } }

	}
	#endregion
	
	
	///<summary>
	/// Define a delegate for all Degree related events.
	///</summary>
	public delegate void DegreeEventHandler(object sender, DegreeEventArgs e);
	
	///<summary>
	/// An object representation of the 'Degree' table. [No description found the database]	
	///</summary>
	[Serializable, DataObject]
	[CLSCompliant(true)]
	public abstract partial class DegreeBase : EntityBase, IEntityId<DegreeKey>, System.IComparable, System.ICloneable, IEditableObject, IComponent, INotifyPropertyChanged
	{		
		#region Variable Declarations
		
		/// <summary>
		///  Hold the inner data of the entity.
		/// </summary>
		private DegreeEntityData entityData;
		
		/// <summary>
		/// 	Hold the original data of the entity, as loaded from the repository.
		/// </summary>
		private DegreeEntityData _originalData;
		
		/// <summary>
		/// 	Hold a backup of the inner data of the entity.
		/// </summary>
		private DegreeEntityData backupData; 
		
		/// <summary>
		/// 	Key used if Tracking is Enabled for the <see cref="EntityLocator" />.
		/// </summary>
		private string entityTrackingKey;
		
		/// <summary>
		/// 	Hold the parent TList&lt;entity&gt; in which this entity maybe contained.
		/// </summary>
		/// <remark>Mostly used for databinding</remark>
		[NonSerialized]
		private TList<Degree> parentCollection;
		
		private bool inTxn = false;
		
		/// <summary>
		/// Occurs when a value is being changed for the specified column.
		/// </summary>	
		[field:NonSerialized]
		public event DegreeEventHandler ColumnChanging;		
		
		/// <summary>
		/// Occurs after a value has been changed for the specified column.
		/// </summary>
		[field:NonSerialized]
		public event DegreeEventHandler ColumnChanged;
		
		#endregion Variable Declarations
		
		#region Constructors
		///<summary>
		/// Creates a new <see cref="DegreeBase"/> instance.
		///</summary>
		public DegreeBase()
		{
			this.entityData = new DegreeEntityData();
			this.backupData = null;
		}		
		
		///<summary>
		/// Creates a new <see cref="DegreeBase"/> instance.
		///</summary>
		///<param name="degreeID"></param>
		///<param name="degreeRank"></param>
		///<param name="degreeTitle"></param>
		///<param name="degreeColor"></param>
		///<param name="degreeStripes"></param>
		///<param name="degreePicture"></param>
		public DegreeBase(System.Int32 degreeID, System.Int32 degreeRank, System.String degreeTitle, System.String degreeColor, 
			System.Int32 degreeStripes, System.Byte[] degreePicture)
		{
			this.entityData = new DegreeEntityData();
			this.backupData = null;

			this.ID = degreeID;
			this.Rank = degreeRank;
			this.Title = degreeTitle;
			this.Color = degreeColor;
			this.Stripes = degreeStripes;
			this.Picture = degreePicture;
		}
		
		///<summary>
		/// A simple factory method to create a new <see cref="Degree"/> instance.
		///</summary>
		///<param name="degreeID"></param>
		///<param name="degreeRank"></param>
		///<param name="degreeTitle"></param>
		///<param name="degreeColor"></param>
		///<param name="degreeStripes"></param>
		///<param name="degreePicture"></param>
		public static Degree CreateDegree(System.Int32 degreeID, System.Int32 degreeRank, System.String degreeTitle, System.String degreeColor, 
			System.Int32 degreeStripes, System.Byte[] degreePicture)
		{
			Degree newDegree = new Degree();
			newDegree.ID = degreeID;
			newDegree.Rank = degreeRank;
			newDegree.Title = degreeTitle;
			newDegree.Color = degreeColor;
			newDegree.Stripes = degreeStripes;
			newDegree.Picture = degreePicture;
			return newDegree;
		}
				
		#endregion Constructors
		
		
		#region Events trigger
		/// <summary>
		/// Raises the <see cref="ColumnChanging" /> event.
		/// </summary>
		/// <param name="column">The <see cref="DegreeColumn"/> which has raised the event.</param>
		public void OnColumnChanging(DegreeColumn column)
		{
			OnColumnChanging(column, null);
			return;
		}
		
		/// <summary>
		/// Raises the <see cref="ColumnChanged" /> event.
		/// </summary>
		/// <param name="column">The <see cref="DegreeColumn"/> which has raised the event.</param>
		public void OnColumnChanged(DegreeColumn column)
		{
			OnColumnChanged(column, null);
			return;
		} 
		
		
		/// <summary>
		/// Raises the <see cref="ColumnChanging" /> event.
		/// </summary>
		/// <param name="column">The <see cref="DegreeColumn"/> which has raised the event.</param>
		/// <param name="value">The changed value.</param>
		public void OnColumnChanging(DegreeColumn column, object value)
		{
			if(IsEntityTracked && EntityState != EntityState.Added)
				EntityManager.StopTracking(EntityTrackingKey);
				
			if (!SuppressEntityEvents)
			{
				DegreeEventHandler handler = ColumnChanging;
				if(handler != null)
				{
					handler(this, new DegreeEventArgs(column, value));
				}
			}
		}
		
		/// <summary>
		/// Raises the <see cref="ColumnChanged" /> event.
		/// </summary>
		/// <param name="column">The <see cref="DegreeColumn"/> which has raised the event.</param>
		/// <param name="value">The changed value.</param>
		public void OnColumnChanged(DegreeColumn column, object value)
		{
			if (!SuppressEntityEvents)
			{
				DegreeEventHandler handler = ColumnChanged;
				if(handler != null)
				{
					handler(this, new DegreeEventArgs(column, value));
				}
			
				// warn the parent list that i have changed
				OnEntityChanged();
			}
		} 
		#endregion
				
		#region Properties	
				
		/// <summary>
		/// 	Gets or sets the ID property. 
		///		
		/// </summary>
		/// <value>This type is int.</value>
		/// <remarks>
		/// This property can not be set to null. 
		/// </remarks>
		[DescriptionAttribute(""), BindableAttribute()]
		[DataObjectField(true, false, false)]
		public virtual System.Int32 ID
		{
			get
			{
				return this.entityData.ID; 
			}
			
			set
			{
				if (this.entityData.ID == value)
					return;
					
					
				OnColumnChanging(DegreeColumn.ID, this.entityData.ID);
				this.entityData.ID = value;
				this.EntityId.ID = value;
				if (this.EntityState == EntityState.Unchanged)
				{
					this.EntityState = EntityState.Changed;
				}
				OnColumnChanged(DegreeColumn.ID, this.entityData.ID);
				OnPropertyChanged("ID");
			}
		}
		
		/// <summary>
		/// 	Get the original value of the ID property.
		///		
		/// </summary>
		/// <remarks>This is the original value of the ID property.</remarks>
		/// <value>This type is int</value>
		[BrowsableAttribute(false)/*, XmlIgnoreAttribute()*/]
		public  virtual System.Int32 OriginalID
		{
			get { return this.entityData.OriginalID; }
			set { this.entityData.OriginalID = value; }
		}
		
		/// <summary>
		/// 	Gets or sets the Rank property. 
		///		
		/// </summary>
		/// <value>This type is int.</value>
		/// <remarks>
		/// This property can not be set to null. 
		/// </remarks>
		[DescriptionAttribute(""), BindableAttribute()]
		[DataObjectField(false, false, false)]
		public virtual System.Int32 Rank
		{
			get
			{
				return this.entityData.Rank; 
			}
			
			set
			{
				if (this.entityData.Rank == value)
					return;
					
					
				OnColumnChanging(DegreeColumn.Rank, this.entityData.Rank);
				this.entityData.Rank = value;
				if (this.EntityState == EntityState.Unchanged)
				{
					this.EntityState = EntityState.Changed;
				}
				OnColumnChanged(DegreeColumn.Rank, this.entityData.Rank);
				OnPropertyChanged("Rank");
			}
		}
		
		/// <summary>
		/// 	Gets or sets the Title property. 
		///		
		/// </summary>
		/// <value>This type is nvarchar.</value>
		/// <remarks>
		/// This property can be set to null. 
		/// </remarks>
		[DescriptionAttribute(""), BindableAttribute()]
		[DataObjectField(false, false, true, 20)]
		public virtual System.String Title
		{
			get
			{
				return this.entityData.Title; 
			}
			
			set
			{
				if (this.entityData.Title == value)
					return;
					
					
				OnColumnChanging(DegreeColumn.Title, this.entityData.Title);
				this.entityData.Title = value;
				if (this.EntityState == EntityState.Unchanged)
				{
					this.EntityState = EntityState.Changed;
				}
				OnColumnChanged(DegreeColumn.Title, this.entityData.Title);
				OnPropertyChanged("Title");
			}
		}
		
		/// <summary>
		/// 	Gets or sets the Color property. 
		///		
		/// </summary>
		/// <value>This type is nvarchar.</value>
		/// <remarks>
		/// This property can be set to null. 
		/// </remarks>
		[DescriptionAttribute(""), BindableAttribute()]
		[DataObjectField(false, false, true, 10)]
		public virtual System.String Color
		{
			get
			{
				return this.entityData.Color; 
			}
			
			set
			{
				if (this.entityData.Color == value)
					return;
					
					
				OnColumnChanging(DegreeColumn.Color, this.entityData.Color);
				this.entityData.Color = value;
				if (this.EntityState == EntityState.Unchanged)
				{
					this.EntityState = EntityState.Changed;
				}
				OnColumnChanged(DegreeColumn.Color, this.entityData.Color);
				OnPropertyChanged("Color");
			}
		}
		
		/// <summary>
		/// 	Gets or sets the Stripes property. 
		///		
		/// </summary>
		/// <value>This type is int.</value>
		/// <remarks>
		/// This property can not be set to null. 
		/// </remarks>
		[DescriptionAttribute(""), BindableAttribute()]
		[DataObjectField(false, false, false)]
		public virtual System.Int32 Stripes
		{
			get
			{
				return this.entityData.Stripes; 
			}
			
			set
			{
				if (this.entityData.Stripes == value)
					return;
					
					
				OnColumnChanging(DegreeColumn.Stripes, this.entityData.Stripes);
				this.entityData.Stripes = value;
				if (this.EntityState == EntityState.Unchanged)
				{
					this.EntityState = EntityState.Changed;
				}
				OnColumnChanged(DegreeColumn.Stripes, this.entityData.Stripes);
				OnPropertyChanged("Stripes");
			}
		}
		
		/// <summary>
		/// 	Gets or sets the Picture property. 
		///		
		/// </summary>
		/// <value>This type is image.</value>
		/// <remarks>
		/// This property can be set to null. 
		/// </remarks>
		[DescriptionAttribute(""), BindableAttribute()]
		[DataObjectField(false, false, true)]
		public virtual System.Byte[] Picture
		{
			get
			{
				return this.entityData.Picture; 
			}
			
			set
			{
				if (this.entityData.Picture == value)
					return;
					
					
				OnColumnChanging(DegreeColumn.Picture, this.entityData.Picture);
				this.entityData.Picture = value;
				if (this.EntityState == EntityState.Unchanged)
				{
					this.EntityState = EntityState.Changed;
				}
				OnColumnChanged(DegreeColumn.Picture, this.entityData.Picture);
				OnPropertyChanged("Picture");
			}
		}
		

		#region Source Foreign Key Property
				
		#endregion
			
		#region Table Meta Data
		/// <summary>
		///		The name of the underlying database table.
		/// </summary>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public override string TableName
		{
			get { return "Degree"; }
		}
		
		/// <summary>
		///		The name of the underlying database table's columns.
		/// </summary>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public override string[] TableColumns
		{
			get
			{
				return new string[] {"ID", "Rank", "Title", "Color", "Stripes", "Picture"};
			}
		}
		#endregion 
		
	
		/// <summary>
		///	Holds a collection of Examination objects
		///	which are related to this object through the relation FK_Examination_Degree
		/// </summary>	
		[BindableAttribute()]
		public TList<Examination> ExaminationCollection
		{
			get { return entityData.ExaminationCollection; }
			set { entityData.ExaminationCollection = value; }	
		}
		
		#endregion
		
		#region IEditableObject
		
		#region  CancelAddNew Event
		/// <summary>
        /// The delegate for the CancelAddNew event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		public delegate void CancelAddNewEventHandler(object sender, EventArgs e);
    
    	/// <summary>
		/// The CancelAddNew event.
		/// </summary>
		[field:NonSerialized]
		public event CancelAddNewEventHandler CancelAddNew ;

		/// <summary>
        /// Called when [cancel add new].
        /// </summary>
        public void OnCancelAddNew()
        {    
			if (!SuppressEntityEvents)
			{
	            CancelAddNewEventHandler handler = CancelAddNew ;
            	if (handler != null)
	            {    
    	            handler(this, EventArgs.Empty) ;
        	    }
	        }
        }
		#endregion 
		
		/// <summary>
		/// Begins an edit on an object.
		/// </summary>
		void IEditableObject.BeginEdit() 
	    {
	        //Console.WriteLine("Start BeginEdit");
	        if (!inTxn) 
	        {
	            this.backupData = this.entityData.Clone() as DegreeEntityData;
	            inTxn = true;
	            //Console.WriteLine("BeginEdit");
	        }
	        //Console.WriteLine("End BeginEdit");
	    }
	
		/// <summary>
		/// Discards changes since the last <c>BeginEdit</c> call.
		/// </summary>
	    void IEditableObject.CancelEdit() 
	    {
	        //Console.WriteLine("Start CancelEdit");
	        if (this.inTxn) 
	        {
	            this.entityData = this.backupData;
	            this.backupData = null;
				this.inTxn = false;

				if (this.bindingIsNew)
	        	//if (this.EntityState == EntityState.Added)
	        	{
					if (this.parentCollection != null)
						this.parentCollection.Remove( (Degree) this ) ;
				}	            
	        }
	        //Console.WriteLine("End CancelEdit");
	    }
	
		/// <summary>
		/// Pushes changes since the last <c>BeginEdit</c> or <c>IBindingList.AddNew</c> call into the underlying object.
		/// </summary>
	    void IEditableObject.EndEdit() 
	    {
	        //Console.WriteLine("Start EndEdit" + this.custData.id + this.custData.lastName);
	        if (this.inTxn) 
	        {
	            this.backupData = null;
				if (this.IsDirty) 
				{
					if (this.bindingIsNew) {
						this.EntityState = EntityState.Added;
						this.bindingIsNew = false ;
					}
					else
						if (this.EntityState == EntityState.Unchanged) 
							this.EntityState = EntityState.Changed ;
				}

				this.bindingIsNew = false ;
	            this.inTxn = false;	            
	        }
	        //Console.WriteLine("End EndEdit");
	    }
	    
	    /// <summary>
        /// Gets or sets the parent collection.
        /// </summary>
        /// <value>The parent collection.</value>
	    [XmlIgnore]
		[Browsable(false)]
	    public override object ParentCollection
	    {
	        get 
	        {
	            return (object)this.parentCollection;
	        }
	        set 
	        {
	            this.parentCollection = value as TList<Degree>;
	        }
	    }
	    
	    /// <summary>
        /// Called when the entity is changed.
        /// </summary>
	    private void OnEntityChanged() 
	    {
	        if (!SuppressEntityEvents && !inTxn && this.parentCollection != null) 
	        {
	            this.parentCollection.EntityChanged(this as Degree);
	        }
	    }


		#endregion
		
		#region Methods	
			
		///<summary>
		///  Revert all changes and restore original values.
		///</summary>
		public override void CancelChanges()
		{
			IEditableObject obj = (IEditableObject) this;
			obj.CancelEdit();

			this.entityData = null;
			this.entityData = this._originalData.Clone() as DegreeEntityData;
		}	
		
		/// <summary>
		/// Accepts the changes made to this object.
		/// </summary>
		/// <remarks>
		/// After calling this method, properties: IsDirty, IsNew are false. IsDeleted flag remains unchanged as it is handled by the parent List.
		/// </remarks>
		public override void AcceptChanges()
		{
			base.AcceptChanges();

			// we keep of the original version of the data
			this._originalData = null;
			this._originalData = this.entityData.Clone() as DegreeEntityData;
		}
		
		#region Comparision with original data
		
		/// <summary>
		/// Determines whether the property value has changed from the original data.
		/// </summary>
		/// <param name="column">The column.</param>
		/// <returns>
		/// 	<c>true</c> if the property value has changed; otherwise, <c>false</c>.
		/// </returns>
		public bool IsPropertyChanged(DegreeColumn column)
		{
			switch(column)
			{
					case DegreeColumn.ID:
					return entityData.ID != _originalData.ID;
					case DegreeColumn.Rank:
					return entityData.Rank != _originalData.Rank;
					case DegreeColumn.Title:
					return entityData.Title != _originalData.Title;
					case DegreeColumn.Color:
					return entityData.Color != _originalData.Color;
					case DegreeColumn.Stripes:
					return entityData.Stripes != _originalData.Stripes;
					case DegreeColumn.Picture:
					return entityData.Picture != _originalData.Picture;
			
				default:
					return false;
			}
		}
		
		/// <summary>
		/// Determines whether the data has changed from original.
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [has data changed]; otherwise, <c>false</c>.
		/// </returns>
		public bool HasDataChanged()
		{
			bool result = false;
			result = result || entityData.ID != _originalData.ID;
			result = result || entityData.Rank != _originalData.Rank;
			result = result || entityData.Title != _originalData.Title;
			result = result || entityData.Color != _originalData.Color;
			result = result || entityData.Stripes != _originalData.Stripes;
			result = result || entityData.Picture != _originalData.Picture;
			return result;
}	
		
		#endregion
		
		#region ICloneable Members
		///<summary>
		///  Returns a Typed Degree Entity 
		///</summary>
		public virtual Degree Copy()
		{
			//shallow copy entity
			Degree copy = new Degree();
			copy.ID = this.ID;
			copy.OriginalID = this.OriginalID;
			copy.Rank = this.Rank;
			copy.Title = this.Title;
			copy.Color = this.Color;
			copy.Stripes = this.Stripes;
			copy.Picture = this.Picture;
					
			copy.AcceptChanges();
			return (Degree)copy;
		}
		
		///<summary>
		/// ICloneable.Clone() Member, returns the Shallow Copy of this entity.
		///</summary>
		public object Clone()
		{
			return this.Copy();
		}
		
		///<summary>
		/// Returns a deep copy of the child collection object passed in.
		///</summary>
		public static object MakeCopyOf(object x)
		{
			if (x is ICloneable)
			{
				// Return a deep copy of the object
				return ((ICloneable)x).Clone();
			}
			else
				throw new System.NotSupportedException("Object Does Not Implement the ICloneable Interface.");
		}
		
		///<summary>
		///  Returns a Typed Degree Entity which is a deep copy of the current entity.
		///</summary>
		public virtual Degree DeepCopy()
		{
			return EntityHelper.Clone<Degree>(this as Degree);	
		}
		#endregion
		
		///<summary>
		/// Returns a value indicating whether this instance is equal to a specified object.
		///</summary>
		///<param name="toObject">An object to compare to this instance.</param>
		///<returns>true if toObject is a <see cref="DegreeBase"/> and has the same value as this instance; otherwise, false.</returns>
		public virtual bool Equals(DegreeBase toObject)
		{
			if (toObject == null)
				return false;
			return Equals(this, toObject);
		}
		
		
		///<summary>
		/// Determines whether the specified <see cref="DegreeBase"/> instances are considered equal.
		///</summary>
		///<param name="Object1">The first <see cref="DegreeBase"/> to compare.</param>
		///<param name="Object2">The second <see cref="DegreeBase"/> to compare. </param>
		///<returns>true if Object1 is the same instance as Object2 or if both are null references or if objA.Equals(objB) returns true; otherwise, false.</returns>
		public static bool Equals(DegreeBase Object1, DegreeBase Object2)
		{
			// both are null
			if (Object1 == null && Object2 == null)
				return true;

			// one or the other is null, but not both
			if (Object1 == null ^ Object2 == null)
				return false;
				
			bool equal = true;
			if (Object1.ID != Object2.ID)
				equal = false;
			if (Object1.Rank != Object2.Rank)
				equal = false;
			if ( Object1.Title != null && Object2.Title != null )
			{
				if (Object1.Title != Object2.Title)
					equal = false;
			}
			else if (Object1.Title == null ^ Object2.Title == null )
			{
				equal = false;
			}
			if ( Object1.Color != null && Object2.Color != null )
			{
				if (Object1.Color != Object2.Color)
					equal = false;
			}
			else if (Object1.Color == null ^ Object2.Color == null )
			{
				equal = false;
			}
			if (Object1.Stripes != Object2.Stripes)
				equal = false;
			if ( Object1.Picture != null && Object2.Picture != null )
			{
				if (Object1.Picture != Object2.Picture)
					equal = false;
			}
			else if (Object1.Picture == null ^ Object2.Picture == null )
			{
				equal = false;
			}
			return equal;
		}
		
		#endregion
		
		#region IComparable Members
		///<summary>
		/// Compares this instance to a specified object and returns an indication of their relative values.
		///<param name="obj">An object to compare to this instance, or a null reference (Nothing in Visual Basic).</param>
		///</summary>
		///<returns>A signed integer that indicates the relative order of this instance and obj.</returns>
		public virtual int CompareTo(object obj)
		{
			throw new NotImplementedException();
			// TODO -> generate a strongly typed IComparer in the concrete class
			//return this. GetPropertyName(SourceTable.PrimaryKey.MemberColumns[0].Name) .CompareTo(((DegreeBase)obj).GetPropertyName(SourceTable.PrimaryKey.MemberColumns[0].Name));
		}
		
		/*
		// static method to get a Comparer object
        public static DegreeComparer GetComparer()
        {
            return new DegreeComparer();
        }
        */

        // Comparer delegates back to Degree
        // Employee uses the integer's default
        // CompareTo method
        /*
        public int CompareTo(Item rhs)
        {
            return this.Id.CompareTo(rhs.Id);
        }
        */

/*
        // Special implementation to be called by custom comparer
        public int CompareTo(Degree rhs, DegreeColumn which)
        {
            switch (which)
            {
            	
            	
            	case DegreeColumn.ID:
            		return this.ID.CompareTo(rhs.ID);
            		
            		                 
            	
            	
            	case DegreeColumn.Rank:
            		return this.Rank.CompareTo(rhs.Rank);
            		
            		                 
            	
            	
            	case DegreeColumn.Title:
            		return this.Title.CompareTo(rhs.Title);
            		
            		                 
            	
            	
            	case DegreeColumn.Color:
            		return this.Color.CompareTo(rhs.Color);
            		
            		                 
            	
            	
            	case DegreeColumn.Stripes:
            		return this.Stripes.CompareTo(rhs.Stripes);
            		
            		                 
            	
            		                 
            }
            return 0;
        }
        */
	
		#endregion
		
		#region IComponent Members
		
		private ISite _site = null;

		/// <summary>
		/// Gets or Sets the site where this data is located.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public ISite Site
		{
			get{ return this._site; }
			set{ this._site = value; }
		}

		#endregion

		#region IDisposable Members
		
		/// <summary>
		/// Notify those that care when we dispose.
		/// </summary>
		[field:NonSerialized]
		public event System.EventHandler Disposed;

		/// <summary>
		/// Clean up. Nothing here though.
		/// </summary>
		public void Dispose()
		{
			this.parentCollection = null;
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}
		
		/// <summary>
		/// Clean up.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				EventHandler handler = Disposed;
				if (handler != null)
					handler(this, EventArgs.Empty);
			}
		}
		
		#endregion
				
		#region IEntityKey<DegreeKey> Members
		
		// member variable for the EntityId property
		private DegreeKey _entityId;

		/// <summary>
		/// Gets or sets the EntityId property.
		/// </summary>
		[XmlIgnore]
		public DegreeKey EntityId
		{
			get
			{
				if ( _entityId == null )
				{
					_entityId = new DegreeKey(this);
				}

				return _entityId;
			}
			set
			{
				if ( value != null )
				{
					value.Entity = this;
				}
				
				_entityId = value;
			}
		}
		
		#endregion
		
		#region EntityTrackingKey
		///<summary>
		/// Provides the tracking key for the <see cref="EntityLocator"/>
		///</summary>
		[XmlIgnore]
		public override string EntityTrackingKey
		{
			get
			{
				if(entityTrackingKey == null)
					entityTrackingKey = @"Degree" 
					+ this.ID.ToString();
				return entityTrackingKey;
			}
			set
		    {
		        if (value != null)
                    entityTrackingKey = value;
		    }
		}
		#endregion 
		
		#region ToString Method
		
		///<summary>
		/// Returns a String that represents the current object.
		///</summary>
		public override string ToString()
		{
			return string.Format(System.Globalization.CultureInfo.InvariantCulture,
				"{7}{6}- ID: {0}{6}- Rank: {1}{6}- Title: {2}{6}- Color: {3}{6}- Stripes: {4}{6}- Picture: {5}{6}", 
				this.ID,
				this.Rank,
				(this.Title == null) ? string.Empty : this.Title.ToString(),
				(this.Color == null) ? string.Empty : this.Color.ToString(),
				this.Stripes,
				(this.Picture == null) ? string.Empty : this.Picture.ToString(),
				Environment.NewLine, 
				this.GetType());
		}
		
		#endregion ToString Method
		
		#region Inner data class
		
	/// <summary>
	///		The data structure representation of the 'Degree' table.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	[EditorBrowsable(EditorBrowsableState.Never)]
	[Serializable]
	internal protected class DegreeEntityData : ICloneable
	{
		#region Variable Declarations
		
		#region Primary key(s)
			/// <summary>			
			/// ID : 
			/// </summary>
			/// <remarks>Member of the primary key of the underlying table "Degree"</remarks>
			public System.Int32 ID;
				
			/// <summary>
			/// keep a copy of the original so it can be used for editable primary keys.
			/// </summary>
			public System.Int32 OriginalID;
			
		#endregion
		
		#region Non Primary key(s)
		
		
		/// <summary>
		/// Rank : 
		/// </summary>
		public System.Int32		  Rank = (int)0;
		
		/// <summary>
		/// Title : 
		/// </summary>
		public System.String		  Title = null;
		
		/// <summary>
		/// Color : 
		/// </summary>
		public System.String		  Color = null;
		
		/// <summary>
		/// Stripes : 
		/// </summary>
		public System.Int32		  Stripes = (int)0;
		
		/// <summary>
		/// Picture : 
		/// </summary>
		public System.Byte[]		  Picture = null;
		#endregion
			
		#endregion Variable Declarations
		
		#region Clone Method

		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public Object Clone()
		{
			DegreeEntityData _tmp = new DegreeEntityData();
						
			_tmp.ID = this.ID;
			_tmp.OriginalID = this.OriginalID;
			
			_tmp.Rank = this.Rank;
			_tmp.Title = this.Title;
			_tmp.Color = this.Color;
			_tmp.Stripes = this.Stripes;
			_tmp.Picture = this.Picture;
			
			return _tmp;
		}
		
		#endregion Clone Method
		
		#region Data Properties

		#region ExaminationCollection
		
		private TList<Examination> examinationDegreeID;
		
		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation examinationDegreeID
		/// </summary>	
		public TList<Examination> ExaminationCollection
		{
			get
			{
				if (examinationDegreeID == null)
				{
				examinationDegreeID = new TList<Examination>();
				}
	
				return examinationDegreeID;
			}
			set { examinationDegreeID = value; }
		}
		
		#endregion

		#endregion Data Properties

	}//End struct


		#endregion
		
		#region Validation
		
		/// <summary>
		/// Assigns validation rules to this object based on model definition.
		/// </summary>
		/// <remarks>This method overrides the base class to add schema related validation.</remarks>
		protected override void AddValidationRules()
		{
			//Validation rules based on database schema.
			ValidationRules.AddRule(Validation.CommonRules.StringMaxLength,new Validation.CommonRules.MaxLengthRuleArgs("Title",20));
			ValidationRules.AddRule(Validation.CommonRules.StringMaxLength,new Validation.CommonRules.MaxLengthRuleArgs("Color",10));
		}
   		#endregion
	
	} // End Class
	
	#region DegreeComparer
		
	/// <summary>
	///	Strongly Typed IComparer
	/// </summary>
	public class DegreeComparer : System.Collections.Generic.IComparer<Degree>
	{
		DegreeColumn whichComparison;
		
		/// <summary>
        /// Initializes a new instance of the <see cref="T:DegreeComparer"/> class.
        /// </summary>
		public DegreeComparer()
        {            
        }               
        
        /// <summary>
        /// Initializes a new instance of the <see cref="T:%=className%>Comparer"/> class.
        /// </summary>
        /// <param name="column">The column to sort on.</param>
        public DegreeComparer(DegreeColumn column)
        {
            this.whichComparison = column;
        }

		/// <summary>
        /// Determines whether the specified <c cref="Degree"/> instances are considered equal.
        /// </summary>
        /// <param name="a">The first <c cref="Degree"/> to compare.</param>
        /// <param name="b">The second <c>Degree</c> to compare.</param>
        /// <returns>true if objA is the same instance as objB or if both are null references or if objA.Equals(objB) returns true; otherwise, false.</returns>
        public bool Equals(Degree a, Degree b)
        {
            return this.Compare(a, b) == 0;
        }

		/// <summary>
        /// Gets the hash code of the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public int GetHashCode(Degree entity)
        {
            return entity.GetHashCode();
        }

        /// <summary>
        /// Performs a case-insensitive comparison of two objects of the same type and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <param name="a">The first object to compare.</param>
        /// <param name="b">The second object to compare.</param>
        /// <returns></returns>
        public int Compare(Degree a, Degree b)
        {
        	EntityPropertyComparer entityPropertyComparer = new EntityPropertyComparer(this.whichComparison.ToString());
        	return entityPropertyComparer.Compare(a, b);
        }

		/// <summary>
        /// Gets or sets the column that will be used for comparison.
        /// </summary>
        /// <value>The comparison column.</value>
        public DegreeColumn WhichComparison
        {
            get { return this.whichComparison; }
            set { this.whichComparison = value; }
        }
	}
	
	#endregion
	
	#region DegreeKey Class

	/// <summary>
	/// Wraps the unique identifier values for the <see cref="Degree"/> object.
	/// </summary>
	[Serializable]
	[CLSCompliant(true)]
	public class DegreeKey : EntityKeyBase
	{
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the DegreeKey class.
		/// </summary>
		public DegreeKey()
		{
		}
		
		/// <summary>
		/// Initializes a new instance of the DegreeKey class.
		/// </summary>
		public DegreeKey(DegreeBase entity)
		{
			Entity = entity;

			#region Init Properties

			if ( entity != null )
			{
				this.id = entity.ID;
			}

			#endregion
		}
		
		/// <summary>
		/// Initializes a new instance of the DegreeKey class.
		/// </summary>
		public DegreeKey(System.Int32 id)
		{
			#region Init Properties

			this.id = id;

			#endregion
		}
		
		#endregion Constructors

		#region Properties
		
		// member variable for the Entity property
		private DegreeBase _entity;
		
		/// <summary>
		/// Gets or sets the Entity property.
		/// </summary>
		public DegreeBase Entity
		{
			get { return _entity; }
			set { _entity = value; }
		}
		
		// member variable for the ID property
		private System.Int32 id;
		
		/// <summary>
		/// Gets or sets the ID property.
		/// </summary>
		public System.Int32 ID
		{
			get { return id; }
			set
			{
				if ( Entity != null )
				{
					Entity.ID = value;
				}
				
				id = value;
			}
		}
		
		#endregion

		#region Methods
		
		/// <summary>
		/// Reads values from the supplied <see cref="IDictionary"/> object into
		/// properties of the current object.
		/// </summary>
		/// <param name="values">An <see cref="IDictionary"/> instance that contains
		/// the key/value pairs to be used as property values.</param>
		public override void Load(IDictionary values)
		{
			#region Init Properties

			if ( values != null )
			{
				ID = ( values["ID"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ID"], typeof(System.Int32)) : (int)0;
			}

			#endregion
		}

		/// <summary>
		/// Creates a new <see cref="IDictionary"/> object and populates it
		/// with the property values of the current object.
		/// </summary>
		/// <returns>A collection of name/value pairs.</returns>
		public override IDictionary ToDictionary()
		{
			IDictionary values = new Hashtable();

			#region Init Dictionary

			values.Add("ID", ID);

			#endregion Init Dictionary

			return values;
		}
		
		///<summary>
		/// Returns a String that represents the current object.
		///</summary>
		public override string ToString()
		{
			return String.Format("ID: {0}{1}",
								ID,
								Environment.NewLine);
		}

		#endregion Methods
	}
	
	#endregion	

	#region DegreeColumn Enum
	
	/// <summary>
	/// Enumerate the Degree columns.
	/// </summary>
	[Serializable]
	public enum DegreeColumn : int
	{
		/// <summary>
		/// ID : 
		/// </summary>
		[EnumTextValue("ID")]
		[ColumnEnum("ID", typeof(System.Int32), System.Data.DbType.Int32, true, false, false)]
		ID = 1,
		/// <summary>
		/// Rank : 
		/// </summary>
		[EnumTextValue("Rank")]
		[ColumnEnum("Rank", typeof(System.Int32), System.Data.DbType.Int32, false, false, false)]
		Rank = 2,
		/// <summary>
		/// Title : 
		/// </summary>
		[EnumTextValue("Title")]
		[ColumnEnum("Title", typeof(System.String), System.Data.DbType.String, false, false, true, 20)]
		Title = 3,
		/// <summary>
		/// Color : 
		/// </summary>
		[EnumTextValue("Color")]
		[ColumnEnum("Color", typeof(System.String), System.Data.DbType.String, false, false, true, 10)]
		Color = 4,
		/// <summary>
		/// Stripes : 
		/// </summary>
		[EnumTextValue("Stripes")]
		[ColumnEnum("Stripes", typeof(System.Int32), System.Data.DbType.Int32, false, false, false)]
		Stripes = 5,
		/// <summary>
		/// Picture : 
		/// </summary>
		[EnumTextValue("Picture")]
		[ColumnEnum("Picture", typeof(System.Byte[]), System.Data.DbType.Binary, false, false, true)]
		Picture = 6
	}//End enum

	#endregion DegreeColumn Enum

} // end namespace
