﻿#region Using directives

using System;

#endregion

namespace KA.BusinessLogicLayer
{	
	///<summary>
	/// An object representation of the 'TrainingExceptions' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class TrainingExceptions : TrainingExceptionsBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="TrainingExceptions"/> instance.
		///</summary>
		public TrainingExceptions():base(){}	
		
		#endregion
	}
}
