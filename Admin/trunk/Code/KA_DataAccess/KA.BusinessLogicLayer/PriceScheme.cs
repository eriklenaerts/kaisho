﻿#region Using directives

using System;

#endregion

namespace KA.BusinessLogicLayer
{	
	///<summary>
	/// An object representation of the 'PriceScheme' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class PriceScheme : PriceSchemeBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="PriceScheme"/> instance.
		///</summary>
		public PriceScheme():base(){}	
		
		#endregion
	}
}
