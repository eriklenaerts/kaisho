﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using System.Diagnostics;
using KA.BusinessLogicLayer;
using KA.DataAccessLayer;

#endregion

namespace KA.DataAccessLayer.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="MemberProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class MemberProviderBaseCore : EntityProviderBase<KA.BusinessLogicLayer.Member, KA.BusinessLogicLayer.MemberKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KA.BusinessLogicLayer.MemberKey key)
		{
			return Delete(transactionManager, key.ID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 id)
		{
			return Delete(null, id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Member_PaymentType key.
		///		FK_Member_PaymentType Description: 
		/// </summary>
		/// <param name="paymentId"></param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Member objects.</returns>
		public KA.BusinessLogicLayer.TList<Member> GetByPaymentId(System.Int32? paymentId)
		{
			int count = -1;
			return GetByPaymentId(paymentId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Member_PaymentType key.
		///		FK_Member_PaymentType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="paymentId"></param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Member objects.</returns>
		/// <remarks></remarks>
		public KA.BusinessLogicLayer.TList<Member> GetByPaymentId(TransactionManager transactionManager, System.Int32? paymentId)
		{
			int count = -1;
			return GetByPaymentId(transactionManager, paymentId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Member_PaymentType key.
		///		FK_Member_PaymentType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="paymentId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Member objects.</returns>
		public KA.BusinessLogicLayer.TList<Member> GetByPaymentId(TransactionManager transactionManager, System.Int32? paymentId, int start, int pageLength)
		{
			int count = -1;
			return GetByPaymentId(transactionManager, paymentId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Member_PaymentType key.
		///		fK_Member_PaymentType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="paymentId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Member objects.</returns>
		public KA.BusinessLogicLayer.TList<Member> GetByPaymentId(System.Int32? paymentId, int start, int pageLength)
		{
			int count =  -1;
			return GetByPaymentId(null, paymentId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Member_PaymentType key.
		///		fK_Member_PaymentType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="paymentId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Member objects.</returns>
		public KA.BusinessLogicLayer.TList<Member> GetByPaymentId(System.Int32? paymentId, int start, int pageLength,out int count)
		{
			return GetByPaymentId(null, paymentId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Member_PaymentType key.
		///		FK_Member_PaymentType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="paymentId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Member objects.</returns>
		public abstract KA.BusinessLogicLayer.TList<Member> GetByPaymentId(TransactionManager transactionManager, System.Int32? paymentId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Member_Group key.
		///		FK_Member_Group Description: 
		/// </summary>
		/// <param name="groupID"></param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Member objects.</returns>
		public KA.BusinessLogicLayer.TList<Member> GetByGroupID(System.Int32? groupID)
		{
			int count = -1;
			return GetByGroupID(groupID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Member_Group key.
		///		FK_Member_Group Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="groupID"></param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Member objects.</returns>
		/// <remarks></remarks>
		public KA.BusinessLogicLayer.TList<Member> GetByGroupID(TransactionManager transactionManager, System.Int32? groupID)
		{
			int count = -1;
			return GetByGroupID(transactionManager, groupID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Member_Group key.
		///		FK_Member_Group Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="groupID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Member objects.</returns>
		public KA.BusinessLogicLayer.TList<Member> GetByGroupID(TransactionManager transactionManager, System.Int32? groupID, int start, int pageLength)
		{
			int count = -1;
			return GetByGroupID(transactionManager, groupID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Member_Group key.
		///		fK_Member_Group Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="groupID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Member objects.</returns>
		public KA.BusinessLogicLayer.TList<Member> GetByGroupID(System.Int32? groupID, int start, int pageLength)
		{
			int count =  -1;
			return GetByGroupID(null, groupID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Member_Group key.
		///		fK_Member_Group Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="groupID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Member objects.</returns>
		public KA.BusinessLogicLayer.TList<Member> GetByGroupID(System.Int32? groupID, int start, int pageLength,out int count)
		{
			return GetByGroupID(null, groupID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Member_Group key.
		///		FK_Member_Group Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="groupID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Member objects.</returns>
		public abstract KA.BusinessLogicLayer.TList<Member> GetByGroupID(TransactionManager transactionManager, System.Int32? groupID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Member_Member key.
		///		FK_Member_Member Description: 
		/// </summary>
		/// <param name="trainingLocationId"></param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Member objects.</returns>
		public KA.BusinessLogicLayer.TList<Member> GetByTrainingLocationId(System.Int32? trainingLocationId)
		{
			int count = -1;
			return GetByTrainingLocationId(trainingLocationId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Member_Member key.
		///		FK_Member_Member Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="trainingLocationId"></param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Member objects.</returns>
		/// <remarks></remarks>
		public KA.BusinessLogicLayer.TList<Member> GetByTrainingLocationId(TransactionManager transactionManager, System.Int32? trainingLocationId)
		{
			int count = -1;
			return GetByTrainingLocationId(transactionManager, trainingLocationId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Member_Member key.
		///		FK_Member_Member Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="trainingLocationId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Member objects.</returns>
		public KA.BusinessLogicLayer.TList<Member> GetByTrainingLocationId(TransactionManager transactionManager, System.Int32? trainingLocationId, int start, int pageLength)
		{
			int count = -1;
			return GetByTrainingLocationId(transactionManager, trainingLocationId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Member_Member key.
		///		fK_Member_Member Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="trainingLocationId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Member objects.</returns>
		public KA.BusinessLogicLayer.TList<Member> GetByTrainingLocationId(System.Int32? trainingLocationId, int start, int pageLength)
		{
			int count =  -1;
			return GetByTrainingLocationId(null, trainingLocationId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Member_Member key.
		///		fK_Member_Member Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="trainingLocationId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Member objects.</returns>
		public KA.BusinessLogicLayer.TList<Member> GetByTrainingLocationId(System.Int32? trainingLocationId, int start, int pageLength,out int count)
		{
			return GetByTrainingLocationId(null, trainingLocationId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Member_Member key.
		///		FK_Member_Member Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="trainingLocationId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Member objects.</returns>
		public abstract KA.BusinessLogicLayer.TList<Member> GetByTrainingLocationId(TransactionManager transactionManager, System.Int32? trainingLocationId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KA.BusinessLogicLayer.Member Get(TransactionManager transactionManager, KA.BusinessLogicLayer.MemberKey key, int start, int pageLength)
		{
			return GetByID(transactionManager, key.ID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key Member_PK index.
		/// </summary>
		/// <param name="id"></param>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.Member"/> class.</returns>
		public KA.BusinessLogicLayer.Member GetByID(System.Int32 id)
		{
			int count = -1;
			return GetByID(null,id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the Member_PK index.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.Member"/> class.</returns>
		public KA.BusinessLogicLayer.Member GetByID(System.Int32 id, int start, int pageLength)
		{
			int count = -1;
			return GetByID(null, id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the Member_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.Member"/> class.</returns>
		public KA.BusinessLogicLayer.Member GetByID(TransactionManager transactionManager, System.Int32 id)
		{
			int count = -1;
			return GetByID(transactionManager, id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the Member_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.Member"/> class.</returns>
		public KA.BusinessLogicLayer.Member GetByID(TransactionManager transactionManager, System.Int32 id, int start, int pageLength)
		{
			int count = -1;
			return GetByID(transactionManager, id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the Member_PK index.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.Member"/> class.</returns>
		public KA.BusinessLogicLayer.Member GetByID(System.Int32 id, int start, int pageLength, out int count)
		{
			return GetByID(null, id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the Member_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.Member"/> class.</returns>
		public abstract KA.BusinessLogicLayer.Member GetByID(TransactionManager transactionManager, System.Int32 id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a KA.BusinessLogicLayer.TList&lt;Member&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="KA.BusinessLogicLayer.TList&lt;Member&gt;"/></returns>
		public static KA.BusinessLogicLayer.TList<Member> Fill(IDataReader reader, KA.BusinessLogicLayer.TList<Member> rows, int start, int pageLength)
		{
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
					return rows; // not enough rows, just return
			}

			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done

				string key = null;
				
				KA.BusinessLogicLayer.Member c = null;
				if (DataRepository.Provider.UseEntityFactory)
				{
					key = @"Member" 
							+ (reader.IsDBNull(reader.GetOrdinal("ID"))?(int)0:(System.Int32)reader["ID"]).ToString();

					c = EntityManager.LocateOrCreate<Member>(
						key.ToString(), // EntityTrackingKey 
						"Member",  //Creational Type
						DataRepository.Provider.EntityCreationalFactoryType,  //Factory used to create entity
						DataRepository.Provider.EnableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KA.BusinessLogicLayer.Member();
				}
				
				if (!DataRepository.Provider.EnableEntityTracking || c.EntityState == EntityState.Added)
                {
					c.SuppressEntityEvents = true;
					c.ID = (System.Int32)reader["ID"];
					c.Lastname = (reader.IsDBNull(reader.GetOrdinal("Lastname")))?null:(System.String)reader["Lastname"];
					c.Firstname = (reader.IsDBNull(reader.GetOrdinal("Firstname")))?null:(System.String)reader["Firstname"];
					c.Address = (reader.IsDBNull(reader.GetOrdinal("Address")))?null:(System.String)reader["Address"];
					c.ZipCode = (reader.IsDBNull(reader.GetOrdinal("ZipCode")))?null:(System.String)reader["ZipCode"];
					c.City = (reader.IsDBNull(reader.GetOrdinal("City")))?null:(System.String)reader["City"];
					c.Province = (reader.IsDBNull(reader.GetOrdinal("Province")))?null:(System.String)reader["Province"];
					c.Country = (reader.IsDBNull(reader.GetOrdinal("Country")))?null:(System.String)reader["Country"];
					c.Email = (reader.IsDBNull(reader.GetOrdinal("Email")))?null:(System.String)reader["Email"];
					c.Telephone = (reader.IsDBNull(reader.GetOrdinal("Telephone")))?null:(System.String)reader["Telephone"];
					c.Sex = (reader.IsDBNull(reader.GetOrdinal("Sex")))?null:(System.String)reader["Sex"];
					c.Birthdate = (reader.IsDBNull(reader.GetOrdinal("Birthdate")))?null:(System.DateTime?)reader["Birthdate"];
					c.LicenseNumber = (reader.IsDBNull(reader.GetOrdinal("LicenseNumber")))?null:(System.Int32?)reader["LicenseNumber"];
					c.GroupID = (reader.IsDBNull(reader.GetOrdinal("GroupID")))?null:(System.Int32?)reader["GroupID"];
					c.IsTrainer = (System.Boolean)reader["IsTrainer"];
					c.IsNew = (System.Boolean)reader["IsNew"];
					c.IsActive = (System.Boolean)reader["IsActive"];
					c.InsuranceEndDate = (reader.IsDBNull(reader.GetOrdinal("InsuranceEndDate")))?null:(System.DateTime?)reader["InsuranceEndDate"];
					c.StartYear = (reader.IsDBNull(reader.GetOrdinal("StartYear")))?null:(System.String)reader["StartYear"];
					c.Foto = (reader.IsDBNull(reader.GetOrdinal("Foto")))?null:(System.Byte[])reader["Foto"];
					c.PaymentId = (reader.IsDBNull(reader.GetOrdinal("PaymentId")))?null:(System.Int32?)reader["PaymentId"];
					c.TrainingLocationId = (reader.IsDBNull(reader.GetOrdinal("TrainingLocationId")))?null:(System.Int32?)reader["TrainingLocationId"];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
			return rows;
		}
		
		/// <summary>
		/// Refreshes the <see cref="KA.BusinessLogicLayer.Member"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KA.BusinessLogicLayer.Member"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KA.BusinessLogicLayer.Member entity)
		{
			if (!reader.Read()) return;
			
			entity.ID = (System.Int32)reader["ID"];
			entity.Lastname = (reader.IsDBNull(reader.GetOrdinal("Lastname")))?null:(System.String)reader["Lastname"];
			entity.Firstname = (reader.IsDBNull(reader.GetOrdinal("Firstname")))?null:(System.String)reader["Firstname"];
			entity.Address = (reader.IsDBNull(reader.GetOrdinal("Address")))?null:(System.String)reader["Address"];
			entity.ZipCode = (reader.IsDBNull(reader.GetOrdinal("ZipCode")))?null:(System.String)reader["ZipCode"];
			entity.City = (reader.IsDBNull(reader.GetOrdinal("City")))?null:(System.String)reader["City"];
			entity.Province = (reader.IsDBNull(reader.GetOrdinal("Province")))?null:(System.String)reader["Province"];
			entity.Country = (reader.IsDBNull(reader.GetOrdinal("Country")))?null:(System.String)reader["Country"];
			entity.Email = (reader.IsDBNull(reader.GetOrdinal("Email")))?null:(System.String)reader["Email"];
			entity.Telephone = (reader.IsDBNull(reader.GetOrdinal("Telephone")))?null:(System.String)reader["Telephone"];
			entity.Sex = (reader.IsDBNull(reader.GetOrdinal("Sex")))?null:(System.String)reader["Sex"];
			entity.Birthdate = (reader.IsDBNull(reader.GetOrdinal("Birthdate")))?null:(System.DateTime?)reader["Birthdate"];
			entity.LicenseNumber = (reader.IsDBNull(reader.GetOrdinal("LicenseNumber")))?null:(System.Int32?)reader["LicenseNumber"];
			entity.GroupID = (reader.IsDBNull(reader.GetOrdinal("GroupID")))?null:(System.Int32?)reader["GroupID"];
			entity.IsTrainer = (System.Boolean)reader["IsTrainer"];
			entity.IsNew = (System.Boolean)reader["IsNew"];
			entity.IsActive = (System.Boolean)reader["IsActive"];
			entity.InsuranceEndDate = (reader.IsDBNull(reader.GetOrdinal("InsuranceEndDate")))?null:(System.DateTime?)reader["InsuranceEndDate"];
			entity.StartYear = (reader.IsDBNull(reader.GetOrdinal("StartYear")))?null:(System.String)reader["StartYear"];
			entity.Foto = (reader.IsDBNull(reader.GetOrdinal("Foto")))?null:(System.Byte[])reader["Foto"];
			entity.PaymentId = (reader.IsDBNull(reader.GetOrdinal("PaymentId")))?null:(System.Int32?)reader["PaymentId"];
			entity.TrainingLocationId = (reader.IsDBNull(reader.GetOrdinal("TrainingLocationId")))?null:(System.Int32?)reader["TrainingLocationId"];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KA.BusinessLogicLayer.Member"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KA.BusinessLogicLayer.Member"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KA.BusinessLogicLayer.Member entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ID = (System.Int32)dataRow["ID"];
			entity.Lastname = (Convert.IsDBNull(dataRow["Lastname"]))?null:(System.String)dataRow["Lastname"];
			entity.Firstname = (Convert.IsDBNull(dataRow["Firstname"]))?null:(System.String)dataRow["Firstname"];
			entity.Address = (Convert.IsDBNull(dataRow["Address"]))?null:(System.String)dataRow["Address"];
			entity.ZipCode = (Convert.IsDBNull(dataRow["ZipCode"]))?null:(System.String)dataRow["ZipCode"];
			entity.City = (Convert.IsDBNull(dataRow["City"]))?null:(System.String)dataRow["City"];
			entity.Province = (Convert.IsDBNull(dataRow["Province"]))?null:(System.String)dataRow["Province"];
			entity.Country = (Convert.IsDBNull(dataRow["Country"]))?null:(System.String)dataRow["Country"];
			entity.Email = (Convert.IsDBNull(dataRow["Email"]))?null:(System.String)dataRow["Email"];
			entity.Telephone = (Convert.IsDBNull(dataRow["Telephone"]))?null:(System.String)dataRow["Telephone"];
			entity.Sex = (Convert.IsDBNull(dataRow["Sex"]))?null:(System.String)dataRow["Sex"];
			entity.Birthdate = (Convert.IsDBNull(dataRow["Birthdate"]))?null:(System.DateTime?)dataRow["Birthdate"];
			entity.LicenseNumber = (Convert.IsDBNull(dataRow["LicenseNumber"]))?null:(System.Int32?)dataRow["LicenseNumber"];
			entity.GroupID = (Convert.IsDBNull(dataRow["GroupID"]))?null:(System.Int32?)dataRow["GroupID"];
			entity.IsTrainer = (System.Boolean)dataRow["IsTrainer"];
			entity.IsNew = (System.Boolean)dataRow["IsNew"];
			entity.IsActive = (System.Boolean)dataRow["IsActive"];
			entity.InsuranceEndDate = (Convert.IsDBNull(dataRow["InsuranceEndDate"]))?null:(System.DateTime?)dataRow["InsuranceEndDate"];
			entity.StartYear = (Convert.IsDBNull(dataRow["StartYear"]))?null:(System.String)dataRow["StartYear"];
			entity.Foto = (Convert.IsDBNull(dataRow["Foto"]))?null:(System.Byte[])dataRow["Foto"];
			entity.PaymentId = (Convert.IsDBNull(dataRow["PaymentId"]))?null:(System.Int32?)dataRow["PaymentId"];
			entity.TrainingLocationId = (Convert.IsDBNull(dataRow["TrainingLocationId"]))?null:(System.Int32?)dataRow["TrainingLocationId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KA.BusinessLogicLayer.Member"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KA.BusinessLogicLayer.Member Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		internal override void DeepLoad(TransactionManager transactionManager, KA.BusinessLogicLayer.Member entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, ChildEntityTypesList innerList)
		{
			if(entity == null)
				return;

			#region PaymentIdSource	
			if (CanDeepLoad(entity, "PaymentType", "PaymentIdSource", deepLoadType, innerList) 
				&& entity.PaymentIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.PaymentId ?? (int)0);
				PaymentType tmpEntity = EntityManager.LocateEntity<PaymentType>(EntityLocator.ConstructKeyFromPkItems(typeof(PaymentType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PaymentIdSource = tmpEntity;
				else
					entity.PaymentIdSource = DataRepository.PaymentTypeProvider.GetById((entity.PaymentId ?? (int)0));
			
				if (deep && entity.PaymentIdSource != null)
				{
					DataRepository.PaymentTypeProvider.DeepLoad(transactionManager, entity.PaymentIdSource, deep, deepLoadType, childTypes, innerList);
				}
			}
			#endregion PaymentIdSource

			#region GroupIDSource	
			if (CanDeepLoad(entity, "Group", "GroupIDSource", deepLoadType, innerList) 
				&& entity.GroupIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.GroupID ?? (int)0);
				Group tmpEntity = EntityManager.LocateEntity<Group>(EntityLocator.ConstructKeyFromPkItems(typeof(Group), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.GroupIDSource = tmpEntity;
				else
					entity.GroupIDSource = DataRepository.GroupProvider.GetByID((entity.GroupID ?? (int)0));
			
				if (deep && entity.GroupIDSource != null)
				{
					DataRepository.GroupProvider.DeepLoad(transactionManager, entity.GroupIDSource, deep, deepLoadType, childTypes, innerList);
				}
			}
			#endregion GroupIDSource

			#region TrainingLocationIdSource	
			if (CanDeepLoad(entity, "Location", "TrainingLocationIdSource", deepLoadType, innerList) 
				&& entity.TrainingLocationIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.TrainingLocationId ?? (int)0);
				Location tmpEntity = EntityManager.LocateEntity<Location>(EntityLocator.ConstructKeyFromPkItems(typeof(Location), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.TrainingLocationIdSource = tmpEntity;
				else
					entity.TrainingLocationIdSource = DataRepository.LocationProvider.GetByID((entity.TrainingLocationId ?? (int)0));
			
				if (deep && entity.TrainingLocationIdSource != null)
				{
					DataRepository.LocationProvider.DeepLoad(transactionManager, entity.TrainingLocationIdSource, deep, deepLoadType, childTypes, innerList);
				}
			}
			#endregion TrainingLocationIdSource
			
			// Load Entity through Provider
			// Deep load child collections  - Call GetByID methods when available
			
			#region ExaminationCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Examination>", "ExaminationCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				Debug.WriteLine("- property 'ExaminationCollection' loaded.");
				#endif 

				entity.ExaminationCollection = DataRepository.ExaminationProvider.GetByMemberID(transactionManager, entity.ID);

				if (deep && entity.ExaminationCollection.Count > 0)
				{
					DataRepository.ExaminationProvider.DeepLoad(transactionManager, entity.ExaminationCollection, deep, deepLoadType, childTypes, innerList);
				}
			}		
			#endregion 
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KA.BusinessLogicLayer.Member object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KA.BusinessLogicLayer.Member instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KA.BusinessLogicLayer.Member Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		internal override bool DeepSave(TransactionManager transactionManager, KA.BusinessLogicLayer.Member entity, DeepSaveType deepSaveType, System.Type[] childTypes, ChildEntityTypesList innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region PaymentIdSource
			if (CanDeepSave(entity, "PaymentType", "PaymentIdSource", deepSaveType, innerList) 
				&& entity.PaymentIdSource != null)
			{
				DataRepository.PaymentTypeProvider.Save(transactionManager, entity.PaymentIdSource);
				entity.PaymentId = entity.PaymentIdSource.Id;
			}
			#endregion 
			
			#region GroupIDSource
			if (CanDeepSave(entity, "Group", "GroupIDSource", deepSaveType, innerList) 
				&& entity.GroupIDSource != null)
			{
				DataRepository.GroupProvider.Save(transactionManager, entity.GroupIDSource);
				entity.GroupID = entity.GroupIDSource.ID;
			}
			#endregion 
			
			#region TrainingLocationIdSource
			if (CanDeepSave(entity, "Location", "TrainingLocationIdSource", deepSaveType, innerList) 
				&& entity.TrainingLocationIdSource != null)
			{
				DataRepository.LocationProvider.Save(transactionManager, entity.TrainingLocationIdSource);
				entity.TrainingLocationId = entity.TrainingLocationIdSource.ID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			this.Save(transactionManager, entity);
			
			



			#region List<Examination>
				if (CanDeepSave(entity, "List<Examination>", "ExaminationCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Examination child in entity.ExaminationCollection)
					{
						child.MemberID = entity.ID;
					}
				
				if (entity.ExaminationCollection.Count > 0 || entity.ExaminationCollection.DeletedItems.Count > 0)
					DataRepository.ExaminationProvider.DeepSave(transactionManager, entity.ExaminationCollection, deepSaveType, childTypes, innerList);
				} 
			#endregion 
				

						
			return true;
		}
		#endregion
	} // end class
	
	#region MemberChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KA.BusinessLogicLayer.Member</c>
	///</summary>
	public enum MemberChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>PaymentType</c> at PaymentIdSource
		///</summary>
		[ChildEntityType(typeof(PaymentType))]
		PaymentType,
			
		///<summary>
		/// Composite Property for <c>Group</c> at GroupIDSource
		///</summary>
		[ChildEntityType(typeof(Group))]
		Group,
			
		///<summary>
		/// Composite Property for <c>Location</c> at TrainingLocationIdSource
		///</summary>
		[ChildEntityType(typeof(Location))]
		Location,
	
		///<summary>
		/// Collection of <c>Member</c> as OneToMany for ExaminationCollection
		///</summary>
		[ChildEntityType(typeof(TList<Examination>))]
		ExaminationCollection,
	}
	
	#endregion MemberChildEntityTypes
	
	#region MemberFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Member"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MemberFilterBuilder : SqlFilterBuilder<MemberColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MemberFilterBuilder class.
		/// </summary>
		public MemberFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the MemberFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MemberFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MemberFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MemberFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MemberFilterBuilder
	
	#region MemberParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Member"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MemberParameterBuilder : ParameterizedSqlFilterBuilder<MemberColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MemberParameterBuilder class.
		/// </summary>
		public MemberParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the MemberParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MemberParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MemberParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MemberParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MemberParameterBuilder
} // end namespace
