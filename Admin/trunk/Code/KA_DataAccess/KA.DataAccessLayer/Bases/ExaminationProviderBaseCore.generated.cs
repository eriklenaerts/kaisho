﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using System.Diagnostics;
using KA.BusinessLogicLayer;
using KA.DataAccessLayer;

#endregion

namespace KA.DataAccessLayer.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ExaminationProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ExaminationProviderBaseCore : EntityProviderBase<KA.BusinessLogicLayer.Examination, KA.BusinessLogicLayer.ExaminationKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KA.BusinessLogicLayer.ExaminationKey key)
		{
			return Delete(transactionManager, key.ID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 id)
		{
			return Delete(null, id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Examination_Location key.
		///		FK_Examination_Location Description: 
		/// </summary>
		/// <param name="locationID"></param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Examination objects.</returns>
		public KA.BusinessLogicLayer.TList<Examination> GetByLocationID(System.Int32 locationID)
		{
			int count = -1;
			return GetByLocationID(locationID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Examination_Location key.
		///		FK_Examination_Location Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="locationID"></param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Examination objects.</returns>
		/// <remarks></remarks>
		public KA.BusinessLogicLayer.TList<Examination> GetByLocationID(TransactionManager transactionManager, System.Int32 locationID)
		{
			int count = -1;
			return GetByLocationID(transactionManager, locationID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Examination_Location key.
		///		FK_Examination_Location Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="locationID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Examination objects.</returns>
		public KA.BusinessLogicLayer.TList<Examination> GetByLocationID(TransactionManager transactionManager, System.Int32 locationID, int start, int pageLength)
		{
			int count = -1;
			return GetByLocationID(transactionManager, locationID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Examination_Location key.
		///		fK_Examination_Location Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="locationID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Examination objects.</returns>
		public KA.BusinessLogicLayer.TList<Examination> GetByLocationID(System.Int32 locationID, int start, int pageLength)
		{
			int count =  -1;
			return GetByLocationID(null, locationID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Examination_Location key.
		///		fK_Examination_Location Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="locationID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Examination objects.</returns>
		public KA.BusinessLogicLayer.TList<Examination> GetByLocationID(System.Int32 locationID, int start, int pageLength,out int count)
		{
			return GetByLocationID(null, locationID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Examination_Location key.
		///		FK_Examination_Location Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="locationID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Examination objects.</returns>
		public abstract KA.BusinessLogicLayer.TList<Examination> GetByLocationID(TransactionManager transactionManager, System.Int32 locationID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Examination_Degree key.
		///		FK_Examination_Degree Description: 
		/// </summary>
		/// <param name="degreeID"></param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Examination objects.</returns>
		public KA.BusinessLogicLayer.TList<Examination> GetByDegreeID(System.Int32 degreeID)
		{
			int count = -1;
			return GetByDegreeID(degreeID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Examination_Degree key.
		///		FK_Examination_Degree Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="degreeID"></param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Examination objects.</returns>
		/// <remarks></remarks>
		public KA.BusinessLogicLayer.TList<Examination> GetByDegreeID(TransactionManager transactionManager, System.Int32 degreeID)
		{
			int count = -1;
			return GetByDegreeID(transactionManager, degreeID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Examination_Degree key.
		///		FK_Examination_Degree Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="degreeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Examination objects.</returns>
		public KA.BusinessLogicLayer.TList<Examination> GetByDegreeID(TransactionManager transactionManager, System.Int32 degreeID, int start, int pageLength)
		{
			int count = -1;
			return GetByDegreeID(transactionManager, degreeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Examination_Degree key.
		///		fK_Examination_Degree Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="degreeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Examination objects.</returns>
		public KA.BusinessLogicLayer.TList<Examination> GetByDegreeID(System.Int32 degreeID, int start, int pageLength)
		{
			int count =  -1;
			return GetByDegreeID(null, degreeID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Examination_Degree key.
		///		fK_Examination_Degree Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="degreeID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Examination objects.</returns>
		public KA.BusinessLogicLayer.TList<Examination> GetByDegreeID(System.Int32 degreeID, int start, int pageLength,out int count)
		{
			return GetByDegreeID(null, degreeID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Examination_Degree key.
		///		FK_Examination_Degree Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="degreeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Examination objects.</returns>
		public abstract KA.BusinessLogicLayer.TList<Examination> GetByDegreeID(TransactionManager transactionManager, System.Int32 degreeID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Examination_Member key.
		///		FK_Examination_Member Description: 
		/// </summary>
		/// <param name="memberID"></param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Examination objects.</returns>
		public KA.BusinessLogicLayer.TList<Examination> GetByMemberID(System.Int32 memberID)
		{
			int count = -1;
			return GetByMemberID(memberID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Examination_Member key.
		///		FK_Examination_Member Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="memberID"></param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Examination objects.</returns>
		/// <remarks></remarks>
		public KA.BusinessLogicLayer.TList<Examination> GetByMemberID(TransactionManager transactionManager, System.Int32 memberID)
		{
			int count = -1;
			return GetByMemberID(transactionManager, memberID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Examination_Member key.
		///		FK_Examination_Member Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="memberID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Examination objects.</returns>
		public KA.BusinessLogicLayer.TList<Examination> GetByMemberID(TransactionManager transactionManager, System.Int32 memberID, int start, int pageLength)
		{
			int count = -1;
			return GetByMemberID(transactionManager, memberID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Examination_Member key.
		///		fK_Examination_Member Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="memberID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Examination objects.</returns>
		public KA.BusinessLogicLayer.TList<Examination> GetByMemberID(System.Int32 memberID, int start, int pageLength)
		{
			int count =  -1;
			return GetByMemberID(null, memberID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Examination_Member key.
		///		fK_Examination_Member Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="memberID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Examination objects.</returns>
		public KA.BusinessLogicLayer.TList<Examination> GetByMemberID(System.Int32 memberID, int start, int pageLength,out int count)
		{
			return GetByMemberID(null, memberID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Examination_Member key.
		///		FK_Examination_Member Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="memberID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.Examination objects.</returns>
		public abstract KA.BusinessLogicLayer.TList<Examination> GetByMemberID(TransactionManager transactionManager, System.Int32 memberID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KA.BusinessLogicLayer.Examination Get(TransactionManager transactionManager, KA.BusinessLogicLayer.ExaminationKey key, int start, int pageLength)
		{
			return GetByID(transactionManager, key.ID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key Examinations_PK index.
		/// </summary>
		/// <param name="id"></param>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.Examination"/> class.</returns>
		public KA.BusinessLogicLayer.Examination GetByID(System.Int32 id)
		{
			int count = -1;
			return GetByID(null,id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the Examinations_PK index.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.Examination"/> class.</returns>
		public KA.BusinessLogicLayer.Examination GetByID(System.Int32 id, int start, int pageLength)
		{
			int count = -1;
			return GetByID(null, id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the Examinations_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.Examination"/> class.</returns>
		public KA.BusinessLogicLayer.Examination GetByID(TransactionManager transactionManager, System.Int32 id)
		{
			int count = -1;
			return GetByID(transactionManager, id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the Examinations_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.Examination"/> class.</returns>
		public KA.BusinessLogicLayer.Examination GetByID(TransactionManager transactionManager, System.Int32 id, int start, int pageLength)
		{
			int count = -1;
			return GetByID(transactionManager, id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the Examinations_PK index.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.Examination"/> class.</returns>
		public KA.BusinessLogicLayer.Examination GetByID(System.Int32 id, int start, int pageLength, out int count)
		{
			return GetByID(null, id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the Examinations_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.Examination"/> class.</returns>
		public abstract KA.BusinessLogicLayer.Examination GetByID(TransactionManager transactionManager, System.Int32 id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a KA.BusinessLogicLayer.TList&lt;Examination&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="KA.BusinessLogicLayer.TList&lt;Examination&gt;"/></returns>
		public static KA.BusinessLogicLayer.TList<Examination> Fill(IDataReader reader, KA.BusinessLogicLayer.TList<Examination> rows, int start, int pageLength)
		{
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
					return rows; // not enough rows, just return
			}

			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done

				string key = null;
				
				KA.BusinessLogicLayer.Examination c = null;
				if (DataRepository.Provider.UseEntityFactory)
				{
					key = @"Examination" 
							+ (reader.IsDBNull(reader.GetOrdinal("ID"))?(int)0:(System.Int32)reader["ID"]).ToString();

					c = EntityManager.LocateOrCreate<Examination>(
						key.ToString(), // EntityTrackingKey 
						"Examination",  //Creational Type
						DataRepository.Provider.EntityCreationalFactoryType,  //Factory used to create entity
						DataRepository.Provider.EnableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KA.BusinessLogicLayer.Examination();
				}
				
				if (!DataRepository.Provider.EnableEntityTracking || c.EntityState == EntityState.Added)
                {
					c.SuppressEntityEvents = true;
					c.ID = (System.Int32)reader["ID"];
					c.MemberID = (System.Int32)reader["MemberID"];
					c.DegreeID = (System.Int32)reader["DegreeID"];
					c.LocationID = (System.Int32)reader["LocationID"];
					c.Date = (reader.IsDBNull(reader.GetOrdinal("Date")))?null:(System.DateTime?)reader["Date"];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
			return rows;
		}
		
		/// <summary>
		/// Refreshes the <see cref="KA.BusinessLogicLayer.Examination"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KA.BusinessLogicLayer.Examination"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KA.BusinessLogicLayer.Examination entity)
		{
			if (!reader.Read()) return;
			
			entity.ID = (System.Int32)reader["ID"];
			entity.MemberID = (System.Int32)reader["MemberID"];
			entity.DegreeID = (System.Int32)reader["DegreeID"];
			entity.LocationID = (System.Int32)reader["LocationID"];
			entity.Date = (reader.IsDBNull(reader.GetOrdinal("Date")))?null:(System.DateTime?)reader["Date"];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KA.BusinessLogicLayer.Examination"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KA.BusinessLogicLayer.Examination"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KA.BusinessLogicLayer.Examination entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ID = (System.Int32)dataRow["ID"];
			entity.MemberID = (System.Int32)dataRow["MemberID"];
			entity.DegreeID = (System.Int32)dataRow["DegreeID"];
			entity.LocationID = (System.Int32)dataRow["LocationID"];
			entity.Date = (Convert.IsDBNull(dataRow["Date"]))?null:(System.DateTime?)dataRow["Date"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KA.BusinessLogicLayer.Examination"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KA.BusinessLogicLayer.Examination Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		internal override void DeepLoad(TransactionManager transactionManager, KA.BusinessLogicLayer.Examination entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, ChildEntityTypesList innerList)
		{
			if(entity == null)
				return;

			#region LocationIDSource	
			if (CanDeepLoad(entity, "Location", "LocationIDSource", deepLoadType, innerList) 
				&& entity.LocationIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.LocationID;
				Location tmpEntity = EntityManager.LocateEntity<Location>(EntityLocator.ConstructKeyFromPkItems(typeof(Location), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.LocationIDSource = tmpEntity;
				else
					entity.LocationIDSource = DataRepository.LocationProvider.GetByID(entity.LocationID);
			
				if (deep && entity.LocationIDSource != null)
				{
					DataRepository.LocationProvider.DeepLoad(transactionManager, entity.LocationIDSource, deep, deepLoadType, childTypes, innerList);
				}
			}
			#endregion LocationIDSource

			#region DegreeIDSource	
			if (CanDeepLoad(entity, "Degree", "DegreeIDSource", deepLoadType, innerList) 
				&& entity.DegreeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.DegreeID;
				Degree tmpEntity = EntityManager.LocateEntity<Degree>(EntityLocator.ConstructKeyFromPkItems(typeof(Degree), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.DegreeIDSource = tmpEntity;
				else
					entity.DegreeIDSource = DataRepository.DegreeProvider.GetByID(entity.DegreeID);
			
				if (deep && entity.DegreeIDSource != null)
				{
					DataRepository.DegreeProvider.DeepLoad(transactionManager, entity.DegreeIDSource, deep, deepLoadType, childTypes, innerList);
				}
			}
			#endregion DegreeIDSource

			#region MemberIDSource	
			if (CanDeepLoad(entity, "Member", "MemberIDSource", deepLoadType, innerList) 
				&& entity.MemberIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.MemberID;
				Member tmpEntity = EntityManager.LocateEntity<Member>(EntityLocator.ConstructKeyFromPkItems(typeof(Member), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.MemberIDSource = tmpEntity;
				else
					entity.MemberIDSource = DataRepository.MemberProvider.GetByID(entity.MemberID);
			
				if (deep && entity.MemberIDSource != null)
				{
					DataRepository.MemberProvider.DeepLoad(transactionManager, entity.MemberIDSource, deep, deepLoadType, childTypes, innerList);
				}
			}
			#endregion MemberIDSource
			
			// Load Entity through Provider
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KA.BusinessLogicLayer.Examination object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KA.BusinessLogicLayer.Examination instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KA.BusinessLogicLayer.Examination Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		internal override bool DeepSave(TransactionManager transactionManager, KA.BusinessLogicLayer.Examination entity, DeepSaveType deepSaveType, System.Type[] childTypes, ChildEntityTypesList innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region LocationIDSource
			if (CanDeepSave(entity, "Location", "LocationIDSource", deepSaveType, innerList) 
				&& entity.LocationIDSource != null)
			{
				DataRepository.LocationProvider.Save(transactionManager, entity.LocationIDSource);
				entity.LocationID = entity.LocationIDSource.ID;
			}
			#endregion 
			
			#region DegreeIDSource
			if (CanDeepSave(entity, "Degree", "DegreeIDSource", deepSaveType, innerList) 
				&& entity.DegreeIDSource != null)
			{
				DataRepository.DegreeProvider.Save(transactionManager, entity.DegreeIDSource);
				entity.DegreeID = entity.DegreeIDSource.ID;
			}
			#endregion 
			
			#region MemberIDSource
			if (CanDeepSave(entity, "Member", "MemberIDSource", deepSaveType, innerList) 
				&& entity.MemberIDSource != null)
			{
				DataRepository.MemberProvider.Save(transactionManager, entity.MemberIDSource);
				entity.MemberID = entity.MemberIDSource.ID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			this.Save(transactionManager, entity);
			
			
						
			return true;
		}
		#endregion
	} // end class
	
	#region ExaminationChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KA.BusinessLogicLayer.Examination</c>
	///</summary>
	public enum ExaminationChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Location</c> at LocationIDSource
		///</summary>
		[ChildEntityType(typeof(Location))]
		Location,
			
		///<summary>
		/// Composite Property for <c>Degree</c> at DegreeIDSource
		///</summary>
		[ChildEntityType(typeof(Degree))]
		Degree,
			
		///<summary>
		/// Composite Property for <c>Member</c> at MemberIDSource
		///</summary>
		[ChildEntityType(typeof(Member))]
		Member,
		}
	
	#endregion ExaminationChildEntityTypes
	
	#region ExaminationFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Examination"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ExaminationFilterBuilder : SqlFilterBuilder<ExaminationColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ExaminationFilterBuilder class.
		/// </summary>
		public ExaminationFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ExaminationFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ExaminationFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ExaminationFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ExaminationFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ExaminationFilterBuilder
	
	#region ExaminationParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Examination"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ExaminationParameterBuilder : ParameterizedSqlFilterBuilder<ExaminationColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ExaminationParameterBuilder class.
		/// </summary>
		public ExaminationParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ExaminationParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ExaminationParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ExaminationParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ExaminationParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ExaminationParameterBuilder
} // end namespace
