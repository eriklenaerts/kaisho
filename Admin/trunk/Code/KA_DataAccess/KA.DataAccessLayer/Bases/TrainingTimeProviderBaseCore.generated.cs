﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using System.Diagnostics;
using KA.BusinessLogicLayer;
using KA.DataAccessLayer;

#endregion

namespace KA.DataAccessLayer.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="TrainingTimeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class TrainingTimeProviderBaseCore : EntityProviderBase<KA.BusinessLogicLayer.TrainingTime, KA.BusinessLogicLayer.TrainingTimeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KA.BusinessLogicLayer.TrainingTimeKey key)
		{
			return Delete(transactionManager, key.ID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 id)
		{
			return Delete(null, id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TrainingTime_Location key.
		///		FK_TrainingTime_Location Description: 
		/// </summary>
		/// <param name="locationID"></param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.TrainingTime objects.</returns>
		public KA.BusinessLogicLayer.TList<TrainingTime> GetByLocationID(System.Int32 locationID)
		{
			int count = -1;
			return GetByLocationID(locationID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TrainingTime_Location key.
		///		FK_TrainingTime_Location Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="locationID"></param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.TrainingTime objects.</returns>
		/// <remarks></remarks>
		public KA.BusinessLogicLayer.TList<TrainingTime> GetByLocationID(TransactionManager transactionManager, System.Int32 locationID)
		{
			int count = -1;
			return GetByLocationID(transactionManager, locationID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TrainingTime_Location key.
		///		FK_TrainingTime_Location Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="locationID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.TrainingTime objects.</returns>
		public KA.BusinessLogicLayer.TList<TrainingTime> GetByLocationID(TransactionManager transactionManager, System.Int32 locationID, int start, int pageLength)
		{
			int count = -1;
			return GetByLocationID(transactionManager, locationID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TrainingTime_Location key.
		///		fK_TrainingTime_Location Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="locationID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.TrainingTime objects.</returns>
		public KA.BusinessLogicLayer.TList<TrainingTime> GetByLocationID(System.Int32 locationID, int start, int pageLength)
		{
			int count =  -1;
			return GetByLocationID(null, locationID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TrainingTime_Location key.
		///		fK_TrainingTime_Location Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="locationID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.TrainingTime objects.</returns>
		public KA.BusinessLogicLayer.TList<TrainingTime> GetByLocationID(System.Int32 locationID, int start, int pageLength,out int count)
		{
			return GetByLocationID(null, locationID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TrainingTime_Location key.
		///		FK_TrainingTime_Location Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="locationID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.TrainingTime objects.</returns>
		public abstract KA.BusinessLogicLayer.TList<TrainingTime> GetByLocationID(TransactionManager transactionManager, System.Int32 locationID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TrainingTime_TrainingPeriod key.
		///		FK_TrainingTime_TrainingPeriod Description: 
		/// </summary>
		/// <param name="trainingPeriodID"></param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.TrainingTime objects.</returns>
		public KA.BusinessLogicLayer.TList<TrainingTime> GetByTrainingPeriodID(System.Int32 trainingPeriodID)
		{
			int count = -1;
			return GetByTrainingPeriodID(trainingPeriodID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TrainingTime_TrainingPeriod key.
		///		FK_TrainingTime_TrainingPeriod Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="trainingPeriodID"></param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.TrainingTime objects.</returns>
		/// <remarks></remarks>
		public KA.BusinessLogicLayer.TList<TrainingTime> GetByTrainingPeriodID(TransactionManager transactionManager, System.Int32 trainingPeriodID)
		{
			int count = -1;
			return GetByTrainingPeriodID(transactionManager, trainingPeriodID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TrainingTime_TrainingPeriod key.
		///		FK_TrainingTime_TrainingPeriod Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="trainingPeriodID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.TrainingTime objects.</returns>
		public KA.BusinessLogicLayer.TList<TrainingTime> GetByTrainingPeriodID(TransactionManager transactionManager, System.Int32 trainingPeriodID, int start, int pageLength)
		{
			int count = -1;
			return GetByTrainingPeriodID(transactionManager, trainingPeriodID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TrainingTime_TrainingPeriod key.
		///		fK_TrainingTime_TrainingPeriod Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="trainingPeriodID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.TrainingTime objects.</returns>
		public KA.BusinessLogicLayer.TList<TrainingTime> GetByTrainingPeriodID(System.Int32 trainingPeriodID, int start, int pageLength)
		{
			int count =  -1;
			return GetByTrainingPeriodID(null, trainingPeriodID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TrainingTime_TrainingPeriod key.
		///		fK_TrainingTime_TrainingPeriod Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="trainingPeriodID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.TrainingTime objects.</returns>
		public KA.BusinessLogicLayer.TList<TrainingTime> GetByTrainingPeriodID(System.Int32 trainingPeriodID, int start, int pageLength,out int count)
		{
			return GetByTrainingPeriodID(null, trainingPeriodID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TrainingTime_TrainingPeriod key.
		///		FK_TrainingTime_TrainingPeriod Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="trainingPeriodID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.TrainingTime objects.</returns>
		public abstract KA.BusinessLogicLayer.TList<TrainingTime> GetByTrainingPeriodID(TransactionManager transactionManager, System.Int32 trainingPeriodID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KA.BusinessLogicLayer.TrainingTime Get(TransactionManager transactionManager, KA.BusinessLogicLayer.TrainingTimeKey key, int start, int pageLength)
		{
			return GetByID(transactionManager, key.ID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_TrainingTime index.
		/// </summary>
		/// <param name="id"></param>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.TrainingTime"/> class.</returns>
		public KA.BusinessLogicLayer.TrainingTime GetByID(System.Int32 id)
		{
			int count = -1;
			return GetByID(null,id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TrainingTime index.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.TrainingTime"/> class.</returns>
		public KA.BusinessLogicLayer.TrainingTime GetByID(System.Int32 id, int start, int pageLength)
		{
			int count = -1;
			return GetByID(null, id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TrainingTime index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.TrainingTime"/> class.</returns>
		public KA.BusinessLogicLayer.TrainingTime GetByID(TransactionManager transactionManager, System.Int32 id)
		{
			int count = -1;
			return GetByID(transactionManager, id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TrainingTime index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.TrainingTime"/> class.</returns>
		public KA.BusinessLogicLayer.TrainingTime GetByID(TransactionManager transactionManager, System.Int32 id, int start, int pageLength)
		{
			int count = -1;
			return GetByID(transactionManager, id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TrainingTime index.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.TrainingTime"/> class.</returns>
		public KA.BusinessLogicLayer.TrainingTime GetByID(System.Int32 id, int start, int pageLength, out int count)
		{
			return GetByID(null, id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TrainingTime index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.TrainingTime"/> class.</returns>
		public abstract KA.BusinessLogicLayer.TrainingTime GetByID(TransactionManager transactionManager, System.Int32 id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a KA.BusinessLogicLayer.TList&lt;TrainingTime&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="KA.BusinessLogicLayer.TList&lt;TrainingTime&gt;"/></returns>
		public static KA.BusinessLogicLayer.TList<TrainingTime> Fill(IDataReader reader, KA.BusinessLogicLayer.TList<TrainingTime> rows, int start, int pageLength)
		{
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
					return rows; // not enough rows, just return
			}

			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done

				string key = null;
				
				KA.BusinessLogicLayer.TrainingTime c = null;
				if (DataRepository.Provider.UseEntityFactory)
				{
					key = @"TrainingTime" 
							+ (reader.IsDBNull(reader.GetOrdinal("ID"))?(int)0:(System.Int32)reader["ID"]).ToString();

					c = EntityManager.LocateOrCreate<TrainingTime>(
						key.ToString(), // EntityTrackingKey 
						"TrainingTime",  //Creational Type
						DataRepository.Provider.EntityCreationalFactoryType,  //Factory used to create entity
						DataRepository.Provider.EnableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KA.BusinessLogicLayer.TrainingTime();
				}
				
				if (!DataRepository.Provider.EnableEntityTracking || c.EntityState == EntityState.Added)
                {
					c.SuppressEntityEvents = true;
					c.ID = (System.Int32)reader["ID"];
					c.TrainingPeriodID = (System.Int32)reader["TrainingPeriodID"];
					c.LocationID = (System.Int32)reader["LocationID"];
					c.DayOfTheWeek = (System.Byte)reader["DayOfTheWeek"];
					c.StartOfTraining = (System.String)reader["StartOfTraining"];
					c.EndOfTraining = (System.String)reader["EndOfTraining"];
					c.Audience = (System.String)reader["Audience"];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
			return rows;
		}
		
		/// <summary>
		/// Refreshes the <see cref="KA.BusinessLogicLayer.TrainingTime"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KA.BusinessLogicLayer.TrainingTime"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KA.BusinessLogicLayer.TrainingTime entity)
		{
			if (!reader.Read()) return;
			
			entity.ID = (System.Int32)reader["ID"];
			entity.TrainingPeriodID = (System.Int32)reader["TrainingPeriodID"];
			entity.LocationID = (System.Int32)reader["LocationID"];
			entity.DayOfTheWeek = (System.Byte)reader["DayOfTheWeek"];
			entity.StartOfTraining = (System.String)reader["StartOfTraining"];
			entity.EndOfTraining = (System.String)reader["EndOfTraining"];
			entity.Audience = (System.String)reader["Audience"];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KA.BusinessLogicLayer.TrainingTime"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KA.BusinessLogicLayer.TrainingTime"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KA.BusinessLogicLayer.TrainingTime entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ID = (System.Int32)dataRow["ID"];
			entity.TrainingPeriodID = (System.Int32)dataRow["TrainingPeriodID"];
			entity.LocationID = (System.Int32)dataRow["LocationID"];
			entity.DayOfTheWeek = (System.Byte)dataRow["DayOfTheWeek"];
			entity.StartOfTraining = (System.String)dataRow["StartOfTraining"];
			entity.EndOfTraining = (System.String)dataRow["EndOfTraining"];
			entity.Audience = (System.String)dataRow["Audience"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KA.BusinessLogicLayer.TrainingTime"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KA.BusinessLogicLayer.TrainingTime Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		internal override void DeepLoad(TransactionManager transactionManager, KA.BusinessLogicLayer.TrainingTime entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, ChildEntityTypesList innerList)
		{
			if(entity == null)
				return;

			#region LocationIDSource	
			if (CanDeepLoad(entity, "Location", "LocationIDSource", deepLoadType, innerList) 
				&& entity.LocationIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.LocationID;
				Location tmpEntity = EntityManager.LocateEntity<Location>(EntityLocator.ConstructKeyFromPkItems(typeof(Location), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.LocationIDSource = tmpEntity;
				else
					entity.LocationIDSource = DataRepository.LocationProvider.GetByID(entity.LocationID);
			
				if (deep && entity.LocationIDSource != null)
				{
					DataRepository.LocationProvider.DeepLoad(transactionManager, entity.LocationIDSource, deep, deepLoadType, childTypes, innerList);
				}
			}
			#endregion LocationIDSource

			#region TrainingPeriodIDSource	
			if (CanDeepLoad(entity, "TrainingPeriod", "TrainingPeriodIDSource", deepLoadType, innerList) 
				&& entity.TrainingPeriodIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.TrainingPeriodID;
				TrainingPeriod tmpEntity = EntityManager.LocateEntity<TrainingPeriod>(EntityLocator.ConstructKeyFromPkItems(typeof(TrainingPeriod), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.TrainingPeriodIDSource = tmpEntity;
				else
					entity.TrainingPeriodIDSource = DataRepository.TrainingPeriodProvider.GetByID(entity.TrainingPeriodID);
			
				if (deep && entity.TrainingPeriodIDSource != null)
				{
					DataRepository.TrainingPeriodProvider.DeepLoad(transactionManager, entity.TrainingPeriodIDSource, deep, deepLoadType, childTypes, innerList);
				}
			}
			#endregion TrainingPeriodIDSource
			
			// Load Entity through Provider
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KA.BusinessLogicLayer.TrainingTime object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KA.BusinessLogicLayer.TrainingTime instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KA.BusinessLogicLayer.TrainingTime Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		internal override bool DeepSave(TransactionManager transactionManager, KA.BusinessLogicLayer.TrainingTime entity, DeepSaveType deepSaveType, System.Type[] childTypes, ChildEntityTypesList innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region LocationIDSource
			if (CanDeepSave(entity, "Location", "LocationIDSource", deepSaveType, innerList) 
				&& entity.LocationIDSource != null)
			{
				DataRepository.LocationProvider.Save(transactionManager, entity.LocationIDSource);
				entity.LocationID = entity.LocationIDSource.ID;
			}
			#endregion 
			
			#region TrainingPeriodIDSource
			if (CanDeepSave(entity, "TrainingPeriod", "TrainingPeriodIDSource", deepSaveType, innerList) 
				&& entity.TrainingPeriodIDSource != null)
			{
				DataRepository.TrainingPeriodProvider.Save(transactionManager, entity.TrainingPeriodIDSource);
				entity.TrainingPeriodID = entity.TrainingPeriodIDSource.ID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			this.Save(transactionManager, entity);
			
			
						
			return true;
		}
		#endregion
	} // end class
	
	#region TrainingTimeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KA.BusinessLogicLayer.TrainingTime</c>
	///</summary>
	public enum TrainingTimeChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Location</c> at LocationIDSource
		///</summary>
		[ChildEntityType(typeof(Location))]
		Location,
			
		///<summary>
		/// Composite Property for <c>TrainingPeriod</c> at TrainingPeriodIDSource
		///</summary>
		[ChildEntityType(typeof(TrainingPeriod))]
		TrainingPeriod,
		}
	
	#endregion TrainingTimeChildEntityTypes
	
	#region TrainingTimeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TrainingTime"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TrainingTimeFilterBuilder : SqlFilterBuilder<TrainingTimeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TrainingTimeFilterBuilder class.
		/// </summary>
		public TrainingTimeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TrainingTimeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TrainingTimeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TrainingTimeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TrainingTimeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TrainingTimeFilterBuilder
	
	#region TrainingTimeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TrainingTime"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TrainingTimeParameterBuilder : ParameterizedSqlFilterBuilder<TrainingTimeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TrainingTimeParameterBuilder class.
		/// </summary>
		public TrainingTimeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TrainingTimeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TrainingTimeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TrainingTimeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TrainingTimeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TrainingTimeParameterBuilder
} // end namespace
