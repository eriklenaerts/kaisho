﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using System.Diagnostics;
using KA.BusinessLogicLayer;
using KA.DataAccessLayer;

#endregion

namespace KA.DataAccessLayer.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="PriceSchemeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class PriceSchemeProviderBaseCore : EntityProviderBase<KA.BusinessLogicLayer.PriceScheme, KA.BusinessLogicLayer.PriceSchemeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KA.BusinessLogicLayer.PriceSchemeKey key)
		{
			return Delete(transactionManager, key.ID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 id)
		{
			return Delete(null, id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PriceScheme_PriceType key.
		///		FK_PriceScheme_PriceType Description: 
		/// </summary>
		/// <param name="priceTypeID"></param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.PriceScheme objects.</returns>
		public KA.BusinessLogicLayer.TList<PriceScheme> GetByPriceTypeID(System.Int32 priceTypeID)
		{
			int count = -1;
			return GetByPriceTypeID(priceTypeID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PriceScheme_PriceType key.
		///		FK_PriceScheme_PriceType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="priceTypeID"></param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.PriceScheme objects.</returns>
		/// <remarks></remarks>
		public KA.BusinessLogicLayer.TList<PriceScheme> GetByPriceTypeID(TransactionManager transactionManager, System.Int32 priceTypeID)
		{
			int count = -1;
			return GetByPriceTypeID(transactionManager, priceTypeID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_PriceScheme_PriceType key.
		///		FK_PriceScheme_PriceType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="priceTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.PriceScheme objects.</returns>
		public KA.BusinessLogicLayer.TList<PriceScheme> GetByPriceTypeID(TransactionManager transactionManager, System.Int32 priceTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByPriceTypeID(transactionManager, priceTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PriceScheme_PriceType key.
		///		fK_PriceScheme_PriceType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="priceTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.PriceScheme objects.</returns>
		public KA.BusinessLogicLayer.TList<PriceScheme> GetByPriceTypeID(System.Int32 priceTypeID, int start, int pageLength)
		{
			int count =  -1;
			return GetByPriceTypeID(null, priceTypeID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PriceScheme_PriceType key.
		///		fK_PriceScheme_PriceType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="priceTypeID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.PriceScheme objects.</returns>
		public KA.BusinessLogicLayer.TList<PriceScheme> GetByPriceTypeID(System.Int32 priceTypeID, int start, int pageLength,out int count)
		{
			return GetByPriceTypeID(null, priceTypeID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PriceScheme_PriceType key.
		///		FK_PriceScheme_PriceType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="priceTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KA.BusinessLogicLayer.PriceScheme objects.</returns>
		public abstract KA.BusinessLogicLayer.TList<PriceScheme> GetByPriceTypeID(TransactionManager transactionManager, System.Int32 priceTypeID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KA.BusinessLogicLayer.PriceScheme Get(TransactionManager transactionManager, KA.BusinessLogicLayer.PriceSchemeKey key, int start, int pageLength)
		{
			return GetByID(transactionManager, key.ID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PriceScheme_PK index.
		/// </summary>
		/// <param name="id"></param>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.PriceScheme"/> class.</returns>
		public KA.BusinessLogicLayer.PriceScheme GetByID(System.Int32 id)
		{
			int count = -1;
			return GetByID(null,id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PriceScheme_PK index.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.PriceScheme"/> class.</returns>
		public KA.BusinessLogicLayer.PriceScheme GetByID(System.Int32 id, int start, int pageLength)
		{
			int count = -1;
			return GetByID(null, id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PriceScheme_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.PriceScheme"/> class.</returns>
		public KA.BusinessLogicLayer.PriceScheme GetByID(TransactionManager transactionManager, System.Int32 id)
		{
			int count = -1;
			return GetByID(transactionManager, id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PriceScheme_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.PriceScheme"/> class.</returns>
		public KA.BusinessLogicLayer.PriceScheme GetByID(TransactionManager transactionManager, System.Int32 id, int start, int pageLength)
		{
			int count = -1;
			return GetByID(transactionManager, id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PriceScheme_PK index.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.PriceScheme"/> class.</returns>
		public KA.BusinessLogicLayer.PriceScheme GetByID(System.Int32 id, int start, int pageLength, out int count)
		{
			return GetByID(null, id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PriceScheme_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.PriceScheme"/> class.</returns>
		public abstract KA.BusinessLogicLayer.PriceScheme GetByID(TransactionManager transactionManager, System.Int32 id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a KA.BusinessLogicLayer.TList&lt;PriceScheme&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="KA.BusinessLogicLayer.TList&lt;PriceScheme&gt;"/></returns>
		public static KA.BusinessLogicLayer.TList<PriceScheme> Fill(IDataReader reader, KA.BusinessLogicLayer.TList<PriceScheme> rows, int start, int pageLength)
		{
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
					return rows; // not enough rows, just return
			}

			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done

				string key = null;
				
				KA.BusinessLogicLayer.PriceScheme c = null;
				if (DataRepository.Provider.UseEntityFactory)
				{
					key = @"PriceScheme" 
							+ (reader.IsDBNull(reader.GetOrdinal("ID"))?(int)0:(System.Int32)reader["ID"]).ToString();

					c = EntityManager.LocateOrCreate<PriceScheme>(
						key.ToString(), // EntityTrackingKey 
						"PriceScheme",  //Creational Type
						DataRepository.Provider.EntityCreationalFactoryType,  //Factory used to create entity
						DataRepository.Provider.EnableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KA.BusinessLogicLayer.PriceScheme();
				}
				
				if (!DataRepository.Provider.EnableEntityTracking || c.EntityState == EntityState.Added)
                {
					c.SuppressEntityEvents = true;
					c.ID = (System.Int32)reader["ID"];
					c.PriceTypeID = (System.Int32)reader["PriceTypeID"];
					c.Quantity = (System.Int16)reader["Quantity"];
					c.Price = (System.Decimal)reader["Price"];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
			return rows;
		}
		
		/// <summary>
		/// Refreshes the <see cref="KA.BusinessLogicLayer.PriceScheme"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KA.BusinessLogicLayer.PriceScheme"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KA.BusinessLogicLayer.PriceScheme entity)
		{
			if (!reader.Read()) return;
			
			entity.ID = (System.Int32)reader["ID"];
			entity.PriceTypeID = (System.Int32)reader["PriceTypeID"];
			entity.Quantity = (System.Int16)reader["Quantity"];
			entity.Price = (System.Decimal)reader["Price"];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KA.BusinessLogicLayer.PriceScheme"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KA.BusinessLogicLayer.PriceScheme"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KA.BusinessLogicLayer.PriceScheme entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ID = (System.Int32)dataRow["ID"];
			entity.PriceTypeID = (System.Int32)dataRow["PriceTypeID"];
			entity.Quantity = (System.Int16)dataRow["Quantity"];
			entity.Price = (System.Decimal)dataRow["Price"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KA.BusinessLogicLayer.PriceScheme"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KA.BusinessLogicLayer.PriceScheme Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		internal override void DeepLoad(TransactionManager transactionManager, KA.BusinessLogicLayer.PriceScheme entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, ChildEntityTypesList innerList)
		{
			if(entity == null)
				return;

			#region PriceTypeIDSource	
			if (CanDeepLoad(entity, "PriceType", "PriceTypeIDSource", deepLoadType, innerList) 
				&& entity.PriceTypeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.PriceTypeID;
				PriceType tmpEntity = EntityManager.LocateEntity<PriceType>(EntityLocator.ConstructKeyFromPkItems(typeof(PriceType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PriceTypeIDSource = tmpEntity;
				else
					entity.PriceTypeIDSource = DataRepository.PriceTypeProvider.GetByID(entity.PriceTypeID);
			
				if (deep && entity.PriceTypeIDSource != null)
				{
					DataRepository.PriceTypeProvider.DeepLoad(transactionManager, entity.PriceTypeIDSource, deep, deepLoadType, childTypes, innerList);
				}
			}
			#endregion PriceTypeIDSource
			
			// Load Entity through Provider
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KA.BusinessLogicLayer.PriceScheme object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KA.BusinessLogicLayer.PriceScheme instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KA.BusinessLogicLayer.PriceScheme Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		internal override bool DeepSave(TransactionManager transactionManager, KA.BusinessLogicLayer.PriceScheme entity, DeepSaveType deepSaveType, System.Type[] childTypes, ChildEntityTypesList innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region PriceTypeIDSource
			if (CanDeepSave(entity, "PriceType", "PriceTypeIDSource", deepSaveType, innerList) 
				&& entity.PriceTypeIDSource != null)
			{
				DataRepository.PriceTypeProvider.Save(transactionManager, entity.PriceTypeIDSource);
				entity.PriceTypeID = entity.PriceTypeIDSource.ID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			this.Save(transactionManager, entity);
			
			
						
			return true;
		}
		#endregion
	} // end class
	
	#region PriceSchemeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KA.BusinessLogicLayer.PriceScheme</c>
	///</summary>
	public enum PriceSchemeChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>PriceType</c> at PriceTypeIDSource
		///</summary>
		[ChildEntityType(typeof(PriceType))]
		PriceType,
		}
	
	#endregion PriceSchemeChildEntityTypes
	
	#region PriceSchemeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PriceScheme"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PriceSchemeFilterBuilder : SqlFilterBuilder<PriceSchemeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PriceSchemeFilterBuilder class.
		/// </summary>
		public PriceSchemeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PriceSchemeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PriceSchemeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PriceSchemeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PriceSchemeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PriceSchemeFilterBuilder
	
	#region PriceSchemeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PriceScheme"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PriceSchemeParameterBuilder : ParameterizedSqlFilterBuilder<PriceSchemeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PriceSchemeParameterBuilder class.
		/// </summary>
		public PriceSchemeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PriceSchemeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PriceSchemeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PriceSchemeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PriceSchemeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PriceSchemeParameterBuilder
} // end namespace
