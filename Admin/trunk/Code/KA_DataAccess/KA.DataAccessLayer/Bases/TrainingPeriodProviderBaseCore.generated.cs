﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using System.Diagnostics;
using KA.BusinessLogicLayer;
using KA.DataAccessLayer;

#endregion

namespace KA.DataAccessLayer.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="TrainingPeriodProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class TrainingPeriodProviderBaseCore : EntityProviderBase<KA.BusinessLogicLayer.TrainingPeriod, KA.BusinessLogicLayer.TrainingPeriodKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KA.BusinessLogicLayer.TrainingPeriodKey key)
		{
			return Delete(transactionManager, key.ID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 id)
		{
			return Delete(null, id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KA.BusinessLogicLayer.TrainingPeriod Get(TransactionManager transactionManager, KA.BusinessLogicLayer.TrainingPeriodKey key, int start, int pageLength)
		{
			return GetByID(transactionManager, key.ID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_TrainingPeriod index.
		/// </summary>
		/// <param name="id"></param>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.TrainingPeriod"/> class.</returns>
		public KA.BusinessLogicLayer.TrainingPeriod GetByID(System.Int32 id)
		{
			int count = -1;
			return GetByID(null,id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TrainingPeriod index.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.TrainingPeriod"/> class.</returns>
		public KA.BusinessLogicLayer.TrainingPeriod GetByID(System.Int32 id, int start, int pageLength)
		{
			int count = -1;
			return GetByID(null, id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TrainingPeriod index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.TrainingPeriod"/> class.</returns>
		public KA.BusinessLogicLayer.TrainingPeriod GetByID(TransactionManager transactionManager, System.Int32 id)
		{
			int count = -1;
			return GetByID(transactionManager, id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TrainingPeriod index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.TrainingPeriod"/> class.</returns>
		public KA.BusinessLogicLayer.TrainingPeriod GetByID(TransactionManager transactionManager, System.Int32 id, int start, int pageLength)
		{
			int count = -1;
			return GetByID(transactionManager, id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TrainingPeriod index.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.TrainingPeriod"/> class.</returns>
		public KA.BusinessLogicLayer.TrainingPeriod GetByID(System.Int32 id, int start, int pageLength, out int count)
		{
			return GetByID(null, id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TrainingPeriod index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KA.BusinessLogicLayer.TrainingPeriod"/> class.</returns>
		public abstract KA.BusinessLogicLayer.TrainingPeriod GetByID(TransactionManager transactionManager, System.Int32 id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a KA.BusinessLogicLayer.TList&lt;TrainingPeriod&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="KA.BusinessLogicLayer.TList&lt;TrainingPeriod&gt;"/></returns>
		public static KA.BusinessLogicLayer.TList<TrainingPeriod> Fill(IDataReader reader, KA.BusinessLogicLayer.TList<TrainingPeriod> rows, int start, int pageLength)
		{
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
					return rows; // not enough rows, just return
			}

			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done

				string key = null;
				
				KA.BusinessLogicLayer.TrainingPeriod c = null;
				if (DataRepository.Provider.UseEntityFactory)
				{
					key = @"TrainingPeriod" 
							+ (reader.IsDBNull(reader.GetOrdinal("ID"))?(int)0:(System.Int32)reader["ID"]).ToString();

					c = EntityManager.LocateOrCreate<TrainingPeriod>(
						key.ToString(), // EntityTrackingKey 
						"TrainingPeriod",  //Creational Type
						DataRepository.Provider.EntityCreationalFactoryType,  //Factory used to create entity
						DataRepository.Provider.EnableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KA.BusinessLogicLayer.TrainingPeriod();
				}
				
				if (!DataRepository.Provider.EnableEntityTracking || c.EntityState == EntityState.Added)
                {
					c.SuppressEntityEvents = true;
					c.ID = (System.Int32)reader["ID"];
					c.OriginalID = c.ID; //(reader.IsDBNull(reader.GetOrdinal("ID")))?(int)0:(System.Int32)reader["ID"];
					c.Start = (System.String)reader["Start"];
					c.End = (System.String)reader["End"];
					c.Name = (System.String)reader["Name"];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
			return rows;
		}
		
		/// <summary>
		/// Refreshes the <see cref="KA.BusinessLogicLayer.TrainingPeriod"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KA.BusinessLogicLayer.TrainingPeriod"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KA.BusinessLogicLayer.TrainingPeriod entity)
		{
			if (!reader.Read()) return;
			
			entity.ID = (System.Int32)reader["ID"];
			entity.OriginalID = (System.Int32)reader["ID"];
			entity.Start = (System.String)reader["Start"];
			entity.End = (System.String)reader["End"];
			entity.Name = (System.String)reader["Name"];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KA.BusinessLogicLayer.TrainingPeriod"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KA.BusinessLogicLayer.TrainingPeriod"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KA.BusinessLogicLayer.TrainingPeriod entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ID = (System.Int32)dataRow["ID"];
			entity.OriginalID = (System.Int32)dataRow["ID"];
			entity.Start = (System.String)dataRow["Start"];
			entity.End = (System.String)dataRow["End"];
			entity.Name = (System.String)dataRow["Name"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KA.BusinessLogicLayer.TrainingPeriod"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KA.BusinessLogicLayer.TrainingPeriod Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		internal override void DeepLoad(TransactionManager transactionManager, KA.BusinessLogicLayer.TrainingPeriod entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, ChildEntityTypesList innerList)
		{
			if(entity == null)
				return;
			
			// Load Entity through Provider
			// Deep load child collections  - Call GetByID methods when available
			
			#region TrainingTimeCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<TrainingTime>", "TrainingTimeCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				Debug.WriteLine("- property 'TrainingTimeCollection' loaded.");
				#endif 

				entity.TrainingTimeCollection = DataRepository.TrainingTimeProvider.GetByTrainingPeriodID(transactionManager, entity.ID);

				if (deep && entity.TrainingTimeCollection.Count > 0)
				{
					DataRepository.TrainingTimeProvider.DeepLoad(transactionManager, entity.TrainingTimeCollection, deep, deepLoadType, childTypes, innerList);
				}
			}		
			#endregion 
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KA.BusinessLogicLayer.TrainingPeriod object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KA.BusinessLogicLayer.TrainingPeriod instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KA.BusinessLogicLayer.TrainingPeriod Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		internal override bool DeepSave(TransactionManager transactionManager, KA.BusinessLogicLayer.TrainingPeriod entity, DeepSaveType deepSaveType, System.Type[] childTypes, ChildEntityTypesList innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			this.Save(transactionManager, entity);
			
			



			#region List<TrainingTime>
				if (CanDeepSave(entity, "List<TrainingTime>", "TrainingTimeCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(TrainingTime child in entity.TrainingTimeCollection)
					{
						child.TrainingPeriodID = entity.ID;
					}
				
				if (entity.TrainingTimeCollection.Count > 0 || entity.TrainingTimeCollection.DeletedItems.Count > 0)
					DataRepository.TrainingTimeProvider.DeepSave(transactionManager, entity.TrainingTimeCollection, deepSaveType, childTypes, innerList);
				} 
			#endregion 
				

						
			return true;
		}
		#endregion
	} // end class
	
	#region TrainingPeriodChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KA.BusinessLogicLayer.TrainingPeriod</c>
	///</summary>
	public enum TrainingPeriodChildEntityTypes
	{

		///<summary>
		/// Collection of <c>TrainingPeriod</c> as OneToMany for TrainingTimeCollection
		///</summary>
		[ChildEntityType(typeof(TList<TrainingTime>))]
		TrainingTimeCollection,
	}
	
	#endregion TrainingPeriodChildEntityTypes
	
	#region TrainingPeriodFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TrainingPeriod"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TrainingPeriodFilterBuilder : SqlFilterBuilder<TrainingPeriodColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TrainingPeriodFilterBuilder class.
		/// </summary>
		public TrainingPeriodFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TrainingPeriodFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TrainingPeriodFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TrainingPeriodFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TrainingPeriodFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TrainingPeriodFilterBuilder
	
	#region TrainingPeriodParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TrainingPeriod"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TrainingPeriodParameterBuilder : ParameterizedSqlFilterBuilder<TrainingPeriodColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TrainingPeriodParameterBuilder class.
		/// </summary>
		public TrainingPeriodParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TrainingPeriodParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TrainingPeriodParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TrainingPeriodParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TrainingPeriodParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TrainingPeriodParameterBuilder
} // end namespace
