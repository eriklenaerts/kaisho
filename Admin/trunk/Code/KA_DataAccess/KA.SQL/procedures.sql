
Use [KA]
Go
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.PriceType_Get_List procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.PriceType_Get_List') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.PriceType_Get_List
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Gets all records from the PriceType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.PriceType_Get_List

AS


				
				SELECT
					[ID],
					[Name]
				FROM
					dbo.[PriceType]
					
				Select @@ROWCOUNT
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.PriceType_GetPaged procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.PriceType_GetPaged') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.PriceType_GetPaged
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Gets records from the PriceType table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.PriceType_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				Create Table #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [ID] int 
				)
				
				-- Insert into the temp table
				declare @SQL as nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex (ID)'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + convert(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [ID]'
				SET @SQL = @SQL + ' FROM dbo.[PriceType]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				exec sp_executesql @SQL

				-- Return paged results
				SELECT O.[ID], O.[Name]
				FROM
				    dbo.[PriceType] O,
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexID > @PageLowerBound
					AND O.[ID] = PageIndex.[ID]
				ORDER BY
				    PageIndex.IndexID
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) as TotalRowCount'
				SET @SQL = @SQL + ' FROM dbo.[PriceType]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				exec sp_executesql @SQL
			
				END
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.PriceType_Insert procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.PriceType_Insert') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.PriceType_Insert
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Inserts a record into the PriceType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.PriceType_Insert
(

	@ID int   ,

	@Name nvarchar (100)  
)
AS


					
				INSERT INTO dbo.[PriceType]
					(
					[ID]
					,[Name]
					)
				VALUES
					(
					@ID
					,@Name
					)
				
									
							
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.PriceType_Update procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.PriceType_Update') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.PriceType_Update
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Updates a record in the PriceType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.PriceType_Update
(

	@ID int   ,

	@OriginalID int   ,

	@Name nvarchar (100)  
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					dbo.[PriceType]
				SET
					[ID] = @ID
					,[Name] = @Name
				WHERE
[ID] = @OriginalID 
				
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.PriceType_Delete procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.PriceType_Delete') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.PriceType_Delete
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Deletes a record in the PriceType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.PriceType_Delete
(

	@ID int   
)
AS


				DELETE FROM dbo.[PriceType] WITH (ROWLOCK) 
				WHERE
					[ID] = @ID
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.PriceType_GetByID procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.PriceType_GetByID') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.PriceType_GetByID
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Select records from the PriceType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.PriceType_GetByID
(

	@ID int   
)
AS


				SELECT
					[ID],
					[Name]
				FROM
					dbo.[PriceType]
				WHERE
					[ID] = @ID
			Select @@ROWCOUNT
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.PriceType_Find procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.PriceType_Find') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.PriceType_Find
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Finds records in the PriceType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.PriceType_Find
(

	@SearchUsingOR bit   = null ,

	@ID int   = null ,

	@Name nvarchar (100)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ID]
	, [Name]
    FROM
	dbo.[PriceType]
    WHERE 
	 ([ID] = @ID OR @ID is null)
	AND ([Name] = @Name OR @Name is null)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ID]
	, [Name]
    FROM
	dbo.[PriceType]
    WHERE 
	 ([ID] = @ID AND @ID is not null)
	OR ([Name] = @Name AND @Name is not null)
	Select @@ROWCOUNT			
  END
				

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Degree_Get_List procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Degree_Get_List') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Degree_Get_List
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Gets all records from the Degree table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Degree_Get_List

AS


				
				SELECT
					[ID],
					[Rank],
					[Title],
					[Color],
					[Stripes],
					[Picture]
				FROM
					dbo.[Degree]
					
				Select @@ROWCOUNT
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Degree_GetPaged procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Degree_GetPaged') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Degree_GetPaged
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Gets records from the Degree table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Degree_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				Create Table #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [ID] int 
				)
				
				-- Insert into the temp table
				declare @SQL as nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex (ID)'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + convert(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [ID]'
				SET @SQL = @SQL + ' FROM dbo.[Degree]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				exec sp_executesql @SQL

				-- Return paged results
				SELECT O.[ID], O.[Rank], O.[Title], O.[Color], O.[Stripes], O.[Picture]
				FROM
				    dbo.[Degree] O,
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexID > @PageLowerBound
					AND O.[ID] = PageIndex.[ID]
				ORDER BY
				    PageIndex.IndexID
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) as TotalRowCount'
				SET @SQL = @SQL + ' FROM dbo.[Degree]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				exec sp_executesql @SQL
			
				END
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Degree_Insert procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Degree_Insert') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Degree_Insert
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Inserts a record into the Degree table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Degree_Insert
(

	@ID int   ,

	@Rank int   ,

	@Title nvarchar (20)  ,

	@Color nvarchar (10)  ,

	@Stripes int   ,

	@Picture image (16)  
)
AS


					
				INSERT INTO dbo.[Degree]
					(
					[ID]
					,[Rank]
					,[Title]
					,[Color]
					,[Stripes]
					,[Picture]
					)
				VALUES
					(
					@ID
					,@Rank
					,@Title
					,@Color
					,@Stripes
					,@Picture
					)
				
									
							
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Degree_Update procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Degree_Update') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Degree_Update
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Updates a record in the Degree table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Degree_Update
(

	@ID int   ,

	@OriginalID int   ,

	@Rank int   ,

	@Title nvarchar (20)  ,

	@Color nvarchar (10)  ,

	@Stripes int   ,

	@Picture image (16)  
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					dbo.[Degree]
				SET
					[ID] = @ID
					,[Rank] = @Rank
					,[Title] = @Title
					,[Color] = @Color
					,[Stripes] = @Stripes
					,[Picture] = @Picture
				WHERE
[ID] = @OriginalID 
				
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Degree_Delete procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Degree_Delete') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Degree_Delete
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Deletes a record in the Degree table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Degree_Delete
(

	@ID int   
)
AS


				DELETE FROM dbo.[Degree] WITH (ROWLOCK) 
				WHERE
					[ID] = @ID
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Degree_GetByID procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Degree_GetByID') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Degree_GetByID
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Select records from the Degree table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Degree_GetByID
(

	@ID int   
)
AS


				SELECT
					[ID],
					[Rank],
					[Title],
					[Color],
					[Stripes],
					[Picture]
				FROM
					dbo.[Degree]
				WHERE
					[ID] = @ID
			Select @@ROWCOUNT
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Degree_Find procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Degree_Find') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Degree_Find
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Finds records in the Degree table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Degree_Find
(

	@SearchUsingOR bit   = null ,

	@ID int   = null ,

	@Rank int   = null ,

	@Title nvarchar (20)  = null ,

	@Color nvarchar (10)  = null ,

	@Stripes int   = null ,

	@Picture image (16)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ID]
	, [Rank]
	, [Title]
	, [Color]
	, [Stripes]
	, [Picture]
    FROM
	dbo.[Degree]
    WHERE 
	 ([ID] = @ID OR @ID is null)
	AND ([Rank] = @Rank OR @Rank is null)
	AND ([Title] = @Title OR @Title is null)
	AND ([Color] = @Color OR @Color is null)
	AND ([Stripes] = @Stripes OR @Stripes is null)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ID]
	, [Rank]
	, [Title]
	, [Color]
	, [Stripes]
	, [Picture]
    FROM
	dbo.[Degree]
    WHERE 
	 ([ID] = @ID AND @ID is not null)
	OR ([Rank] = @Rank AND @Rank is not null)
	OR ([Title] = @Title AND @Title is not null)
	OR ([Color] = @Color AND @Color is not null)
	OR ([Stripes] = @Stripes AND @Stripes is not null)
	Select @@ROWCOUNT			
  END
				

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.TrainingExceptions_Get_List procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.TrainingExceptions_Get_List') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.TrainingExceptions_Get_List
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Gets all records from the TrainingExceptions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.TrainingExceptions_Get_List

AS


				
				SELECT
					[ID],
					[Day]
				FROM
					dbo.[TrainingExceptions]
					
				Select @@ROWCOUNT
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.TrainingExceptions_GetPaged procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.TrainingExceptions_GetPaged') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.TrainingExceptions_GetPaged
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Gets records from the TrainingExceptions table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.TrainingExceptions_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				Create Table #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [ID] int 
				)
				
				-- Insert into the temp table
				declare @SQL as nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex (ID)'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + convert(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [ID]'
				SET @SQL = @SQL + ' FROM dbo.[TrainingExceptions]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				exec sp_executesql @SQL

				-- Return paged results
				SELECT O.[ID], O.[Day]
				FROM
				    dbo.[TrainingExceptions] O,
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexID > @PageLowerBound
					AND O.[ID] = PageIndex.[ID]
				ORDER BY
				    PageIndex.IndexID
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) as TotalRowCount'
				SET @SQL = @SQL + ' FROM dbo.[TrainingExceptions]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				exec sp_executesql @SQL
			
				END
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.TrainingExceptions_Insert procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.TrainingExceptions_Insert') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.TrainingExceptions_Insert
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Inserts a record into the TrainingExceptions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.TrainingExceptions_Insert
(

	@ID int    OUTPUT,

	@Day smalldatetime   
)
AS


					
				INSERT INTO dbo.[TrainingExceptions]
					(
					[Day]
					)
				VALUES
					(
					@Day
					)
				
				-- Get the identity value
				SET @ID = SCOPE_IDENTITY()
									
							
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.TrainingExceptions_Update procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.TrainingExceptions_Update') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.TrainingExceptions_Update
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Updates a record in the TrainingExceptions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.TrainingExceptions_Update
(

	@ID int   ,

	@Day smalldatetime   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					dbo.[TrainingExceptions]
				SET
					[Day] = @Day
				WHERE
[ID] = @ID 
				
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.TrainingExceptions_Delete procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.TrainingExceptions_Delete') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.TrainingExceptions_Delete
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Deletes a record in the TrainingExceptions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.TrainingExceptions_Delete
(

	@ID int   
)
AS


				DELETE FROM dbo.[TrainingExceptions] WITH (ROWLOCK) 
				WHERE
					[ID] = @ID
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.TrainingExceptions_GetByID procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.TrainingExceptions_GetByID') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.TrainingExceptions_GetByID
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Select records from the TrainingExceptions table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.TrainingExceptions_GetByID
(

	@ID int   
)
AS


				SELECT
					[ID],
					[Day]
				FROM
					dbo.[TrainingExceptions]
				WHERE
					[ID] = @ID
			Select @@ROWCOUNT
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.TrainingExceptions_Find procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.TrainingExceptions_Find') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.TrainingExceptions_Find
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Finds records in the TrainingExceptions table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.TrainingExceptions_Find
(

	@SearchUsingOR bit   = null ,

	@ID int   = null ,

	@Day smalldatetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ID]
	, [Day]
    FROM
	dbo.[TrainingExceptions]
    WHERE 
	 ([ID] = @ID OR @ID is null)
	AND ([Day] = @Day OR @Day is null)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ID]
	, [Day]
    FROM
	dbo.[TrainingExceptions]
    WHERE 
	 ([ID] = @ID AND @ID is not null)
	OR ([Day] = @Day AND @Day is not null)
	Select @@ROWCOUNT			
  END
				

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.PriceScheme_Get_List procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.PriceScheme_Get_List') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.PriceScheme_Get_List
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Gets all records from the PriceScheme table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.PriceScheme_Get_List

AS


				
				SELECT
					[ID],
					[PriceTypeID],
					[Quantity],
					[Price]
				FROM
					dbo.[PriceScheme]
					
				Select @@ROWCOUNT
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.PriceScheme_GetPaged procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.PriceScheme_GetPaged') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.PriceScheme_GetPaged
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Gets records from the PriceScheme table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.PriceScheme_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				Create Table #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [ID] int 
				)
				
				-- Insert into the temp table
				declare @SQL as nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex (ID)'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + convert(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [ID]'
				SET @SQL = @SQL + ' FROM dbo.[PriceScheme]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				exec sp_executesql @SQL

				-- Return paged results
				SELECT O.[ID], O.[PriceTypeID], O.[Quantity], O.[Price]
				FROM
				    dbo.[PriceScheme] O,
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexID > @PageLowerBound
					AND O.[ID] = PageIndex.[ID]
				ORDER BY
				    PageIndex.IndexID
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) as TotalRowCount'
				SET @SQL = @SQL + ' FROM dbo.[PriceScheme]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				exec sp_executesql @SQL
			
				END
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.PriceScheme_Insert procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.PriceScheme_Insert') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.PriceScheme_Insert
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Inserts a record into the PriceScheme table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.PriceScheme_Insert
(

	@ID int    OUTPUT,

	@PriceTypeID int   ,

	@Quantity smallint   ,

	@Price smallmoney   
)
AS


					
				INSERT INTO dbo.[PriceScheme]
					(
					[PriceTypeID]
					,[Quantity]
					,[Price]
					)
				VALUES
					(
					@PriceTypeID
					,@Quantity
					,@Price
					)
				
				-- Get the identity value
				SET @ID = SCOPE_IDENTITY()
									
							
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.PriceScheme_Update procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.PriceScheme_Update') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.PriceScheme_Update
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Updates a record in the PriceScheme table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.PriceScheme_Update
(

	@ID int   ,

	@PriceTypeID int   ,

	@Quantity smallint   ,

	@Price smallmoney   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					dbo.[PriceScheme]
				SET
					[PriceTypeID] = @PriceTypeID
					,[Quantity] = @Quantity
					,[Price] = @Price
				WHERE
[ID] = @ID 
				
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.PriceScheme_Delete procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.PriceScheme_Delete') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.PriceScheme_Delete
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Deletes a record in the PriceScheme table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.PriceScheme_Delete
(

	@ID int   
)
AS


				DELETE FROM dbo.[PriceScheme] WITH (ROWLOCK) 
				WHERE
					[ID] = @ID
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.PriceScheme_GetByPriceTypeID procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.PriceScheme_GetByPriceTypeID') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.PriceScheme_GetByPriceTypeID
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Select records from the PriceScheme table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.PriceScheme_GetByPriceTypeID
(

	@PriceTypeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[ID],
					[PriceTypeID],
					[Quantity],
					[Price]
				FROM
					dbo.[PriceScheme]
				WHERE
					[PriceTypeID] = @PriceTypeID
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.PriceScheme_GetByID procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.PriceScheme_GetByID') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.PriceScheme_GetByID
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Select records from the PriceScheme table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.PriceScheme_GetByID
(

	@ID int   
)
AS


				SELECT
					[ID],
					[PriceTypeID],
					[Quantity],
					[Price]
				FROM
					dbo.[PriceScheme]
				WHERE
					[ID] = @ID
			Select @@ROWCOUNT
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.PriceScheme_Find procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.PriceScheme_Find') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.PriceScheme_Find
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Finds records in the PriceScheme table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.PriceScheme_Find
(

	@SearchUsingOR bit   = null ,

	@ID int   = null ,

	@PriceTypeID int   = null ,

	@Quantity smallint   = null ,

	@Price smallmoney   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ID]
	, [PriceTypeID]
	, [Quantity]
	, [Price]
    FROM
	dbo.[PriceScheme]
    WHERE 
	 ([ID] = @ID OR @ID is null)
	AND ([PriceTypeID] = @PriceTypeID OR @PriceTypeID is null)
	AND ([Quantity] = @Quantity OR @Quantity is null)
	AND ([Price] = @Price OR @Price is null)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ID]
	, [PriceTypeID]
	, [Quantity]
	, [Price]
    FROM
	dbo.[PriceScheme]
    WHERE 
	 ([ID] = @ID AND @ID is not null)
	OR ([PriceTypeID] = @PriceTypeID AND @PriceTypeID is not null)
	OR ([Quantity] = @Quantity AND @Quantity is not null)
	OR ([Price] = @Price AND @Price is not null)
	Select @@ROWCOUNT			
  END
				

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.TrainingPeriod_Get_List procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.TrainingPeriod_Get_List') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.TrainingPeriod_Get_List
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Gets all records from the TrainingPeriod table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.TrainingPeriod_Get_List

AS


				
				SELECT
					[ID],
					[Start],
					[End],
					[Name]
				FROM
					dbo.[TrainingPeriod]
					
				Select @@ROWCOUNT
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.TrainingPeriod_GetPaged procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.TrainingPeriod_GetPaged') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.TrainingPeriod_GetPaged
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Gets records from the TrainingPeriod table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.TrainingPeriod_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				Create Table #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [ID] int 
				)
				
				-- Insert into the temp table
				declare @SQL as nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex (ID)'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + convert(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [ID]'
				SET @SQL = @SQL + ' FROM dbo.[TrainingPeriod]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				exec sp_executesql @SQL

				-- Return paged results
				SELECT O.[ID], O.[Start], O.[End], O.[Name]
				FROM
				    dbo.[TrainingPeriod] O,
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexID > @PageLowerBound
					AND O.[ID] = PageIndex.[ID]
				ORDER BY
				    PageIndex.IndexID
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) as TotalRowCount'
				SET @SQL = @SQL + ' FROM dbo.[TrainingPeriod]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				exec sp_executesql @SQL
			
				END
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.TrainingPeriod_Insert procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.TrainingPeriod_Insert') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.TrainingPeriod_Insert
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Inserts a record into the TrainingPeriod table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.TrainingPeriod_Insert
(

	@ID int   ,

	@Start char (5)  ,

	@End char (5)  ,

	@Name nvarchar (100)  
)
AS


					
				INSERT INTO dbo.[TrainingPeriod]
					(
					[ID]
					,[Start]
					,[End]
					,[Name]
					)
				VALUES
					(
					@ID
					,@Start
					,@End
					,@Name
					)
				
									
							
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.TrainingPeriod_Update procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.TrainingPeriod_Update') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.TrainingPeriod_Update
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Updates a record in the TrainingPeriod table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.TrainingPeriod_Update
(

	@ID int   ,

	@OriginalID int   ,

	@Start char (5)  ,

	@End char (5)  ,

	@Name nvarchar (100)  
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					dbo.[TrainingPeriod]
				SET
					[ID] = @ID
					,[Start] = @Start
					,[End] = @End
					,[Name] = @Name
				WHERE
[ID] = @OriginalID 
				
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.TrainingPeriod_Delete procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.TrainingPeriod_Delete') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.TrainingPeriod_Delete
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Deletes a record in the TrainingPeriod table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.TrainingPeriod_Delete
(

	@ID int   
)
AS


				DELETE FROM dbo.[TrainingPeriod] WITH (ROWLOCK) 
				WHERE
					[ID] = @ID
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.TrainingPeriod_GetByID procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.TrainingPeriod_GetByID') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.TrainingPeriod_GetByID
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Select records from the TrainingPeriod table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.TrainingPeriod_GetByID
(

	@ID int   
)
AS


				SELECT
					[ID],
					[Start],
					[End],
					[Name]
				FROM
					dbo.[TrainingPeriod]
				WHERE
					[ID] = @ID
			Select @@ROWCOUNT
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.TrainingPeriod_Find procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.TrainingPeriod_Find') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.TrainingPeriod_Find
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Finds records in the TrainingPeriod table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.TrainingPeriod_Find
(

	@SearchUsingOR bit   = null ,

	@ID int   = null ,

	@Start char (5)  = null ,

	@End char (5)  = null ,

	@Name nvarchar (100)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ID]
	, [Start]
	, [End]
	, [Name]
    FROM
	dbo.[TrainingPeriod]
    WHERE 
	 ([ID] = @ID OR @ID is null)
	AND ([Start] = @Start OR @Start is null)
	AND ([End] = @End OR @End is null)
	AND ([Name] = @Name OR @Name is null)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ID]
	, [Start]
	, [End]
	, [Name]
    FROM
	dbo.[TrainingPeriod]
    WHERE 
	 ([ID] = @ID AND @ID is not null)
	OR ([Start] = @Start AND @Start is not null)
	OR ([End] = @End AND @End is not null)
	OR ([Name] = @Name AND @Name is not null)
	Select @@ROWCOUNT			
  END
				

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.PaymentType_Get_List procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.PaymentType_Get_List') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.PaymentType_Get_List
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Gets all records from the PaymentType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.PaymentType_Get_List

AS


				
				SELECT
					[id],
					[Code],
					[Description]
				FROM
					dbo.[PaymentType]
					
				Select @@ROWCOUNT
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.PaymentType_GetPaged procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.PaymentType_GetPaged') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.PaymentType_GetPaged
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Gets records from the PaymentType table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.PaymentType_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				Create Table #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [id] int 
				)
				
				-- Insert into the temp table
				declare @SQL as nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex (id)'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + convert(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [id]'
				SET @SQL = @SQL + ' FROM dbo.[PaymentType]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				exec sp_executesql @SQL

				-- Return paged results
				SELECT O.[id], O.[Code], O.[Description]
				FROM
				    dbo.[PaymentType] O,
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexID > @PageLowerBound
					AND O.[id] = PageIndex.[id]
				ORDER BY
				    PageIndex.IndexID
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) as TotalRowCount'
				SET @SQL = @SQL + ' FROM dbo.[PaymentType]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				exec sp_executesql @SQL
			
				END
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.PaymentType_Insert procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.PaymentType_Insert') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.PaymentType_Insert
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Inserts a record into the PaymentType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.PaymentType_Insert
(

	@Id int   ,

	@Code char (1)  ,

	@Description nchar (50)  
)
AS


					
				INSERT INTO dbo.[PaymentType]
					(
					[id]
					,[Code]
					,[Description]
					)
				VALUES
					(
					@Id
					,@Code
					,@Description
					)
				
									
							
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.PaymentType_Update procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.PaymentType_Update') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.PaymentType_Update
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Updates a record in the PaymentType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.PaymentType_Update
(

	@Id int   ,

	@OriginalId int   ,

	@Code char (1)  ,

	@Description nchar (50)  
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					dbo.[PaymentType]
				SET
					[id] = @Id
					,[Code] = @Code
					,[Description] = @Description
				WHERE
[id] = @OriginalId 
				
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.PaymentType_Delete procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.PaymentType_Delete') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.PaymentType_Delete
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Deletes a record in the PaymentType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.PaymentType_Delete
(

	@Id int   
)
AS


				DELETE FROM dbo.[PaymentType] WITH (ROWLOCK) 
				WHERE
					[id] = @Id
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.PaymentType_GetById procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.PaymentType_GetById') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.PaymentType_GetById
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Select records from the PaymentType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.PaymentType_GetById
(

	@Id int   
)
AS


				SELECT
					[id],
					[Code],
					[Description]
				FROM
					dbo.[PaymentType]
				WHERE
					[id] = @Id
			Select @@ROWCOUNT
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.PaymentType_Find procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.PaymentType_Find') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.PaymentType_Find
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Finds records in the PaymentType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.PaymentType_Find
(

	@SearchUsingOR bit   = null ,

	@Id int   = null ,

	@Code char (1)  = null ,

	@Description nchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [id]
	, [Code]
	, [Description]
    FROM
	dbo.[PaymentType]
    WHERE 
	 ([id] = @Id OR @Id is null)
	AND ([Code] = @Code OR @Code is null)
	AND ([Description] = @Description OR @Description is null)
						
  END
  ELSE
  BEGIN
    SELECT
	  [id]
	, [Code]
	, [Description]
    FROM
	dbo.[PaymentType]
    WHERE 
	 ([id] = @Id AND @Id is not null)
	OR ([Code] = @Code AND @Code is not null)
	OR ([Description] = @Description AND @Description is not null)
	Select @@ROWCOUNT			
  END
				

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Group_Get_List procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Group_Get_List') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Group_Get_List
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Gets all records from the Group table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Group_Get_List

AS


				
				SELECT
					[ID],
					[Name]
				FROM
					dbo.[Group]
					
				Select @@ROWCOUNT
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Group_GetPaged procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Group_GetPaged') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Group_GetPaged
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Gets records from the Group table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Group_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				Create Table #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [ID] int 
				)
				
				-- Insert into the temp table
				declare @SQL as nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex (ID)'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + convert(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [ID]'
				SET @SQL = @SQL + ' FROM dbo.[Group]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				exec sp_executesql @SQL

				-- Return paged results
				SELECT O.[ID], O.[Name]
				FROM
				    dbo.[Group] O,
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexID > @PageLowerBound
					AND O.[ID] = PageIndex.[ID]
				ORDER BY
				    PageIndex.IndexID
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) as TotalRowCount'
				SET @SQL = @SQL + ' FROM dbo.[Group]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				exec sp_executesql @SQL
			
				END
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Group_Insert procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Group_Insert') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Group_Insert
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Inserts a record into the Group table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Group_Insert
(

	@ID int    OUTPUT,

	@Name nvarchar (50)  
)
AS


					
				INSERT INTO dbo.[Group]
					(
					[Name]
					)
				VALUES
					(
					@Name
					)
				
				-- Get the identity value
				SET @ID = SCOPE_IDENTITY()
									
							
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Group_Update procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Group_Update') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Group_Update
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Updates a record in the Group table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Group_Update
(

	@ID int   ,

	@Name nvarchar (50)  
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					dbo.[Group]
				SET
					[Name] = @Name
				WHERE
[ID] = @ID 
				
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Group_Delete procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Group_Delete') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Group_Delete
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Deletes a record in the Group table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Group_Delete
(

	@ID int   
)
AS


				DELETE FROM dbo.[Group] WITH (ROWLOCK) 
				WHERE
					[ID] = @ID
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Group_GetByID procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Group_GetByID') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Group_GetByID
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Select records from the Group table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Group_GetByID
(

	@ID int   
)
AS


				SELECT
					[ID],
					[Name]
				FROM
					dbo.[Group]
				WHERE
					[ID] = @ID
			Select @@ROWCOUNT
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Group_Find procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Group_Find') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Group_Find
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Finds records in the Group table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Group_Find
(

	@SearchUsingOR bit   = null ,

	@ID int   = null ,

	@Name nvarchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ID]
	, [Name]
    FROM
	dbo.[Group]
    WHERE 
	 ([ID] = @ID OR @ID is null)
	AND ([Name] = @Name OR @Name is null)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ID]
	, [Name]
    FROM
	dbo.[Group]
    WHERE 
	 ([ID] = @ID AND @ID is not null)
	OR ([Name] = @Name AND @Name is not null)
	Select @@ROWCOUNT			
  END
				

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Location_Get_List procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Location_Get_List') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Location_Get_List
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Gets all records from the Location table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Location_Get_List

AS


				
				SELECT
					[ID],
					[Name]
				FROM
					dbo.[Location]
					
				Select @@ROWCOUNT
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Location_GetPaged procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Location_GetPaged') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Location_GetPaged
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Gets records from the Location table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Location_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				Create Table #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [ID] int 
				)
				
				-- Insert into the temp table
				declare @SQL as nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex (ID)'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + convert(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [ID]'
				SET @SQL = @SQL + ' FROM dbo.[Location]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				exec sp_executesql @SQL

				-- Return paged results
				SELECT O.[ID], O.[Name]
				FROM
				    dbo.[Location] O,
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexID > @PageLowerBound
					AND O.[ID] = PageIndex.[ID]
				ORDER BY
				    PageIndex.IndexID
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) as TotalRowCount'
				SET @SQL = @SQL + ' FROM dbo.[Location]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				exec sp_executesql @SQL
			
				END
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Location_Insert procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Location_Insert') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Location_Insert
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Inserts a record into the Location table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Location_Insert
(

	@ID int   ,

	@Name nvarchar (100)  
)
AS


					
				INSERT INTO dbo.[Location]
					(
					[ID]
					,[Name]
					)
				VALUES
					(
					@ID
					,@Name
					)
				
									
							
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Location_Update procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Location_Update') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Location_Update
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Updates a record in the Location table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Location_Update
(

	@ID int   ,

	@OriginalID int   ,

	@Name nvarchar (100)  
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					dbo.[Location]
				SET
					[ID] = @ID
					,[Name] = @Name
				WHERE
[ID] = @OriginalID 
				
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Location_Delete procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Location_Delete') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Location_Delete
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Deletes a record in the Location table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Location_Delete
(

	@ID int   
)
AS


				DELETE FROM dbo.[Location] WITH (ROWLOCK) 
				WHERE
					[ID] = @ID
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Location_GetByID procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Location_GetByID') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Location_GetByID
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Select records from the Location table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Location_GetByID
(

	@ID int   
)
AS


				SELECT
					[ID],
					[Name]
				FROM
					dbo.[Location]
				WHERE
					[ID] = @ID
			Select @@ROWCOUNT
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Location_Find procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Location_Find') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Location_Find
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Finds records in the Location table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Location_Find
(

	@SearchUsingOR bit   = null ,

	@ID int   = null ,

	@Name nvarchar (100)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ID]
	, [Name]
    FROM
	dbo.[Location]
    WHERE 
	 ([ID] = @ID OR @ID is null)
	AND ([Name] = @Name OR @Name is null)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ID]
	, [Name]
    FROM
	dbo.[Location]
    WHERE 
	 ([ID] = @ID AND @ID is not null)
	OR ([Name] = @Name AND @Name is not null)
	Select @@ROWCOUNT			
  END
				

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.TrainingTime_Get_List procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.TrainingTime_Get_List') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.TrainingTime_Get_List
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Gets all records from the TrainingTime table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.TrainingTime_Get_List

AS


				
				SELECT
					[ID],
					[TrainingPeriodID],
					[LocationID],
					[DayOfTheWeek],
					[StartOfTraining],
					[EndOfTraining],
					[Audience]
				FROM
					dbo.[TrainingTime]
					
				Select @@ROWCOUNT
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.TrainingTime_GetPaged procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.TrainingTime_GetPaged') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.TrainingTime_GetPaged
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Gets records from the TrainingTime table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.TrainingTime_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				Create Table #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [ID] int 
				)
				
				-- Insert into the temp table
				declare @SQL as nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex (ID)'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + convert(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [ID]'
				SET @SQL = @SQL + ' FROM dbo.[TrainingTime]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				exec sp_executesql @SQL

				-- Return paged results
				SELECT O.[ID], O.[TrainingPeriodID], O.[LocationID], O.[DayOfTheWeek], O.[StartOfTraining], O.[EndOfTraining], O.[Audience]
				FROM
				    dbo.[TrainingTime] O,
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexID > @PageLowerBound
					AND O.[ID] = PageIndex.[ID]
				ORDER BY
				    PageIndex.IndexID
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) as TotalRowCount'
				SET @SQL = @SQL + ' FROM dbo.[TrainingTime]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				exec sp_executesql @SQL
			
				END
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.TrainingTime_Insert procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.TrainingTime_Insert') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.TrainingTime_Insert
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Inserts a record into the TrainingTime table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.TrainingTime_Insert
(

	@ID int    OUTPUT,

	@TrainingPeriodID int   ,

	@LocationID int   ,

	@DayOfTheWeek tinyint   ,

	@StartOfTraining char (5)  ,

	@EndOfTraining char (5)  ,

	@Audience nvarchar (100)  
)
AS


					
				INSERT INTO dbo.[TrainingTime]
					(
					[TrainingPeriodID]
					,[LocationID]
					,[DayOfTheWeek]
					,[StartOfTraining]
					,[EndOfTraining]
					,[Audience]
					)
				VALUES
					(
					@TrainingPeriodID
					,@LocationID
					,@DayOfTheWeek
					,@StartOfTraining
					,@EndOfTraining
					,@Audience
					)
				
				-- Get the identity value
				SET @ID = SCOPE_IDENTITY()
									
							
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.TrainingTime_Update procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.TrainingTime_Update') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.TrainingTime_Update
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Updates a record in the TrainingTime table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.TrainingTime_Update
(

	@ID int   ,

	@TrainingPeriodID int   ,

	@LocationID int   ,

	@DayOfTheWeek tinyint   ,

	@StartOfTraining char (5)  ,

	@EndOfTraining char (5)  ,

	@Audience nvarchar (100)  
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					dbo.[TrainingTime]
				SET
					[TrainingPeriodID] = @TrainingPeriodID
					,[LocationID] = @LocationID
					,[DayOfTheWeek] = @DayOfTheWeek
					,[StartOfTraining] = @StartOfTraining
					,[EndOfTraining] = @EndOfTraining
					,[Audience] = @Audience
				WHERE
[ID] = @ID 
				
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.TrainingTime_Delete procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.TrainingTime_Delete') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.TrainingTime_Delete
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Deletes a record in the TrainingTime table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.TrainingTime_Delete
(

	@ID int   
)
AS


				DELETE FROM dbo.[TrainingTime] WITH (ROWLOCK) 
				WHERE
					[ID] = @ID
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.TrainingTime_GetByLocationID procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.TrainingTime_GetByLocationID') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.TrainingTime_GetByLocationID
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Select records from the TrainingTime table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.TrainingTime_GetByLocationID
(

	@LocationID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[ID],
					[TrainingPeriodID],
					[LocationID],
					[DayOfTheWeek],
					[StartOfTraining],
					[EndOfTraining],
					[Audience]
				FROM
					dbo.[TrainingTime]
				WHERE
					[LocationID] = @LocationID
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.TrainingTime_GetByTrainingPeriodID procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.TrainingTime_GetByTrainingPeriodID') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.TrainingTime_GetByTrainingPeriodID
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Select records from the TrainingTime table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.TrainingTime_GetByTrainingPeriodID
(

	@TrainingPeriodID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[ID],
					[TrainingPeriodID],
					[LocationID],
					[DayOfTheWeek],
					[StartOfTraining],
					[EndOfTraining],
					[Audience]
				FROM
					dbo.[TrainingTime]
				WHERE
					[TrainingPeriodID] = @TrainingPeriodID
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.TrainingTime_GetByID procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.TrainingTime_GetByID') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.TrainingTime_GetByID
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Select records from the TrainingTime table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.TrainingTime_GetByID
(

	@ID int   
)
AS


				SELECT
					[ID],
					[TrainingPeriodID],
					[LocationID],
					[DayOfTheWeek],
					[StartOfTraining],
					[EndOfTraining],
					[Audience]
				FROM
					dbo.[TrainingTime]
				WHERE
					[ID] = @ID
			Select @@ROWCOUNT
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.TrainingTime_Find procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.TrainingTime_Find') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.TrainingTime_Find
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Finds records in the TrainingTime table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.TrainingTime_Find
(

	@SearchUsingOR bit   = null ,

	@ID int   = null ,

	@TrainingPeriodID int   = null ,

	@LocationID int   = null ,

	@DayOfTheWeek tinyint   = null ,

	@StartOfTraining char (5)  = null ,

	@EndOfTraining char (5)  = null ,

	@Audience nvarchar (100)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ID]
	, [TrainingPeriodID]
	, [LocationID]
	, [DayOfTheWeek]
	, [StartOfTraining]
	, [EndOfTraining]
	, [Audience]
    FROM
	dbo.[TrainingTime]
    WHERE 
	 ([ID] = @ID OR @ID is null)
	AND ([TrainingPeriodID] = @TrainingPeriodID OR @TrainingPeriodID is null)
	AND ([LocationID] = @LocationID OR @LocationID is null)
	AND ([DayOfTheWeek] = @DayOfTheWeek OR @DayOfTheWeek is null)
	AND ([StartOfTraining] = @StartOfTraining OR @StartOfTraining is null)
	AND ([EndOfTraining] = @EndOfTraining OR @EndOfTraining is null)
	AND ([Audience] = @Audience OR @Audience is null)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ID]
	, [TrainingPeriodID]
	, [LocationID]
	, [DayOfTheWeek]
	, [StartOfTraining]
	, [EndOfTraining]
	, [Audience]
    FROM
	dbo.[TrainingTime]
    WHERE 
	 ([ID] = @ID AND @ID is not null)
	OR ([TrainingPeriodID] = @TrainingPeriodID AND @TrainingPeriodID is not null)
	OR ([LocationID] = @LocationID AND @LocationID is not null)
	OR ([DayOfTheWeek] = @DayOfTheWeek AND @DayOfTheWeek is not null)
	OR ([StartOfTraining] = @StartOfTraining AND @StartOfTraining is not null)
	OR ([EndOfTraining] = @EndOfTraining AND @EndOfTraining is not null)
	OR ([Audience] = @Audience AND @Audience is not null)
	Select @@ROWCOUNT			
  END
				

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Member_Get_List procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Member_Get_List') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Member_Get_List
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Gets all records from the Member table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Member_Get_List

AS


				
				SELECT
					[ID],
					[Lastname],
					[Firstname],
					[Address],
					[ZipCode],
					[City],
					[Province],
					[Country],
					[Email],
					[Telephone],
					[Sex],
					[Birthdate],
					[LicenseNumber],
					[GroupID],
					[IsTrainer],
					[IsNew],
					[IsActive],
					[InsuranceEndDate],
					[StartYear],
					[Foto],
					[PaymentId],
					[TrainingLocationId]
				FROM
					dbo.[Member]
					
				Select @@ROWCOUNT
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Member_GetPaged procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Member_GetPaged') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Member_GetPaged
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Gets records from the Member table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Member_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				Create Table #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [ID] int 
				)
				
				-- Insert into the temp table
				declare @SQL as nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex (ID)'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + convert(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [ID]'
				SET @SQL = @SQL + ' FROM dbo.[Member]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				exec sp_executesql @SQL

				-- Return paged results
				SELECT O.[ID], O.[Lastname], O.[Firstname], O.[Address], O.[ZipCode], O.[City], O.[Province], O.[Country], O.[Email], O.[Telephone], O.[Sex], O.[Birthdate], O.[LicenseNumber], O.[GroupID], O.[IsTrainer], O.[IsNew], O.[IsActive], O.[InsuranceEndDate], O.[StartYear], O.[Foto], O.[PaymentId], O.[TrainingLocationId]
				FROM
				    dbo.[Member] O,
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexID > @PageLowerBound
					AND O.[ID] = PageIndex.[ID]
				ORDER BY
				    PageIndex.IndexID
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) as TotalRowCount'
				SET @SQL = @SQL + ' FROM dbo.[Member]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				exec sp_executesql @SQL
			
				END
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Member_Insert procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Member_Insert') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Member_Insert
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Inserts a record into the Member table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Member_Insert
(

	@ID int    OUTPUT,

	@Lastname nvarchar (27)  ,

	@Firstname nvarchar (50)  ,

	@Address nvarchar (26)  ,

	@ZipCode nvarchar (50)  ,

	@City nvarchar (27)  ,

	@Province nvarchar (50)  ,

	@Country nvarchar (50)  ,

	@Email nvarchar (50)  ,

	@Telephone nvarchar (50)  ,

	@Sex nvarchar (2)  ,

	@Birthdate smalldatetime   ,

	@LicenseNumber int   ,

	@GroupID int   ,

	@IsTrainer bit   ,

	@IsNew bit   ,

	@IsActive bit   ,

	@InsuranceEndDate smalldatetime   ,

	@StartYear nvarchar (50)  ,

	@Foto image (16)  ,

	@PaymentId int   ,

	@TrainingLocationId int   
)
AS


					
				INSERT INTO dbo.[Member]
					(
					[Lastname]
					,[Firstname]
					,[Address]
					,[ZipCode]
					,[City]
					,[Province]
					,[Country]
					,[Email]
					,[Telephone]
					,[Sex]
					,[Birthdate]
					,[LicenseNumber]
					,[GroupID]
					,[IsTrainer]
					,[IsNew]
					,[IsActive]
					,[InsuranceEndDate]
					,[StartYear]
					,[Foto]
					,[PaymentId]
					,[TrainingLocationId]
					)
				VALUES
					(
					@Lastname
					,@Firstname
					,@Address
					,@ZipCode
					,@City
					,@Province
					,@Country
					,@Email
					,@Telephone
					,@Sex
					,@Birthdate
					,@LicenseNumber
					,@GroupID
					,@IsTrainer
					,@IsNew
					,@IsActive
					,@InsuranceEndDate
					,@StartYear
					,@Foto
					,@PaymentId
					,@TrainingLocationId
					)
				
				-- Get the identity value
				SET @ID = SCOPE_IDENTITY()
									
							
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Member_Update procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Member_Update') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Member_Update
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Updates a record in the Member table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Member_Update
(

	@ID int   ,

	@Lastname nvarchar (27)  ,

	@Firstname nvarchar (50)  ,

	@Address nvarchar (26)  ,

	@ZipCode nvarchar (50)  ,

	@City nvarchar (27)  ,

	@Province nvarchar (50)  ,

	@Country nvarchar (50)  ,

	@Email nvarchar (50)  ,

	@Telephone nvarchar (50)  ,

	@Sex nvarchar (2)  ,

	@Birthdate smalldatetime   ,

	@LicenseNumber int   ,

	@GroupID int   ,

	@IsTrainer bit   ,

	@IsNew bit   ,

	@IsActive bit   ,

	@InsuranceEndDate smalldatetime   ,

	@StartYear nvarchar (50)  ,

	@Foto image (16)  ,

	@PaymentId int   ,

	@TrainingLocationId int   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					dbo.[Member]
				SET
					[Lastname] = @Lastname
					,[Firstname] = @Firstname
					,[Address] = @Address
					,[ZipCode] = @ZipCode
					,[City] = @City
					,[Province] = @Province
					,[Country] = @Country
					,[Email] = @Email
					,[Telephone] = @Telephone
					,[Sex] = @Sex
					,[Birthdate] = @Birthdate
					,[LicenseNumber] = @LicenseNumber
					,[GroupID] = @GroupID
					,[IsTrainer] = @IsTrainer
					,[IsNew] = @IsNew
					,[IsActive] = @IsActive
					,[InsuranceEndDate] = @InsuranceEndDate
					,[StartYear] = @StartYear
					,[Foto] = @Foto
					,[PaymentId] = @PaymentId
					,[TrainingLocationId] = @TrainingLocationId
				WHERE
[ID] = @ID 
				
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Member_Delete procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Member_Delete') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Member_Delete
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Deletes a record in the Member table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Member_Delete
(

	@ID int   
)
AS


				DELETE FROM dbo.[Member] WITH (ROWLOCK) 
				WHERE
					[ID] = @ID
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Member_GetByPaymentId procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Member_GetByPaymentId') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Member_GetByPaymentId
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Select records from the Member table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Member_GetByPaymentId
(

	@PaymentId int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[ID],
					[Lastname],
					[Firstname],
					[Address],
					[ZipCode],
					[City],
					[Province],
					[Country],
					[Email],
					[Telephone],
					[Sex],
					[Birthdate],
					[LicenseNumber],
					[GroupID],
					[IsTrainer],
					[IsNew],
					[IsActive],
					[InsuranceEndDate],
					[StartYear],
					[Foto],
					[PaymentId],
					[TrainingLocationId]
				FROM
					dbo.[Member]
				WHERE
					[PaymentId] = @PaymentId
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Member_GetByGroupID procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Member_GetByGroupID') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Member_GetByGroupID
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Select records from the Member table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Member_GetByGroupID
(

	@GroupID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[ID],
					[Lastname],
					[Firstname],
					[Address],
					[ZipCode],
					[City],
					[Province],
					[Country],
					[Email],
					[Telephone],
					[Sex],
					[Birthdate],
					[LicenseNumber],
					[GroupID],
					[IsTrainer],
					[IsNew],
					[IsActive],
					[InsuranceEndDate],
					[StartYear],
					[Foto],
					[PaymentId],
					[TrainingLocationId]
				FROM
					dbo.[Member]
				WHERE
					[GroupID] = @GroupID
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Member_GetByTrainingLocationId procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Member_GetByTrainingLocationId') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Member_GetByTrainingLocationId
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Select records from the Member table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Member_GetByTrainingLocationId
(

	@TrainingLocationId int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[ID],
					[Lastname],
					[Firstname],
					[Address],
					[ZipCode],
					[City],
					[Province],
					[Country],
					[Email],
					[Telephone],
					[Sex],
					[Birthdate],
					[LicenseNumber],
					[GroupID],
					[IsTrainer],
					[IsNew],
					[IsActive],
					[InsuranceEndDate],
					[StartYear],
					[Foto],
					[PaymentId],
					[TrainingLocationId]
				FROM
					dbo.[Member]
				WHERE
					[TrainingLocationId] = @TrainingLocationId
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Member_GetByID procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Member_GetByID') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Member_GetByID
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Select records from the Member table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Member_GetByID
(

	@ID int   
)
AS


				SELECT
					[ID],
					[Lastname],
					[Firstname],
					[Address],
					[ZipCode],
					[City],
					[Province],
					[Country],
					[Email],
					[Telephone],
					[Sex],
					[Birthdate],
					[LicenseNumber],
					[GroupID],
					[IsTrainer],
					[IsNew],
					[IsActive],
					[InsuranceEndDate],
					[StartYear],
					[Foto],
					[PaymentId],
					[TrainingLocationId]
				FROM
					dbo.[Member]
				WHERE
					[ID] = @ID
			Select @@ROWCOUNT
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Member_Find procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Member_Find') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Member_Find
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Finds records in the Member table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Member_Find
(

	@SearchUsingOR bit   = null ,

	@ID int   = null ,

	@Lastname nvarchar (27)  = null ,

	@Firstname nvarchar (50)  = null ,

	@Address nvarchar (26)  = null ,

	@ZipCode nvarchar (50)  = null ,

	@City nvarchar (27)  = null ,

	@Province nvarchar (50)  = null ,

	@Country nvarchar (50)  = null ,

	@Email nvarchar (50)  = null ,

	@Telephone nvarchar (50)  = null ,

	@Sex nvarchar (2)  = null ,

	@Birthdate smalldatetime   = null ,

	@LicenseNumber int   = null ,

	@GroupID int   = null ,

	@IsTrainer bit   = null ,

	@IsNew bit   = null ,

	@IsActive bit   = null ,

	@InsuranceEndDate smalldatetime   = null ,

	@StartYear nvarchar (50)  = null ,

	@Foto image (16)  = null ,

	@PaymentId int   = null ,

	@TrainingLocationId int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ID]
	, [Lastname]
	, [Firstname]
	, [Address]
	, [ZipCode]
	, [City]
	, [Province]
	, [Country]
	, [Email]
	, [Telephone]
	, [Sex]
	, [Birthdate]
	, [LicenseNumber]
	, [GroupID]
	, [IsTrainer]
	, [IsNew]
	, [IsActive]
	, [InsuranceEndDate]
	, [StartYear]
	, [Foto]
	, [PaymentId]
	, [TrainingLocationId]
    FROM
	dbo.[Member]
    WHERE 
	 ([ID] = @ID OR @ID is null)
	AND ([Lastname] = @Lastname OR @Lastname is null)
	AND ([Firstname] = @Firstname OR @Firstname is null)
	AND ([Address] = @Address OR @Address is null)
	AND ([ZipCode] = @ZipCode OR @ZipCode is null)
	AND ([City] = @City OR @City is null)
	AND ([Province] = @Province OR @Province is null)
	AND ([Country] = @Country OR @Country is null)
	AND ([Email] = @Email OR @Email is null)
	AND ([Telephone] = @Telephone OR @Telephone is null)
	AND ([Sex] = @Sex OR @Sex is null)
	AND ([Birthdate] = @Birthdate OR @Birthdate is null)
	AND ([LicenseNumber] = @LicenseNumber OR @LicenseNumber is null)
	AND ([GroupID] = @GroupID OR @GroupID is null)
	AND ([IsTrainer] = @IsTrainer OR @IsTrainer is null)
	AND ([IsNew] = @IsNew OR @IsNew is null)
	AND ([IsActive] = @IsActive OR @IsActive is null)
	AND ([InsuranceEndDate] = @InsuranceEndDate OR @InsuranceEndDate is null)
	AND ([StartYear] = @StartYear OR @StartYear is null)
	AND ([PaymentId] = @PaymentId OR @PaymentId is null)
	AND ([TrainingLocationId] = @TrainingLocationId OR @TrainingLocationId is null)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ID]
	, [Lastname]
	, [Firstname]
	, [Address]
	, [ZipCode]
	, [City]
	, [Province]
	, [Country]
	, [Email]
	, [Telephone]
	, [Sex]
	, [Birthdate]
	, [LicenseNumber]
	, [GroupID]
	, [IsTrainer]
	, [IsNew]
	, [IsActive]
	, [InsuranceEndDate]
	, [StartYear]
	, [Foto]
	, [PaymentId]
	, [TrainingLocationId]
    FROM
	dbo.[Member]
    WHERE 
	 ([ID] = @ID AND @ID is not null)
	OR ([Lastname] = @Lastname AND @Lastname is not null)
	OR ([Firstname] = @Firstname AND @Firstname is not null)
	OR ([Address] = @Address AND @Address is not null)
	OR ([ZipCode] = @ZipCode AND @ZipCode is not null)
	OR ([City] = @City AND @City is not null)
	OR ([Province] = @Province AND @Province is not null)
	OR ([Country] = @Country AND @Country is not null)
	OR ([Email] = @Email AND @Email is not null)
	OR ([Telephone] = @Telephone AND @Telephone is not null)
	OR ([Sex] = @Sex AND @Sex is not null)
	OR ([Birthdate] = @Birthdate AND @Birthdate is not null)
	OR ([LicenseNumber] = @LicenseNumber AND @LicenseNumber is not null)
	OR ([GroupID] = @GroupID AND @GroupID is not null)
	OR ([IsTrainer] = @IsTrainer AND @IsTrainer is not null)
	OR ([IsNew] = @IsNew AND @IsNew is not null)
	OR ([IsActive] = @IsActive AND @IsActive is not null)
	OR ([InsuranceEndDate] = @InsuranceEndDate AND @InsuranceEndDate is not null)
	OR ([StartYear] = @StartYear AND @StartYear is not null)
	OR ([PaymentId] = @PaymentId AND @PaymentId is not null)
	OR ([TrainingLocationId] = @TrainingLocationId AND @TrainingLocationId is not null)
	Select @@ROWCOUNT			
  END
				

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Examination_Get_List procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Examination_Get_List') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Examination_Get_List
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Gets all records from the Examination table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Examination_Get_List

AS


				
				SELECT
					[ID],
					[MemberID],
					[DegreeID],
					[LocationID],
					[Date]
				FROM
					dbo.[Examination]
					
				Select @@ROWCOUNT
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Examination_GetPaged procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Examination_GetPaged') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Examination_GetPaged
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Gets records from the Examination table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Examination_GetPaged
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				Create Table #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [ID] int 
				)
				
				-- Insert into the temp table
				declare @SQL as nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex (ID)'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + convert(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [ID]'
				SET @SQL = @SQL + ' FROM dbo.[Examination]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				exec sp_executesql @SQL

				-- Return paged results
				SELECT O.[ID], O.[MemberID], O.[DegreeID], O.[LocationID], O.[Date]
				FROM
				    dbo.[Examination] O,
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexID > @PageLowerBound
					AND O.[ID] = PageIndex.[ID]
				ORDER BY
				    PageIndex.IndexID
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) as TotalRowCount'
				SET @SQL = @SQL + ' FROM dbo.[Examination]'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				exec sp_executesql @SQL
			
				END
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Examination_Insert procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Examination_Insert') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Examination_Insert
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Inserts a record into the Examination table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Examination_Insert
(

	@ID int    OUTPUT,

	@MemberID int   ,

	@DegreeID int   ,

	@LocationID int   ,

	@Date smalldatetime   
)
AS


					
				INSERT INTO dbo.[Examination]
					(
					[MemberID]
					,[DegreeID]
					,[LocationID]
					,[Date]
					)
				VALUES
					(
					@MemberID
					,@DegreeID
					,@LocationID
					,@Date
					)
				
				-- Get the identity value
				SET @ID = SCOPE_IDENTITY()
									
							
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Examination_Update procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Examination_Update') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Examination_Update
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Updates a record in the Examination table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Examination_Update
(

	@ID int   ,

	@MemberID int   ,

	@DegreeID int   ,

	@LocationID int   ,

	@Date smalldatetime   
)
AS


				
				
				-- Modify the updatable columns
				UPDATE
					dbo.[Examination]
				SET
					[MemberID] = @MemberID
					,[DegreeID] = @DegreeID
					,[LocationID] = @LocationID
					,[Date] = @Date
				WHERE
[ID] = @ID 
				
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Examination_Delete procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Examination_Delete') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Examination_Delete
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Deletes a record in the Examination table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Examination_Delete
(

	@ID int   
)
AS


				DELETE FROM dbo.[Examination] WITH (ROWLOCK) 
				WHERE
					[ID] = @ID
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Examination_GetByLocationID procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Examination_GetByLocationID') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Examination_GetByLocationID
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Select records from the Examination table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Examination_GetByLocationID
(

	@LocationID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[ID],
					[MemberID],
					[DegreeID],
					[LocationID],
					[Date]
				FROM
					dbo.[Examination]
				WHERE
					[LocationID] = @LocationID
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Examination_GetByDegreeID procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Examination_GetByDegreeID') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Examination_GetByDegreeID
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Select records from the Examination table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Examination_GetByDegreeID
(

	@DegreeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[ID],
					[MemberID],
					[DegreeID],
					[LocationID],
					[Date]
				FROM
					dbo.[Examination]
				WHERE
					[DegreeID] = @DegreeID
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Examination_GetByMemberID procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Examination_GetByMemberID') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Examination_GetByMemberID
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Select records from the Examination table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Examination_GetByMemberID
(

	@MemberID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[ID],
					[MemberID],
					[DegreeID],
					[LocationID],
					[Date]
				FROM
					dbo.[Examination]
				WHERE
					[MemberID] = @MemberID
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Examination_GetByID procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Examination_GetByID') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Examination_GetByID
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Select records from the Examination table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Examination_GetByID
(

	@ID int   
)
AS


				SELECT
					[ID],
					[MemberID],
					[DegreeID],
					[LocationID],
					[Date]
				FROM
					dbo.[Examination]
				WHERE
					[ID] = @ID
			Select @@ROWCOUNT
					
			

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

	

-- Drop the dbo.Examination_Find procedure
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dbo.Examination_Find') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.Examination_Find
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: woensdag 7 mei 2008

-- Created By: Kaisho (www.Kaisho.be)
-- Purpose: Finds records in the Examination table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE dbo.Examination_Find
(

	@SearchUsingOR bit   = null ,

	@ID int   = null ,

	@MemberID int   = null ,

	@DegreeID int   = null ,

	@LocationID int   = null ,

	@Date smalldatetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ID]
	, [MemberID]
	, [DegreeID]
	, [LocationID]
	, [Date]
    FROM
	dbo.[Examination]
    WHERE 
	 ([ID] = @ID OR @ID is null)
	AND ([MemberID] = @MemberID OR @MemberID is null)
	AND ([DegreeID] = @DegreeID OR @DegreeID is null)
	AND ([LocationID] = @LocationID OR @LocationID is null)
	AND ([Date] = @Date OR @Date is null)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ID]
	, [MemberID]
	, [DegreeID]
	, [LocationID]
	, [Date]
    FROM
	dbo.[Examination]
    WHERE 
	 ([ID] = @ID AND @ID is not null)
	OR ([MemberID] = @MemberID AND @MemberID is not null)
	OR ([DegreeID] = @DegreeID AND @DegreeID is not null)
	OR ([LocationID] = @LocationID AND @LocationID is not null)
	OR ([Date] = @Date AND @Date is not null)
	Select @@ROWCOUNT			
  END
				

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

