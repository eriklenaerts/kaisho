
#region Using directives

using System;
using System.Collections;
using System.Collections.Specialized;


using System.Web.Configuration;
using System.Data;
using System.Data.Common;
using System.Configuration.Provider;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

using KA.BusinessLogicLayer;
using KA.DataAccessLayer;
using KA.DataAccessLayer.Bases;

#endregion

namespace KA.DataAccessLayer.SqlClient
{
	/// <summary>
	/// This class is the Sql implementation of the NetTiersProvider.
	/// </summary>
	public sealed class SqlNetTiersProvider : KA.DataAccessLayer.Bases.NetTiersProvider
	{
		private static object syncRoot = new Object();
		private string _applicationName;
        private string _connectionString;
        private bool _useStoredProcedure;
        string _providerInvariantName;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="SqlNetTiersProvider"/> class.
		///</summary>
		public SqlNetTiersProvider()
		{	
		}		
		
		/// <summary>
        /// Initializes the provider.
        /// </summary>
        /// <param name="name">The friendly name of the provider.</param>
        /// <param name="config">A collection of the name/value pairs representing the provider-specific attributes specified in the configuration for this provider.</param>
        /// <exception cref="T:System.ArgumentNullException">The name of the provider is null.</exception>
        /// <exception cref="T:System.InvalidOperationException">An attempt is made to call <see cref="M:System.Configuration.Provider.ProviderBase.Initialize(System.String,System.Collections.Specialized.NameValueCollection)"></see> on a provider after the provider has already been initialized.</exception>
        /// <exception cref="T:System.ArgumentException">The name of the provider has a length of zero.</exception>
		public override void Initialize(string name, NameValueCollection config)
        {
            // Verify that config isn't null
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }

            // Assign the provider a default name if it doesn't have one
            if (String.IsNullOrEmpty(name))
            {
                name = "SqlNetTiersProvider";
            }

            // Add a default "description" attribute to config if the
            // attribute doesn't exist or is empty
            if (string.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "NetTiers Sql provider");
            }

            // Call the base class's Initialize method
            base.Initialize(name, config);

            // Initialize _applicationName
            _applicationName = config["applicationName"];

            if (string.IsNullOrEmpty(_applicationName))
            {
                _applicationName = "/";
            }
            config.Remove("applicationName");


            #region "Initialize UseStoredProcedure"
            string storedProcedure  = config["useStoredProcedure"];
           	if (string.IsNullOrEmpty(storedProcedure))
            {
                throw new ProviderException("Empty or missing useStoredProcedure");
            }
            this._useStoredProcedure = Convert.ToBoolean(config["useStoredProcedure"]);
            config.Remove("useStoredProcedure");
            #endregion

			#region ConnectionString

			// Initialize _connectionString
			_connectionString = config["connectionString"];
			config.Remove("connectionString");

			string connect = config["connectionStringName"];
			config.Remove("connectionStringName");

			if ( String.IsNullOrEmpty(_connectionString) )
			{
				if ( String.IsNullOrEmpty(connect) )
				{
					throw new ProviderException("Empty or missing connectionStringName");
				}

				if ( DataRepository.ConnectionStrings[connect] == null )
				{
					throw new ProviderException("Missing connection string");
				}

				_connectionString = DataRepository.ConnectionStrings[connect].ConnectionString;
			}

            if ( String.IsNullOrEmpty(_connectionString) )
            {
                throw new ProviderException("Empty connection string");
			}

			#endregion
            
             #region "_providerInvariantName"

            // initialize _providerInvariantName
            this._providerInvariantName = config["providerInvariantName"];

            if (String.IsNullOrEmpty(_providerInvariantName))
            {
                throw new ProviderException("Empty or missing providerInvariantName");
            }
            config.Remove("providerInvariantName");

            #endregion

        }
		
		/// <summary>
		/// Creates a new <c cref="TransactionManager"/> instance from the current datasource.
		/// </summary>
		/// <returns></returns>
		public override TransactionManager CreateTransaction()
		{
			return new TransactionManager(this._connectionString);
		}
		
		/// <summary>
		/// Gets a value indicating whether to use stored procedure or not.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this repository use stored procedures; otherwise, <c>false</c>.
		/// </value>
		public bool UseStoredProcedure
		{
			get {return this._useStoredProcedure;}
			set {this._useStoredProcedure = value;}
		}
		
		 /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        /// <value>The connection string.</value>
		public string ConnectionString
		{
			get {return this._connectionString;}
			set {this._connectionString = value;}
		}
		
		/// <summary>
	    /// Gets or sets the invariant provider name listed in the DbProviderFactories machine.config section.
	    /// </summary>
	    /// <value>The name of the provider invariant.</value>
	    public string ProviderInvariantName
	    {
	        get { return this._providerInvariantName; }
	        set { this._providerInvariantName = value; }
	    }		
		
		///<summary>
		/// Indicates if the current <c cref="NetTiersProvider"/> implementation supports Transacton.
		///</summary>
		public override bool IsTransactionSupported
		{
			get
			{
				return true;
			}
		}

		
		#region "PriceTypeProvider"
			
		private SqlPriceTypeProvider innerSqlPriceTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="PriceType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PriceTypeProviderBase PriceTypeProvider
		{
			get
			{
				if (innerSqlPriceTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPriceTypeProvider == null)
						{
							this.innerSqlPriceTypeProvider = new SqlPriceTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPriceTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <c cref="SqlPriceTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPriceTypeProvider SqlPriceTypeProvider
		{
			get {return PriceTypeProvider as SqlPriceTypeProvider;}
		}
		
		#endregion
		
		
		#region "DegreeProvider"
			
		private SqlDegreeProvider innerSqlDegreeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Degree"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override DegreeProviderBase DegreeProvider
		{
			get
			{
				if (innerSqlDegreeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlDegreeProvider == null)
						{
							this.innerSqlDegreeProvider = new SqlDegreeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlDegreeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <c cref="SqlDegreeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlDegreeProvider SqlDegreeProvider
		{
			get {return DegreeProvider as SqlDegreeProvider;}
		}
		
		#endregion
		
		
		#region "TrainingExceptionsProvider"
			
		private SqlTrainingExceptionsProvider innerSqlTrainingExceptionsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TrainingExceptions"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TrainingExceptionsProviderBase TrainingExceptionsProvider
		{
			get
			{
				if (innerSqlTrainingExceptionsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTrainingExceptionsProvider == null)
						{
							this.innerSqlTrainingExceptionsProvider = new SqlTrainingExceptionsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTrainingExceptionsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <c cref="SqlTrainingExceptionsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlTrainingExceptionsProvider SqlTrainingExceptionsProvider
		{
			get {return TrainingExceptionsProvider as SqlTrainingExceptionsProvider;}
		}
		
		#endregion
		
		
		#region "PriceSchemeProvider"
			
		private SqlPriceSchemeProvider innerSqlPriceSchemeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="PriceScheme"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PriceSchemeProviderBase PriceSchemeProvider
		{
			get
			{
				if (innerSqlPriceSchemeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPriceSchemeProvider == null)
						{
							this.innerSqlPriceSchemeProvider = new SqlPriceSchemeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPriceSchemeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <c cref="SqlPriceSchemeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPriceSchemeProvider SqlPriceSchemeProvider
		{
			get {return PriceSchemeProvider as SqlPriceSchemeProvider;}
		}
		
		#endregion
		
		
		#region "TrainingPeriodProvider"
			
		private SqlTrainingPeriodProvider innerSqlTrainingPeriodProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TrainingPeriod"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TrainingPeriodProviderBase TrainingPeriodProvider
		{
			get
			{
				if (innerSqlTrainingPeriodProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTrainingPeriodProvider == null)
						{
							this.innerSqlTrainingPeriodProvider = new SqlTrainingPeriodProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTrainingPeriodProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <c cref="SqlTrainingPeriodProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlTrainingPeriodProvider SqlTrainingPeriodProvider
		{
			get {return TrainingPeriodProvider as SqlTrainingPeriodProvider;}
		}
		
		#endregion
		
		
		#region "PaymentTypeProvider"
			
		private SqlPaymentTypeProvider innerSqlPaymentTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="PaymentType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PaymentTypeProviderBase PaymentTypeProvider
		{
			get
			{
				if (innerSqlPaymentTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPaymentTypeProvider == null)
						{
							this.innerSqlPaymentTypeProvider = new SqlPaymentTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPaymentTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <c cref="SqlPaymentTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPaymentTypeProvider SqlPaymentTypeProvider
		{
			get {return PaymentTypeProvider as SqlPaymentTypeProvider;}
		}
		
		#endregion
		
		
		#region "GroupProvider"
			
		private SqlGroupProvider innerSqlGroupProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Group"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override GroupProviderBase GroupProvider
		{
			get
			{
				if (innerSqlGroupProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlGroupProvider == null)
						{
							this.innerSqlGroupProvider = new SqlGroupProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlGroupProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <c cref="SqlGroupProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlGroupProvider SqlGroupProvider
		{
			get {return GroupProvider as SqlGroupProvider;}
		}
		
		#endregion
		
		
		#region "LocationProvider"
			
		private SqlLocationProvider innerSqlLocationProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Location"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override LocationProviderBase LocationProvider
		{
			get
			{
				if (innerSqlLocationProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlLocationProvider == null)
						{
							this.innerSqlLocationProvider = new SqlLocationProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlLocationProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <c cref="SqlLocationProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlLocationProvider SqlLocationProvider
		{
			get {return LocationProvider as SqlLocationProvider;}
		}
		
		#endregion
		
		
		#region "TrainingTimeProvider"
			
		private SqlTrainingTimeProvider innerSqlTrainingTimeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TrainingTime"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TrainingTimeProviderBase TrainingTimeProvider
		{
			get
			{
				if (innerSqlTrainingTimeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTrainingTimeProvider == null)
						{
							this.innerSqlTrainingTimeProvider = new SqlTrainingTimeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTrainingTimeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <c cref="SqlTrainingTimeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlTrainingTimeProvider SqlTrainingTimeProvider
		{
			get {return TrainingTimeProvider as SqlTrainingTimeProvider;}
		}
		
		#endregion
		
		
		#region "MemberProvider"
			
		private SqlMemberProvider innerSqlMemberProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Member"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override MemberProviderBase MemberProvider
		{
			get
			{
				if (innerSqlMemberProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlMemberProvider == null)
						{
							this.innerSqlMemberProvider = new SqlMemberProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlMemberProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <c cref="SqlMemberProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlMemberProvider SqlMemberProvider
		{
			get {return MemberProvider as SqlMemberProvider;}
		}
		
		#endregion
		
		
		#region "ExaminationProvider"
			
		private SqlExaminationProvider innerSqlExaminationProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Examination"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ExaminationProviderBase ExaminationProvider
		{
			get
			{
				if (innerSqlExaminationProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlExaminationProvider == null)
						{
							this.innerSqlExaminationProvider = new SqlExaminationProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlExaminationProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <c cref="SqlExaminationProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlExaminationProvider SqlExaminationProvider
		{
			get {return ExaminationProvider as SqlExaminationProvider;}
		}
		
		#endregion
		
		
		
		#region "General data access methods"

		#region "ExecuteNonQuery"
		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override int ExecuteNonQuery(string storedProcedureName, params object[] parameterValues)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			return database.ExecuteNonQuery(storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override int ExecuteNonQuery(TransactionManager transactionManager, string storedProcedureName, params object[] parameterValues)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			return database.ExecuteNonQuery(transactionManager.TransactionObject, storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="commandWrapper">The command wrapper.</param>
		public override void ExecuteNonQuery(DbCommand commandWrapper)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			database.ExecuteNonQuery(commandWrapper);	
			
		}

		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandWrapper">The command wrapper.</param>
		public override void ExecuteNonQuery(TransactionManager transactionManager, DbCommand commandWrapper)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			database.ExecuteNonQuery(commandWrapper, transactionManager.TransactionObject);	
		}


		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override int ExecuteNonQuery(CommandType commandType, string commandText)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			return database.ExecuteNonQuery(commandType, commandText);	
		}
		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override int ExecuteNonQuery(TransactionManager transactionManager, CommandType commandType, string commandText)
		{
			Database database = transactionManager.Database;			
			return database.ExecuteNonQuery(transactionManager.TransactionObject , commandType, commandText);				
		}
		#endregion

		#region "ExecuteDataReader"
		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override IDataReader ExecuteReader(string storedProcedureName, params object[] parameterValues)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);			
			return database.ExecuteReader(storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override IDataReader ExecuteReader(TransactionManager transactionManager, string storedProcedureName, params object[] parameterValues)
		{
			Database database = transactionManager.Database;
			return database.ExecuteReader(transactionManager.TransactionObject, storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="commandWrapper">The command wrapper.</param>
		/// <returns></returns>
		public override IDataReader ExecuteReader(DbCommand commandWrapper)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);			
			return database.ExecuteReader(commandWrapper);	
		}

		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandWrapper">The command wrapper.</param>
		/// <returns></returns>
		public override IDataReader ExecuteReader(TransactionManager transactionManager, DbCommand commandWrapper)
		{
			Database database = transactionManager.Database;
			return database.ExecuteReader(commandWrapper, transactionManager.TransactionObject);	
		}


		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override IDataReader ExecuteReader(CommandType commandType, string commandText)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			return database.ExecuteReader(commandType, commandText);	
		}
		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override IDataReader ExecuteReader(TransactionManager transactionManager, CommandType commandType, string commandText)
		{
			Database database = transactionManager.Database;			
			return database.ExecuteReader(transactionManager.TransactionObject , commandType, commandText);				
		}
		#endregion

		#region "ExecuteDataSet"
		/// <summary>
		/// Executes the data set.
		/// </summary>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override DataSet ExecuteDataSet(string storedProcedureName, params object[] parameterValues)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);			
			return database.ExecuteDataSet(storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the data set.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override DataSet ExecuteDataSet(TransactionManager transactionManager, string storedProcedureName, params object[] parameterValues)
		{
			Database database = transactionManager.Database;
			return database.ExecuteDataSet(transactionManager.TransactionObject, storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the data set.
		/// </summary>
		/// <param name="commandWrapper">The command wrapper.</param>
		/// <returns></returns>
		public override DataSet ExecuteDataSet(DbCommand commandWrapper)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);			
			return database.ExecuteDataSet(commandWrapper);	
		}

		/// <summary>
		/// Executes the data set.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandWrapper">The command wrapper.</param>
		/// <returns></returns>
		public override DataSet ExecuteDataSet(TransactionManager transactionManager, DbCommand commandWrapper)
		{
			Database database = transactionManager.Database;
			return database.ExecuteDataSet(commandWrapper, transactionManager.TransactionObject);	
		}


		/// <summary>
		/// Executes the data set.
		/// </summary>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override DataSet ExecuteDataSet(CommandType commandType, string commandText)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			return database.ExecuteDataSet(commandType, commandText);	
		}
		/// <summary>
		/// Executes the data set.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override DataSet ExecuteDataSet(TransactionManager transactionManager, CommandType commandType, string commandText)
		{
			Database database = transactionManager.Database;			
			return database.ExecuteDataSet(transactionManager.TransactionObject , commandType, commandText);				
		}
		#endregion

		#region "ExecuteScalar"
		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override object ExecuteScalar(string storedProcedureName, params object[] parameterValues)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);			
			return database.ExecuteScalar(storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override object ExecuteScalar(TransactionManager transactionManager, string storedProcedureName, params object[] parameterValues)
		{
			Database database = transactionManager.Database;
			return database.ExecuteScalar(transactionManager.TransactionObject, storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="commandWrapper">The command wrapper.</param>
		/// <returns></returns>
		public override object ExecuteScalar(DbCommand commandWrapper)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);			
			return database.ExecuteScalar(commandWrapper);	
		}

		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandWrapper">The command wrapper.</param>
		/// <returns></returns>
		public override object ExecuteScalar(TransactionManager transactionManager, DbCommand commandWrapper)
		{
			Database database = transactionManager.Database;
			return database.ExecuteScalar(commandWrapper, transactionManager.TransactionObject);	
		}

		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override object ExecuteScalar(CommandType commandType, string commandText)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			return database.ExecuteScalar(commandType, commandText);	
		}
		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override object ExecuteScalar(TransactionManager transactionManager, CommandType commandType, string commandText)
		{
			Database database = transactionManager.Database;			
			return database.ExecuteScalar(transactionManager.TransactionObject , commandType, commandText);				
		}
		#endregion

		#endregion


	}
}
