<%--
 * $Id: ValidationRuleArgs.cst,v 1.2 2006/02/18 00:04:05 bgjohnso Exp $
 * Last modified by $Author: jroland $
 * Last modified at $Date: 2006-03-16 16:53:08 -0600 (Thu, 16 Mar 2006) $
 * $Revision: 62 $
--%>
<%@ CodeTemplate Src="..\..\TemplateLib\CommonSqlCode.cs" Inherits="MoM.Templates.CommonSqlCode" Language="C#" TargetLanguage="C#" Description="BrokenRule class for validation."%>
<%@ Assembly Name="SchemaExplorer" %>
<%@ Import Namespace="SchemaExplorer" %>
<%@ Assembly Name="System.Design" %>

<%@ Property Name="NameSpace" Optional="False" Type="System.String" Category="Style" Description="Object Namespace." %>
<%@ Assembly Name="SchemaExplorer" %>
using System;
using System.Collections.Generic;
using System.Text;

namespace <%=NameSpace%>.Validation
{
   /// <summary>
   /// Object that provides additional information about an validation rule.
   /// </summary>
   public class ValidationRuleArgs
   {
      private string _propertyName;
      private string _description;

      /// <summary>
      /// The name of the property to be validated.
      /// </summary>
      public string PropertyName
      {
         get { return _propertyName; }
      }

      /// <summary>
      /// Detailed description of why the rule was invalidated.  This should be set from the method handling the rule.
      /// </summary>
      public string Description
      {
         get { return _description; }
         set { _description = value; }
      }

      /// <summary>
      /// Creates an instance of the object
      /// </summary>
      /// <param name="propertyName">The name of the property to be validated.</param>
      public ValidationRuleArgs(string propertyName)
      {
         _propertyName = propertyName;
      }

      /// <summary>
      /// Return a string representation of the object.
      /// </summary>
      public override string ToString()
      {
         return _propertyName;
      }
   }
}
