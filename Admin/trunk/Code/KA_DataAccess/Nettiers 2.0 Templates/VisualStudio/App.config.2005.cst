<%@ CodeTemplate Language="C#" TargetLanguage="XML" Description="Configuration file." Debug="true" ResponseEncoding="UTF-8"%>
<%@ Property Name="IsWebConfig" Type="System.Boolean" Default="False" Category="General" Description="Indicates whether the current configuration file is a web.config file." %>
<%@ Property Name="DALNameSpace" Type="System.String" Category="Data" Description="DAL Namespace." %>
<%@ Property Name="ConnectionString" Type="System.String" Category="Data" Description="sql connectionstring." %>
<%@ Property Name="FactoryType" Type="System.String" Category="Data" Description="The Creational Factory used to create the entities." %>
<%@ Property Name="WebServiceUrl" Type="System.String" Category="Data" Description="Web service URL." %>
<%@ Property Name="NetTiersSqlProvider" Type="System.Boolean" Category="Data" Description="Indicates if we use the SqlClient Provider." %>
<%@ Property Name="ProviderInvariantName" Type="System.String" Category="Data" Description="The DBProviderFactory name." %>
<?xml version="1.0" encoding="utf-8" ?>
<configuration>
  <configSections>
       <section name="netTiersService"
        type="<%=DALNameSpace%>.Bases.NetTiersServiceSection, <%=DALNameSpace%>"
        allowDefinition="MachineToApplication"
        restartOnExternalChanges="true" />
  </configSections>
  
  <connectionStrings>
    <add name="netTiersConnectionString" connectionString="<%=ConnectionString%>" />
  </connectionStrings>

  <netTiersService defaultProvider="<% if (NetTiersSqlProvider) {%>SqlNetTiersProvider<% } else {%>GenericNetTiersProvider<%}%>">
    <providers>
    <% if (NetTiersSqlProvider) { %>
    <!--
    *** SqlClient Provider ***
    	connectionStringName: sqlclient connection string to the db
    	useStoredProcedure: if trueindicates that we use the stored procedures, otherwise, we use parametrized queries that are embedded.
    -->
      <add 
        name="SqlNetTiersProvider" 
        type="<%=DALNameSpace%>.SqlClient.SqlNetTiersProvider, <%=DALNameSpace%>.SqlClient"
        connectionStringName="netTiersConnectionString"
        useStoredProcedure="false"
        providerInvariantName="System.Data.SqlClient"
        entityFactoryType="<%= FactoryType %>"
		useEntityFactory="true"
		enableEntityTracking="false"
        enableMethodAuthorization="false"
        defaultCommandTimeout="30"
        />
    <% } else { %>
        <!--
    *** GenericClient Provider ***
    this is the generic DbProviderFactory .netTiers provider
    	connectionStringName: the connection string to the db
    	useStoredProcedure: if trueindicates that we use the stored procedures, otherwise, we use parametrized queries that are embedded.
    	providerInvariantName: indicate the provider you want to use, for the moment only the following list is supported:
    		- System.Data.SQLite
    -->
      <add 
        name="GenericNetTiersProvider" 
        type="<%=DALNameSpace%>.GenericClient.GenericNetTiersProvider, <%=DALNameSpace%>.GenericClient"
        connectionStringName="netTiersConnectionString"
        useStoredProcedure="false"
        providerInvariantName="<%=ProviderInvariantName%>"
		entityFactoryType="<%= FactoryType %>"
		useEntityFactory="true"
		enableEntityTracking="false"
        enableMethodAuthorization="false"
        defaultCommandTimeout="30"
        />
    <% } %>    
      <!-- 
      	*** WebserviceClient Provider ***
      	The url parameter indicates the webservices url (ex: http://localhost/NorthWind/NorthWindServices.aspx)
      <add 
        name="WsNetTiersProvider" 
        type="<%=DALNameSpace%>.WebServiceClient.WsNetTiersProvider, <%=DALNameSpace%>.WebServiceClient"
        url="<%=WebServiceUrl%>"
        />
     -->
    </providers>
  </netTiersService>
  
   <% if (IsWebConfig) { %>
	<system.web>
		<compilation defaultLanguage="c#" debug="true"/>
	</system.web>
	<% } %>
</configuration>
