	

#region Using Directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Data;

using KA.BusinessLogicLayer;
using KA.BusinessLogicLayer.Validation;

using KA.DataAccessLayer;
using Microsoft.Practices.EnterpriseLibrary.Logging;

#endregion

namespace KA.ServiceLayer
{		
	
	///<summary>
	/// An component type implementation of the 'TrainingTime' table.
	///</summary>
	/// <remarks>
	/// All custom implementations should be done here.
	/// </remarks>
	[CLSCompliant(true)]
	public partial class TrainingTimeService : KA.ServiceLayer.TrainingTimeServiceBase
	{
		/// <summary>
		/// Initializes a new instance of the TrainingTimeService class.
		/// </summary>
		public TrainingTimeService() : base()
		{
		}
		
	}//End Class


} // end namespace
