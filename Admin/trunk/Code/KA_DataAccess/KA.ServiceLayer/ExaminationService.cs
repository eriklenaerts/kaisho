	

#region Using Directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Data;

using KA.BusinessLogicLayer;
using KA.BusinessLogicLayer.Validation;

using KA.DataAccessLayer;
using Microsoft.Practices.EnterpriseLibrary.Logging;

#endregion

namespace KA.ServiceLayer
{		
	
	///<summary>
	/// An component type implementation of the 'Examination' table.
	///</summary>
	/// <remarks>
	/// All custom implementations should be done here.
	/// </remarks>
	[CLSCompliant(true)]
	public partial class ExaminationService : KA.ServiceLayer.ExaminationServiceBase
	{
		/// <summary>
		/// Initializes a new instance of the ExaminationService class.
		/// </summary>
		public ExaminationService() : base()
		{
		}
		
	}//End Class


} // end namespace
