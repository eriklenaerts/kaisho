	

#region Using Directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Data;

using KA.BusinessLogicLayer;
using KA.BusinessLogicLayer.Validation;

using KA.DataAccessLayer;
using Microsoft.Practices.EnterpriseLibrary.Logging;

#endregion

namespace KA.ServiceLayer
{		
	
	///<summary>
	/// An component type implementation of the 'TrainingExceptions' table.
	///</summary>
	/// <remarks>
	/// All custom implementations should be done here.
	/// </remarks>
	[CLSCompliant(true)]
	public partial class TrainingExceptionsService : KA.ServiceLayer.TrainingExceptionsServiceBase
	{
		/// <summary>
		/// Initializes a new instance of the TrainingExceptionsService class.
		/// </summary>
		public TrainingExceptionsService() : base()
		{
		}
		
	}//End Class


} // end namespace
