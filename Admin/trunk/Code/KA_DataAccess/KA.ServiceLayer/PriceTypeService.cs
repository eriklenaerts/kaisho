	

#region Using Directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Data;

using KA.BusinessLogicLayer;
using KA.BusinessLogicLayer.Validation;

using KA.DataAccessLayer;
using Microsoft.Practices.EnterpriseLibrary.Logging;

#endregion

namespace KA.ServiceLayer
{		
	
	///<summary>
	/// An component type implementation of the 'PriceType' table.
	///</summary>
	/// <remarks>
	/// All custom implementations should be done here.
	/// </remarks>
	[CLSCompliant(true)]
	public partial class PriceTypeService : KA.ServiceLayer.PriceTypeServiceBase
	{
		/// <summary>
		/// Initializes a new instance of the PriceTypeService class.
		/// </summary>
		public PriceTypeService() : base()
		{
		}
		
	}//End Class


} // end namespace
