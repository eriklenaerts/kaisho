using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using PluginInterface;

namespace Aanwezigheidslijst
{
    public partial class AanwezigheidslijstUI : UserControl, IPlugin
    {
        public AanwezigheidslijstUI()
        {
            InitializeComponent();
        }

        private void RefreshReportForMonth()
        {
            DateTime tmpDate = new DateTime(dtpMonths.Value.Year, dtpMonths.Value.Month, 1, 0, 0, 0, 0, DateTimeKind.Utc);

            GetUsersAndTrainingDaysTableAdapter.Connection.ConnectionString = ConnectionString;
            GetUsersAndTrainingDaysTableAdapter.Fill(dsAanwezigheidslijst.GetUsersAndTrainingDays, tmpDate);

            //add parameters to the project to set the reportTitle
            ReportParameter[] parameters = new ReportParameter[2];
            parameters[0] = new ReportParameter("TitleMonth", dtpMonths.Value.ToString("MMMM"), false);
            parameters[1] = new ReportParameter("TitleYear", dtpMonths.Value.Year.ToString(), false);
            rvAanwezigheidslijst.LocalReport.SetParameters(parameters);
            rvAanwezigheidslijst.RefreshReport();
        
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            dtpMonths.Value = dtpMonths.Value.AddMonths(-1);
            RefreshReportForMonth();
        }

        private void btnForward_Click(object sender, EventArgs e)
        {
            dtpMonths.Value = dtpMonths.Value.AddMonths(1);
            RefreshReportForMonth();
        }

        private void dtpMonths_ValueChanged(object sender, EventArgs e)
        {
            RefreshReportForMonth();
        }

        #region IPlugin Members
        IPluginHost myPluginHost = null;
        string myPluginName = "Aanwezigheidslijst";
        string myPluginAuthor = "Dimitris Dierickx";
        string myPluginDescription = "Aanwezigheidslijst van alle leden";
        string myPluginVersion = "1.0.1";
        private string _connectionstring;

        void PluginInterface.IPlugin.Dispose()
        {        }

        public string Description
        {
            get
            {
                return myPluginDescription;
            }
        }

        public string Author
        {
            get
            {
                return myPluginAuthor;
            }
        }

        public IPluginHost Host
        {
            get
            {
                return myPluginHost;
            }
            set
            {
                myPluginHost = value;
            }
        }

        public UserControl MainInterface
        {
            get
            {
                return this;
            }
        }

        public string Version
        {
            get
            {
                return myPluginVersion;
            }
        }

        public string ConnectionString
        {
            get
            {
                return _connectionstring;
            }
            set
            {
                _connectionstring = value;
            }
        }

        public string PluginName
        {
            get { return myPluginName; }
        }

        public void Initialize(string connectionstring)
        {
            _connectionstring = connectionstring;
            RefreshReportForMonth();
        }
        #endregion
    }
}
