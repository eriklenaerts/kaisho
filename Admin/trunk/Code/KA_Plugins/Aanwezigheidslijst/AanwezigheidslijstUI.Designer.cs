namespace Aanwezigheidslijst
{
    partial class AanwezigheidslijstUI
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.GetUsersAndTrainingDaysBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dsAanwezigheidslijst = new Aanwezigheidslijst.dsAanwezigheidslijst();
            this.btnBack = new System.Windows.Forms.Button();
            this.lblMonth = new System.Windows.Forms.Label();
            this.dtpMonths = new System.Windows.Forms.DateTimePicker();
            this.GetUsersAndTrainingDaysTableAdapter = new Aanwezigheidslijst.dsAanwezigheidslijstTableAdapters.GetUsersAndTrainingDaysTableAdapter();
            this.btnForward = new System.Windows.Forms.Button();
            this.rvAanwezigheidslijst = new Microsoft.Reporting.WinForms.ReportViewer();
            ((System.ComponentModel.ISupportInitialize)(this.GetUsersAndTrainingDaysBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsAanwezigheidslijst)).BeginInit();
            this.SuspendLayout();
            // 
            // GetUsersAndTrainingDaysBindingSource
            // 
            this.GetUsersAndTrainingDaysBindingSource.DataMember = "GetUsersAndTrainingDays";
            this.GetUsersAndTrainingDaysBindingSource.DataSource = this.dsAanwezigheidslijst;
            // 
            // dsAanwezigheidslijst
            // 
            this.dsAanwezigheidslijst.DataSetName = "dsAanwezigheidslijst";
            this.dsAanwezigheidslijst.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(179, 15);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(34, 23);
            this.btnBack.TabIndex = 7;
            this.btnBack.Text = "<<";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // lblMonth
            // 
            this.lblMonth.AutoSize = true;
            this.lblMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMonth.ForeColor = System.Drawing.Color.Black;
            this.lblMonth.Location = new System.Drawing.Point(13, 20);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(81, 13);
            this.lblMonth.TabIndex = 6;
            this.lblMonth.Text = "Month picker";
            // 
            // dtpMonths
            // 
            this.dtpMonths.CustomFormat = "MM/yyyy";
            this.dtpMonths.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonths.Location = new System.Drawing.Point(100, 16);
            this.dtpMonths.Name = "dtpMonths";
            this.dtpMonths.Size = new System.Drawing.Size(77, 20);
            this.dtpMonths.TabIndex = 5;
            this.dtpMonths.ValueChanged += new System.EventHandler(this.dtpMonths_ValueChanged);
            // 
            // GetUsersAndTrainingDaysTableAdapter
            // 
            this.GetUsersAndTrainingDaysTableAdapter.ClearBeforeFill = true;
            // 
            // btnForward
            // 
            this.btnForward.Location = new System.Drawing.Point(219, 15);
            this.btnForward.Name = "btnForward";
            this.btnForward.Size = new System.Drawing.Size(34, 23);
            this.btnForward.TabIndex = 8;
            this.btnForward.Text = ">>";
            this.btnForward.UseVisualStyleBackColor = true;
            this.btnForward.Click += new System.EventHandler(this.btnForward_Click);
            
            // 
            // rvAanwezigheidslijst
            // 
            //this.rvAanwezigheidslijst.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rvAanwezigheidslijst.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            reportDataSource1.Name = "dsAanwezigheidslijst_GetUsersAndTrainingDays";
            reportDataSource1.Value = this.GetUsersAndTrainingDaysBindingSource;
            this.rvAanwezigheidslijst.LocalReport.DataSources.Add(reportDataSource1);
            this.rvAanwezigheidslijst.LocalReport.ReportEmbeddedResource = "Aanwezigheidslijst.reportAanwezigheidslijst.rdlc";
            this.rvAanwezigheidslijst.Location = new System.Drawing.Point(5, 45);
            this.rvAanwezigheidslijst.Name = "rvAanwezigheidslijst";
            this.rvAanwezigheidslijst.Size = new System.Drawing.Size(1034, 542);
            this.rvAanwezigheidslijst.TabIndex = 0;
            // 
            // AanwezigheidslijstUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(245)))), ((int)(((byte)(248)))));
            this.Controls.Add(this.btnForward);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.lblMonth);
            this.Controls.Add(this.dtpMonths);
            this.Controls.Add(this.rvAanwezigheidslijst);
            this.Name = "AanwezigheidslijstUI";
            this.Size = new System.Drawing.Size(1044, 592);
            ((System.ComponentModel.ISupportInitialize)(this.GetUsersAndTrainingDaysBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsAanwezigheidslijst)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label lblMonth;
        private System.Windows.Forms.DateTimePicker dtpMonths;
        private System.Windows.Forms.BindingSource GetUsersAndTrainingDaysBindingSource;
        private dsAanwezigheidslijst dsAanwezigheidslijst;
        private Aanwezigheidslijst.dsAanwezigheidslijstTableAdapters.GetUsersAndTrainingDaysTableAdapter GetUsersAndTrainingDaysTableAdapter;
        private System.Windows.Forms.Button btnForward;
        private Microsoft.Reporting.WinForms.ReportViewer rvAanwezigheidslijst;
    }
}
