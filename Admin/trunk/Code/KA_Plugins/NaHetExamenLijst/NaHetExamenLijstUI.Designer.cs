﻿namespace NaHetExamenLijst
{
    partial class NaHetExamenLijstUI
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource4 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.MemberBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dsMembersAndDegrees = new NaHetExamenLijst.dsMembersAndDegrees();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.MemberTableAdapter = new NaHetExamenLijst.dsMembersAndDegreesTableAdapters.MemberTableAdapter();
            this.label1 = new System.Windows.Forms.Label();
            this.ddlExams = new System.Windows.Forms.ComboBox();
            this.ExamDatesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dsExamDates = new NaHetExamenLijst.dsExamDates();
            this.examinationTableAdapter = new NaHetExamenLijst.dsExamDatesTableAdapters.ExaminationTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.MemberBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsMembersAndDegrees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExamDatesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsExamDates)).BeginInit();
            this.SuspendLayout();
            // 
            // MemberBindingSource
            // 
            this.MemberBindingSource.DataMember = "Member";
            this.MemberBindingSource.DataSource = this.dsMembersAndDegrees;
            // 
            // dsMembersAndDegrees
            // 
            this.dsMembersAndDegrees.DataSetName = "dsMembersAndDegrees";
            this.dsMembersAndDegrees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            reportDataSource4.Name = "dsMembersAndDegrees_Member";
            reportDataSource4.Value = this.MemberBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource4);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "NaHetExamenLijst.NaHetExamenReport.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 38);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(1012, 720);
            this.reportViewer1.TabIndex = 0;
            // 
            // MemberTableAdapter
            // 
            this.MemberTableAdapter.ClearBeforeFill = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Examen datum";
            // 
            // ddlExams
            // 
            this.ddlExams.FormattingEnabled = true;
            this.ddlExams.Location = new System.Drawing.Point(98, 13);
            this.ddlExams.Name = "ddlExams";
            this.ddlExams.Size = new System.Drawing.Size(157, 21);
            this.ddlExams.TabIndex = 3;
            this.ddlExams.SelectedIndexChanged += new System.EventHandler(this.ddlExams_SelectedIndexChanged);
            // 
            // ExamDatesBindingSource
            // 
            this.ExamDatesBindingSource.DataMember = "Examination";
            this.ExamDatesBindingSource.DataSource = this.dsExamDates;
            // 
            // dsExamDates
            // 
            this.dsExamDates.DataSetName = "dsExamDates";
            this.dsExamDates.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // examinationTableAdapter
            // 
            this.examinationTableAdapter.ClearBeforeFill = true;
            // 
            // NaHetExamenLijstUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(245)))), ((int)(((byte)(248)))));
            this.Controls.Add(this.ddlExams);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.reportViewer1);
            this.Name = "NaHetExamenLijstUI";
            this.Size = new System.Drawing.Size(1015, 761);
            ((System.ComponentModel.ISupportInitialize)(this.MemberBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsMembersAndDegrees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExamDatesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsExamDates)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource MemberBindingSource;
        private dsMembersAndDegrees dsMembersAndDegrees;
        private NaHetExamenLijst.dsMembersAndDegreesTableAdapters.MemberTableAdapter MemberTableAdapter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ddlExams;
        private System.Windows.Forms.BindingSource ExamDatesBindingSource;
        private dsExamDates dsExamDates;
        private NaHetExamenLijst.dsExamDatesTableAdapters.ExaminationTableAdapter examinationTableAdapter;
    }
}
