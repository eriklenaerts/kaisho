﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using PluginInterface;
using Microsoft.Reporting.WinForms;

namespace NaHetExamenLijst
{
    public partial class NaHetExamenLijstUI : UserControl, IPlugin
    {
        public NaHetExamenLijstUI()
        {
            InitializeComponent();
        }

        #region IPlugin Members
        IPluginHost myPluginHost = null;
        string myPluginName = "Na het examen lijst";
        string myPluginAuthor = "Dimitris Dierickx";
        string myPluginDescription = "Examenlijst (goed voor bond)";
        string myPluginVersion = "1.0.1";
        private string _connectionstring;

        void PluginInterface.IPlugin.Dispose()
        { }

        public string Description
        {
            get
            {
                return myPluginDescription;
            }
        }

        public string Author
        {
            get
            {
                return myPluginAuthor;
            }
        }

        public IPluginHost Host
        {
            get
            {
                return myPluginHost;
            }
            set
            {
                myPluginHost = value;
            }
        }

        public UserControl MainInterface
        {
            get
            {
                return this;
            }
        }

        public string Version
        {
            get
            {
                return myPluginVersion;
            }
        }

        public string ConnectionString
        {
            get
            {
                return _connectionstring;
            }
            set
            {
                _connectionstring = value;
            }
        }

        public string PluginName
        {
            get { return myPluginName; }
        }

        public void Initialize(string connectionstring)
        {
            _connectionstring = connectionstring;
            LoadExamDropdown();
            //Het rapport wordt automatisch gerefreshed wanneer we in loadExamDropdown van selectedIndex veranderen.
        }

        #endregion

        private void LoadExamDropdown()
        {
            examinationTableAdapter.Connection.ConnectionString = ConnectionString;
            examinationTableAdapter.Fill(dsExamDates.Examination);

            ddlExams.DataSource = dsExamDates.Examination;
            ddlExams.DisplayMember = "Date";
            //Selecteer default de laatste examen datum
            if(ddlExams.Items.Count > 0)
                ddlExams.SelectedIndex = 0;
        }

        private void RefreshReport()
        {
            MemberTableAdapter.Connection.ConnectionString = ConnectionString;

            //we gebruiken niet gewoon dtpExamenDatum.Value omdat hier ook nog een tijd bijzit, hierdoor geeft de qeury niet het
            //juiste resultaat terug. We maken dus een nieuwe datum met alleen het jaar, maand en dag.
            DateTime examDate = Convert.ToDateTime(((System.Data.DataRowView)ddlExams.SelectedItem).Row[0]);
            MemberTableAdapter.Fill(dsMembersAndDegrees.Member, examDate); 

            //add parameters to the project to set the reportTitle
            ReportParameter[] parameters = new ReportParameter[1];
            parameters[0] = new ReportParameter("ExamenDatum", examDate.ToString("dd/MM/yyyy"), false);
            reportViewer1.LocalReport.SetParameters(parameters);

            reportViewer1.RefreshReport();
        }

        private void ddlExams_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshReport();
        }
    }
}
