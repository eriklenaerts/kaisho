using System;
using System.Windows.Forms;
using PluginInterface;
using Microsoft.Reporting.WinForms;

namespace BetalingsLijst
{
    public partial class BetalingsLijstControl : System.Windows.Forms.UserControl, IPlugin
    {
        private string _connectionstring;
        public BetalingsLijstControl()
        {
            InitializeComponent();
        }

        #region IPlugin Members

        IPluginHost myPluginHost = null;
        string myPluginName = "Betalingslijst";
        string myPluginAuthor = "Dimitris Dierickx";
        string myPluginDescription = "Betalingslijst per maand";
        string myPluginVersion = "1.0.0";

        void PluginInterface.IPlugin.Dispose()
        {
            // TODO:  Add ctlMain.PluginInterface.IPlugin.Dispose implementation
        }

        public string Description
        {
            get
            {
                // TODO:  Add ctlMain.Description getter implementation
                return myPluginDescription;
            }
        }

        public string Author
        {
            get
            {
                // TODO:  Add ctlMain.Author getter implementation
                return myPluginAuthor;
            }
        }

        public IPluginHost Host
        {
            get
            {
                // TODO:  Add ctlMain.Host getter implementation
                return myPluginHost;
            }
            set
            {
                myPluginHost = value;
            }
        }

        public UserControl MainInterface
        {
            get
            {
                // TODO:  Add ctlMain.MainInterface getter implementation
                return this;
            }
        }

        public string Version
        {
            get
            {
                // TODO:  Add ctlMain.Version getter implementation
                return myPluginVersion;
            }
        }

        #endregion

        private void RefreshReportForMonth()
        {
            DateTime tmpDate = new DateTime(dateTimePicker1.Value.Year, 1, 1, 0, 0, 0, 0, DateTimeKind.Local);

            getBillsTableAdapter.Connection.ConnectionString = ConnectionString;
            getBillsTableAdapter.Fill(this.kaBills1.GetBills, tmpDate);

            //add parameters to the project to set the reportTitle
            ReportParameter[] parameters = new ReportParameter[1];
            parameters[0] = new ReportParameter("TitleYear", dateTimePicker1.Value.ToString("MMMM yyyy"), false);
            this.reportViewer1.LocalReport.SetParameters(parameters);
            reportViewer1.RefreshReport();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            RefreshReportForMonth();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dateTimePicker1.Value = dateTimePicker1.Value.AddMonths(-1);
            RefreshReportForMonth();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dateTimePicker1.Value = dateTimePicker1.Value.AddMonths(1);
            RefreshReportForMonth();
        }

        #region IPlugin Members


        public string ConnectionString
        {
            get
            {
                return _connectionstring;
            }
            set
            {
                _connectionstring = value;
            }
        }

        public string PluginName
        {
            get { return myPluginName; }
        }

        public void Initialize(string connectionstring)
        {
            _connectionstring = connectionstring;
            RefreshReportForMonth();
        }

        #endregion
    }
}
