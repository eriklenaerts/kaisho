﻿namespace WindowsFormsControlLibrary1
{
    partial class JaarRapportBrasschaatControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource3 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource4 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.Brasschaat_JeugdBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dS1 = new WindowsFormsControlLibrary1.DS1();
            this.Brasschaat_VolwassenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.Niet_brasschaat_JeugdBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.Niet_Brasschaat_VolwassenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nietbrasschaatJeugdBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dS1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.cmbYear = new System.Windows.Forms.ComboBox();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.brasschaatVolwassenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.brasschaatJeugdBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nietBrasschaatVolwassenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.brasschaat_VolwassenTableAdapter = new WindowsFormsControlLibrary1.DS1TableAdapters.Brasschaat_VolwassenTableAdapter();
            this.brasschaat_JeugdTableAdapter = new WindowsFormsControlLibrary1.DS1TableAdapters.Brasschaat_JeugdTableAdapter();
            this.niet_Brasschaat_VolwassenTableAdapter = new WindowsFormsControlLibrary1.DS1TableAdapters.Niet_Brasschaat_VolwassenTableAdapter();
            this.niet_brasschaat_JeugdTableAdapter = new WindowsFormsControlLibrary1.DS1TableAdapters.Niet_brasschaat_JeugdTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.Brasschaat_JeugdBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dS1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Brasschaat_VolwassenBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Niet_brasschaat_JeugdBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Niet_Brasschaat_VolwassenBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nietbrasschaatJeugdBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dS1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.brasschaatVolwassenBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.brasschaatJeugdBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nietBrasschaatVolwassenBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // Brasschaat_JeugdBindingSource
            // 
            this.Brasschaat_JeugdBindingSource.DataMember = "Brasschaat_Jeugd";
            this.Brasschaat_JeugdBindingSource.DataSource = this.dS1;
            // 
            // dS1
            // 
            this.dS1.DataSetName = "DS1";
            this.dS1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // Brasschaat_VolwassenBindingSource
            // 
            this.Brasschaat_VolwassenBindingSource.DataMember = "Brasschaat_Volwassen";
            this.Brasschaat_VolwassenBindingSource.DataSource = this.dS1;
            // 
            // Niet_brasschaat_JeugdBindingSource
            // 
            this.Niet_brasschaat_JeugdBindingSource.DataMember = "Niet_brasschaat_Jeugd";
            this.Niet_brasschaat_JeugdBindingSource.DataSource = this.dS1;
            // 
            // Niet_Brasschaat_VolwassenBindingSource
            // 
            this.Niet_Brasschaat_VolwassenBindingSource.DataMember = "Niet_Brasschaat_Volwassen";
            this.Niet_Brasschaat_VolwassenBindingSource.DataSource = this.dS1;
            // 
            // nietbrasschaatJeugdBindingSource
            // 
            this.nietbrasschaatJeugdBindingSource.DataMember = "Niet_brasschaat_Jeugd";
            this.nietbrasschaatJeugdBindingSource.DataSource = this.dS1BindingSource;
            // 
            // dS1BindingSource
            // 
            this.dS1BindingSource.DataSource = this.dS1;
            this.dS1BindingSource.Position = 0;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            reportDataSource1.Name = "DS1_Brasschaat_Jeugd";
            reportDataSource1.Value = this.Brasschaat_JeugdBindingSource;
            reportDataSource2.Name = "DS1_Brasschaat_Volwassen";
            reportDataSource2.Value = this.Brasschaat_VolwassenBindingSource;
            reportDataSource3.Name = "DS1_Niet_brasschaat_Jeugd";
            reportDataSource3.Value = this.Niet_brasschaat_JeugdBindingSource;
            reportDataSource4.Name = "DS1_Niet_Brasschaat_Volwassen";
            reportDataSource4.Value = this.Niet_Brasschaat_VolwassenBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource2);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource3);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource4);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "WindowsFormsControlLibrary1.JaarRapportBrasschaat2.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 39);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.reportViewer1.Size = new System.Drawing.Size(1039, 546);
            this.reportViewer1.TabIndex = 0;
            // 
            // cmbYear
            // 
            this.cmbYear.FormattingEnabled = true;
            this.cmbYear.Location = new System.Drawing.Point(79, 12);
            this.cmbYear.Name = "cmbYear";
            this.cmbYear.Size = new System.Drawing.Size(84, 21);
            this.cmbYear.TabIndex = 9;
            this.cmbYear.SelectedIndexChanged += new System.EventHandler(this.cmbYear_SelectedIndexChanged);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(210, 10);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(34, 23);
            this.btnNext.TabIndex = 8;
            this.btnNext.Text = ">>";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrevious
            // 
            this.btnPrevious.Location = new System.Drawing.Point(170, 10);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(34, 23);
            this.btnPrevious.TabIndex = 7;
            this.btnPrevious.Text = "<<";
            this.btnPrevious.UseVisualStyleBackColor = true;
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(1, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Year picker";
            // 
            // brasschaatVolwassenBindingSource
            // 
            this.brasschaatVolwassenBindingSource.DataMember = "Brasschaat_Volwassen";
            this.brasschaatVolwassenBindingSource.DataSource = this.dS1BindingSource;
            // 
            // brasschaatJeugdBindingSource
            // 
            this.brasschaatJeugdBindingSource.DataMember = "Brasschaat_Jeugd";
            this.brasschaatJeugdBindingSource.DataSource = this.dS1BindingSource;
            // 
            // nietBrasschaatVolwassenBindingSource
            // 
            this.nietBrasschaatVolwassenBindingSource.DataMember = "Niet_Brasschaat_Volwassen";
            this.nietBrasschaatVolwassenBindingSource.DataSource = this.dS1BindingSource;
            // 
            // brasschaat_VolwassenTableAdapter
            // 
            this.brasschaat_VolwassenTableAdapter.ClearBeforeFill = true;
            // 
            // brasschaat_JeugdTableAdapter
            // 
            this.brasschaat_JeugdTableAdapter.ClearBeforeFill = true;
            // 
            // niet_Brasschaat_VolwassenTableAdapter
            // 
            this.niet_Brasschaat_VolwassenTableAdapter.ClearBeforeFill = true;
            // 
            // niet_brasschaat_JeugdTableAdapter
            // 
            this.niet_brasschaat_JeugdTableAdapter.ClearBeforeFill = true;
            // 
            // JaarRapportBrasschaatControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(245)))), ((int)(((byte)(248)))));
            this.Controls.Add(this.cmbYear);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnPrevious);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.reportViewer1);
            this.Name = "JaarRapportBrasschaatControl";
            this.Size = new System.Drawing.Size(1042, 589);
            ((System.ComponentModel.ISupportInitialize)(this.Brasschaat_JeugdBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dS1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Brasschaat_VolwassenBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Niet_brasschaat_JeugdBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Niet_Brasschaat_VolwassenBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nietbrasschaatJeugdBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dS1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.brasschaatVolwassenBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.brasschaatJeugdBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nietBrasschaatVolwassenBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.ComboBox cmbYear;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.Label label1;
        private WindowsFormsControlLibrary1.DS1TableAdapters.TableAdapterManager MemberTableAdapter;
        private System.Windows.Forms.BindingSource dS1BindingSource;
        private DS1 dS1;
        private System.Windows.Forms.BindingSource nietbrasschaatJeugdBindingSource;
        private System.Windows.Forms.BindingSource brasschaatVolwassenBindingSource;
        private WindowsFormsControlLibrary1.DS1TableAdapters.Brasschaat_VolwassenTableAdapter brasschaat_VolwassenTableAdapter;
        private System.Windows.Forms.BindingSource brasschaatJeugdBindingSource;
        private WindowsFormsControlLibrary1.DS1TableAdapters.Brasschaat_JeugdTableAdapter brasschaat_JeugdTableAdapter;
        private System.Windows.Forms.BindingSource nietBrasschaatVolwassenBindingSource;
        private WindowsFormsControlLibrary1.DS1TableAdapters.Niet_Brasschaat_VolwassenTableAdapter niet_Brasschaat_VolwassenTableAdapter;
        private WindowsFormsControlLibrary1.DS1TableAdapters.Niet_brasschaat_JeugdTableAdapter niet_brasschaat_JeugdTableAdapter;
        private System.Windows.Forms.BindingSource Brasschaat_JeugdBindingSource;
        private System.Windows.Forms.BindingSource Brasschaat_VolwassenBindingSource;
        private System.Windows.Forms.BindingSource Niet_brasschaat_JeugdBindingSource;
        private System.Windows.Forms.BindingSource Niet_Brasschaat_VolwassenBindingSource;
    }
}
