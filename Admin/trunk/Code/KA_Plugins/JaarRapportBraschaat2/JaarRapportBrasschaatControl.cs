﻿using System;
using System.Windows.Forms;
using PluginInterface;
using Microsoft.Reporting.WinForms;

namespace WindowsFormsControlLibrary1
{
    public partial class JaarRapportBrasschaatControl : UserControl, IPlugin
    {
        private string _connectionstring;
        public JaarRapportBrasschaatControl()
        {
            InitializeComponent();
        }

        #region IPlugin Members

        IPluginHost myPluginHost = null;
        string myPluginName = "Jaar rapport Brasschaat 2";
        string myPluginAuthor = "Dimitris Dierickx";
        string myPluginDescription = "Jaar rapport Brasschaat 2";
        string myPluginVersion = "1.0.0";

        void PluginInterface.IPlugin.Dispose()
        {
            // TODO:  Add ctlMain.PluginInterface.IPlugin.Dispose implementation
        }

        public string Description
        {
            get
            {
                // TODO:  Add ctlMain.Description getter implementation
                return myPluginDescription;
            }
        }

        public string Author
        {
            get
            {
                // TODO:  Add ctlMain.Author getter implementation
                return myPluginAuthor;
            }
        }

        public IPluginHost Host
        {
            get
            {
                // TODO:  Add ctlMain.Host getter implementation
                return myPluginHost;
            }
            set
            {
                myPluginHost = value;
            }
        }

        public UserControl MainInterface
        {
            get
            {
                // TODO:  Add ctlMain.MainInterface getter implementation
                return this;
            }
        }

        public string Version
        {
            get
            {
                // TODO:  Add ctlMain.Version getter implementation
                return myPluginVersion;
            }
        }

        #endregion

        public string ConnectionString
        {
            get
            {
                return _connectionstring;
            }
            set
            {
                _connectionstring = value;
            }
        }

        public string PluginName
        {
            get { return myPluginName; }
        }

        public void Initialize(string connectionstring)
        {
            _connectionstring = connectionstring;
            LoadYears();
            RefreshReportForYear();
        }

        private void RefreshReportForYear()
        {
            DateTime tmpDate = new DateTime((int)cmbYear.SelectedItem, 1, 1, 0, 0, 0, 0, DateTimeKind.Local);

            brasschaat_VolwassenTableAdapter.Connection.ConnectionString = ConnectionString;
            brasschaat_VolwassenTableAdapter.Fill(this.dS1.Brasschaat_Volwassen, tmpDate);

            brasschaat_JeugdTableAdapter.Connection.ConnectionString = ConnectionString;
            brasschaat_JeugdTableAdapter.Fill(this.dS1.Brasschaat_Jeugd, tmpDate);

            niet_Brasschaat_VolwassenTableAdapter.Connection.ConnectionString = ConnectionString;
            niet_Brasschaat_VolwassenTableAdapter.Fill(this.dS1.Niet_Brasschaat_Volwassen, tmpDate);

            niet_brasschaat_JeugdTableAdapter.Connection.ConnectionString = ConnectionString;
            niet_brasschaat_JeugdTableAdapter.Fill(this.dS1.Niet_brasschaat_Jeugd, tmpDate);

            //add parameters to the project to set the reportTitle
            ReportParameter[] parameters = new ReportParameter[1];
            parameters[0] = new ReportParameter("param_jaar", cmbYear.SelectedItem.ToString(), false);
            this.reportViewer1.LocalReport.SetParameters(parameters);
            reportViewer1.RefreshReport();
        }

        private void LoadYears()
        {
            cmbYear.Items.Clear();
            for (int i = 1990; i < 2050; i++)
            {
                cmbYear.Items.Add(i);
            }

            if (DateTime.Now.Month <= 8)
                cmbYear.SelectedItem = DateTime.Now.AddYears(-1).Year;
            else
                cmbYear.SelectedItem = DateTime.Now.Year;
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            cmbYear.SelectedItem = ((int)cmbYear.SelectedItem) - 1;
            RefreshReportForYear();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            cmbYear.SelectedItem = ((int)cmbYear.SelectedItem) + 1;
            RefreshReportForYear();
        }

        private void cmbYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshReportForYear();
        }
    }
}
