using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using PluginInterface;
using Microsoft.Reporting.WinForms;
using KA.ServiceLayer;
using KA.BusinessLogicLayer;
using System.Web.UI.WebControls;

namespace VoorHetExamenLijst
{
    public partial class VoorHetExamenLijstUI : UserControl, IPlugin
    {
        public VoorHetExamenLijstUI()
        {
            InitializeComponent();
        }

        private void LoadDropDown()
        {
            /*
            ddlDegree.Items.Clear();

            ListItem listItem = new ListItem("All", "0");
            ddlDegree.Items.Add(listItem);
            listItem = new ListItem("Beginner", "-9");
            ddlDegree.Items.Add(listItem);
            listItem = new ListItem("8 Kyu", "-8");
            ddlDegree.Items.Add(listItem);
            listItem = new ListItem("7 Kyu", "-7");
            ddlDegree.Items.Add(listItem);
            listItem = new ListItem("6 Kyu", "-6");
            ddlDegree.Items.Add(listItem);
            listItem = new ListItem("5 Kyu", "-5");
            ddlDegree.Items.Add(listItem);
            listItem = new ListItem("4 Kyu", "-4");
            ddlDegree.Items.Add(listItem);
            listItem = new ListItem("3 Kyu", "-3");
            ddlDegree.Items.Add(listItem);
            listItem = new ListItem("2 Kyu", "-2");
            ddlDegree.Items.Add(listItem);
            listItem = new ListItem("1 Kyu", "-1");
            ddlDegree.Items.Add(listItem);
            listItem = new ListItem("Sho dan", "1");
            ddlDegree.Items.Add(listItem);
            listItem = new ListItem("Ni dan", "2");
            ddlDegree.Items.Add(listItem);
            listItem = new ListItem("San dan", "3");
            ddlDegree.Items.Add(listItem);
            listItem = new ListItem("Yon dan", "4");
            ddlDegree.Items.Add(listItem);
            listItem = new ListItem("Go dan", "5");
            ddlDegree.Items.Add(listItem);
            listItem = new ListItem("Roku dan", "6");
            ddlDegree.Items.Add(listItem);
            listItem = new ListItem("Shichi dan", "7");
            ddlDegree.Items.Add(listItem);
            listItem = new ListItem("Hachi dan", "8");
            ddlDegree.Items.Add(listItem);
            listItem = new ListItem("Ku dan", "9");
            ddlDegree.Items.Add(listItem);
            listItem = new ListItem("Ju dan", "10");
            ddlDegree.Items.Add(listItem);

            /*Geeft fout bij nettiersprovider vandaar tijdelijk hardcoded*/
            /*
            DegreeService degreeService = new DegreeService();
            TList<Degree> degrees = degreeService.GetAll();

            foreach (Degree degree in degrees)
            { 
                ListItem listItem = new ListItem(degree.Title, degree.Rank.ToString());
                ddlDegree.Items.Add(listItem);
            }
             */
        }

        #region IPlugin Members
        IPluginHost myPluginHost = null;
        string myPluginName = "Voor het examen lijst";
        string myPluginAuthor = "Dimitris Dierickx";
        string myPluginDescription = "Lijst van leden en hun huidige graad";
        string myPluginVersion = "1.0.0";
        private string _connectionstring;

        void PluginInterface.IPlugin.Dispose()
        { }

        public string Description
        {
            get
            {
                return myPluginDescription;
            }
        }

        public string Author
        {
            get
            {
                return myPluginAuthor;
            }
        }

        public IPluginHost Host
        {
            get
            {
                return myPluginHost;
            }
            set
            {
                myPluginHost = value;
            }
        }

        public UserControl MainInterface
        {
            get
            {
                return this;
            }
        }

        public string Version
        {
            get
            {
                return myPluginVersion;
            }
        }

        public string ConnectionString
        {
            get
            {
                return _connectionstring;
            }
            set
            {
                _connectionstring = value;
            }
        }

        public string PluginName
        {
            get { return myPluginName; }
        }

        public void Initialize(string connectionstring)
        {
            //LoadDropDown();
            _connectionstring = connectionstring;
            RefreshReport();
        }

       
        #endregion

        private void RefreshReport()
        {
            MembersAndDegreesTableAdapter.Connection.ConnectionString = ConnectionString;
            MembersAndDegreesTableAdapter.Fill(MembersAndDegrees.MembersAndDegreesTable);

            //add parameters to the project to set the reportTitle
            //ReportParameter[] parameters = new ReportParameter[1];
            //parameters[0] = new ReportParameter("Degree", ddlDegree.Items[ddlDegree.SelectedIndex].ToString(), false);
            //reportViewer1.LocalReport.SetParameters(parameters);

            reportViewer1.RefreshReport();
        }

        private void ddlDegree_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshReport();
        }
    }
}
