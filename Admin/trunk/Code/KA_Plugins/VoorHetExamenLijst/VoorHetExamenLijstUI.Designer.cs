namespace VoorHetExamenLijst
{
    partial class VoorHetExamenLijstUI
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.MembersAndDegreesTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.MembersAndDegrees = new VoorHetExamenLijst.MembersAndDegrees();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.MembersAndDegreesTableAdapter = new VoorHetExamenLijst.MembersAndDegreesTableAdapters.MembersAndDegreesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.MembersAndDegreesTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MembersAndDegrees)).BeginInit();
            this.SuspendLayout();
            // 
            // MembersAndDegreesTableBindingSource
            // 
            this.MembersAndDegreesTableBindingSource.DataMember = "MembersAndDegreesTable";
            this.MembersAndDegreesTableBindingSource.DataSource = this.MembersAndDegrees;
            // 
            // MembersAndDegrees
            // 
            this.MembersAndDegrees.DataSetName = "MembersAndDegrees";
            this.MembersAndDegrees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "MembersAndDegrees_MembersAndDegreesTable";
            reportDataSource1.Value = this.MembersAndDegreesTableBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "VoorHetExamenLijst.VoorHetExamenReport.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(1071, 685);
            this.reportViewer1.TabIndex = 0;
            // 
            // MembersAndDegreesTableAdapter
            // 
            this.MembersAndDegreesTableAdapter.ClearBeforeFill = true;
            // 
            // VoorHetExamenLijstUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.reportViewer1);
            this.Name = "VoorHetExamenLijstUI";
            this.Size = new System.Drawing.Size(1071, 685);
            ((System.ComponentModel.ISupportInitialize)(this.MembersAndDegreesTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MembersAndDegrees)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource MembersAndDegreesTableBindingSource;
        private MembersAndDegrees MembersAndDegrees;
        private VoorHetExamenLijst.MembersAndDegreesTableAdapters.MembersAndDegreesTableAdapter MembersAndDegreesTableAdapter;
    }
}
