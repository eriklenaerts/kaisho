namespace AlgemeneStatistieken
{
    partial class AlgemeneStatistiekenControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.MemberBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.KADataSet = new AlgemeneStatistieken.KADataSet();
            this.gradenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.MemberTableAdapter = new AlgemeneStatistieken.KADataSetTableAdapters.MemberTableAdapter();
            this.gradenTableAdapter = new AlgemeneStatistieken.KADataSetTableAdapters.GradenTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.MemberBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KADataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gradenBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // MemberBindingSource
            // 
            this.MemberBindingSource.DataMember = "Member";
            this.MemberBindingSource.DataSource = this.KADataSet;
            // 
            // KADataSet
            // 
            this.KADataSet.DataSetName = "KADataSet";
            this.KADataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gradenBindingSource
            // 
            this.gradenBindingSource.DataMember = "Graden";
            this.gradenBindingSource.DataSource = this.KADataSet;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            reportDataSource1.Name = "KADataSet_Member";
            reportDataSource1.Value = this.MemberBindingSource;
            reportDataSource2.Name = "KADataSet_Graden";
            reportDataSource2.Value = this.gradenBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource2);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "AlgemeneStatistieken.Report1.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(3, 3);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(1048, 688);
            this.reportViewer1.TabIndex = 0;
            // 
            // MemberTableAdapter
            // 
            this.MemberTableAdapter.ClearBeforeFill = true;
            // 
            // gradenTableAdapter
            // 
            this.gradenTableAdapter.ClearBeforeFill = true;
            // 
            // AlgemeneStatistiekenControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(245)))), ((int)(((byte)(248)))));
            this.Controls.Add(this.reportViewer1);
            this.Name = "AlgemeneStatistiekenControl";
            this.Size = new System.Drawing.Size(1054, 694);
            ((System.ComponentModel.ISupportInitialize)(this.MemberBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KADataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gradenBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource MemberBindingSource;
        private KADataSet KADataSet;
        private AlgemeneStatistieken.KADataSetTableAdapters.MemberTableAdapter MemberTableAdapter;
        private System.Windows.Forms.BindingSource gradenBindingSource;
        private AlgemeneStatistieken.KADataSetTableAdapters.GradenTableAdapter gradenTableAdapter;
    }
}
