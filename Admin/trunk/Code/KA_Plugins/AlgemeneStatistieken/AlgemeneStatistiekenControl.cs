using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using PluginInterface;

namespace AlgemeneStatistieken
{
    public partial class AlgemeneStatistiekenControl : System.Windows.Forms.UserControl, IPlugin
    {
        public AlgemeneStatistiekenControl()
        {
            InitializeComponent();
        }

        #region IPlugin Members

        IPluginHost myPluginHost = null;
        string myPluginName = "Algemene Statistieken";
        string myPluginAuthor = "Dimitris Dierickx";
        string myPluginDescription = "Graden rapport / mannen - vrouwen verhouding";
        string myPluginVersion = "1.0.0";
        private string _connectionstring;

        void PluginInterface.IPlugin.Dispose()
        {
        }

        public string Description
        {
            get
            {
                return myPluginDescription;
            }
        }

        public string Author
        {
            get
            {
                return myPluginAuthor;
            }
        }

        public IPluginHost Host
        {
            get
            {
                return myPluginHost;
            }
            set
            {
                myPluginHost = value;
            }
        }



        public UserControl MainInterface
        {
            get
            {
                return this;
            }
        }

        public string Version
        {
            get
            {
                return myPluginVersion;
            }
        }

        public string ConnectionString
        {
            get
            {
                return _connectionstring;
            }
            set
            {
                _connectionstring = value;
            }
        }

        public string PluginName
        {
            get { return myPluginName; }
        }

        public void Initialize(string connectionstring)
        {
            _connectionstring = connectionstring;
            RefreshReport();
        }

        #endregion

        private void RefreshReport()
        {
            gradenTableAdapter.Connection.ConnectionString = ConnectionString;
            MemberTableAdapter.Connection.ConnectionString = ConnectionString;

            gradenTableAdapter.Fill(this.KADataSet.Graden);
            MemberTableAdapter.Fill(this.KADataSet.Member);   
            reportViewer1.RefreshReport();
        }
    }
}
