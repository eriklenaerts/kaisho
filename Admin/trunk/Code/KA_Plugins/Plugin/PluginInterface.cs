using System;
using System.Windows.Forms;

namespace PluginInterface
{
	public interface IPlugin
	{
		IPluginHost Host {get;set;}

        string ConnectionString { get; set;}
        string Name { get;}
		string PluginName {get;}
		string Description {get;}
		string Author {get;}
		string Version {get;}
		
		System.Windows.Forms.UserControl MainInterface {get;}
		
		void Initialize(string connectionstring);
		void Dispose();
	
	}
	
	public interface IPluginHost
	{
		void Feedback(string Feedback, IPlugin Plugin);	
	}
}
