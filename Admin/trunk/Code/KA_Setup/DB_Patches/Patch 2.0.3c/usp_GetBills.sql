USE [master]
GO
select name from master..sysdatabases
GO
-- Detach any previous attached DB
EXEC sp_detach_db 'KA', 'true';
GO

-- Attach the specified DB
CREATE DATABASE [KA] ON 
( FILENAME = N'C:\temp\KA.mdf' ),
( FILENAME = N'C:\temp\KA_Log.ldf' )
 FOR ATTACH ;
GO

-- Upgrade Stored procedure
USE [KA]
GO
/****** Object:  StoredProcedure [dbo].[GetBills]    Script Date: 09/11/2008 21:35:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author: Dierickx Dimitris
-- Create date: 21/06/2007
-- Description: Gets all the users, months and the amounts they have to pay
-- =============================================

ALTER PROCEDURE [dbo].[GetBills]
@Year datetime
AS
BEGIN

/*

De ze stored procedure krijgt telkens een datum binnen van het type 1/1/jaar. Alleen het jaar is dus variabel
Om de maanden te kunnen selecteren worden deze in een tijdelijke tabel gezet door een while lus te creeren die stopt 
als het jaar veranderd van de @year parameter
Hierna gaan we de actieve members ophalen, hun groepnaam, groepID en de prijs die ze moeten betalen.
*/
 
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.

SET NOCOUNT ON;

/************Haal de maanden op************/
set datefirst 1; --indicates the first day of the week (here its monday) American standards use 7, sunday
declare @tmpYear datetime; --hierin houden we het jaar bij, zodra deze veranderd moeten we stoppen in de while lus
set @tmpYear = @year; 
DECLARE @Months table( maand datetime);

while Month(@Year) != Month(@tmpYear)-2 --Veranderd van year naar Month
BEGIN
      insert into @Months (maand)
      values (@Year);
      set @Year = dateAdd(MM,1,@Year);
END

/******************************************/

/************Haal members op en bereken prijs**********/
DECLARE @Members table( id int,
                                   groupID int,
                                   groupName varchar(50),
                                   firstName varchar(50),
                                   price decimal(4,2),
                                   paymentid char(1));
DECLARE CursorMember CURSOR FOR SELECT id, isTrainer, groupID, paymentid FROM Member where IsActive = 'true';
declare @id int;
declare @isTrainer bit;
declare @groupID int;
declare @priceID int;
declare @groupName varchar(50);
declare @firstName varchar(50);
declare @price varchar(50);
declare @paymentID char(1);

--@idMembers wordt gebruikt om een uniek ID mee te geven aan de member
--Als deze geen groupID heeft (null) dan wordt er dit memberID meegegeven zodat
--het duidelijk is voor het rapport dat deze in een aparte groep zit (doordat dit nummer uniek is)
--het laagste member id dat kan voorkomen is het hoogste groepID + 1 zodat deze niet met elkaar vermengd kunnen worden
declare @idMembers int;
set @idMembers = (select max(groupID) from member);
OPEN CursorMember
FETCH NEXT FROM CursorMember into @id, @isTrainer, @groupID, @paymentid;
WHILE @@FETCH_STATUS = 0
begin
      --eerst gaan we zien wat voor prijstype we moeten aanrekene trainer of gewoon
      if ((select isTrainer from Member where member.ID = @ID) = 'true')
      begin 
            set @priceID = 2;
            set @price = (select price from PriceScheme where priceTypeID = @priceID ); 
      end
      else
      begin
            set @priceID = 1;
      end

      --haal de prijs op als het niet om een trainer gaat (een trainer hoeft niet te betalen)
      if ((select isTrainer from Member where member.ID = @ID) = 'false')
      begin
            set @price = (select price 
            from PriceScheme 
            where priceTypeID = @priceID 
            and quantity = case
            when (select count(groupID) from member where groupid = @groupID and IsActive = 'true' and IsTrainer = 'false') > (select max(quantity) from PriceScheme) then (select max(quantity) from PriceScheme) --5 then 5 --
            when (select count(groupID) from member where groupid = @groupID and IsActive = 'true' and IsTrainer = 'false') = 0 then 1
            else (select count(groupID) from member where groupid = @groupID and IsActive = 'true' and IsTrainer = 'false') 
            end);
      end
      else
      begin
            --haal de prijs op voor een trainer
            set @price = (select price from PriceScheme where priceTypeID = @priceID)
      end
      --als de groupID null is dan nemen we de lastname als groepnaam
      set @groupname = isnull((select name from [group] where id in (select groupid from member where ID = @id)), (select lastname from member where ID = @id))
      set @firstName = (select firstname from member where id = @id);
      set @idMembers = @idMembers +1;
      insert into @Members 
      values (@idMembers, @groupID, @groupName, @firstName, @price, @paymentID);
      FETCH NEXT FROM CursorMember into @id, @isTrainer, @groupID, @paymentID;
end

CLOSE CursorMember
DEALLOCATE CursorMember

select mo.maand, isnull(m.groupID, m.id) as groupID, m.groupName, firstName, price, (select code from PaymentType where id = m.paymentid) as PaymentCode
from @Members m, @months mo
order by m.groupName

END
