using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using KA.ServiceLayer;
using KA.BusinessLogicLayer;
using System.IO;

namespace KA_ImageExplorer
{
    public partial class Form1 : Form
    {
        DegreeService _degreeService;
        public Form1()
        {
            InitializeComponent();
            _degreeService = new DegreeService();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            TList<Degree> degrees = _degreeService.GetAll();
            foreach (Degree degree in degrees)
            {
                TreeNode node = new TreeNode();
                node.Text = degree.Title;
                node.Tag = degree;

                treeView1.Nodes.Add(node);
            }
        }

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            
                    Degree degree = (Degree)e.Node.Tag;
                    if (degree.Picture != null)
                    {
                        MemoryStream stream = new MemoryStream(degree.Picture);
                        Image image = Image.FromStream(stream);
                        pictureBox1.Image = image;
                    }
       }

        protected static byte[] ReadBitmap2ByteArray(string fileName)
        {
            using (Image image = Image.FromFile(fileName))
            {
                MemoryStream stream = new MemoryStream();
                image.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }

        private void treeView1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "PNG Images(*.png)|*.png;";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    byte[] byteArray = ReadBitmap2ByteArray(dialog.FileName);

                    Degree degree = (Degree)e.Node.Tag;
                    degree.Picture = byteArray;
                    _degreeService.Update(degree);
                }
                catch (Exception ex) { MessageBox.Show(ex.Message, "Error"); };
            }
        }
    }
}