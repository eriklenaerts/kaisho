using System;
using System.Collections.Generic;
using System.Text;

namespace KA
{
    public class MessageCenterMessage
    {
        private string _type;

        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        private string _message;

        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

        public MessageCenterMessage(string type, string message)
        {
            _type = type;
            _message = message;
        }
	
	
    }
}
