using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace KA_Common
{
    public class Enums
    {
        public enum MainCategory
        { 
            Aanwezigheidslijst,
            Betalingslijst,
            Ledenlijst,
            Groepsbeheer,
            Examens,
            Statistieken,
            Sluiten
        }

        public enum Controller
        {
            MemberListController, MemberDetailController, MemberHistoryController, ExamListController, StatisticsController, StatisticsViewerController, FamilyListController, FamilyDetailController, TrainingDaysController, OptionsController, TrainingDaysExceptionsController, TrainingDaysDetailController, TrainingDaysExceptionsDetailController, AddHistory}

        [Flags]
        public enum Actions
        {
            add, delete, update, copy, refresh, detail, showview, ExamList, ReportClicked, ReportPlugin, LoadReports, GetFilteredMembers, GetFilteredFamilies, deleteReport, ShowHistory, Apply, LoadTrainingDays, LoadTrainingExceptions, RefreshDegree
        }
    }
}

