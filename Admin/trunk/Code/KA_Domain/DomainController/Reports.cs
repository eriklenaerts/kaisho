using System;
using System.Collections.Generic;
using System.Text;

namespace KA_Domain.DomainController
{
    public class Reports
    {
        public static List<Rapport> GetAll()
        {
         List<Rapport> reports = new List<Rapport>();

            reports.Add(new Rapport("Mannen & Vrouwen", "Rapport om het evenwicht tussen mannen en vrouwen te bekijken gecombineerd met hun graad", @"c:\rapportfolder\rapport"));
            reports.Add(new Rapport("Graden & Leeftijd", "Rapport om gemiddelde graad per leeftijd te bekijken", @"c:\rapportfolder\rapport"));
            reports.Add(new Rapport("Lid & #trainigen", "Rapport om het aantal trainingen per lid te bekijken op jaarbasis", @"c:\rapportfolder\rapport"));
            reports.Add(new Rapport("Kengetallen", "Rapport voor de kengetallen voor de Karate bond", @"c:\rapportfolder\rapport"));

           return reports;
        }
    
    }
}
