using System;
using System.Collections.Generic;
using System.Text;

namespace KA_Domain.DomainController
{
    public class Members
    {
        public static List<Member> GetAll()
        {
            List<Member> members = new List<Member>();

            members.Add(new Member("Dimitris Dierickx",9,  "Lodezielendslaan", "40", "6", "2160", "Wommelgem", true));
            members.Add(new Member("Bart Callaerts",6, "Herentalsebaan", "133", "3", "2150", "Borsbeek", true));
            members.Add(new Member("Erik Lenaerts",1, "E. Verelstlei", "14", "", "2150", "Borsbeek", true));

            return members;
        }

        //public static void FillRapportenLijst()
        //{
        //    List<Rapport> reports = new List<Rapport>();

        //    reports.Add(new Rapport("Mannen & Vrouwen", "Rapport om het evenwicht tussen mannen en vrouwen te bekijken gecombineerd met hun graad", @"c:\rapportfolder\rapport"));
        //    reports.Add(new Rapport("Graden & Leeftijd", "Rapport om gemiddelde graad per leeftijd te bekijken", @"c:\rapportfolder\rapport"));
        //    reports.Add(new Rapport("Lid & #trainigen", "Rapport om het aantal trainingen per lid te bekijken op jaarbasis", @"c:\rapportfolder\rapport"));
        //    reports.Add(new Rapport("Kengetallen", "Rapport voor de kengetallen voor de Karate bond", @"c:\rapportfolder\rapport"));

        //    Statistics.Instance.FillGrid(reports);
        //}

        //public static void FillExamenLedenLijst()
        //{
        //    List<Lid> leden = new List<Lid>();

        //    leden.Add(new Lid("Dimitris Dierickx", Belts._8kyu, "Lodezielendslaan", "40", "6", "2160", "Wommelgem"));
        //    leden.Add(new Lid("Bart Callaerts", Belts._6kyu, "Herentalsebaan", "133", "3", "2150", "Borsbeek"));
        //    leden.Add(new Lid("Erik Lenaerts", Belts._1kyu, "E. Verelstlei", "14", "", "2150", "Borsbeek"));

        //    Examens.Instance.FillGrid(leden);
        //}
    }
}
