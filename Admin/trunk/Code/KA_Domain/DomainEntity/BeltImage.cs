using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace KA_Domain
{
    public class BeltImage
    {
        public static Bitmap GetBeltImage(int index)
        { 
            switch (index)
            {
                case 1:
                    return KA_Domain.Properties.Resource1._9kyu;
                case 2:
                    return KA_Domain.Properties.Resource1._8kyu;
                case 4:
                    return KA_Domain.Properties.Resource1._7kyu;
                case 5:
                    return KA_Domain.Properties.Resource1._6kyu;
                case 6:
                    return KA_Domain.Properties.Resource1._5kyu;
                case 7:
                    return KA_Domain.Properties.Resource1._4kyu;
                case 8:
                    return KA_Domain.Properties.Resource1._3kyu;
                case 9:
                    return KA_Domain.Properties.Resource1._2kyu;
                case 10:
                    return KA_Domain.Properties.Resource1._1kyu;
                case 11:
                    return KA_Domain.Properties.Resource1._1dan;
                case 12:
                    return KA_Domain.Properties.Resource1._2dan;
                case 13:
                    return KA_Domain.Properties.Resource1._3dan;
                case 14:
                    return KA_Domain.Properties.Resource1._4dan;
                case 15:
                    return KA_Domain.Properties.Resource1._5dan;
		    default:
                return null;
                 break;
                 return null;
	        }   
            
        }
    }
}
