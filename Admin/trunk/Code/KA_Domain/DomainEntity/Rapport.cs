using System;
using System.Collections.Generic;
using System.Text;

namespace KA_Domain
{
    public class Rapport
    {
        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private string _path;

        public string Path
        {
            get { return _path; }
            set { _path = value; }
        }

        public Rapport(string name, string description, string path)
        {
            _name = name;
            _description = description;
            _path = path;
        }
	
	
    }
}
