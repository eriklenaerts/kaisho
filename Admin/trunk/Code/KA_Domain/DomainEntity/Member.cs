using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace KA_Domain
{
    public class Member
    {
        private string _naam;

        public string Naam
        {
            get { return _naam; }
            set { _naam = value; }
        }

        private int _graad;

        public int Graad
        {
            get { return _graad; }
            set { _graad = value; }
        }

        private Bitmap _graadImage;

        public Bitmap GraadImage
        {
            get { return _graadImage; }
            set { _graadImage = value; }
        }


        private string _straat;

        public string Straat 
        {
            get { return _straat; }
            set { _straat = value; }
        }

        private string _nr;

        public string Nr
        {
            get { return _nr; }
            set { _nr = value; }
        }

        private string _bus;

        public string Bus
        {
            get { return _bus; }
            set { _bus = value; }
        }

        private string _postcode;

        public string Postcode
        {
            get { return _postcode; }
            set { _postcode = value; }
        }

        private string _gemeente;

        public string Gemeente
        {
            get { return _gemeente; }
            set { _gemeente = value; }
        }

        private bool _isActive;

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        public Member(string naam, int graad,  string straat, string nr, string bus, string postcode, string gemeente, bool isActive)
        {
            _naam = naam;
            _graad = graad;
            _graadImage = BeltImage.GetBeltImage(graad);
            _straat = straat;
            _nr = nr;
            _bus = bus;
            _postcode = postcode;
            _gemeente = gemeente;
            _isActive = isActive;
        }
	
	
	

    }
}
