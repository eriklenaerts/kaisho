USE [KA]


/****** Object:  Table [dbo].[tblGraad]    Script Date: 07/03/2007 20:48:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGraad]') AND type in (N'U'))
DROP TABLE [dbo].[tblGraad]
GO


/****** Object:  Table [dbo].[tblLid]    Script Date: 07/03/2007 20:47:14 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[tblLid_FK00]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblLid]'))
ALTER TABLE [dbo].[tblLid] DROP CONSTRAINT [tblLid_FK00]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLid]') AND type in (N'U'))
DROP TABLE [dbo].[tblLid]
GO


/****** Object:  Table [dbo].[tblFamilie]    Script Date: 07/03/2007 20:49:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFamilie]') AND type in (N'U'))
DROP TABLE [dbo].[tblFamilie]
GO