
USE [master]
GO
/****** Object:  Database [KA]    Script Date: 07/15/2007 17:10:49 ******/
IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'KA')
DROP DATABASE [KA]


/****** Object:  Database [KA]    Script Date: 07/15/2007 17:10:17 ******/
CREATE DATABASE [KA] ON  PRIMARY 
( NAME = N'KA', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL.4\MSSQL\DATA\KA.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'KA_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL.4\MSSQL\DATA\KA_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
EXEC dbo.sp_dbcmptlevel @dbname=N'KA', @new_cmptlevel=90
GO