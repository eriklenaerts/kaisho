USE [KA]
GO

/****** Object:  Table [dbo].[Examination]    Script Date: 07/03/2007 21:14:29 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Examination_Member]') AND parent_object_id = OBJECT_ID(N'[dbo].[Examination]'))
ALTER TABLE [dbo].[Examination] DROP CONSTRAINT [FK_Examination_Member]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Examination_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[Examination]'))
ALTER TABLE [dbo].[Examination] DROP CONSTRAINT [FK_Examination_Location]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Examination_Degree]') AND parent_object_id = OBJECT_ID(N'[dbo].[Examination]'))
ALTER TABLE [dbo].[Examination] DROP CONSTRAINT [FK_Examination_Degree]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Examination]') AND type in (N'U'))
DROP TABLE [dbo].[Examination]
GO


/****** Object:  Table [dbo].[Member]    Script Date: 07/03/2007 21:15:17 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Member_Group]') AND parent_object_id = OBJECT_ID(N'[dbo].[Member]'))
ALTER TABLE [dbo].[Member] DROP CONSTRAINT [FK_Member_Group]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Member]') AND type in (N'U'))
DROP TABLE [dbo].[Member]
GO


/****** Object:  Table [dbo].[Group]    Script Date: 07/03/2007 21:15:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Group]') AND type in (N'U'))
DROP TABLE [dbo].[Group]
GO


/****** Object:  Table [dbo].[Degree]    Script Date: 07/03/2007 21:14:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Degree]') AND type in (N'U'))
DROP TABLE [dbo].[Degree]
GO


/****** Object:  Table [dbo].[PriceScheme]    Script Date: 07/03/2007 21:14:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PriceScheme_PriceType]') AND parent_object_id = OBJECT_ID(N'[dbo].[PriceScheme]'))
ALTER TABLE [dbo].[PriceScheme] DROP CONSTRAINT [FK_PriceScheme_PriceType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PriceScheme]') AND type in (N'U'))
DROP TABLE [dbo].[PriceScheme]
GO


/****** Object:  Table [dbo].[PriceType]    Script Date: 07/15/2007 17:09:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PriceType]') AND type in (N'U'))
DROP TABLE [dbo].[PriceType]



/****** Object:  Table [dbo].[TrainingTime]    Script Date: 07/15/2007 17:42:29 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrainingTime_TrainingPeriod]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainingTime]'))
ALTER TABLE [dbo].[TrainingTime] DROP CONSTRAINT [FK_TrainingTime_TrainingPeriod]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrainingTime_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainingTime]'))
ALTER TABLE [dbo].[TrainingTime] DROP CONSTRAINT [FK_TrainingTime_Location]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrainingTime]') AND type in (N'U'))
DROP TABLE [dbo].[TrainingTime]



/****** Object:  Table [dbo].[TrainingPeriod]    Script Date: 07/15/2007 17:43:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrainingPeriod]') AND type in (N'U'))
DROP TABLE [dbo].[TrainingPeriod]



/****** Object:  Table [dbo].[TrainingExceptions]    Script Date: 07/15/2007 17:43:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrainingExceptions]') AND type in (N'U'))
DROP TABLE [dbo].[TrainingExceptions]



/****** Object:  Table [dbo].[Location]    Script Date: 07/15/2007 18:08:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Location]') AND type in (N'U'))
DROP TABLE [dbo].[Location]