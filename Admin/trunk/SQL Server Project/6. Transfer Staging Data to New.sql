USE KA
GO

/****** Transfer all the Families into the Group table *****/
PRINT 'Insert groups:'
SET IDENTITY_INSERT [dbo].[GROUP] ON

INSERT INTO [dbo].[GROUP] ([ID], [NAME])
SELECT [FamilieID], [FamilieNaam]
FROM [tblFamilie]
WHERE [FamilieID] > 2
GO

SET IDENTITY_INSERT [dbo].[GROUP] OFF

/****** Transfer all the Leden into the Member table *****/
SET IDENTITY_INSERT [dbo].[Member] ON


PRINT 'Insert members:'
INSERT INTO [dbo].[Member]
           ([ID]
		   ,[Lastname]
           ,[Firstname]
           ,[Address]
           ,[ZipCode]
           ,[City]
           ,[Province]
           ,[Country]
           ,[Email]
           ,[Telephone]
           ,[Sex]
           ,[Birthdate]
           ,[LicenseNumber]
           ,[GroupID]
           ,[IsGMEmployee]
           ,[IsTrainer]
           ,[IsNew]
           ,[IsActive]
           ,[InsuranceEndDate]
           ,[StartYear]
           ,[Foto])
SELECT [LidID],
	   [Achternaam],
	   [Voornaam],
	   [Straat],
	   [PostCode],
	   [Woonplaats],
	   [Provincie],
	   'Belgi�' AS 'Land',
	   [Email],
	   [Telefoon],
	   [Geslacht],
	   CAST([Geboortedatum] AS SMALLDATETIME),
	   [LicentieNummer],
	   [Groep_FamilieID],
	   [Groep_GM],
	   [Groep_Trainer],
	   [Nieuw],
	   CASE [Inactief] 
			WHEN 0 THEN 1
			WHEN 1 THEN 0
	   END AS 'Active',
	   [VerzekeringGeldigTot],
	   [IngeschrevenOp],
	   [Foto]
FROM [tblLid]

GO

SET IDENTITY_INSERT [dbo].[Member] OFF


/****** Transfer all the Graden into the Examination table *****/
SET IDENTITY_INSERT [dbo].[Examination] ON

PRINT 'Insert examinations:'
INSERT INTO [dbo].[Examination] ([ID], [MemberID], [DegreeID], [Date], [LocationID] )
SELECT [GraadID],
	   [LidID],
	   CASE SUBSTRING([Graad], 1, 2)
		WHEN '01' THEN 1
		WHEN '02' THEN 2
		WHEN '03' THEN 3
		WHEN '04' THEN 4 
		WHEN '05' THEN 5
		WHEN '06' THEN 6
		WHEN '07' THEN 7
		WHEN '08' THEN 8
		WHEN '09' THEN 9
		WHEN '10' THEN 10
		WHEN '11' THEN 11
		WHEN '12' THEN 12
	   END AS 'Graad',
	   [ExamenDatum],
	   CASE LOWER([ExamenPlaats])
		WHEN 'brasschaat' THEN 1
		WHEN 'wommelgem' THEN 2
		WHEN 'vkv nationaal' THEN 3
		WHEN 'nationaal' THEN 3
		ELSE 0
	   END AS 'LocationID'
FROM [tblGraad] INNER JOIN [Member] ON [tblGraad].[LidId] = [Member].[Id]

GO

SET IDENTITY_INSERT [dbo].[Examination] OFF

