USE [KA]

-- GM Flag resetten
-- omdat GM niet langer sponsert, moeten alle leden op False worden gezet ifv van de GM vlag
-- dit is tijdelijk omdat in v2.0.2 deze vlag uit de DB verdwijnt.
UPDATE Member
SET IsGMEmployee = 0