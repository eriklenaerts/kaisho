USE [KA]
GO
-- Stored Procedure

-- =============================================
-- Author:		Dierickx Dimitris
-- Create date: 21/06/2007
-- Description:	Gets all the users, months and  the amounts they have to pay
-- =============================================
CREATE PROCEDURE [dbo].[GetBills]
	@Year datetime
AS
BEGIN
/*
De ze stored procedure krijgt telkens een datum binnen van het type 1/1/jaar. Alleen het jaar is dus variabel
Om de maanden te kunnen selecteren worden deze in een tijdelijke tabel gezet door een while lus te creeren die stopt 
als het jaar veranderd van de @year parameter

Hierna gaan we de actieve members ophalen, hun groepnaam, groepID en de prijs die ze moeten betalen.

*/


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/************Haal de maanden op************/

	set datefirst 1; --indicates the first day of the week (here its monday) American standards use 7, sunday
	declare @tmpYear datetime; --hierin houden we het jaar bij, zodra deze veranderd moeten we stoppen in de while lus
	set @tmpYear = @year; 

	DECLARE @Months table( maand datetime);

	while year(@Year) = year(@tmpYear)
	BEGIN
		insert into @Months (maand)
		values (@Year);
		set @Year = dateAdd(MM,1,@Year);
	END

	/******************************************/


	/************Haal members op en bereken prijs**********/
	DECLARE @Members table( id int,
							groupID int,
							groupName varchar(50),
							firstName varchar(50),
							price decimal(4,2));

	DECLARE CursorMember CURSOR FOR SELECT id, isTrainer, isGMEmployee, groupID FROM Member where IsActive = 'true';
	declare @id int;
	declare @isTrainer bit;
	declare @isGMEmployee bit;
	declare @groupID int;

	declare @priceID int;
	declare @groupName varchar(50);
	declare @firstName varchar(50);
	declare @price varchar(50);

	--@idMembers wordt gebruikt om een uniek ID mee te geven aan de member
	--Als deze geen groupID heeft (null) dan wordt er dit memberID meegegeven zodat
	--het duidelijk is voor het rapport dat deze in een aparte groep zit (doordat dit nummer uniek is)
	--het laagste member id dat kan voorkomen is het hoogste groepID + 1 zodat deze niet met elkaar vermengd kunnen worden
	declare @idMembers int;
	set @idMembers = (select max(groupID) from member);

	OPEN CursorMember

	FETCH NEXT FROM CursorMember into @id, @isTrainer, @isGMEmployee, @groupID;

	WHILE @@FETCH_STATUS = 0
	begin
		--eerst gaan we zien wat voor prijstype we moeten aanrekene trainer, GM werknemer of gewoon
		if ((select isTrainer from Member where member.ID = @ID) = 'true')
		begin 
			set @priceID = 2;
			set @price = (select price from PriceScheme where priceTypeID = @priceID );	
		end
		else if ((select isGMEmployee from member where member.ID = @ID) = 'true')
		begin 
			set @priceID = 3;
		end
		else
		begin
			set @priceID = 1;
		end

		--haal de prijs op als het niet om een trainer gaat (een trainer betaalt steeds apart niet te betalen)
		if ((select isTrainer from Member where member.ID = @ID) = 'false')
		begin
			set @price = (select price 
							from PriceScheme 
							where priceTypeID = @priceID 
							and quantity = case
												when (select count(groupID) from member where groupid = @groupID and IsActive = 'true' and IsTrainer = 'false') > (select max(quantity) from PriceScheme) then (select max(quantity) from PriceScheme) --5 then 5 --
												when (select count(groupID) from member where groupid = @groupID and IsActive = 'true' and IsTrainer = 'false') = 0 then 1
												else (select count(groupID) from member where groupid = @groupID and IsActive = 'true' and IsTrainer = 'false') 
											end);
		end
		else
		begin
			--haal de prijs op voor een trainer
			set @price = (select price from PriceScheme where priceTypeID = @priceID)
		end

		--als de groupID null is dan nemen we de lastname als groepnaam
		set @groupname = isnull((select name from [group] where id in (select groupid from member where ID = @id)), (select lastname from member where ID = @id))
		set @firstName = (select firstname from member where id = @id);
		set @idMembers = @idMembers +1;

		insert into @Members 
		values (@idMembers, @groupID, @groupName, @firstName, @price);

		FETCH NEXT FROM CursorMember into @id, @isTrainer, @isGMEmployee, @groupID;
	end

	CLOSE CursorMember
	DEALLOCATE CursorMember

	select mo.maand, isnull(m.groupID, m.id) as groupID, m.groupName, firstName, price
	from @Members m, @months mo
	order by m.groupName
END

GO

-- Stored Procedure

-- =============================================
-- Author:		Dimitris Dierickx
-- Create date: 21/06/2007
-- Description:	Get the users and the trainingdays for a specific month
-- =============================================
CREATE PROCEDURE [dbo].[GetUsersAndTrainingDays]
	@Month datetime
AS
BEGIN
	set dateformat dmy;
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--specifier dat maandag de eerste dag van de week is
	set datefirst 1;

	declare @tmpMonth datetime; --hierin houden we de maand bij, zodra deze veranderd moeten we stoppen in de while lus
	set @tmpMonth = @Month;

	DECLARE @TrainingDaysForMonth table( TrainingDay datetime,
										 Training bit);

	while Month(@Month) = Month(@tmpMonth)
	BEGIN
		if datepart(dw, @Month) in (select dayOfTheWeek from TrainingTime) --dw = weekday
		BEGIN
			if @Month in (select [Day] from trainingExceptions)
			BEGIN
				insert into @TrainingDaysForMonth (TrainingDay, Training)
				values (@Month, 0);
			END
			else
			BEGIN
				insert into @TrainingDaysForMonth (TrainingDay, Training)
				values (@Month, 1);
			END
		END
		set @Month = dateAdd(dd,1,@month);
	END

	SELECT m.LastName, m.firstName, t.TrainingDay, t.Training
	FROM Member m, @TrainingDaysForMonth t
	where m.IsActive = 'true'
	order by m.lastName
END


GO