USE KA
GO

PRINT 'Creating Degree data'
INSERT INTO [KA].[dbo].[Degree]([ID], [Rank], [Title], [Color], [Stripes]) VALUES (1, -9, N'Beginner', N'White', 0)
INSERT INTO [KA].[dbo].[Degree]([ID], [Rank], [Title], [Color], [Stripes]) VALUES (2, -8, N'8 Kyu', N'White', 1)
INSERT INTO [KA].[dbo].[Degree]([ID], [Rank], [Title], [Color], [Stripes]) VALUES (3, -7, N'7 Kyu', N'White', 2)
INSERT INTO [KA].[dbo].[Degree]([ID], [Rank], [Title], [Color], [Stripes]) VALUES (4, -6, N'6 Kyu', N'White', 3)
INSERT INTO [KA].[dbo].[Degree]([ID], [Rank], [Title], [Color], [Stripes]) VALUES (5, -5, N'5 Kyu', N'Blue', 0)
INSERT INTO [KA].[dbo].[Degree]([ID], [Rank], [Title], [Color], [Stripes]) VALUES (6, -4, N'4 Kyu', N'Blue', 1)
INSERT INTO [KA].[dbo].[Degree]([ID], [Rank], [Title], [Color], [Stripes]) VALUES (7, -3, N'3 Kyu', N'Brown', 0)
INSERT INTO [KA].[dbo].[Degree]([ID], [Rank], [Title], [Color], [Stripes]) VALUES (8, -2, N'2 Kyu', N'Brown', 1)
INSERT INTO [KA].[dbo].[Degree]([ID], [Rank], [Title], [Color], [Stripes]) VALUES (9, -2, N'1 Kyu', N'Brown', 2)
INSERT INTO [KA].[dbo].[Degree]([ID], [Rank], [Title], [Color], [Stripes]) VALUES (10, 1, N'Sho dan', N'Black', 0)
INSERT INTO [KA].[dbo].[Degree]([ID], [Rank], [Title], [Color], [Stripes]) VALUES (11, 2, N'Ni dan', N'Black', 1)
INSERT INTO [KA].[dbo].[Degree]([ID], [Rank], [Title], [Color], [Stripes]) VALUES (12, 3, N'San dan', N'Black', 2)
INSERT INTO [KA].[dbo].[Degree]([ID], [Rank], [Title], [Color], [Stripes]) VALUES (13, 4, N'Yon dan', N'Black', 3)
INSERT INTO [KA].[dbo].[Degree]([ID], [Rank], [Title], [Color], [Stripes]) VALUES (14, 5, N'Go dan', N'Black', 4)
INSERT INTO [KA].[dbo].[Degree]([ID], [Rank], [Title], [Color], [Stripes]) VALUES (15, 6, N'Roku dan', N'Black', 5)
INSERT INTO [KA].[dbo].[Degree]([ID], [Rank], [Title], [Color], [Stripes]) VALUES (16, 7, N'Shichi dan', N'Black', 6)
INSERT INTO [KA].[dbo].[Degree]([ID], [Rank], [Title], [Color], [Stripes]) VALUES (17, 8, N'Hachi dan', N'Black', 7)
INSERT INTO [KA].[dbo].[Degree]([ID], [Rank], [Title], [Color], [Stripes]) VALUES (18, 9, N'Ku dan', N'Black', 8)
INSERT INTO [KA].[dbo].[Degree]([ID], [Rank], [Title], [Color], [Stripes]) VALUES (19, 10, N'Ju dan', N'Black', 9)

PRINT 'Creating PriceType data'
INSERT INTO [KA].[dbo].[PriceType]([ID], [Name]) VALUES (1, N'Familie')
INSERT INTO [KA].[dbo].[PriceType]([ID], [Name]) VALUES (2, N'Trainer')
INSERT INTO [KA].[dbo].[PriceType]([ID], [Name]) VALUES (3, N'General Motors')

PRINT 'Creating PriceScheme data'
INSERT INTO [KA].[dbo].[PriceScheme]([PriceTypeID], [Quantity], [Price]) VALUES (1, 1, 20.0)
INSERT INTO [KA].[dbo].[PriceScheme]([PriceTypeID], [Quantity], [Price]) VALUES (1, 2, 30.0)
INSERT INTO [KA].[dbo].[PriceScheme]([PriceTypeID], [Quantity], [Price]) VALUES (1, 3, 35.0)
INSERT INTO [KA].[dbo].[PriceScheme]([PriceTypeID], [Quantity], [Price]) VALUES (1, 4, 35.0)
INSERT INTO [KA].[dbo].[PriceScheme]([PriceTypeID], [Quantity], [Price]) VALUES (1, 5, 35.0)
INSERT INTO [KA].[dbo].[PriceScheme]([PriceTypeID], [Quantity], [Price]) VALUES (2, 1, 0.0)
INSERT INTO [KA].[dbo].[PriceScheme]([PriceTypeID], [Quantity], [Price]) VALUES (3, 1, 13.0)
INSERT INTO [KA].[dbo].[PriceScheme]([PriceTypeID], [Quantity], [Price]) VALUES (3, 2, 25.0)
INSERT INTO [KA].[dbo].[PriceScheme]([PriceTypeID], [Quantity], [Price]) VALUES (3, 3, 30.0)
INSERT INTO [KA].[dbo].[PriceScheme]([PriceTypeID], [Quantity], [Price]) VALUES (3, 4, 30.0)
INSERT INTO [KA].[dbo].[PriceScheme]([PriceTypeID], [Quantity], [Price]) VALUES (3, 5, 30.0)

PRINT 'Creating Location data'
INSERT INTO [KA].[dbo].[Location] ([ID], [Name]) VALUES (0, N'Onbekend')
INSERT INTO [KA].[dbo].[Location] ([ID], [Name]) VALUES (1, N'Brasschaat')
INSERT INTO [KA].[dbo].[Location] ([ID], [Name]) VALUES (2, N'Wommelgem')
INSERT INTO [KA].[dbo].[Location] ([ID], [Name]) VALUES (3, N'Nationaal')

PRINT 'Creating TrainingPeriod data'
INSERT INTO [KA].[dbo].[TrainingPeriod] ([ID], [Start], [End], [Name]) VALUES (1, '09-01', '30-06', N'Training sezoen')
INSERT INTO [KA].[dbo].[TrainingPeriod] ([ID], [Start], [End], [Name]) VALUES (2, '01-07', '31-08', N'Vakantie periode')

PRINT 'Creating TrainingTime data'
INSERT INTO [KA].[dbo].[TrainingTime]([TrainingPeriodID], [LocationID], [DayOfTheWeek], [StartOfTraining], [EndOfTraining], [Audience]) VALUES (1, 1, 1, '18:15', '19:15', N'Juniors');
INSERT INTO [KA].[dbo].[TrainingTime]([TrainingPeriodID], [LocationID], [DayOfTheWeek], [StartOfTraining], [EndOfTraining], [Audience]) VALUES (1, 1, 1, '19:15', '21:00', N'Basistraining Volwassenen');
INSERT INTO [KA].[dbo].[TrainingTime]([TrainingPeriodID], [LocationID], [DayOfTheWeek], [StartOfTraining], [EndOfTraining], [Audience]) VALUES (1, 2, 2, '18:30', '20:15', N'Basistraining');
INSERT INTO [KA].[dbo].[TrainingTime]([TrainingPeriodID], [LocationID], [DayOfTheWeek], [StartOfTraining], [EndOfTraining], [Audience]) VALUES (1, 2, 5, '18:30', '20:00', N'Basistraining');
INSERT INTO [KA].[dbo].[TrainingTime]([TrainingPeriodID], [LocationID], [DayOfTheWeek], [StartOfTraining], [EndOfTraining], [Audience]) VALUES (1, 2, 5, '20:00', '20:30', N'Sempai training');
INSERT INTO [KA].[dbo].[TrainingTime]([TrainingPeriodID], [LocationID], [DayOfTheWeek], [StartOfTraining], [EndOfTraining], [Audience]) VALUES (1, 2, 6, '10:00', '11:00', N'Juniors');
INSERT INTO [KA].[dbo].[TrainingTime]([TrainingPeriodID], [LocationID], [DayOfTheWeek], [StartOfTraining], [EndOfTraining], [Audience]) VALUES (1, 1, 6, '11:00', '12:30', N'Gevorderden');
INSERT INTO [KA].[dbo].[TrainingTime]([TrainingPeriodID], [LocationID], [DayOfTheWeek], [StartOfTraining], [EndOfTraining], [Audience]) VALUES (1, 1, 6, '12:30', '13:30', N'Basistraining Volwassenen');
INSERT INTO [KA].[dbo].[TrainingTime]([TrainingPeriodID], [LocationID], [DayOfTheWeek], [StartOfTraining], [EndOfTraining], [Audience]) VALUES (2, 1, 6, '10:00', '11:00', N'Juniors');
INSERT INTO [KA].[dbo].[TrainingTime]([TrainingPeriodID], [LocationID], [DayOfTheWeek], [StartOfTraining], [EndOfTraining], [Audience]) VALUES (2, 1, 6, '11:00', '12:30', N'Volwassenen');
INSERT INTO [KA].[dbo].[TrainingTime]([TrainingPeriodID], [LocationID], [DayOfTheWeek], [StartOfTraining], [EndOfTraining], [Audience]) VALUES (2, 1, 6, '12:30', '13:30', N'Demonstratieteam dorpdag');
