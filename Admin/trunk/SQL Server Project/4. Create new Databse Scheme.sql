USE [KA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[Group]    Script Date: 07/03/2007 21:04:09 ******/
CREATE TABLE [dbo].[Group](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL
 CONSTRAINT [Group_PK] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]



/****** Object:  Table [dbo].[Member]    Script Date: 07/03/2007 20:55:14 ******/
CREATE TABLE [dbo].[Member](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Lastname] [nvarchar](27) NULL,
	[Firstname] [nvarchar](50) NULL,
	[Address] [nvarchar](26) NULL,
	[ZipCode] [nvarchar](50) NULL,
	[City] [nvarchar](27) NULL,
	[Province] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Telephone] [nvarchar](50) NULL,
	[Sex] [nvarchar](2) NULL,
	[Birthdate] [smalldatetime] NULL,
	[LicenseNumber] [int] NULL,
	[GroupID] [int] NULL,
	[IsGMEmployee] [bit] NOT NULL DEFAULT ((0)),
	[IsTrainer] [bit] NOT NULL DEFAULT ((0)),
	[IsNew] [bit] NOT NULL DEFAULT ((1)),
	[IsActive] [bit] NOT NULL DEFAULT ((1)),
	[InsuranceEndDate] [smalldatetime] NULL,
	[StartYear] [nvarchar](50) NULL,
	[Foto] [image] NULL
 CONSTRAINT [Member_PK] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


ALTER TABLE [dbo].[Member]  WITH NOCHECK ADD CONSTRAINT [FK_Member_Group] FOREIGN KEY([GroupID])
REFERENCES [dbo].[Group] ([ID]) ON UPDATE NO ACTION ON DELETE  NO ACTION 
GO



/****** Object:  Table [dbo].[Degrees]    Script Date: 07/03/2007 21:05:36 ******/
CREATE TABLE [dbo].[Degree](
	[ID] [int] NOT NULL,
	[Rank] [int] NOT NULL,
	[Title] [nvarchar](20) NULL,
	[Color] [nvarchar](10) NULL,
	[Stripes] [int] NOT NULL,
	[Picture] [image] NULL
 CONSTRAINT [Degrees_PK] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]




/****** Object:  Table [dbo].[Location]    Script Date: 07/15/2007 18:05:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Location](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]




/****** Object:  Table [dbo].[Examinations]    Script Date: 07/03/2007 21:05:36 ******/
CREATE TABLE [dbo].[Examination](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NOT NULL,
	[DegreeID] [int] NOT NULL,
	[LocationID] INT NOT NULL,
	[Date] [smalldatetime] NULL,
 CONSTRAINT [Examinations_PK] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[Examination] ADD CONSTRAINT	 [FK_Examination_Location] FOREIGN KEY([LocationID])
REFERENCES [dbo].[Location]([ID]) ON UPDATE NO ACTION ON DELETE  NO ACTION 
GO

ALTER TABLE [dbo].[Examination] ADD CONSTRAINT	 [FK_Examination_Degree] FOREIGN KEY([DegreeID])
REFERENCES [dbo].[Degree]([ID]) ON UPDATE NO ACTION ON DELETE  NO ACTION 
GO

ALTER TABLE [dbo].[Examination] ADD CONSTRAINT	 [FK_Examination_Member] FOREIGN KEY([MemberID])
REFERENCES [dbo].[Member]([ID]) ON UPDATE NO ACTION ON DELETE  NO ACTION 
GO



/****** Object:  Table [dbo].[PriceType]    Script Date: 07/15/2007 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PriceType](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PriceType_PK] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]




/****** Object:  Table [dbo].[PriceScheme]    Script Date: 07/15/2007 16:59:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PriceScheme](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PriceTypeID] [int] NOT NULL,
	[Quantity] [smallint] NOT NULL,
	[Price] [smallmoney] NOT NULL,
 CONSTRAINT [PriceScheme_PK] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[PriceScheme] ADD CONSTRAINT [FK_PriceScheme_PriceType] FOREIGN KEY([PriceTypeID])
REFERENCES [dbo].[PriceType]([ID]) ON UPDATE NO ACTION ON DELETE  NO ACTION 
GO



/****** Object:  Table [dbo].[TrainingPeriod]    Script Date: 07/15/2007 17:40:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrainingPeriod](
	[ID] [int] NOT NULL,
	[Start] [char](5) NOT NULL,
	[End] [char](5) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_TrainingPeriod] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]



/****** Object:  Table [dbo].[TrainingTime]    Script Date: 07/15/2007 17:41:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrainingTime](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TrainingPeriodID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[DayOfTheWeek] [tinyint] NOT NULL,
	[StartOfTraining] [char](5) NOT NULL,
	[EndOfTraining] [char](5) NOT NULL,
	[Audience] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_TrainingTime] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[TrainingTime] ADD CONSTRAINT [FK_TrainingTime_Location] FOREIGN KEY([LocationID])
REFERENCES [dbo].[Location]([ID]) ON UPDATE NO ACTION ON DELETE  NO ACTION 
GO

ALTER TABLE [dbo].[TrainingTime] ADD CONSTRAINT [FK_TrainingTime_TrainingPeriod] FOREIGN KEY([TrainingPeriodID])
REFERENCES [dbo].[TrainingPeriod]([ID]) ON UPDATE NO ACTION ON DELETE  NO ACTION 
GO



/****** Object:  Table [dbo].[TrainingExceptions]    Script Date: 07/15/2007 17:41:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrainingExceptions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Day] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Holidays] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
